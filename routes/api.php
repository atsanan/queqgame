<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->get('/getLocationZone/', 'WildMonstersController@locationZone');
Route::middleware('api')->post('/getMallFloor/', 'MallFloorsController@search');
Route::middleware('api')->post('/getShop/', 'ShopsController@search');
Route::middleware('api')->get('/getShopCode/{shopCode}', 'ShopsController@shopCode');
Route::middleware('api')->get('/shops/{id}', 'ShopsController@shopDetail');
Route::middleware('api')->get('/privilegeDefaultPlayers/searchPlayer', 'PrivilegeDefaultsPlayerController@searchPlayer');
Route::middleware('api')->get('/privilegeDefault/searchByPrivilegeGroups', 'PrivilegeDefaultsController@searchByPrivilegeGroups');
Route::middleware('api')->get('/mallFloors/searchByMall', 'MallFloorsController@searchByMall');
Route::middleware('api')->get('/news/searchByShop', 'NewsController@searchByShop');
Route::middleware('api')->get('/news/{id}', 'NewsController@detail');
Route::middleware('api')->get('/shops/searchByMallFloor', 'ShopsController@searchByMallFloor');
Route::middleware('api')->get('/shops/searchByPlayer', 'ShopsController@searchByPlayer');
Route::middleware('api')->get('/shops/searchByPlayerFixbug', 'ShopsController@searchByPlayerFixBug');
