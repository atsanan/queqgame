<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\ItemCategorys;

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
// Route::get('/register', function () {
//   // abort(404);
// });

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/excel', 'UserController@excel')->name('user.excel');
Route::get('/report/monstersDefault', 'MonstersDefaultController@report')->name('report.monstersDefault');
Route::get('/monstersDefault/excel', 'MonstersDefaultController@excel')->name('monstersDefault.excel');
Route::get('/monstersDefault/{id}/player', 'MonstersDefaultController@player')->name('player');
Route::get('/report/wildMonsters', 'WildMonstersController@report')->name('report.wildMonsters');
Route::get('/wildMonsters/excel', 'WildMonstersController@excel')->name('wildMonsters.excel');
Route::get('/wildMonsters/excelIndex', 'WildMonstersController@excelIndex')->name('wildMonsters.excelIndex');
Route::get('/wildMonstersInstantiate/excel', 'WildMonstersController@excelInstantiate')->name('monstersInstantiate.excel');
Route::get('/report/monstersInstantiate', 'WildMonstersController@instantiate')->name('report.monstersInstantiate');
Route::get('/report/itemPlayer', 'ItemPlayerController@report')->name('report.itemPlayer');
Route::get('/report/itemCoupon', 'ItemPlayerController@itemCoupon')->name('report.itemCoupon');
Route::get('/report/itemCouponExcel', 'ItemPlayerController@itemCouponExcel')->name('itemCoupon.excel');
Route::get('/itemPlayer/excel', 'ItemPlayerController@excel')->name('itemPlayer.excel');
Route::get('/report/monsterInShop', 'MonstersInShopController@report')->name('report.monsterInShop');
Route::get('/monsterInShop/excel', 'MonstersInShopController@excel')->name('monsterInShop.excel');
Route::post('/itemPlayer/search', 'ItemPlayerController@search')->name('search');
Route::get('/privilegeShops/excel', 'PrivilegeShopsController@excel')->name('privilegeShops.excel');
Route::post('/privilegeShops/search', 'PrivilegeShopsController@search')->name('privilegeShops.search');
Route::get('/shops/excelPrivilegeShops', 'ShopsController@excelPrivilegeShops')->name('shops.excelPrivilegeShops');
Route::get('/shops/excelPromotion', 'ShopsController@excelPromotion')->name('shops.excelPromotion');
Route::post('/shops/searchIndex', 'ShopsController@searchIndex')->name('shops.searchIndex');
Route::get('/playerLogs/excel', 'PlayerLogsController@excel')->name('playerLogs.excel');
Route::get('/activities/calender', 'ActivitiesController@calender')->name('activities.calender');
Route::get('/activities/{id}/view', 'ActivitiesController@view')->name('activities.view');
Route::get('/couponItem/importCsv', 'CouponItemController@importCsv')->name('couponItem.importCsv');
Route::post('/couponItem/storeCsv', 'CouponItemController@storeCsv')->name('couponItem.storeCsv');
Route::resource('playerLogs', 'PlayerLogsController');
Route::resource('itemPlayer', 'ItemPlayerController');
Route::resource('players', 'PlayersController');
Route::resource('itemDefaults', 'ItemDefaultsController');
Route::resource('itemCategorys', 'ItemCategorysController');
Route::post('/news/search', 'NewsController@search')->name('news.search');
Route::resource('news', 'NewsController');
Route::get('/news/push/{id}', 'NewsController@push')->name('push');
Route::resource('newsTypes', 'NewsTypesController');
Route::resource('malls', 'MallsController');
Route::resource('shops', 'ShopsController');
Route::resource('shopAds', 'ShopAdsController');
Route::resource('shopAsserts', 'ShopAssertsController');
Route::resource('shopCategories', 'ShopCategoriesController');
Route::resource('zoneTypes', 'ZoneTypeController');
Route::resource('stickerStores', 'StickerStoreController');
Route::resource('authenTypes', 'AuthenTypeController');
Route::resource('mallFloors', 'MallFloorsController');
Route::resource('costumes', 'CostumesController');
Route::resource('digitalBoardAsserts', 'DigitalBoardAssertsController');
Route::resource('wildItems', 'WildItemsController');
Route::resource('wildMonsters', 'WildMonstersController');
Route::resource('testUpload', 'TestUploadController');
Route::resource('privilegeDefaults', 'PrivilegeDefaultsController');
Route::resource('monstersDefaultsTypes', 'MonstersDefaultsTypesController');
Route::resource('monstersDefault', 'MonstersDefaultController');
Route::resource('privilegeGroups', 'PrivilegeGroupsController');
Route::resource('privilegeShops', 'PrivilegeShopsController');
Route::resource('monsterInShop', 'MonstersInShopController');
Route::resource('user', 'UserController');
Route::resource('mailBox', 'MailBoxController');
Route::get('/mailBox/push/{id}', 'MailBoxController@push')->name('push');
Route::resource('digitalBoard', 'DigitalBoardController');
Route::delete('/digitalBoard/deleteThumbnail/{id}/{image}/{type}', 'DigitalBoardController@deleteThumbnail');
Route::resource('monstersPlayer', 'MonstersPlayerController');
Route::get('/privacyPolicy/view/', 'PrivacyPolicyController@view')->name('privacyPolicy.view');
Route::get('/privacyPolicy/{version}/setupCurrent', 'PrivacyPolicyController@setupCurrent')->name('privacyPolicy.setupCurrent');
Route::resource('privacyPolicy', 'PrivacyPolicyController');
Route::resource('device', 'DeviceController');
Route::get('device/{id}/view', 'DeviceController@view')->name('view');
Route::resource('loginReward', 'LoginRewardController');
Route::get('thumbnail/edit', 'ThumbnailImageController@edit')->name('edit');
Route::put('thumbnail/setImage', 'ThumbnailImageController@setImage')->name('setImage');
Route::get('giveMonster/edit', 'GiveMonsterController@edit')->name('edit');
Route::resource('giveMonster', 'GiveMonsterController');
Route::resource('thumbnail', 'ThumbnailImageController');
Route::resource('roles', 'RoleController');
Route::get('genCouponPassword/{json}', function ($json) {
    $code = "{\"\couponPassword\"\:{$json}";
    \QrCode::size(250)
        ->format('png')
        ->generate($json);
    return view('qrCode', compact('code'));
});

Route::get('genCouponPassword/{json}', function ($json) {
    $code = '{"couponPassword":"' . $json . '"}';
    \QrCode::size(250)
        ->format('png')
        ->generate($json);
    return view('qrCode', compact('code'));
});
Route::get('genQrCode/{code}', function ($code) {

    \QrCode::size(250)
        ->format('png')
        ->generate($code);
    return view('qrCode', compact('code'));
});

Route::resource('networkAccess', 'NetworkAccessController');
Route::resource('ipAccess', 'IpAccessController');
Route::resource('partnerTypes', 'PartnerTypesController');
Route::resource('activities', 'ActivitiesController');
Route::delete('couponItem/destroyItemPlayer/{id}', 'CouponItemController@deleteItemPlayer')->name('couponItem.destroyItemPlayer');
Route::get('couponItem/createMultipleCode', 'CouponItemController@createMultipleCode')->name('couponItem.createMultipleCode');
Route::post('/couponItem/storeMultipleCode', 'CouponItemController@storeMultipleCode')->name('couponItem.storeMultipleCode');
Route::resource('couponItem', 'CouponItemController');
