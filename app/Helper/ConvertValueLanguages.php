<?php 
namespace App\Helper;

class ConvertValueLanguages {
  public static function convertData($field)
  {
    return [ 'eng'=>($field['eng']==null)?"":$field['eng'],
    'thai'=>($field['thai']==null)?"":$field['thai'],
    'chi1'=>($field['chi1']==null)?"":$field['chi1'],
    'chi2'=>($field['chi2']==null)?"":$field['chi2']
    ];
  }
}
?>