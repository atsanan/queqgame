<?php

namespace App\Helper;

use App\Helper\Thumbnail;
use App\Models\Setting;

use App\Http\Controllers\Controller;

class UploadFile
{
    public static function uploadFileNameLogo($request, $model, $path, $action, $input, $field)
    {
        $AssertImageSlot = $model->$input;
        $thumbnail = Setting::where([])->first();
        if (isset($AssertImageSlot)) {
            $imageName = uniqid() . '.' . $request->file($input)->getClientOriginalExtension();
            $request->file($input)->move(base_path() . $path, $imageName);
            Thumbnail::createThumbnailFile($imageName, $thumbnail->width, $thumbnail->height);
            if ($action == "create") {
                $model->$field = $imageName;
            } else {
                $model->update([$field => $imageName]);
            }
        } else {
            if ($action == "create") {
                $model->$field = "";
            }
        }
    }

    public static function uploadImage($request, $field)
    {
        $imageSlot = uniqid() . '.' . $request->file($field)->getClientOriginalExtension();
        $request->file($field)->move(base_path() . Controller::$imagePath, $imageSlot);
        return $imageSlot;
    }
}
