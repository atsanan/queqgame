<?php

namespace App\Helper;

use Image;

class Thumbnail
{
    public static function createThumbnailFile($nameFile, $width, $height)
    {

        $oldFile = base_path() . '/public/media/images/' . $nameFile;
        $newFile = base_path() . '/public/media/thumbnail/' . $nameFile;
        $allowed =  array('gif', 'png', 'jpg');
        $ext = pathinfo($oldFile, PATHINFO_EXTENSION);
        if (in_array($ext, $allowed)) {

            if (copy($oldFile, $newFile)) {
                if ($height > 0) {
                    $img = Image::make($newFile)->resize($width, $height);
                    $img->save($newFile);
                } else {
                    $img = Image::make($newFile)->resize($width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($newFile);
                }
            }
        }
    }
}
