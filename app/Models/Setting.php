<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Setting extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'setting';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'privacyPolicyVersion' => 'int',
            'clearCacheVerion' => 'int',
            'serverAlertVerion' => 'int',
            'serverAlertMessage' => 'int',
            'isFixedLoaderShaderIOS' => 'bool',
            'isFixedLoaderShaderAndroid' => 'bool',
            'isDebug' => 'bool',
            'CoverageTreeManifestUrlBackup' => 'string',
            'CoverageTreeManifestUrlUrl' => 'string',
            'monsterFoodSecMax' => ['type' => 'array(int)'],
            'monsterHappySecMax' => ['type' => 'array(int)'],
            'monsterHeathSecMax' => ['type' => 'array(int)'],
            'monsterQuest1SecMax' => ['type' => 'array(int)'],
            'monsterQuest2ecMax' => ['type' => 'array(int)'],
            'monsterQuest3SecMax' => ['type' => 'array(int)'],
            'monsterImmunity' => ['type' => 'array(int)'],
            'shopCode'       => ['type' => 'int'],
            'privacyPolicyVersion' => ['type' => 'int'],
            'privacyPolicyUrl' => ['type' => 'string'],
            'width' => ['type' => 'int'],
            'height' => ['type' => 'int'],
            'locationGameZone' => ['type' => LocationZoneGame::class],
        ];
    }
}


class LocationZoneGame extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)'],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}
