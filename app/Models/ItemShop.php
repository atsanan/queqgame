<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ItemShop extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'itemShop';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'itemId' => ['type' => 'objectid'],
            'toMall' => ['type' => 'objectid'],
            'toMallFloor' => ['type' => 'objectid'],
            'toShop' => ['type' => 'objectid'],
        ];
    }
}
