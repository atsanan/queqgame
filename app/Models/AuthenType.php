<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class AuthenType extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'authenTypes';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'authenTypeName' => ['type' => 'string' ],
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }
}
