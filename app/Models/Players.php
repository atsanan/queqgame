<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Players extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'players';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'playerName' => ['type' => 'string'],
            'coin'  => ['type' => 'int'],
            'diamond'  => ['type' => 'int'],
            'gender' => ['type' => 'string'],
            'teamPlayerId' => ['type' => 'int'],
            'costumeList' => ['type' => 'array(objectid)'],
            'userId' => ['type' => 'objectid'],
            'costumeSelectId' => ['type' => 'objectid'],
        ];
    }

    public function TeamPlayers()
    {
        return $this->belongsTo('App\Models\TeamPlayers', 'teamPlayerId')->withDefault();
    }

    public function Costumes()
    {
        return $this->belongsTo('App\Models\Costumes', 'costumeSelectId')->withDefault();
    }
}
