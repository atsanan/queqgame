<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ZoneTypes extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'zonetypes';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'zoneTypeName' => ['type' => 'array(string)'],
            'isActive' => ['type' => 'bool',   'default' => false],
        ];
    }

    public static function queryZoneType()
    {

        $queryZoneTypes = ZoneTypes::where([])->get();
        $zoneTypes = [];
        foreach ($queryZoneTypes as $type) {
            $zoneTypes[$type->id] = $type->zoneTypeName['eng'];
        }
        return $zoneTypes;
    }
}
