<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class GiveMonster extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'giveMonster';
    protected $guarded = [];
    public static function SCHEMAS()
    {
        return [
            'title' => ['type' => 'string'],
            'detail' => ['type' => 'string'],
            'mDefaultId' => ['type' => 'objectid'],
            'giveVersion' => ['type' => 'int'],
        ];
    }
}
