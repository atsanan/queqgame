<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ShopAds extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'shopAds';
    protected $guarded = [];
    
    public static function SCHEMAS()
    {
        return [
            'shopAdsName' => ['type' => 'array(string)' ],
            'shopAdsImage' => ['type' => 'string' ],
            'shopAdsImageLists' => ['type' => 'array(string)' ],
        ];
    }

    public static function queryShopAds(){

        $queryShopAds= ShopAds::where([])->get();
        $shopAdses=[];
        
        if($queryShopAds){
            foreach($queryShopAds as $shopAds){
                $shopAdses[$shopAds->id]=$shopAds->shopAdsName['eng'];
            }
        }
     
       return $shopAdses;
    }
}
