<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class LoginReward extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'loginReward';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'title' => ['type' => 'string'],
            'detail' => ['type' => 'string'],
            'linkListName' => ['type' => 'string'],
            'diamond' => ['type' => 'int', 'default' => 0],
            'indexReward' => ['type' => 'int', 'default' => 0],
            'diamondActive' => ['type' => 'bool', 'default' => false],
            'imageName' => ['type' => 'string', 'default' => ""],
            'thumbnail' => ['type' => 'string', 'default' => ""],
            'itemId' => ['type' => 'objectid'],
            'itemActive' => ['type' => 'bool', 'default' => false],
            'mDefaultId' => ['type' => 'objectid'],
            'monsterActive' => ['type' => 'bool', 'default' => false],
            'isActive'    => ['type' => 'bool',   'default' => false],

        ];
    }
}
