<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class NetworkAccess extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'networkAccess';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'server' => ['type' => 'string'],
            'apiKey' => ['type' => 'string'],
            'description' => ['type' => 'string'],
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }

    public static function queryNetworkAccess()
    {

        $queryNetworkAccess = NetworkAccess::where([])->get();
        $result = [];
        if (count($queryNetworkAccess) > 0) {
            foreach ($queryNetworkAccess as $data) {
                $result[$data->id] = $data->server;
            }
        }
        return $result;
    }
}
