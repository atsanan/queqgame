<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class MonstersDefaultsTypes extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'monsterDefaultsTypes';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'monstersTypeName' => ['type' => 'array(string)' ],
            'order'       => ['type' => 'int'],  
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }

}