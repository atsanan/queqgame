<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Costumes extends MongoModel
{
    
    protected $connection = 'mongodb';
    protected $collection = 'costumes';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'costumeName' => ['type' => 'array(string)' ],
            'costumeDetail' => ['type' => 'array(string)' ],
            'coin'       => ['type' => 'int'],
            'diamond'       => ['type' => 'int'],
            'costumesAssertVersion' => ['type' => 'int'],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'custumesAssertModelIOS'=> ['type' => 'string'],
            'custumesAssertModelAndroid'=> ['type' => 'string'],
            'gender'=> ['type' => 'string'],
        
        ];
    }

}
