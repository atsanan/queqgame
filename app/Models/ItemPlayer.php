<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ItemPlayer extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'itemPlayers';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'couponHashKey' => ['type' => 'string'],
            'count' => ['type' => 'int'],
            'isCoupon'       => ['type' => 'bool'],
            'itemId' => ['type' => 'objectid'],
            'coin'       => ['type' => 'int'],
            'couponGiftId'       => ['type' => 'string'],
            'couponQRImage'       => ['type' => 'string'],
            'couponPassword'       => ['type' => 'string'],
            'playerId'    => ['type' => 'objectid'],
            'uniqueId'    => ['type' => 'string'],
            'createAt'    => ['type' => 'datetime'],
            'couponDatetime1'    => ['type' => 'datetime'],
            'couponDatetime2'    => ['type' => 'datetime'],

        ];
    }

    public function Player()
    {
        return $this->belongsTo('App\Models\Players', 'playerId')->withDefault();
    }
}
