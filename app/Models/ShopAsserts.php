<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ShopAsserts extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'shopAsserts';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'shopAssertName' => ['type' => 'array(string)'],
            'shopAssertDetail' => ['type' => 'array(string)'],
            'shopAssertModel' => ['type' => 'string'],
            'shopAssertVersion' => ['type' => 'int'],
            'fileNameLogo1' => ['type' => 'string'],
            'fileNameLogo2' => ['type' => 'string'],
            'fileNameLogo3' => ['type' => 'string'],
            'shopAssertModelIOS' => ['type' => 'string'],
            'shopAssertModelAndroid' => ['type' => 'string']

        ];
    }

    public static function queryShopAsserts()
    {

        $queryShopAsserts = ShopAsserts::where([])->get();
        $ShopAsserts = [];

        if ($queryShopAsserts) {
            foreach ($queryShopAsserts as $ShopAssert) {
                $ShopAsserts[$ShopAssert->id] = $ShopAssert->shopAssertName['eng'];
            }
        }
        return $ShopAsserts;
    }
}
