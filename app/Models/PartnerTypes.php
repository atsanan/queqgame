<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class PartnerTypes extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'partnerTypes';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'partnerTypeName' => ['type' => 'string'],
            'isActive'    => ['type' => 'bool', 'default' => false],
        ];
    }

    public static function queryPartnerTypes()
    {

        $queryPatherTypes = PartnerTypes::where(['isActive' => true])->get();
        $data = [];
        foreach ($queryPatherTypes as $item) {
            $data[$item->_id] = $item->partnerTypeName;
        }
        return $data;
    }
}
