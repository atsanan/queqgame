<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class DigitalBoard extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'digitalBoard';
    protected $guarded = [];
    public static function SCHEMAS()
    {
        return [
            'title' => ['type' => 'array(string)'],
            'body' => ['type' => 'array(string)'],
            'digitalBoardAssertId' => ['type' => 'objectid'],
            'zoneType' => ['type' => 'string'],
            'mallId' => ['type' => 'objectid'],
            'mallFloorId' => ['type' => 'objectid'],
            'shopId' => ['type' => 'objectid'],
            'location' => ['type' => Locations::class],
            'x' => ['type' => 'float'],
            'y' => ['type' => 'float'],
            'z' => ['type' => 'float'],
            'imageSlot1' => ['type' => 'string', 'default' => ""],
            'imageSlot2' => ['type' => 'string', 'default' => ""],
            'clickType' => ['type' => 'int'],
            'imageThumbnailList' => ['type' => 'array(string)', 'default' => []],
            'imageFullScreenList' => ['type' => 'array(string)', 'default' => []],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'isShowContent' => ['type' => 'bool',   'default' => false],
        ];
    }
    public function digitalBoardAssert()
    {
        return $this->belongsTo('App\Models\DigitalBoardAsserts', 'digitalBoardAssertId')->withDefault();
    }


    public function Malls()
    {
        return $this->belongsTo('App\Models\Malls', 'mallId')->withDefault();
    }
    public function MallFloors()
    {
        return $this->belongsTo('App\Models\MallFloor', 'mallFloorId')->withDefault();
    }

    public function Shops()
    {
        return $this->belongsTo('App\Models\Shops', 'shopId')->withDefault();
    }
}

class Locations extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)', 'default' => [0, 0]],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}
