<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class UserPlayer extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'users';
    protected $guarded = [];
    public static function SCHEMAS()
    {
        return [
            'name' => ['type' => 'string'],
            'firstname' => ['type' => 'string'],
            'lastname' => ['type' => 'string'],
            'email' => ['type' => 'string'],
            'gender' => ['type' => 'string'],
            'birthdate' => ['type' => 'string'],
            'tel' => ['type' => 'string'],
            'authenTypeId' => ['type' => 'int'],
            'authenId' => ['type' => 'string'],
            'authenToken' => ['type' => 'string'],
            'verified' => ['type' => 'bool'],
            'verifiedAt' => ['type' => 'datetime'],
            'picture' => ['type' => 'string'],
            'pushMessagesAccessToken' => ['type' => 'string'],
            'time' => ['type' => 'int'],
            'createAt' => ['type' => 'datetime'],
            'lastLogin' => ['type' => 'datetime'],
            'deviceId' => ['type' => 'string'],

        ];
    }

    public static function getUser()
    {
        $query = UserPlayer::raw(function ($collection) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => '_id',
                        'foreignField' => 'userId',
                        'as' => 'players'
                    )),
                    array('$unwind' => '$players'),
                )
            );
        });
        $arr = [];
        if ($query) {
            foreach ($query as $data) {
                $arr[$data->id] = $data->email;
            }
        }

        return $arr;
    }
}
