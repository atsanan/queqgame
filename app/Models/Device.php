<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Device extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'devices';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'deviceId' => ['type' => 'string'],
            'deviceName' => ['type' => 'string'],
            'deviceOS' => ['type' => 'string'],
            'deviceType' => ['type' => 'string'],
            'deviceModel' => ['type' => 'string'],
            'privacyPolicyVersion' => ['type' => 'int'],
            'createAt' => ['type' => 'datetime'],
            'lastModified' => ['type' => 'datetime']
        ];
    }
}
