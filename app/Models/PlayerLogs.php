<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class PlayerLogs extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'playerLogs';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'title' => ['type' => 'string'],
            'detail' => ['type' => 'string'],
            'playerId' => ['type' => 'objectid'],
            'coin' => ['type' => 'int'],
            'diamond' => ['type' => 'int'],
            'money' => ['type' => 'string'],
            'itemId' => ['type' => 'objectid'],
            'mDefaultId' => ['type' => 'objectid'],
            'createAt' => ['type' => 'datetime'],
        ];
    }

    public function Monsters()
    {
        return $this->belongsTo('App\Models\MonstersDefault', 'mDefaultId')->withDefault();
    }
    public function Items()
    {
        return $this->belongsTo('App\Models\ItemDefault', 'itemId')->withDefault();
    }

    public function Player()
    {
        return $this->belongsTo('App\Models\Players', 'playerId')->withDefault();
    }
}
