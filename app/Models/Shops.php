<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Shops extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'shops';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'shopName' => ['type' => 'array(string)'],
            'shopDetail' => ['type' => 'array(string)'],
            'shopCategoryId' => ['type' => 'array(objectid)'],
            'mainTypes' => ['type' => 'array(string)'],
            'mallFloorId' => ['type' => 'objectid'],
            'shopAssertId' => ['type' => 'objectid'],
            'shopAdsId' => ['type' => 'objectid'],
            'location' => ['type' => LocationsShop::class],
            'shopTel' => ['type' => 'string'],
            'shopUrl' => ['type' => 'string'],
            'shopCode' => ['type' => 'string'],
            'mapReach' => ['type' => 'int'],
            'order'       => ['type' => 'int'],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'isSponser'    => ['type' => 'bool',   'default' => false],
            'filenameLogo1' => ['type' => 'string'],
            'filenameLogo2' => ['type' => 'string'],
            'address' => ['type' => 'array(string)'],
            'workingTime' => ['type' => 'array(string)'],
            'minAge'       => ['type' => 'int'],
            'isPromotionAvailable' => ['type' => 'bool',   'default' => false],
            'monsterRating'       => ['type' => 'int', 'default' => 0],
        ];
    }

    public function shopCategories()
    {
        return $this->belongsTo('App\Models\ShopCategories', 'shopCategoryId')->withDefault();
    }

    public function mallFloor()
    {
        return $this->belongsTo('App\Models\MallFloor', 'mallFloorId')->withDefault();
    }

    public function shopAssert()
    {
        return $this->belongsTo('App\Models\ShopAsserts', 'shopAssertId')->withDefault();
    }

    public function shopAds()
    {
        return $this->belongsTo('App\Models\ShopAds', 'shopAdsId')->withDefault();
    }

    public static function queryShop($mallFloorId = null)
    {

        if ($mallFloorId == null) {
            $queryShops = Shops::where([])->get();
        } else {
            $queryShops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId($mallFloorId)])->get();
        }

        $shops = [];
        foreach ($queryShops as $type) {
            $shops[$type->id] = $type->shopName['eng'];
        }

        return $shops;
    }
}

class LocationsShop extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)'],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}
