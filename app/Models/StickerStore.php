<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class StickerStore extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'stickerStores';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'stickerStoreName' => ['type' => 'array(string)' ],
            'stickerStoreDetail' => ['type' => 'array(string)'],
            'stickerStoreLavel'  => ['type' => 'int'],  
            'stickerStoreImage' => ['type' => 'string'],
            'coin' => ['type' => 'int'],
            'diamond' => ['type' => 'int'],
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }
}
