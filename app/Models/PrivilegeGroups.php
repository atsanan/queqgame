<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class PrivilegeGroups extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'privilegeGroups';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'privilegeGroupNames' => ['type' => 'array(string)'],
            'privilegeGroupDetails' => ['type' => 'array(string)'],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'order'       => ['type' => 'int'],
            'filenameLogo1' => ['type' => 'string'],
            'filenameLogo2' => ['type' => 'string']

        ];
    }

    public static function queryPrivilegeGroups()
    {

        $queryPrivilegeGroups = PrivilegeGroups::where([])->get();
        $privilegeGroups = [];
        foreach ($queryPrivilegeGroups as $row) {
            $privilegeGroups[$row->id] = $row->privilegeGroupNames['eng'];
        }

        return $privilegeGroups;
    }
}
