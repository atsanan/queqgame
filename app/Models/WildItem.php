<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class WildItem extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'wildItems';
    protected $guarded = [];
    public static function SCHEMAS()
    {
        return [
            'couponGiftName' => ['type' => 'array(string)' ],
            'itemId' => ['type' => 'objectid'],            
            'zoneTypeId' => ['type' => 'objectid'],
            'toMall' => ['type' => 'objectid'],
            'toMallFloor' => ['type' => 'objectid'],
            'startDateTime' => ['type' => 'datetime'],
            'expiredDateTime' => ['type' => 'datetime'],
            'toShop' => ['type' => 'objectid'],
            'couponGiftId'       => ['type' => 'string'],
            'count'       => ['type' => 'int'],
            'countMax'       => ['type' => 'int'],  
            'isActive'    => ['type' => 'bool',   'default' => false],
            'isReachDateTime'    => ['type' => 'bool',   'default' => false],
            'isGoldenMinutes'    => ['type' => 'bool',   'default' => false],
            'isToItemPlayer'    => ['type' => 'int',   'default' => 0],
        ];
    }

    public function itemDefaults()
    {
        return $this->belongsTo('App\Models\ItemDefault','itemId')->withDefault();
    }

    public function ZoneTypes()
    {
        return $this->belongsTo('App\Models\ZoneTypes','zoneTypeId')->withDefault();
    }

    public function Malls()
    {
        return $this->belongsTo('App\Models\Malls','toMall')->withDefault();
    }

    public function MallFloors()
    {
        return $this->belongsTo('App\Models\MallFloor','toMallFloor')->withDefault();
    }

    public function Shops()
    {
        return $this->belongsTo('App\Models\Shops','toShop')->withDefault();
    }

}
