<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class News extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'news';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'newsName' => ['type' => 'array(string)'],
            'newsDetail' => ['type' => 'array(string)'],
            'filenameImage1' => ['type' => 'string'],
            'filenameImage2' => ['type' => 'string'],
            'filenameImage3' => ['type' => 'string'],
            'shopId' => ['type' => 'objectid'],
            'newsType' => ['type' => 'objectid'],
            'wildItemId' => ['type' => 'objectid'],
            'gender'       => ['type' => 'string'],
            'location' => ['type' => Locations::class],
            'pushUrl'       => ['type' => 'string'],
            'pushTokenUser'       => ['type' => 'string'],
            'order'       => ['type' => 'int'],
            'minAge'       => ['type' => 'int'],
            'maxAge'       => ['type' => 'int'],
            'newsActive'    => ['type' => 'bool',   'default' => false],
            'isNearBy'    => ['type' => 'bool',   'default' => false],
            'mainTypes' => ['type' => 'array(string)'],
            'startDateTime' => ['type' => 'datetime'],
            'expiredDateTime' => ['type' => 'datetime'],
            'isReachDateTime'    => ['type' => 'bool',   'default' => false],
            'isSponser'    => ['type' => 'bool',   'default' => false],
            'isHighlight' => ['type' => 'bool', 'default' => false],
        ];
    }

    public function shops()
    {
        return $this->belongsTo('App\Models\Shops', 'shopId')->withDefault();
    }

    public function newsTypes()
    {
        return $this->belongsTo('App\Models\NewsTypes', 'newsType')->withDefault();
    }

    public function wildItems()
    {
        return $this->belongsTo('App\Models\WildItem', 'wildItemId')->withDefault();
    }


    public static function queryNews()
    {

        $queryNews = News::all();
        $news = [];
        foreach ($queryNews as $type) {
            //    if ($type->newsTypes->newsTypeName['eng'] == "Promotion") {
            $news[$type->id] = $type->newsName['eng'];
            //    }
        }
        return $news;
    }
}
class Locations extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)'],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}
