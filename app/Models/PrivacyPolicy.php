<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;
 
class PrivacyPolicy extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'privacyPolicy';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'privacyPolicyVersion' => ['type' => 'int'],
            'privacyPolicyTitle' => ['type' => 'array(string)'],
            'privacyPolicyBody' => ['type' => 'array(string)'],
            'isActive'    => ['type' => 'bool', 'default' => false],
        ];
    }
}
