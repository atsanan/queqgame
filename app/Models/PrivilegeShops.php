<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model  as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class PrivilegeShops extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'privilegeShops';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'privilegeShopTitles' => ['type' => 'array(string)'],
            'privilegeShopDetails' => ['type' => 'array(string)'],
            'filenameLogo1' => ['type' => 'string'],
            'filenameLogo2' => ['type' => 'string'],
            'filenameLogo3' => ['type' => 'string'],
            'startDateTime' => ['type' => 'datetime'],
            'expiredDateTime' => ['type' => 'datetime'],
            'shopId' => ['type' => 'objectid'],
            'privilegeDefaultId' => ['type' => 'objectid'],
            'order' => ['type' => 'int'],
            'isActive' => ['type' => 'bool',   'default' => false],
            'isReachDateTime'  => ['type' => 'bool',   'default' => false],
            'isGoldenMinutes' => ['type' => 'bool',   'default' => false],
            'linkListName' => ['type' => 'string'],
        ];
    }

    public function shops()
    {
        return $this->belongsTo('App\Models\Shops', 'shopId')->withDefault();
    }

    public function privilegeDefaults()
    {
        return $this->belongsTo('App\Models\PrivilegeDefaults', 'privilegeDefaultId')->withDefault();
    }
}
