<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class DigitalBoardAsserts extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'digitalBoardAsserts';
    protected $guarded = [];

    public static function SCHEMAS()
    {

        return [
            'title' => ['type' => 'array(string)'],
            'body' => ['type' => 'array(string)'],
            'displayType' => ['type' => 'int'],
            'imageSlot1' => ['type' => 'string', 'default' => ""],
            'imageSlot2' => ['type' => 'string', 'default' => ""],
            'modelInMapIos' => ['type' => 'string', 'default' => ""],
            'modelInMapAndroid' => ['type' => 'string', 'default' => ""],
            'modelInMapVersion' => ['type' => 'int'],
            'modelStageIos' => ['type' => 'string', 'default' => ""],
            'modelStageAndroid' => ['type' => 'string', 'default' => ""],
            'modelStageVersion' => ['type' => 'int'],
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }

    public static function queryData()
    {

        $query = DigitalBoardAsserts::where([])->get();
        $data = [];
        foreach ($query as $type) {
            $data[$type->id] = $type->title['eng'];
        }

        return $data;
    }
}
