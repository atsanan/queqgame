<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class MallFloor extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'mallFloors';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'mallFloorName' => ['type' => 'array(string)' ],
            'mallFloorDetail' => ['type' => 'array(string)' ],
            'mapIndoorFloorKey' => ['type' => 'string'],
            'mapIndoorFloorData' => ['type' => 'string'],            
            'mallId' => ['type' => 'objectid'],
        ];
    }

    public static function queryMallFloors($mallId = null){

        if($mallId==null){
            $queryMallFloors= MallFloor::where([])->get();
        }else {
            $queryMallFloors= MallFloor::where(['mallId'=>new \MongoDB\BSON\ObjectId($mallId)])->get();
        }
        $mallFloors=[];
        if($queryMallFloors){
            foreach($queryMallFloors as $mallFloor){
                $mallFloors[$mallFloor->id]=$mallFloor->mallFloorName['eng'];
            }
        }
        return $mallFloors;
    }

    public function Malls()
    {
        return $this->belongsTo('App\Models\Malls','mallId')->withDefault();
    }

}
