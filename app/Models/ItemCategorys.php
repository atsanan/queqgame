<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ItemCategorys extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'itemCategorys';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'itemCategoryName'=> ['type' => 'string'],
            'itemCategoryOrder'=> ['type' => 'int']
        ];
    }
    
    public static function queryItemCategorys(){

        $queryItemCategorys= ItemCategorys::where([])->get();
        $itemCategorys=[];
        
        if($queryItemCategorys){
            foreach($queryItemCategorys as $itemCategory){
                $itemCategorys[$itemCategory->id]=$itemCategory->itemCategoryName;
            }
        }
        return $itemCategorys;
    }

}
