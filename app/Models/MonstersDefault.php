<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class MonstersDefault extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'monsterDefaults';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'mDefaultName' => ['type' => 'array(string)'],
            'mDefaultTextDetail' => ['type' => 'array(string)'],
            'mDefaultTypeId'       => ['type' => 'objectid'],
            'mDefaultJobLevel'       => ['type' => 'int'],
            'mDefaultJobCategory' => ['type' => 'array(objectid)'],
            'mDefaultGroupId'       => ['type' => 'int'],
            'mDefaultGroupLevel'       => ['type' => 'int'],
            'mDefaultAssetVersion'       => ['type' => 'int'],
            'mDefaultOrder'       => ['type' => 'int'],
            'mDefaultAssetModel'       => ['type' => 'string'],
            'mDefaultAssetImageSlot'       => ['type' => 'string'],
            'mDefaultAssetImageSlot2'       => ['type' => 'string'],
            'mDefaultAssetImageSlot3'       => ['type' => 'string'],
            'mDefaultAssertModelIOS' => ['type' => 'string'],
            'mDefaultAssertModelAndroid' => ['type' => 'string'],
        ];
    }

    public function monstersDefaultsTypes()
    {
        return $this->belongsTo('App\Models\MonstersDefaultsTypes', 'mDefaultTypeId')->withDefault();
    }

    public static function queryMonstersDefault()
    {

        $queryMonstersDefaults = MonstersDefault::where([])->get();
        $monstersDefaults = [];
        foreach ($queryMonstersDefaults as $type) {
            $monstersDefaults[$type->id] = $type->mDefaultName['eng'];
        }

        return $monstersDefaults;
    }
}
