<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class PrivilegeDefaults extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'privilegeDefaults';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'privilegeDefaultNames' => ['type' => 'array(string)'],
            'privilegeDefaultDetails' => ['type' => 'array(string)'],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'privilegeGroupId' => ['type' => 'objectid'],
            'order'       => ['type' => 'int'],
            'filenameLogo1' => ['type' => 'string'],
            'filenameLogo2' => ['type' => 'string']

        ];
    }

    public function privilegeGroups()
    {
        return $this->belongsTo('App\Models\PrivilegeGroups', 'privilegeGroupId')->withDefault();
    }

    public static function queryPrivilegeDefaults()
    {

        $queryMonstersDefaults = PrivilegeDefaults::where([])->get();
        $privilegeDefaults = [];
        foreach ($queryMonstersDefaults as $row) {
            $privilegeDefaults[$row->id] = $row->privilegeDefaultNames['eng'];
        }

        return $privilegeDefaults;
    }
}
