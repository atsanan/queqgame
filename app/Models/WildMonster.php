<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class WildMonster extends MongoModel
{
    const ACTIVE_WILDMONSTER = false;

    protected $connection = 'mongodb';
    protected $collection = 'wildMonsters';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'mDefaultId' => ['type' => 'objectid'],
            'toMall' => ['type' => 'objectid'],
            'toMallFloor' => ['type' => 'objectid'],
            'toShop' => ['type' => 'objectid'],
            'zoneTypeId' => ['type' => 'objectid'],
            'startDateTime' => ['type' => 'datetime'],
            'expiredDateTime' => ['type' => 'datetime'],
            'count' => ['type' => 'int'],
            'countMax'       => ['type' => 'int'],
            'pickPercent' => ['type' => 'int'],
            'itemStock'       => ['type' => 'int'],
            'diamond'       => ['type' => 'int'],
            'order'       => ['type' => 'int'],
            'itemId' => ['type' => 'objectid'],
            'isPickToBag' => ['type' => 'bool', 'default' => false],
            'isGoldenMinutes'    => ['type' => 'bool',   'default' => false],
            'isReachDateTime'    => ['type' => 'bool',   'default' => false],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'isRealPosition'    => ['type' => 'bool',   'default' => false],
            'isFDGetCoupon'    => ['type' => 'bool',   'default' => false],
            'location' => ['type' => LocationsWildMonster::class],
        ];
    }

    public function monstersDefaults()
    {
        return $this->belongsTo('App\Models\MonstersDefault', 'mDefaultId')->withDefault();
    }

    public function Malls()
    {
        return $this->belongsTo('App\Models\Malls', 'toMall')->withDefault();
    }

    public function MallFloors()
    {
        return $this->belongsTo('App\Models\MallFloor', 'toMallFloor')->withDefault();
    }

    public function Shops()
    {
        return $this->belongsTo('App\Models\Shops', 'toShop')->withDefault();
    }

    public function ZoneTypes()
    {
        return $this->belongsTo('App\Models\ZoneTypes', 'zoneTypeId')->withDefault();
    }

    public function Item()
    {
        return $this->belongsTo('App\Models\ItemDefault', 'itemId')->withDefault();
    }
}


class LocationsWildMonster extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)'],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}
