<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ItemDefault extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'itemDefaults';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'nameReport' => ['type' => 'string'],
            'itemName' => ['type' => 'array(string)'],
            'itemDetail' => ['type' => 'array(string)'],
            'itemCondition' => ['type' => 'array(string)'],
            'itemAssertModel'       => ['type' => 'string'],
            'itemAssertImageSlots'       => ['type' => 'array(string)'],
            'itemCategoryId' => ['type' => 'objectid'],
            'expireDate' => ['type' => 'datetime', 'default' => null],
            'coin'       => ['type' => 'int'],
            'diamond'       => ['type' => 'int'],
            'itemOrder'       => ['type' => 'int'],
            'itemAssertVersion'       => ['type' => 'int'],
            'isActive'    => ['type' => 'bool', 'default' => false],
            'itemAssertModelIOS' => ['type' => 'string'],
            'itemAssertModelAndroid' => ['type' => 'string'],
            'itemAssertImageSlot1' => ['type' => 'string'],
            'itemAssertImageSlot2' => ['type' => 'string'],
            'itemStoreBy'  => ['type' => 'string'],
            'itemDetailEffect' => ['type' => 'string'],
            'itemDetailStore' => ['type' => 'string'],
            'itemStoreName' => ['type' => 'array(string)'],
            'itemStoreDetail' => ['type' => 'array(string)'],
            'itemUrl' => ['type' => 'string'],
            'isStore' => ['type' => 'bool', 'default' => false],
            'mainTab' => ['type' => 'array(string)'],
            'subTab' => ['type' => 'array(string)'],
            'codeJson' => ['type' => 'string'],
            'diamondInclude' => ['type' => 'int'],
            'shopId' => ['type' => 'array(objectid)'],
            'shopSelect' => ['type' => 'objectid'],
            'partnerTypeId' => ['type' => 'array(objectid)'],
            'shortText' => ['type' => 'string'],
            'isHighlight' => ['type' => 'bool', 'default' => false],
            'isReachDateTime'    => ['type' => 'bool',   'default' => false],
            'isRealPosition'    => ['type' => 'bool',   'default' => false],
            'location' => ['type' => LocationItem::class],
        ];
    }

    public function itemCategory()
    {
        return $this->belongsTo('App\Models\ItemCategorys', 'itemCategoryId')->withDefault();
    }


    public static function queryItem()
    {

        $query = self::where([])->get();
        $arr = [];

        if ($query) {
            foreach ($query as $data) {
                $arr[$data->id] = $data->itemName['eng'];
            }
        }

        return $arr;
    }
}


class LocationItem extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)'],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}
