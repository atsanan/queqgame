<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Malls extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'malls';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'mallName' => ['type' => 'array(string)'],
            'mallDetail' => ['type' => 'array(string)'],
            'mallAddress' => ['type' => 'array(string)'],
            'location' => ['type' => LocationsMall::class],
            'mapIndoorId' => ['type' => 'string'],
            'mapOutdoorId' => ['type' => 'string'],
            'isActive'    => ['type' => 'bool',   'default' => false],
            'isSponser'    => ['type' => 'bool',   'default' => false],
            'isFixedBugFloor' => ['type' => 'bool',   'default' => false],
            'order'       => ['type' => 'int'],
            'indoorStartFloor'       => ['type' => 'int'],
            'indoorDistanceFromInterest'       => ['type' => 'int'],
            'filenameLogo1' => ['type' => 'string'],
            'filenameLogo2' => ['type' => 'string']

        ];
    }

    public static function queryMall()
    {

        $queryMalls = Malls::where([])->get();
        $malls = [];
        foreach ($queryMalls as $type) {
            $malls[$type->id] = $type->mallName['eng'];
        }

        return $malls;
    }
}

class LocationsMall extends NestedMongoModel
{
    public static function SCHEMAS()
    {
        return [
            'coordinates' => ['type' => 'array(float)'],
            'type'    => ['type' => 'string',   'default' => 'Point'],
        ];
    }
}

