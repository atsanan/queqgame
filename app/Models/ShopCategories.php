<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class ShopCategories extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'shopCategories';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [

            'shopCategorieName' => ['type' => 'array(string)' ]            
        ];
    }

    public static function queryShopCategories(){

        $queryShopCategories= ShopCategories::where([])->get();
        $shopCategories=[];
        
        if($queryShopCategories){
            foreach($queryShopCategories as $shopCategory){
                $shopCategories[$shopCategory->id]=$shopCategory->shopCategorieName['eng'];
            }
        }
        return $shopCategories;
    }

    

}
