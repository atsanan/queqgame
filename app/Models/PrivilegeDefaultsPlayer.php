<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class PrivilegeDefaultsPlayer extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'privilegeDefaultPlayers';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'privilegeDefaultId' => ['type' => 'objectid'],
            'playerId' => ['type' => 'objectid'],
        ];
    }
}
