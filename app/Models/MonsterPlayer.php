<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class MonsterPlayer extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'monsterPlayers';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'mDefaultId' => ['type' => 'objectid'],


        ];
    }
}
