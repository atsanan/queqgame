<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class Activities extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'activities';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'activityTitle' => ['type' => 'array(string)'],
            'activityDetail' => ['type' => 'array(string)'],
            'activityTypes' => ['type' => 'array(string)'],
            'startDateTime' => ['type' => 'datetime'],
            'expiredDateTime' => ['type' => 'datetime'],
            'image' => ['type' => 'string'],
            'color' => ['type' => 'string'],
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }

    public function ZoneTypes()
    {
        return $this->belongsTo('App\Models\ZoneTypes', 'zoneTypeId')->withDefault();
    }

    public function Malls()
    {
        return $this->belongsTo('App\Models\Malls', 'toMall')->withDefault();
    }

    public function MallFloors()
    {
        return $this->belongsTo('App\Models\MallFloor', 'toMallFloor')->withDefault();
    }

    public function Shops()
    {
        return $this->belongsTo('App\Models\Shops', 'toShop')->withDefault();
    }
}
