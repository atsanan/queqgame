<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class MailBox extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'mailBox';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'title' => ['type' => 'string'],
            'detail' => ['type' => 'string'],
            'linkListName' => ['type' => 'string'],
            'url' => ['type' => 'string', 'default' => ""],
            'coin' => ['type' => 'int', 'default' => 0],
            'coinActive' => ['type' => 'bool', 'default' => false],
            'diamond' => ['type' => 'int', 'default' => 0],
            'diamondActive' => ['type' => 'bool', 'default' => false],
            'imageName' => ['type' => 'string', 'default' => ""],
            'thumbnail' => ['type' => 'string', 'default' => ""],
            'itemId' => ['type' => 'objectid'],
            'itemActive' => ['type' => 'bool', 'default' => false],
            'mDefaultId' => ['type' => 'objectid'],
            'monsterActive' => ['type' => 'bool', 'default' => false],
            'userId' => ['type' => 'objectid'],
            'playerId' => ['type' => 'objectid'],
            'isActive'    => ['type' => 'bool', 'default' => false],
            'createAt'    => ['type' => 'datetime',]
        ];
    }
}
