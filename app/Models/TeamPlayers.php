<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class TeamPlayers extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'teamPlayers';
    protected $guarded = [];
   
}
