<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class IpAccess extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'ipAccess';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'networkAccessId' => ['type' => 'objectid'],
            'ipName' => ['type' => 'string'],
            'ipAddress' => ['type' => 'string'],
            'isActive'    => ['type' => 'bool',   'default' => true],
        ];
    }

    public function networkAccess()
    {
        return $this->belongsTo('App\Models\NetworkAccess', 'networkAccessId')->withDefault();
    }
}
