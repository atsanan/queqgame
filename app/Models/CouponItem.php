<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class CouponItem extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'couponItems';
    protected $guarded = [];

    public static function SCHEMAS()
    {
        return [
            'code' => ['type' => 'string'],
            'comment' => ['type' => 'string'],
            'itemId' => ['type' => 'objectid'],
            'itemPlayerId' => ['type' => 'objectid'],
            'isActive'    => ['type' => 'bool',   'default' => false],
        ];
    }

    public function itemPlayer()
    {
        return $this->belongsTo('App\Models\ItemPlayer', 'itemPlayerId')->withDefault();
    }
}
