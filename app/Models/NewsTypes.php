<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Helper\MongoModel;
use App\Helper\NestedMongoModel;

class NewsTypes extends MongoModel
{
    protected $connection = 'mongodb';
    protected $collection = 'newsTypes';
    protected $guarded = [];
   
    public static function SCHEMAS()
    {
        return [

            'newsTypeName' => ['type' => 'array(string)' ],
            'newsTypeDetail' => ['type' => 'array(string)' ],
            'order'       => ['type' => 'int'],
        ];
    }
}
