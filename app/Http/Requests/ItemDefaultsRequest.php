<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class ItemDefaultsRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'coin' => 'required',
                    'diamond' => 'required',
                    'itemOrder' => 'required',
                    'itemAssertVersion' => 'required',
                    'itemCategoryId' => 'required',
                    // 'itemAssertModel' => 'required',
                    // 'itemAssertImageSlot' => 'required',

                ];
            }
            case 'PUT':
            case 'PATCH': {
                $id = $this->route('itemDefault')->id;
                return [
                    'coin' => 'required',
                    'diamond' => 'required',
                    'itemOrder' => 'required',
                    'itemAssertVersion' => 'required',
                    'itemCategoryId' => 'required',
                  
                ];
            }
            default:
                break;
        }


    }



}
