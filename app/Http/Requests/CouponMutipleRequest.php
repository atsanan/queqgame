<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

Validator::extend('greater_than', function ($attribute, $value, $otherValue) {
    return intval($value) > intval($otherValue[0]);
});

class CouponMutipleRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    return [
                        'itemId' => 'required',
                        'amount' => 'required|gte:1|lte:50',
                    ];
                }
            case 'PUT':
            case 'PATCH': {

                    return [];
                }
            default:
                break;
        }
    }
}
