<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class WildMonstersRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'mDefaultId' => 'required',
                    //'toMallFloor' => 'required',
                    //'toShop' => 'required',
                    'zoneTypeId' => 'required',
                    'count' => 'required',

                ];
            }
            case 'PUT':
            case 'PATCH': {
                $id = $this->route('wildMonster')->id;
                return [
                    'mDefaultId' => 'required',
                    //'toMallFloor' => 'required',
                    //'toShop' => 'required',
                    'zoneTypeId' => 'required',
                    'count' => 'required',
                ];
            }
            default:
                break;
        }


    }



}
