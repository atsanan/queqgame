<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class MallsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {

                    return [
                        // 'mapIndoorId' => 'required',
                        // 'mapOutdoorId' => 'required',
                        // 'order' => 'required',
                        // 'filenameLogo1' => 'required',
                        // 'filenameLogo2' => 'required',
                        //'filenameLogo1' => 'dimensions:width=120,height=120',
                        //'filenameLogo2' => 'dimensions:width=1044,height=202'
                        'longitude' => [function ($attribute, $value, $fail) {

                            $pattern =  '/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,25})?))$/';

                            if (!preg_match($pattern, $value)) {
                                $fail(':Incorrect format');
                            }
                        }],
                        'latitude' => [function ($attribute, $value, $fail) {
                            $pattern =  '/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,25})?))$/';

                            if (!preg_match($pattern, $value)) {
                                $fail(':Incorrect format');
                            }
                        }]

                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    $id = $this->route('mall')->id;
                    return [
                        // 'mapIndoorId' => 'required',
                        // 'mapOutdoorId' => 'required',
                        // 'order' => 'required',
                        //'filenameLogo1' => 'dimensions:width=120,height=120',
                        //'filenameLogo2' => 'dimensions:width=430,height=120'
                        'longitude' => [function ($attribute, $value, $fail) {

                            $pattern =  '/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,25})?))$/';

                            if (!preg_match($pattern, $value)) {
                                $fail(':Incorrect format');
                            }
                        }],
                        'latitude' => [function ($attribute, $value, $fail) {
                            $pattern =  '/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,25})?))$/';

                            if (!preg_match($pattern, $value)) {
                                $fail(':Incorrect format');
                            }
                        }]
                    ];
                }
            default:
                break;
        }
    }
}
