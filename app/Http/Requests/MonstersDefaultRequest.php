<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class MonstersDefaultRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET': 
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    // 'title' => 'required|unique:blog_categories|min:3',
                    // 'mDefaultName.eng' => 'required',
                    // 'mDefaultName.thai' => 'required',
                    // 'mDefaultName.chi1' => 'required',
                    // 'mDefaultName.chi2' => 'required',
                    // 'mDefaultTextDetail.eng' => 'required',
                    // 'mDefaultTextDetail.thai' => 'required',
                    // 'mDefaultTextDetail.chi1' => 'required',
                    // 'mDefaultTextDetail.chi2' => 'required',
                    // 'mDefaultTypeId' => 'required',
                    // 'mDefaultJobCategory[]'=>'required',
                    // 'mDefaultJobLevel' => 'required',
                    // 'mDefaultGroupId' => 'required',
                    // 'mDefaultGroupLevel' => 'required',
                    // 'mDefaultAssetVersion' => 'required',
                    // 'mDefaultOrder' => 'required',
                    // 'mDefaultAssetModel'=>'required',
                    //'mDefaultAssetImageSlot'=>'dimensions:width=200,height=200'
                ];
            }
            case 'PUT':
            case 'PATCH': {
                $id = $this->route('monstersDefault')->id;
                return [
                    // 'title' => 'required|min:3|unique:blog_categories,title,' . $id
                    // 'mDefaultName.eng] => 'required',
                    // 'mDefaultName[thai]' => 'required',
                    // 'mDefaultName[chi1]' => 'required',
                    // 'mDefaultName[chi2]' => 'required',
                    // 'mDefaultTextDetail[eng]' => 'required',
                    // 'mDefaultTextDetail[thai]' => 'required',
                    // 'mDefaultTextDetail[chi1]' => 'required',
                    // 'mDefaultTextDetail[chi2]' => 'required',
                    // 'mDefaultTypeId' => 'required',
                    // 'mDefaultJobCategory[]'=>'required',
                    // 'mDefaultJobLevel' => 'required',
                    // 'mDefaultGroupId' => 'required',
                    // 'mDefaultGroupLevel' => 'required',
                    // 'mDefaultAssetVersion' => 'required',
                    // 'mDefaultOrder' => 'required',
                    //'mDefaultAssetImageSlot'=>'dimensions:width=200,height=200'
                ];
            }
            default:
                break;
        }
    }



}
