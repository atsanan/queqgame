<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class ShopAssertsRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    // 'shopAssertModel' => 'required',
                    // 'shopAssertVersion' => 'required',
                    // 'fileNameLogo1' => 'required',
                    // 'fileNameLogo2' => 'required',
                    // 'filenameLogo1' => 'dimensions:width=120,height=120',
                    // 'filenameLogo2' => 'dimensions:width=430,height=120'

                ];
            }
            case 'PUT':
            case 'PATCH': {
                $id = $this->route('shopAssert')->id;
                return [
                    // 'shopAssertModel' => 'required',
                    // 'shopAssertVersion' => 'required',
                    // 'filenameLogo1' => 'dimensions:width=120,height=120',
                    // 'filenameLogo2' => 'dimensions:width=430,height=120'
                ];
            }
            default:
                break;
        }


    }



}
