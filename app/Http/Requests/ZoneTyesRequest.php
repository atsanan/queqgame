<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class ZoneTyesRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    // 'mallName' => 'required|unique:blog_categories|min:3',

                ];
            }
            case 'PUT':
            case 'PATCH': {
                $id = $this->route('zoneType')->id;
                return [
                    // 'mallName' => 'required|min:3|unique:blog_categories,mallName,' . $id
                ];
            }
            default:
                break;
        }


    }



}
