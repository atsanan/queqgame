<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\PrivacyPolicy;
use App\Http\Requests\PrivacyPolicyRequest;
use App\Helper\ConvertValueLanguages;
use Illuminate\Support\Facades\Input;
use App\Models\Setting;

class PrivacyPolicyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['view']);
    }

    public function index()
    {
        $privacyPolicy = PrivacyPolicy::all();
        $setting = Setting::where([])->first();
        return view('privacyPolicy.index', compact('privacyPolicy', 'setting'));
    }

    public function create()
    {
        return view('privacyPolicy.create');
    }


    public function store(PrivacyPolicyRequest $request)
    {
        $model =  new PrivacyPolicy($request->all());
        $model->privacyPolicyTitle = ConvertValueLanguages::convertData($model->privacyPolicyTitle);
        $model->privacyPolicyBody = ConvertValueLanguages::convertData($model->privacyPolicyBody);

        if ($model->save()) {
            return redirect('privacyPolicy')->with('success', trans('privacyPolicy/message.success.create'));
        } else {
            return Redirect::route('privacyPolicy')->withInput()->with('error', trans('privacyPolicy/message.error.create'));
        }
    }

    public function edit(PrivacyPolicy $privacyPolicy)
    {
        return view('privacyPolicy.edit', compact('privacyPolicy'));
    }

    public function update(PrivacyPolicyRequest $request, PrivacyPolicy $privacyPolicy)
    {
        if ($privacyPolicy->update($request->all())) {
            $privacyPolicy->update(['privacyPolicyTitle' => ConvertValueLanguages::convertData($privacyPolicy->privacyPolicyTitle)]);
            $privacyPolicy->update(['privacyPolicyBody' => ConvertValueLanguages::convertData($privacyPolicy->privacyPolicyBody)]);
            return redirect('privacyPolicy/' . $privacyPolicy->id . '/edit')->with('success', 'Success Update');
        } else {
            return Redirect::route('privacyPolicy')->withInput()->with('error', trans('privacyPolicy/message.error.create'));
        }
    }

    public function view()
    {
        $privacyPolicy = PrivacyPolicy::where(['privacyPolicyVersion' => (int)Input::get('version')])->first();
        return view('privacyPolicy.view', compact('privacyPolicy'));
    }

    public function setupCurrent($version)
    {
        $model = Setting::where([])->first();
        $model->privacyPolicyVersion = $version;
        $model->privacyPolicyUrl = url('/privacyPolicy/view') . "?version=" . $version . "&lang=eng";
        if ($model->update()) {
            $privacyPolicy = PrivacyPolicy::where(['privacyPolicyVersion' => (int)$version])->first();

            return redirect('privacyPolicy/' . $privacyPolicy->_id . '/edit')->with('success', 'Success Update Version');
        }
    }
}
