<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\NetworkAccess;
use App\Models\IpAccess;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class IpAccessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Setting_Resource');
    }

    public function index()
    {
        $ipAccess = IpAccess::with(['networkaccess'])->get()->toArray();

        return view('ipAccess.index', compact('ipAccess'));
    }

    public function create()
    {
        $networkAccess = NetworkAccess::queryNetworkAccess();
        $networkAccessId = Input::get('networkAccessId');
        return view('ipAccess.create', compact('networkAccess', 'networkAccessId'));
    }
    public function store(Request $request)
    {
        $model = new IpAccess($request->all());
        if ($model->save()) {

            return redirect('ipAccess/' . $model->id . '/edit')->with('success', 'Success create');
        } else {
            return Redirect::route('ipAccess')->withInput()->with('error', trans('malls/message.error.create'));
        }
    }

    public function edit(IpAccess $ipAccess)
    {
        $networkAccess = NetworkAccess::queryNetworkAccess();
        $networkAccessId = $ipAccess->networkAccessId;
        return view('ipAccess.edit', compact('ipAccess', 'networkAccess', 'networkAccessId'));
    }


    public function update(Request $request, IpAccess $ipAccess)
    {
        if ($ipAccess->update($request->all())) {
            return redirect('ipAccess/' . $ipAccess->id . '/edit')->with('success', 'Success Update');
        } else {
            return Redirect::route('ipAccess')->withInput()->with('error', trans('malls/message.error.create'));
        }
    }

    public function destroy($id)
    {

        $ipAccess = IpAccess::find($id);

        if ($ipAccess) {
            $ipAccess->delete();
            return redirect('networkAccess/' . $ipAccess->networkAccessId . '/edit')->with('success', trans('Success delete IpAddress'));
        } else {
            return redirect('ipAccess')->with('error', trans('ipAccess/message.error.delete'));
        }
    }
}
