<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\NetworkAccess;
use App\Models\IpAccess;
use App\Http\Requests\NetworkAccessRequest;

class NetworkAccessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Setting_Resource');
    }

    public function index()
    {
        $networkAccess = NetworkAccess::all();
        return view('networkAccess.index', compact('networkAccess'));
    }

    public function create()
    {

        return view('networkAccess.create');
    }

    public function store(NetworkAccessRequest $request)
    {
        $model = new NetworkAccess($request->all());
        if ($model->save()) {

            return redirect('networkAccess/' . $model->id . '/edit')->with('success', 'Success create');
        } else {
            return Redirect::route('ipAccess')->withInput()->with('error', trans('malls/message.error.create'));
        }
    }

    public function edit(NetworkAccess $networkAccess)
    {
        $ipAccess = IpAccess::where(['networkAccessId' => new \MongoDB\BSON\ObjectId($networkAccess->id)])->get();
        return view('networkAccess.edit', compact('networkAccess', 'ipAccess'));
    }


    public function update(NetworkAccess $networkAccess, NetworkAccessRequest $request)
    {
        if ($networkAccess->update($request->all())) {
            return redirect('networkAccess/' . $networkAccess->id . '/edit')->with('success', 'Success Update');
        } else {
            return Redirect::route('ipAccess')->withInput()->with('error', trans('malls/message.error.create'));
        }
    }


    public function destroy($id)
    {

        $networkAccess = NetworkAccess::find($id);

        if ($networkAccess) {
            $networkAccess->delete();
            return redirect('networkAccess')->with('success', trans('Success delete IpAddress'));
        } else {
            return redirect('networkAccess')->with('error', trans('ipAccess/message.error.delete'));
        }
    }
}
