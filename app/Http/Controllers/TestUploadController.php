<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestUploadController extends Controller
{
    public function index()
    {
        return view('upload');
    }

    public function store(Request $request)
    {
      $file = $request->file('image');

      $destinationPath = 'media/images';
      $new_file_name = uniqid().'.'.$file->getClientOriginalExtension();
      $file->move($destinationPath, $new_file_name);
      echo "$destinationPath/$new_file_name";
    }
}
