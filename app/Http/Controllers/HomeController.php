<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Players;
use App\Models\ItemDefault;
use App\Models\MonstersDefault;
use App\Models\StickerStore;
use App\Models\Costumes;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countPlayer = Players::count();
        $countMonsterDefault = MonstersDefault::count();
        $countItemDefault = ItemDefault::count();
        $countStickerStore = StickerStore::count();
        $countCostumes = Costumes::count();
        return view('home', compact(
            'countPlayer',
            'countMonsterDefault',
            'countItemDefault',
            'countStickerStore',
            'countCostumes'
        ));
    }
}
