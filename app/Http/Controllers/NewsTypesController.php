<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\NewsTypes;
use App\Http\Requests\NewsTypesRequest;

class NewsTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Promotion_Resource');
    }

    public function index()
    {
        $newsTypes = NewsTypes::all();

        return view('newsTypes.index', compact('newsTypes'));
    }

    public function create()
    {
        $order = 0;
        return view('newsTypes.create', compact('order'));
    }

    public function store(NewsTypesRequest $request)
    {
        $newsType = new NewsTypes($request->all());

        if ($newsType->save()) {
            return redirect('newsTypes')->with('success', trans('newsTypes/message.success.create'));
        } else {
            return Redirect::route('newsTypes')->withInput()->with('error', trans('newsTypes/message.error.create'));
        }
    }

    public function edit(NewsTypes $newsType)
    {
        $order = $newsType->order;

        return view('newsTypes.edit', compact('newsType', 'order'));
    }

    public function update(NewsTypesRequest $request, NewsTypes $newsType)
    {
        if ($newsType->update($request->all())) {

            return redirect('newsTypes')->with('success', trans('newsTypes/message.success.update'));
        } else {
            return Redirect::route('newsTypes')->withInput()->with('error', trans('newsTypes/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $newsType = NewsTypes::find($id);

        if ($newsType->delete()) {
            return redirect('newsTypes')->with('success', trans('newsTypes/message.success.destroy'));
        } else {
            return Redirect::route('newsTypes')->withInput()->with('error', trans('newsTypes/message.error.delete'));
        }
    }

    public function data()
    {
        $models = NewsTypes::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (NewsTypes $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
