<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\GiveMonster;
use App\Models\MonstersDefault;
use App\Http\Requests\CostumesRequest;

class GiveMonsterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $giveMonster = GiveMonster::where([])->first();
        $monstersDefaults = MonstersDefault::queryMonstersDefault();
        return view('giveMonster.edit', compact('giveMonster', 'monstersDefaults'));
    }


    public function update(Request $request, GiveMonster $giveMonster)
    {
        if ($giveMonster->update($request->all())) {
            return redirect('giveMonster/edit')->with('success', 'Success Update');
        } else {
            return Redirect::route('giveMonster')->withInput()->with('error', trans('malls/message.error.create'));
        }
    }
}
