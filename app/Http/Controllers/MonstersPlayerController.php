<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\MonsterPlayer;

class MonstersPlayerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Report_Resource');
    }

    public function destroy($id)
    {
        $monsterPlayer = MonsterPlayer::find($id);

        if ($monsterPlayer) {
            $monsterPlayer->delete();
            $playerId = $monsterPlayer->playerId;
            return redirect("players/{$playerId}/edit")->with('success', trans('malls/message.success.destroy'));
        } else {
            return Redirect::route('malls')->withInput()->with('error', trans('malls/message.error.delete'));
        }
    }
}
