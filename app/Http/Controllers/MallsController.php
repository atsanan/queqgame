<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Http\Requests\MallsRequest;
use App\Helper\ConvertValueLanguages;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class MallsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Mall_Resource');
    }

    public function index()
    {

        $search = Input::get('search');
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);

        if ($field == "mallName") {
            $fieldSort = "mallName.eng";
        } else {
            $fieldSort = $field;
        }
        $data = Malls::with([]);
        if (isset($search)) {

            if (isset($sort)) {
                $data = $data->where("mallName.eng", "like", "%$search%")
                    ->orderBy($fieldSort, $sort)->get();
            } else {
                $data = $data->where("mallName.eng", "like", "%$search%")->get();
            }
        } else {
            if (isset($sort)) {
                $data = $data->orderBy($fieldSort, $sort)->get();
            } else {
                $data = $data->orderBy("mallName.eng", "asc")->get();
            }
        }


        $malls  = new LengthAwarePaginator($data->slice($offset, 50), $data->count(), 50);

        $malls->setPath('/malls');
        return view('malls.index', compact('malls', 'search', 'field', 'sort'));
    }

    public function create()
    {
        $filenameLogos1 = null;
        $filenameLogos2 = null;
        $order = 0;
        return view('malls.create', compact('order', 'filenameLogos1', 'filenameLogos2'));
    }

    public function store(MallsRequest $request)
    {
        $mall = new Malls($request->all());

        $imageFile1 = $mall->filenameLogo1;
        $imagePath1 = '/public/media/images/';
        $imageFile2 = $mall->filenameLogo2;
        $imagePath2 = '/public/media/images/';

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
            $mall->filenameLogo1 = $imageName1;
            $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
            $mall->filenameLogo2 = $imageName2;
            $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
        }

        $mall->mallName = ConvertValueLanguages::convertData($mall->mallName);
        $mall->mallDetail = ConvertValueLanguages::convertData($mall->mallDetail);
        $mall->mallAddress = ConvertValueLanguages::convertData($mall->mallAddress);
        $mall->location = ['coordinates' => [(float) $mall->longitude, (float) $mall->latitude], 'type' => 'Point'];
        if ($mall->save()) {

            return redirect('malls/' . $mall->id . '/edit')->with('success', 'Success create');
        } else {
            return Redirect::route('malls')->withInput()->with('error', trans('malls/message.error.create'));
        }
    }

    public function edit(Malls $mall)
    {
        $mallFloors = MallFloor::where(['mallId' => new \MongoDB\BSON\ObjectId($mall->id)])->get();
        $filenameLogos1 = $mall->filenameLogo1;
        $filenameLogos2 = $mall->filenameLogo2;
        $order = $mall->order;

        $mall->longitude = $mall->location['coordinates'][0];
        $mall->latitude = $mall->location['coordinates'][1];

        return view('malls.edit', compact('mall', 'mallFloors', 'order', 'filenameLogos1', 'filenameLogos2'));
    }

    public function update(MallsRequest $request, Malls $mall)
    {



        if ($mall->update($request->all())) {
            $imageFile1 = $request->filenameLogo1;
            $imagePath1 = '/public/media/images/';
            $imageFile2 = $request->filenameLogo2;
            $imagePath2 = '/public/media/images/';

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
                $mall->update(['filenameLogo1' => $imageName1]);
                $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
            }

            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
                $mall->update(['filenameLogo2' => $imageName2]);
                $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
            }

            $mall->update(['mallName' => ConvertValueLanguages::convertData($mall->mallName)]);
            $mall->update(['mallDetail' => ConvertValueLanguages::convertData($mall->mallDetail)]);
            $mall->update(['mallAddress' => ConvertValueLanguages::convertData($mall->mallAddress)]);
            $location = ['coordinates' => [(float) $mall->longitude, (float) $mall->latitude], 'type' => 'Point'];
            $mall->update(['location' => $location]);

            return redirect('malls/' . $mall->id . '/edit')->with('success', 'Success Update');
        } else {
            return Redirect::route('malls')->withInput()->with('error', trans('malls/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $mall = Malls::find($id);

        if ($mall) {
            $mall->delete();
            return redirect('malls')->with('success', trans('malls/message.success.destroy'));
        } else {
            return redirect('malls')->with('error', trans('malls/message.error.delete'));
        }
    }

    public function data()
    {
        $models = Malls::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (Malls $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
