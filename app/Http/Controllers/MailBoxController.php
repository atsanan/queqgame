<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemDefault;
use App\Models\MonstersDefault;
use App\Models\PrivilegeDefaults;
use App\Models\UserPlayer;
use App\Models\Players;
use App\Models\MailBox;
use App\Models\PrivilegeGroups;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\News;
use App\Models\Shops;
use App\Http\Requests\MailBoxsRequest;
use App\Helper\Thumbnail;
use App\Models\Setting;

class MailBoxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Promotion_Resource');
    }

    public function index()
    {
        $search = "";
        $mailBox = MailBox::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'userId',
                        'foreignField' => '_id',
                        'as' => 'user'
                    )),
                    array('$unwind' => '$user'),
                )
            );
        });
        return view('mailBox.index', compact('mailBox'));
    }
    public function create()
    {
        $player = UserPlayer::getUser();
        $privilegeGroups = PrivilegeGroups::queryPrivilegeGroups();
        $privilegeDefaults = PrivilegeDefaults::queryPrivilegeDefaults();
        $itemDefaults = ItemDefault::queryItem();
        $monsterDefaults = MonstersDefault::queryMonstersDefault();
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();
        $news = News::queryNews();
        return view('mailBox.create', compact(
            'itemDefaults',
            'monsterDefaults',
            'privilegeDefaults',
            'privilegeGroups',
            'player',
            'malls',
            'mallFloors',
            'shops',
            'news'
        ));
    }

    public function edit(MailBox $mailBox)
    {
        $privilegeGroups = PrivilegeGroups::queryPrivilegeGroups();
        $privilegeDefaults =  PrivilegeDefaults::queryPrivilegeDefaults();
        $itemDefaults = ItemDefault::queryItem();
        $monsterDefaults = MonstersDefault::queryMonstersDefault();
        $player = UserPlayer::getUser();

        return view('mailBox.edit', compact('mailBox', 'itemDefaults', 'monsterDefaults', 'privilegeDefaults', 'privilegeGroups', 'player'));
    }

    public function update(MailBoxsRequest $request, MailBox $mailBox)
    {
        if ($mailBox->update($request->all())) {
            return redirect('mailBox/' . $mailBox->id . '/edit')->with('success', 'update mailBox Success');;
        } else {
            return redirect('mailBox/')->withInput();
        }
    }


    public function getUser()
    {
        $query = UserPlayer::raw(function ($collection) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => '_id',
                        'foreignField' => 'userId',
                        'as' => 'players'
                    )),
                    array('$unwind' => '$players'),
                )
            );
        });
        $arr = [];
        if ($query) {
            foreach ($query as $data) {
                $arr[$data->id] = $data->email;
            }
        }

        return $arr;
    }

    public function store(MailBoxsRequest $request)
    {
        if (!$request->ajax()) {
            $params = $request->all();
            $size = Setting::where([])->first();
            $imageName = "";
            $thumbnail = "";
            $imagePath = '/public/media/images/';
            if ($params['image'] != "") {
                $imageName = $params['image'];
            } else if (isset($params['filename'])) {

                $imageName = uniqid() . '.' . $request->file('filename')->getClientOriginalExtension();
                $request->file('filename')->move(base_path() . $imagePath, $imageName);
                Thumbnail::createThumbnailFile($imageName, $size->width, $size->height);
            }

            if ($params['thumbnail'] != "") {
                $thumbnail = $params['thumbnail'];
            } else if (isset($params['thumbnailName'])) {
                $thumbnail = uniqid() . '.' . $request->file('thumbnailName')->getClientOriginalExtension();
                $request->file('thumbnailName')->move(base_path() . $imagePath, $thumbnail);
                Thumbnail::createThumbnailFile($thumbnail, $size->width, $size->height);
            }
            $data = [];
            if (isset($params['userId'])) {

                for ($i = 0; $i < count($params['userId']); $i++) {

                    array_push($data, $this->qetParam($params, $params['userId'][$i], $imageName, $thumbnail));
                }
                MailBox::insert($data);
            } else {
                $user = UserPlayer::where([])->get();
                for ($i = 0; $i < count($user); $i++) {

                    array_push($data, $this->qetParam($params, $user[$i]->_id, $imageName, $thumbnail));
                }
                MailBox::insert($data);
            }

            return redirect("mailBox/create")->with('success', 'Save mailBox Success');
        }
    }

    public function qetParam($params,  $userId, $imageName, $thumbnail)
    {
        $player = Players::where(['userId' => new \MongoDB\BSON\ObjectId($userId)])->first();

        return [
            'linkListName' => $params['linkListName'],
            'userId' => new \MongoDB\BSON\ObjectId($userId),
            'playerId' => new \MongoDB\BSON\ObjectId($player->_id),
            'title' => $params['title'],
            'detail' => $params['detail'],
            'coin' => isset($params['coin']) ? $params['coin'] : 0,
            'url' => isset($params['url']) ? $params['url'] : "",
            'coinActive' => isset($params['coinActive']),
            'diamond' => isset($params['diamond']) ? $params['diamond'] : 0,
            'diamondActive' => isset($params['diamondActive']),
            'mDefaultId' => isset($params['mDefaultId']) ? new \MongoDB\BSON\ObjectId($params['mDefaultId']) : "",
            'monsterActive' => isset($params['monsterActive']),
            'itemId' => isset($params['itemId']) ? new \MongoDB\BSON\ObjectId($params['itemId']) : "",
            'itemActive' => isset($params['itemActive']),
            'imageName' => $imageName,
            'thumbnail' => $thumbnail,
            'isActive' => isset($params['isActive']),
            'createAt' => new \MongoDB\BSON\UTCDateTime(now()),
            'updateAt' => new \MongoDB\BSON\UTCDateTime(now()),
        ];
    }

    public function destroy($id)
    {

        $mailBox = MailBox::find($id);

        if ($mailBox->delete()) {
            return redirect('mailBox')->with('success', trans('mailBox/message.success.destroy'));
        } else {
            return Redirect::route('mailBox')->withInput()->with('error', trans('mailBox/message.error.delete'));
        }
    }

    public function push($id)
    {

        return redirect("mailBox/{$id}/edit")
            ->with('success', 'Send mailBox Success');
    }
}
