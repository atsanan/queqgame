<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\MonstersDefault;
use App\Models\MonstersDefaultsTypes;
use App\Http\Requests\MonstersDefaultRequest;
use App\Models\Malls;
use App\Helper\ConvertValueLanguages;
use App\Models\MonsterPlayer;
use Excel;
use App\Exports\BladeExport;

class MonstersDefaultController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Resource');
        $this->middleware('role:Report_Resource', ['only' => ['report', 'excel']]);
    }

    public function index()
    {
        $monsters = MonstersDefault::all();

        return view('monstersDefault.index', compact('monsters'));
    }

    public function create()
    {

        $monstersDefaultsTypes = $this->querymonstersDefaultsType();
        $monstersDefaults = $this->querymonstersDefault();
        $imageSlot = null;
        $imageSlot2 = null;
        $imageSlot3 = null;
        $assertModelIOS = null;
        $assertModelAndroid = null;
        $order = 0;

        return view(
            'monstersDefault.create',
            compact(
                'order',
                'imageSlot',
                'imageSlot2',
                'imageSlot3',
                'monstersDefaultsTypes',
                'monstersDefaults',
                'assertModelIOS',
                'assertModelAndroid'
            )
        );
    }

    public function store(MonstersDefaultRequest $request)
    {
        $monstersDefault = new MonstersDefault($request->all());

        $assetFile = $monstersDefault->AssetModel;
        $assetPath = '/public/media/assets/';
        $imageFile = $monstersDefault->mDefaultAssetImageSlot;
        $imageFile2 = $monstersDefault->mDefaultAssetImageSlot2;
        $imageFile3 = $monstersDefault->mDefaultAssetImageSlot3;
        $imagePath = '/public/media/images/';
        // print_r($monstersDefault);
        // die();


        $mDefaultAssertModelIOS = $monstersDefault->AssertModelIOS;
        $assetIOSPath = '/public/media/ios/monsterDefaults/';
        $mDefaultAssertModelAndroid = $monstersDefault->AssertModelAndroid;
        $assetAndroidPath = '/public/media/android/monsterDefaults/';

        if (isset($mDefaultAssertModelIOS)) {
            $generateAssertModelIOS = uniqid();
            foreach ($mDefaultAssertModelIOS as $assertModelIOS) {
                $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                if (!!$getAssetModelTypeIOS) {
                    $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                    $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                } else {
                    $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                }
            }
            $monstersDefault->mDefaultAssertModelIOS = $generateAssertModelIOS;
        } else {
            $monstersDefault->mDefaultAssertModelIOS = "";
        }

        if (isset($mDefaultAssertModelAndroid)) {
            $generateAssertModelAndroid = uniqid();
            foreach ($mDefaultAssertModelAndroid as $assertModelAndroid) {
                $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                if (!!$getAssetModelTypeAndroid) {
                    $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                } else {
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                }
            }
            $monstersDefault->mDefaultAssertModelAndroid = $generateAssertModelAndroid;
        } else {
            $monstersDefault->mDefaultAssertModelAndroid = "";
        }


        // if(isset($assetFile)){
        //     $generateName = uniqid();
        //     foreach($assetFile as $assets){
        //         $getAssetType = $assets->getClientOriginalExtension();

        //         if(!!$getAssetType){
        //         $assetName = $generateName . '.' . $getAssetType;
        //         $assets->move(base_path() . $assetPath , $assetName);
        //         }else{
        //         $assets->move(base_path() . $assetPath , $generateName);
        //         }
        //     }
        //     $monstersDefault->mDefaultAssetModel = $generateName;
        // }

        if (isset($imageFile)) {
            $imageName =  uniqid() . '.' . $request->file('mDefaultAssetImageSlot')->getClientOriginalExtension();
            $monstersDefault->mDefaultAssetImageSlot = $imageName;
            $request->file('mDefaultAssetImageSlot')->move(base_path() . $imagePath, $imageName);
        } else {
            $monstersDefault->mDefaultAssetImageSlot = "";
        }

        if (isset($imageFile2)) {
            $imageName2 =  uniqid() . '.' . $request->file('mDefaultAssetImageSlot2')->getClientOriginalExtension();
            $monstersDefault->mDefaultAssetImageSlot2 = $imageName2;
            $request->file('mDefaultAssetImageSlot2')->move(base_path() . $imagePath, $imageName2);
        } else {
            $monstersDefault->mDefaultAssetImageSlot2 = "";
        }

        if (isset($imageFile3)) {
            $imageName3 =  uniqid() . '.' . $request->file('mDefaultAssetImageSlot3')->getClientOriginalExtension();
            $monstersDefault->mDefaultAssetImageSlot3 = $imageName3;
            $request->file('mDefaultAssetImageSlot3')->move(base_path() . $imagePath, $imageName3);
        } else {
            $monstersDefault->mDefaultAssetImageSlot3 = "";
        }

        $monstersDefault->mDefaultName = ConvertValueLanguages::convertData($monstersDefault->mDefaultName);
        $monstersDefault->mDefaultTextDetail = ConvertValueLanguages::convertData($monstersDefault->mDefaultTextDetail);

        if ($monstersDefault->save()) {
            return redirect('monstersDefault/' . $monstersDefault->_id . '/edit')->with('success', 'Save Monster Success');
        } else {
            return Redirect::route('monstersDefault')->withInput()->with('error', trans('monstersDefault/message.error.create'));
        }
    }


    public function edit(MonstersDefault $monstersDefault)
    {
        $monstersDefaultsTypes = $this->querymonstersDefaultsType();
        $monstersDefaults = $this->querymonstersDefault();
        $imageSlot = $monstersDefault->mDefaultAssetImageSlot;
        $assertModelIOS = $monstersDefault->mDefaultAssertModelIOS;
        $assertModelAndroid = $monstersDefault->mDefaultAssertModelAndroid;
        $order = $monstersDefault->order;
        $imageSlot2 = $monstersDefault->mDefaultAssetImageSlot2;
        $imageSlot3 = $monstersDefault->mDefaultAssetImageSlot3;

        return view(
            'monstersDefault.edit',
            compact(
                'order',
                'monstersDefault',
                'imageSlot',
                'imageSlot2',
                'imageSlot3',
                'monstersDefaultsTypes',
                'monstersDefaults',
                'assertModelIOS',
                'assertModelAndroid'
            )
        );
    }


    public function update(MonstersDefaultRequest $request, MonstersDefault $monstersDefault)
    {

        if ($monstersDefault->update($request->all())) {
            $assetFile = $request->AssetModel;
            $assetPath = '/public/media/assets/';
            $imageFile = $request->mDefaultAssetImageSlot;
            $imageFile2 = $request->mDefaultAssetImageSlot2;
            $imageFile3 = $request->mDefaultAssetImageSlot3;
            $imagePath = '/public/media/images/';

            $mDefaultAssertModelIOS = $monstersDefault->AssertModelIOS;
            $assetIOSPath = '/public/media/ios/monsterDefaults/';
            $mDefaultAssertModelAndroid = $monstersDefault->AssertModelAndroid;
            $assetAndroidPath = '/public/media/android/monsterDefaults/';

            if (isset($mDefaultAssertModelIOS)) {
                $generateAssertModelIOS = uniqid();
                foreach ($mDefaultAssertModelIOS as $assertModelIOS) {
                    $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                    if (!!$getAssetModelTypeIOS) {
                        $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                        $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                    } else {
                        $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                    }
                }
                $monstersDefault->update(['mDefaultAssertModelIOS' => $generateAssertModelIOS]);
            }

            if (isset($mDefaultAssertModelAndroid)) {

                $generateAssertModelAndroid = uniqid();
                foreach ($mDefaultAssertModelAndroid as $assertModelAndroid) {
                    $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                    if (!!$getAssetModelTypeAndroid) {
                        $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                    } else {
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                    }
                }
                $monstersDefault->update(['mDefaultAssertModelAndroid' => $generateAssertModelAndroid]);
            }

            if (isset($assetFile)) {
                $generateName = uniqid();
                foreach ($assetFile as $assets) {
                    $getAssetType = $assets->getClientOriginalExtension();

                    if (!!$getAssetType) {
                        $assetName = $generateName . '.' . $getAssetType;
                        $assets->move(base_path() . $assetPath, $assetName);
                    } else {
                        $assets->move(base_path() . $assetPath, $generateName);
                    }
                }
                $monstersDefault->update(['mDefaultAssetModel' => $generateName]);
            }

            if (isset($imageFile)) {
                $imageName =  uniqid() . '.' . $request->file('mDefaultAssetImageSlot')->getClientOriginalExtension();
                $monstersDefault->update(['mDefaultAssetImageSlot' => $imageName]);
                $request->file('mDefaultAssetImageSlot')->move(base_path() . $imagePath, $imageName);
            }

            if (isset($imageFile2)) {
                $imageName2 =  uniqid() . '.' . $request->file('mDefaultAssetImageSlot2')->getClientOriginalExtension();
                $monstersDefault->update(['mDefaultAssetImageSlot2' => $imageName2]);
                $request->file('mDefaultAssetImageSlot2')->move(base_path() . $imagePath, $imageName2);
            }

            if (isset($imageFile3)) {
                $imageName3 =  uniqid() . '.' . $request->file('mDefaultAssetImageSlot3')->getClientOriginalExtension();
                $monstersDefault->update(['mDefaultAssetImageSlot3' => $imageName3]);
                $request->file('mDefaultAssetImageSlot3')->move(base_path() . $imagePath, $imageName3);
            }

            $monstersDefault->update(['mDefaultName' => ConvertValueLanguages::convertData($monstersDefault->mDefaultName)]);
            $monstersDefault->update(['mDefaultTextDetail' => ConvertValueLanguages::convertData($monstersDefault->mDefaultTextDetail)]);


            return redirect('monstersDefault/' . $monstersDefault->_id . '/edit')->with('success', 'Edit Monster Success');
        } else {
            return Redirect::route('monstersDefault')->withInput()->with('error', trans('monstersDefault/message.error.update'));
        }
    }

    public function destroy($id)
    {

        $monstersDefault = MonstersDefault::find($id);

        if ($monstersDefault) {
            $monstersDefault->delete();
            return redirect('monstersDefault')->with('success', trans('monstersDefault/message.success.destroy'));
        } else {
            return redirect('monstersDefault')->with('error', trans('monstersDefault/message.error.delete'));
        }
    }

    public function data()
    {
        $models = MonstersDefault::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (MonstersDefault $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function querymonstersDefaultsType()
    {
        $querymonstersDefaultsTypes = MonstersDefaultsTypes::where([])->get();
        $monstersDefaultsTypes = [];
        foreach ($querymonstersDefaultsTypes as $type) {
            $monstersDefaultsTypes[$type->id] = $type->monstersTypeName['eng'];
        }
        return $monstersDefaultsTypes;
    }

    public function querymonstersDefault()
    {
        $querymonstersDefaults = MonstersDefault::where([])->get();
        $monstersDefaults = [];
        foreach ($querymonstersDefaults as $type) {
            $monstersDefaults[$type->id] = $type->mDefaultName['eng'];
        }
        return $monstersDefaults;
    }

    public function report()
    {
        $search = "";
        $monsterDefaults = MonsterPlayer::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'monsterDefaults',
                        'localField' => 'mDefaultId',
                        'foreignField' => '_id',
                        'as' => 'monsterDefaults'
                    )),
                    array('$unwind' => '$monsterDefaults'),
                    array(
                        '$group' => array(
                            '_id' => '$mDefaultId', 'sum' => array('$sum' => 1),
                            'monsterDefaults' => array('$addToSet' => '$monsterDefaults')
                        ),
                    ),
                )
            );
        });

        $total = 0;
        foreach ($monsterDefaults as $row) {
            $total += $row->sum;
        }

        return view('monstersDefault.report', compact('monsterDefaults', 'total'));
    }

    public function excel()
    {
        $search = "";
        $monsterDefaults = MonsterPlayer::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'monsterDefaults',
                        'localField' => 'mDefaultId',
                        'foreignField' => '_id',
                        'as' => 'monsterDefaults'
                    )),
                    array('$unwind' => '$monsterDefaults'),
                    array(
                        '$group' => array(
                            '_id' => '$mDefaultId', 'sum' => array('$sum' => 1),
                            'monsterDefaults' => array('$addToSet' => '$monsterDefaults')
                        ),
                    ),
                )
            );
        });

        $total = 0;
        foreach ($monsterDefaults as $row) {
            $total += $row->sum;
        }

        $array = [];
        if (count($monsterDefaults) > 0) {
            foreach ($monsterDefaults as $row) {
                array_push(
                    $array,
                    array(
                        "mDefaultName" => $row->monsterDefaults[0]->mDefaultName->eng,
                        "sum" => $row->sum,
                        "percent" => round(($row->sum / $total) * 100, 2)
                    )
                );
            }
        }

        //print_r($array);

        return Excel::download(new BladeExport($array), 'monsterDefault.xlsx');
    }


    public function player($id)
    {
        $search = "";
        $monsterPlayer = MonsterPlayer::raw(function ($collection) use ($id) {
            return $collection->aggregate(
                array(

                    array('$match' => array('mDefaultId' => new \MongoDB\BSON\ObjectId($id))),
                    array(
                        '$group' => array(
                            '_id' => '$playerId', 'sum' => array('$sum' => 1),
                            'player' => array('$addToSet' => '$player')
                        ),
                    ),

                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => '_id',
                        'foreignField' => '_id',
                        'as' => 'players'
                    )),
                    array('$unwind' => '$players'),
                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'players.userId',
                        'foreignField' => '_id',
                        'as' => 'user'
                    )),

                    array('$unwind' => '$user'),

                    array('$sort' => array('sum' => -1)),
                )
            );
        });
        $sum = array_sum(array_map(function ($var) {
            return $var['sum'];
        }, $monsterPlayer->toArray()));
        return view('monstersDefault.player', compact('monsterPlayer', 'sum'));
    }
}
