<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Players;
use App\Models\TeamPlayers;
use App\Models\Costumes;
use App\Http\Requests\PlayersRequest;
use App\Models\MonsterPlayer;
use App\Models\ItemPlayer;
use App\Models\PlayerLogs;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class PlayersController extends Controller
{

    public $teamPlayers = ["Shopping", "Entertainment", "Eating"];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Player_Resource');
    }

    public function index()
    {

        $search = Input::get('search');
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);
        $data = Players::with(['costumes']);

        if (isset($search)) {
            $costume = Costumes::where('costumeName.eng', 'like', "%$search%")->get();
            $costumeId =  $costume->map(function ($item) {

                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $data = $data->where("playerName", "like", "%$search%")
                ->orWhere("gender", "like", "%$search%")
                ->orWhereIn('costumeSelectId', $costumeId);
        }

        $data = $data->get()->toArray();

        if (isset($field)) {
            if ($field == "playerName") {
                $array = array_map(function ($element) {
                    return $element['playerName'];
                }, $data);
            } else  if ($field == "gender") {
                $array = array_map(function ($element) {
                    if (isset($element['gender'])) {
                        return $element['gender'];
                    }
                }, $data);
            } else  if ($field == "coin") {
                $array = array_map(function ($element) {
                    if (isset($element['coin'])) {
                        return $element['coin'];
                    }
                }, $data);
            } else  if ($field == "diamond") {
                $array = array_map(function ($element) {
                    if (isset($element['diamond'])) {
                        return $element['diamond'];
                    }
                }, $data);
            } else  if ($field == "costume") {
                $array = array_map(function ($element) {
                    if (isset($element['costumes']['costumeName']['eng'])) {
                        return $element['costumes']['costumeName']['eng'];
                    }
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['playerName'];
            }, $data);
        }

        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        // if (isset($sort)) {
        //     $data = $data->orderBy($field, $sort);
        // } else {
        //     $data = $data->orderBy("playerName", "asc");
        // }
        array_multisort($array, $sortData, $data);
        $players  = new LengthAwarePaginator(array_slice($data, $offset, 50), count($data), 50);
        $players->setPath('/players');
        return view('players.index', compact('players', 'field', 'search', 'sort'));
    }

    public function create()
    {
        $teamPlayers = $this->queryTeamPlayer();
        $costumes = $this->queryCostume();

        return view('players.create', compace('teamPlayers', 'costumes'));
    }

    public function store(PlayersRequest $request)
    {
        $player = new Players($request->all());

        if ($player->save()) {
            return redirect('players')->with('success', trans('players/message.success.create'));
        } else {
            return Redirect::route('players')->withInput()->with('error', trans('players/message.error.create'));
        }
    }

    public function edit(Players $player)
    {

        $costumes = $this->queryCostume();
        $teamPlayers = $this->teamPlayers;
        $monsterDefaults = MonsterPlayer::raw(function ($collection)  use ($player) {

            $query = array(
                array('$lookup' => array(
                    'from' => 'monsterDefaults',
                    'localField' => 'mDefaultId',
                    'foreignField' => '_id',
                    'as' => 'monsterDefaults'
                )),
                array('$unwind' => '$monsterDefaults'),
                array('$lookup' => array(
                    'from' => 'joinChannels',
                    'localField' => '_id',
                    'foreignField' => 'mPlayerId',
                    'as' => 'joinChannels'
                )),
                array('$unwind' => '$joinChannels'),

                array('$lookup' => array(
                    'from' => 'channels',
                    'localField' => 'joinChannels.channelId',
                    'foreignField' => '_id',
                    'as' => 'channels'
                )),
                array('$unwind' => '$channels'),

                array('$lookup' => array(
                    'from' => 'shops',
                    'localField' => 'channels.shopId',
                    'foreignField' => '_id',
                    'as' => 'shops'
                )),
                array('$unwind' => '$shops'),

                array('$lookup' => array(
                    'from' => 'players',
                    'localField' => 'playerId',
                    'foreignField' => '_id',
                    'as' => 'player'
                )),
                array('$unwind' => '$player'),

                array('$lookup' => array(
                    'from' => 'users',
                    'localField' => 'player.userId',
                    'foreignField' => '_id',
                    'as' => 'users'
                )),
                array('$unwind' => '$users'),


                array('$lookup' => array(
                    'from' => 'mallFloors',
                    'localField' => 'shops.mallFloorId',
                    'foreignField' => '_id',
                    'as' => 'mallFloor'
                )),
                array('$unwind' => '$mallFloor'),


                array('$lookup' => array(
                    'from' => 'malls',
                    'localField' => 'mallFloor.mallId',
                    'foreignField' => '_id',
                    'as' => 'mall'
                )),
                array('$unwind' => '$mall'),
                array('$match' => array('playerId' => new \MongoDB\BSON\ObjectId($player->_id))),

            );
            return $collection->aggregate($query);
        });
        $monsterInbug = MonsterPlayer::raw(function ($collection)  use ($player) {
            $query = array(
                array('$lookup' => array(
                    'from' => 'monsterDefaults',
                    'localField' => 'mDefaultId',
                    'foreignField' => '_id',
                    'as' => 'monsterDefaults'
                )),
                array('$unwind' => '$monsterDefaults'),
                array('$lookup' => array(
                    'from' => 'joinChannels',
                    'localField' => '_id',
                    'foreignField' => 'mPlayerId',
                    'as' => 'joinChannels'
                )),
                array('$match' => array(
                    'joinChannels' => array('$size' => 0),
                    'playerId' => new \MongoDB\BSON\ObjectId($player->_id)
                ))
            );
            return $collection->aggregate($query);
        });
        $itemPlayer = ItemPlayer::raw(
            function ($collection) use ($player) {
                $query = array(
                    array('$lookup' => array(
                        'from' => 'itemDefaults',
                        'localField' => 'itemId',
                        'foreignField' => '_id',
                        'as' => 'item'
                    )),

                    array('$unwind' => '$item'),
                    array('$match' => array('playerId' => new \MongoDB\BSON\ObjectId($player->_id))),
                );
                return $collection->aggregate($query);
            }
        );

        $playerLogs = PlayerLogs::where('playerId', new \MongoDB\BSON\ObjectId($player->_id))->get();

        return view('players.edit', compact(
            'player',
            'teamPlayers',
            'costumes',
            'monsterDefaults',
            'itemPlayer',
            'monsterInbug',
            'playerLogs'
        ));
    }

    public function update(PlayersRequest $request, Players $player)
    {

        if ($player->update($request->all())) {

            return redirect('players')->with('success', trans('players/message.success.update'));
        } else {
            return Redirect::route('players')->withInput()->with('error', trans('players/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $player = Players::find($id);

        if ($player->delete()) {
            return redirect('players')->with('success', trans('players/message.success.destroy'));
        } else {
            return Redirect::route('players')->withInput()->with('error', trans('players/message.error.delete'));
        }
    }

    public function data()
    {
        $models = Players::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (Players $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function queryTeamPlayer()
    {

        $queryTeamPlayers = TeamPlayers::where([])->get();

        $teamPlayers = [];
        foreach ($queryTeamPlayers as $type) {
            $teamPlayers[$type->id] = $type->teamPlayerName['eng'];
        }

        return $teamPlayers;
    }

    public function queryCostume()
    {

        $queryCostumes = Costumes::where([])->get();

        $costumes = [];
        foreach ($queryCostumes as $type) {
            $costumes[$type->id] = $type->costumeName['eng'];
        }

        return $costumes;
    }
}
