<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\DigitalBoardAsserts;
use App\Models\Costumes;
use App\Http\Requests\DigitalBoardAssertsRequest;
use App\Helper\ConvertValueLanguages;

class DigitalBoardAssertsController extends Controller
{
    private $digitalType = ["2D", "3D"];
    private $assetIOSPath = '/public/media/ios/digitalBoardAssert/';
    private $assetAndroidPath = '/public/media/android/digitalBoardAssert/';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:DigitalBoard_Resource');
    }

    public function index()
    {
        $digitalBoardAsserts = DigitalBoardAsserts::all();
        return view('digitalBoardAsserts.index', compact('digitalBoardAsserts'));
    }

    public function create()
    {
        $modelInMapVersion = 1;
        $modelStageVersion = 1;
        $digitalType = $this->digitalType;
        return view('digitalBoardAsserts.create', compact('modelInMapVersion', 'digitalType', 'modelStageVersion'));
    }

    public function store(DigitalBoardAssertsRequest $request)
    {


        $digitalBoardAsserts = new DigitalBoardAsserts($request->all());

        $fileNameImageSlot1 = $request->fileNameImageSlot1;
        $fileNameImageSlot2 = $request->fileNameImageSlot2;
        $fileModelInMapIOS = $request->fileModelInMapIOS;
        $fileModelInMapAndroid = $request->fileModelInMapAndroid;
        $fileModelStageIOS = $request->fileModelStageIOS;
        $fileModelStageAndroid = $request->fileModelStageAndroid;

        if (isset($fileNameImageSlot1)) {
            $digitalBoardAsserts->imageSlot1 =  $this->uploadImage($request, "fileNameImageSlot1");
        }
        if (isset($fileNameImageSlot2)) {
            $digitalBoardAsserts->imageSlot2 = $this->uploadImage($request, "fileNameImageSlot2");
        }
        if (isset($fileModelInMapIOS)) {
            $digitalBoardAsserts->modelInMapIos = $this->uploadAsserts($fileModelInMapIOS, $this->assetIOSPath . "modelInMap/");
        }
        if (isset($fileModelInMapAndroid)) {
            $digitalBoardAsserts->modelInMapAndroid = $this->uploadAsserts($fileModelInMapAndroid, $this->assetAndroidPath . "modelInMap/");
        }

        if (isset($fileModelStageIOS)) {
            $digitalBoardAsserts->modelStageIos = $this->uploadAsserts($fileModelStageIOS, $this->assetIOSPath . "stage/");
        }
        $digitalBoardAsserts->title = ConvertValueLanguages::convertData($digitalBoardAsserts->title);
        $digitalBoardAsserts->body = ConvertValueLanguages::convertData($digitalBoardAsserts->body);

        if (isset($fileModelStageAndroid)) {
            $digitalBoardAsserts->modelInStageAndroid = $this->uploadAsserts($fileModelStageAndroid, $this->assetAndroidPath . "stage/");
        }

        if ($digitalBoardAsserts->save()) {
            return redirect('digitalBoardAsserts')->with('success', trans('digitalBoardAsserts/message.success.create'));
        } else {
            return Redirect::route('digitalBoardAsserts')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }

    public function uploadAsserts($fileModel, $path)
    {
        $generateAssertModel = uniqid();
        foreach ($fileModel as $assertModel) {
            $getAssetModelType = $assertModel->getClientOriginalExtension();
            if (!!$getAssetModelType) {
                $assetModelName = $generateAssertModel . '.' . $getAssetModelType;
                $assertModel->move(base_path() . $path, $assetModelName);
            } else {
                $assertModel->move(base_path() . $path, $generateAssertModel);
            }
        }

        return $generateAssertModel;
    }

    public function uploadImage($request, $field)
    {
        $imageSlot = uniqid() . '.' . $request->file($field)->getClientOriginalExtension();
        $request->file($field)->move(base_path() . parent::$imagePath, $imageSlot);
        return $imageSlot;
    }

    public function edit($id)
    {
        $digitalBoardAsserts = DigitalBoardAsserts::find($id);

        $modelInMapVersion = $digitalBoardAsserts->modelInMapVersion;
        $modelStageVersion = $digitalBoardAsserts->modelStageVersion;
        $digitalType = $this->digitalType;
        return view('digitalBoardAsserts.edit', compact(
            'modelInMapVersion',
            'digitalType',
            'modelStageVersion',
            'digitalBoardAsserts'
        ));
    }

    public function update($id, DigitalBoardAssertsRequest $request)
    {
        $digitalBoardAsserts = DigitalBoardAsserts::find($id);
        $fileNameImageSlot1 = $request->fileNameImageSlot1;
        $fileNameImageSlot2 = $request->fileNameImageSlot2;
        $fileModelInMapIOS = $request->fileModelInMapIOS;
        $fileModelInMapAndroid = $request->fileModelInMapAndroid;
        $fileModelStageIOS = $request->fileModelStageIOS;
        $fileModelStageAndroid = $request->fileModelStageAndroid;

        if ($digitalBoardAsserts->update($request->all())) {
            if (isset($fileNameImageSlot1)) {
                $digitalBoardAsserts->update(['imageSlot1' =>  $this->uploadImage($request, "fileNameImageSlot1")]);
            }
            if (isset($fileNameImageSlot2)) {
                $digitalBoardAsserts->update(['imageSlot2' => $this->uploadImage($request, "fileNameImageSlot2")]);
            }
            if (isset($fileModelInMapIOS)) {
                $digitalBoardAsserts->update(['modelInMapIos' => $this->uploadAsserts($fileModelInMapIOS, $this->assetIOSPath . "modelInMap/")]);
            }
            if (isset($fileModelInMapAndroid)) {
                $digitalBoardAsserts->update(['modelInMapAndroid' => $this->uploadAsserts($fileModelInMapAndroid, $this->assetAndroidPath . "modelInMap/")]);
            }

            if (isset($fileModelStageIOS)) {
                $digitalBoardAsserts->update(['modelStageIos' => $this->uploadAsserts($fileModelStageIOS, $this->assetIOSPath . "stage/")]);
            }

            if (isset($fileModelStageAndroid)) {
                $digitalBoardAsserts->update(['modelStageAndroid' => $this->uploadAsserts($fileModelStageAndroid, $this->assetAndroidPath . "stage/")]);
            }

            $digitalBoardAsserts->update(['title' => ConvertValueLanguages::convertData($digitalBoardAsserts->title)]);
            $digitalBoardAsserts->update(['body' => ConvertValueLanguages::convertData($digitalBoardAsserts->body)]);


            return redirect('digitalBoardAsserts')->with('success', trans('digitalBoardAsserts/message.success.create'));
        } else {
            return Redirect::route('digitalBoardAsserts')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }
    public function destroy($id)
    {
        $digitalBoardAsserts = DigitalBoardAsserts::find($id);

        if ($digitalBoardAsserts) {
            $digitalBoardAsserts->delete();
            return redirect('digitalBoardAsserts')->with('success', trans('digitalBoardAsserts/message.success.create'));
        } else {
            return redirect('digitalBoardAsserts')->with('error', trans('costumes/message.error.create'));
        }
    }
}
