<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ItemCategorys;
use App\Http\Requests\ItemCategorysRequest;

class ItemCategorysController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Resource');
    }

    public function index()
    {
        $itemCategorys = ItemCategorys::all();
        return view('itemCategorys.index', compact('itemCategorys'));
    }

    public function create()
    {
        return view('itemCategorys.create');
    }

    public function store(ItemCategorysRequest $request)
    {
        $itemCategory = new ItemCategorys($request->all());

        if ($itemCategory->save()) {
            return redirect('itemCategorys')->with('success', trans('itemCategorys/message.success.create'));
        } else {
            return Redirect::route('itemCategorys')->withInput()->with('error', trans('itemCategorys/message.error.create'));
        }
    }

    public function edit(ItemCategorys $itemCategory)
    {
        // echo $itemCategory;
        return view('itemCategorys.edit', compact('itemCategory'));
    }

    public function update(ItemCategorysRequest $request, ItemCategorys $itemCategory)
    {

        if ($itemCategory->update($request->all())) {
            return redirect('itemCategorys')->with('success', trans('itemCategorys/message.success.update'));
        } else {
            return Redirect::route('itemCategorys')->withInput()->with('error', trans('itemCategorys/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $itemCategory = ItemCategorys::find($id);

        if ($itemCategory) {
            $itemCategory->delete();
            return redirect('itemCategorys')->with('success', trans('itemCategorys/message.success.destroy'));
        } else {
            return redirect('itemCategorys')->with('error', trans('itemCategorys/message.error.delete'));
        }
    }

    public function data()
    {
        $models = ItemCategorys::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (ItemCategorys $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
