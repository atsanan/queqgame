<?php

namespace App\Http\Controllers;

use App\Models\PartnerTypes;
use App\Http\Requests\PartnerTypesRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class PartnerTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');
        if ($page == null) {
            $page = 1;
        }
        $field = Input::get('field');
        $data = PartnerTypes::with([]);

        if (isset($search)) {
            $data = $data->where('patherTypeName', 'like', "%$search%");
        }
        $data = $data->get()->toArray();
        $offset = ($page - 1) * 50;

        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }

        if (isset($field)) {
            if ($field == "partnerTypeName") {
                $array = array_map(function ($element) {
                    return $element['partnerTypeName'];
                }, $data);
            } else {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['partnerTypeName'];
            }, $data);
        }
        array_multisort($array, $sortData, $data);
        $partnerTypes  =  new LengthAwarePaginator(array_slice($data,  $offset, 50), count($data), 50);
        $partnerTypes->setPath('/partnerTypes');
        return view('partnerTypes.index', compact('partnerTypes', 'field', 'sort', 'search'));
    }

    public function create()
    {
        return view('partnerTypes.create');
    }


    public function store(PartnerTypesRequest $request)
    {
        $partnerTypes = new PartnerTypes($request->all());
        if ($partnerTypes->save()) {
            return redirect("partnerTypes/{$partnerTypes->id}/edit")->with('success', 'Success Save Data');
        } else {
            return redirect("partnerTypes/create")->withInput()->with('error', 'Error Create');
        }
    }

    public function edit(PartnerTypes $partnerType)
    {

        return view('partnerTypes.edit', compact('partnerType'));
    }

    public function update(PartnerTypesRequest $request, PartnerTypes $partnerType)
    {
        if ($partnerType->update($request->all())) {
            return redirect("partnerTypes/{$partnerType->id}/edit")->with('success', 'Success Update Data');
        } else {

            return redirect("partnerTypes/{$partnerType->id}/edit")->with('error', 'Error Update Data');
        }
    }


    public function destroy($id)
    {
        $partnerType = PartnerTypes::find($id);

        if ($partnerType) {
            $partnerType->delete();
            return redirect("partnerTypes/")->with('success', 'Success Delete Data');
        } else {
            return redirect("partnerTypes/")->with('error', trans('success', 'Error Delete Data'));
        }
    }
}
