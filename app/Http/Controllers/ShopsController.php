<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\Shops;
use App\Models\ShopAds;
use App\Models\ShopAsserts;
use App\Models\ShopCategories;
use App\Models\PrivilegeShops;
use App\Models\News;
use App\Http\Requests\ShopsRequest;
use Illuminate\Support\Facades\Input;
use App\Helper\ConvertValueLanguages;
use App\Models\UserPlayer;
use Illuminate\Pagination\LengthAwarePaginator;
use Excel;
use App\Exports\BladeExport;

class ShopsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['search', 'searchByPlayer', 'searchByPlayerFixBug', 'shopCode', 'shopDetail']);
        $this->middleware('role:Mall_Resource')->except(['search', 'searchByPlayer', 'searchByPlayerFixBug', 'shopCode', 'shopDetail']);
    }
    public $mainTypes = ['Eating' => 'Eating', 'Shopping' => 'Shopping', 'Entertain' => 'Entertain', 'BankAndFinance' => 'BankAndFinance', 'Other' => 'Other'];
    public function index()
    {
        $search = Input::get('search');
        $data = Shops::with(['mallFloor.malls']);
        $sort = Input::get('sort');
        $page = Input::get('page');
        $field = Input::get('field');
        $malls = Malls::queryMall();
        $mallId = Input::get('mallId');
        $_mallId = explode(",", $mallId);

        $whereMallId = [];
        if (isset($mallId)) {
            foreach ($_mallId as $value) {
                $whereMallId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryMallFloors = MallFloor::whereIn('mallId', $whereMallId)->get();
        } else {
            $queryMallFloors = MallFloor::get();
        }

        $_findMallFloorId = [];
        $mallFloors = [];
        if ($queryMallFloors) {
            foreach ($queryMallFloors as $mallFloor) {
                $mallFloors[$mallFloor->id] = $mallFloor->mallFloorName['eng'];
                $_findMallFloorId[] = new \MongoDB\BSON\ObjectId($mallFloor->id);
            }

            if (isset($mallId)) {
                $data = $data->whereIn('mallFloorId', $_findMallFloorId);
            }
        }


        $mallFloorId = Input::get('mallFloorId');
        $_mallFloorId = explode(",", $mallFloorId);
        $pushMallFloorId = [];
        if (isset($mallFloorId)) {

            foreach ($_mallFloorId as $value) {
                $pushMallFloorId[] = new \MongoDB\BSON\ObjectId($value);
            }

            $data = $data->whereIn('mallFloorId', $pushMallFloorId);
        }


        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;

        if (isset($search)) {


            $data = $data->where("shopName.eng", "like", "%$search%");
        }

        $result = $data->get()->toArray();

        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }


        if (isset($field)) {
            if ($field == "shopName.eng") {
                $array = array_map(function ($element) {
                    if (isset($element['shopName']['eng'])) {
                        return $element['shopName']['eng'];
                    }
                }, $result);
            } else if ($field == "isSponser") {
                $array = array_map(function ($element) {
                    if (isset($element['isSponser'])) {
                        return $element['isSponser'];
                    }
                }, $result);
            } else if ($field == "isActive") {
                $array = array_map(function ($element) {
                    if (isset($element['isActive'])) {
                        return $element['isActive'];
                    }
                }, $result);
            } else if ($field == "malls") {
                $array = array_map(function ($element) {
                    if (isset($element['mall_floor']['malls']['mallName']['eng'])) {
                        return $element['mall_floor']['malls']['mallName']['eng'];
                    }
                }, $result);
            }
        } else {
            $array = array_map(function ($element) {
                if (isset($element['mall_floor']['malls']['mallName']['eng'])) {
                    return $element['mall_floor']['malls']['mallName']['eng'];
                }
            }, $result);
        }

        array_multisort($array, $sortData, $result);
        $shops  = new LengthAwarePaginator(array_slice($result, $offset, 50), count($result), 50);

        $shops->setPath('/shops');
        return view('shops.index', compact(
            'shops',
            'search',
            'field',
            'sort',
            'malls',
            'mallFloors',
            'mallId',
            '_mallId',
            'mallFloorId',
            '_mallFloorId'
        ));
    }

    public function create()
    {
        $mallFloorId = Input::get('mallFloorId');
        $mallFloors = $this->queryMallFloors();
        $shopCategories = ShopCategories::queryShopCategories();
        $shopAsserts = ShopAsserts::queryShopAsserts();
        $shopAdses = ShopAds::queryShopAds();
        $shopAds = null;
        $shopAssert = null;
        $shopCategory = null;
        $fileNameLogos1 = null;
        $fileNameLogos2 = null;
        $order = 0;
        $mainTypes = $this->mainTypes;

        return view('shops.create', compact(
            'mallFloorId',
            'order',
            'mallFloors',
            'shopCategories',
            'shopCategory',
            'shopAsserts',
            'shopAssert',
            'shopAdses',
            'shopAds',
            'fileNameLogos1',
            'fileNameLogos2',
            'mainTypes'
        ));
    }

    public function store(ShopsRequest $request)
    {
        $shop = new Shops($request->all());


        $imageFile1 = $shop->filenameLogo1;
        $imagePath1 = '/public/media/images/';
        $imageFile2 = $shop->filenameLogo2;
        $imagePath2 = '/public/media/images/';
        if (empty($request->mainTypes)) {
            $shop->mainTypes = [];
        }

        if (empty($request->shopCategoryId)) {
            $shop->shopCategoryId = [];
        }

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
            $shop->filenameLogo1 = $imageName1;
            $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
            $shop->filenameLogo2 = $imageName2;
            $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
        }

        $shop->shopName = ConvertValueLanguages::convertData($shop->shopName);
        $shop->shopDetail = ConvertValueLanguages::convertData($shop->shopDetail);
        $shop->address = ConvertValueLanguages::convertData($shop->address);
        $shop->workingTime = ConvertValueLanguages::convertData($shop->workingTime);
        $shop->location = ['coordinates' => [(float) $shop->longitude, (float) $shop->latitude], 'type' => 'Point'];
        if ($shop->save()) {
            return redirect('shops/' . $shop->id . '/edit')->with('success', 'Save Shop Success');
        } else {
            return Redirect::route('shops/' . $shop->id . '/edit')->withInput()->with('error', trans('shops/message.error.create'));
        }
    }


    public function edit(Shops $shop)
    {
        $mallFloorId = $shop->mallFloorId;
        $mallFloors = $this->queryMallFloors();
        $shopCategories = ShopCategories::queryShopCategories();
        $shopAsserts = ShopAsserts::queryShopAsserts();
        $shopAdses = ShopAds::queryShopAds();
        $shopAds = $shop->shopAdsId;
        $shopCategory = $shop->shopCategoryId;
        $shopAssert = $shop->shopAssertId;
        $fileNameLogos1 = $shop->filenameLogo1;
        $fileNameLogos2 = $shop->filenameLogo2;
        $order = $shop->order;
        $field = Input::get('field');
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');
        $data = PrivilegeShops::with(['shops', 'privilegeDefaults']);
        $data = $data->where('shopId', new \MongoDB\BSON\ObjectId($shop->id));
        $data = $data->get()->toArray();
        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);

        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = true;
                }
            } else {
                $data[$i]['isExpired'] = true;
            }
        }
        $index = array_keys(array_column($data, 'isExpired'), false);
        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        if (isset($field)) {
            if ($field == "privilegeDefault") {
                $array = array_map(function ($element) {
                    return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
                }, $data);
            } else  if ($field == "shops") {
                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {
                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else  if ($field == "privilegeShopTitles") {
                $array = array_map(function ($element) {
                    return $element['privilegeShopTitles']['eng'];
                }, $data);
            } else  if ($field == "isReachDateTime") {
                $array = array_map(function ($element) {
                    return $element['isReachDateTime'];
                }, $data);
            } else  if ($field == "isGoldenMinutes") {
                $array = array_map(function ($element) {
                    return $element['isGoldenMinutes'];
                }, $data);
            } else  if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            }
            if ($field != "isExpired") {
                if (count($index) > 0) {
                    foreach ($index as $i) {
                        unset($data[$i]);
                        unset($array[$i]);
                    }
                }
            }
        } else {
            $array = array_map(function ($element) {
                return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
            }, $data);
            if (count($index) > 0) {
                foreach ($index as $i) {
                    unset($data[$i]);
                    unset($array[$i]);
                }
            }
        }


        array_multisort($array, $sortData, $data);
        $privilegeShops  = new LengthAwarePaginator(array_slice($data, $offset, 50), count($data), 50);
        $privilegeShops->setPath("/shops/{$shop->id}/edit");


        $fieldNews = Input::get('fieldNews');
        $sortNews = Input::get('sortNews');
        $dataNews = News::with(['shops', 'newsTypes']);
        $dataNews = $dataNews->where('shopId', new \MongoDB\BSON\ObjectId($shop->id));
        $dataNews = $dataNews->get()->toArray();

        for ($i = 0; $i < count($dataNews); $i++) {
            if (isset($dataNews[$i]['isReachDateTime'])) {
                if ($dataNews[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($dataNews[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $dataNews[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $dataNews[$i]['isExpired'] = true;
                }
            } else {
                $dataNews[$i]['isExpired'] = true;
            }
        }

        $indexNews = array_keys(array_column($dataNews, 'isExpired'), false);

        if (isset($sortNews)) {
            if ($sortNews == "desc") {
                $sortDataNews = SORT_DESC;
            } else {
                $sortDataNews = SORT_ASC;
            }
        } else {
            $sortDataNews = SORT_ASC;
        }

        if (isset($fieldNews)) {
            if ($fieldNews == "newsName") {
                $arrayNews = array_map(function ($element) {
                    if (isset($element['newsName']['eng'])) {

                        return $element['newsName']['eng'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "shops") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {

                        return $element['shops']['shopName']['eng'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "newsType") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['news_types']['newsTypeName']['eng'])) {

                        return $element['news_types']['newsTypeName']['eng'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "gender") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['gender'])) {

                        return $element['gender'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "minAge") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['minAge'])) {

                        return $element['minAge'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "maxAge") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['maxAge'])) {

                        return $element['maxAge'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "isActive") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['newsActive'])) {

                        return $element['newsActive'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "isReachDateTime") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['isReachDateTime'])) {
                        return $element['isReachDateTime'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "isExpired") {
                $arrayNews = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $dataNews);
            }

            if ($fieldNews != "isExpired") {
                if (count($index) > 0) {
                    foreach ($indexNews as $i) {
                        unset($dataNews[$i]);
                        unset($arrayNews[$i]);
                    }
                }
            }
        } else {
            $arrayNews = array_map(function ($element) {
                return $element['newsName']['eng'];
            }, $dataNews);

            if (count($indexNews) > 0) {
                foreach ($indexNews as $i) {
                    unset($dataNews[$i]);
                    unset($arrayNews[$i]);
                }
            }
        }



        array_multisort($arrayNews, $sortDataNews, $dataNews);

        $news  = $dataNews;
        $mainTypes = $this->mainTypes;

        $shop->longitude = $shop->location['coordinates'][0];
        $shop->latitude = $shop->location['coordinates'][1];
        return view('shops.edit', compact(
            'shop',
            'order',
            'privilegeShops',
            'mallFloorId',
            'mallFloors',
            'shopCategories',
            'shopCategory',
            'shopAsserts',
            'shopAssert',
            'shopAdses',
            'shopAds',
            'fileNameLogos1',
            'fileNameLogos2',
            'news',
            'mainTypes',
            'search',
            'field',
            'sort',
            'fieldNews',
            'sortNews'
        ));
    }

    public function excelPrivilegeShops()
    {
        $id = Input::get('id');
        $field = Input::get('field');
        $search = Input::get('search');
        $sort = Input::get('sort');
        $data = PrivilegeShops::with(['shops', 'privilegeDefaults']);
        $data = $data->where('shopId', new \MongoDB\BSON\ObjectId($id));
        if (isset($search)) {
            $data = $data->where('privilegeShopTitles.eng', 'like', "%$search%");
            $shop = Shops::where('shopName.eng', 'like', "%$search%")->get();
            $shopId =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('shopId', $shopId);

            $privilegeDefaults = PrivilegeDefaults::where('privilegeDefaultNames.eng', 'like', "%$search%")->get();
            $privilegeDefaultId =  $privilegeDefaults->map(function ($item) {

                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('privilegeDefaultId', $privilegeDefaultId);
        }
        $data = $data->get()->toArray();

        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);


        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = !($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = false;
                }
            } else {
                $data[$i]['isExpired'] = false;
            }
        }

        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        $field = Input::get('field');
        if (isset($field)) {
            if ($field == "privilegeDefault") {
                $array = array_map(function ($element) {
                    return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
                }, $data);
            } else  if ($field == "shops") {
                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {
                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else  if ($field == "privilegeShopTitles") {
                $array = array_map(function ($element) {
                    return $element['privilegeShopTitles']['eng'];
                }, $data);
            } else  if ($field == "isReachDateTime") {
                $array = array_map(function ($element) {
                    return $element['isReachDateTime'];
                }, $data);
            } else  if ($field == "isGoldenMinutes") {
                $array = array_map(function ($element) {
                    return $element['isGoldenMinutes'];
                }, $data);
            } else  if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
            }, $data);
        }

        array_multisort($array, $sortData, $data);
        $array = [];
        if (count($data) > 0) {
            for ($i = 0; $i < count($data); $i++) {

                array_push(
                    $array,
                    array(
                        'PrivilegeDefaults' =>
                        isset($data[$i]['privilege_defaults']['privilegeDefaultNames']['eng']) ?
                            $data[$i]['privilege_defaults']['privilegeDefaultNames']['eng'] : "",
                        'Title' => $data[$i]['privilegeShopTitles']['eng'],
                        'IsReachDateTime' => $data[$i]['isReachDateTime'],
                        'Continued' => $data[$i]['isExpired'],
                        'IsGoldenMinutes' => $data[$i]['isGoldenMinutes'],
                        'IsActive' => $data[$i]['isActive'],
                    )
                );
            }
            return Excel::download(new BladeExport($array), 'privilegeShop.xlsx');
        } else {
            return redirect("/shops/{$shop->id}/edit");
        }
    }

    public function excelPromotion()
    {
        $id = Input::get('id');
        $fieldNews = Input::get('fieldNews');
        $sortNews = Input::get('sortNews');
        $dataNews = News::with(['shops', 'newsTypes']);
        $dataNews = $dataNews->where('shopId', new \MongoDB\BSON\ObjectId($id));
        $dataNews = $dataNews->get()->toArray();

        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);

        for ($i = 0; $i < count($dataNews); $i++) {
            if (isset($dataNews[$i]['isReachDateTime'])) {
                if ($dataNews[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($dataNews[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $dataNews[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $dataNews[$i]['isExpired'] = true;
                }
            } else {
                $dataNews[$i]['isExpired'] = true;
            }
        }
        if (isset($sortNews)) {
            if ($sortNews == "desc") {
                $sortDataNews = SORT_DESC;
            } else {
                $sortDataNews = SORT_ASC;
            }
        } else {
            $sortDataNews = SORT_ASC;
        }

        if (isset($fieldNews)) {
            if ($fieldNews == "newsName") {
                $arrayNews = array_map(function ($element) {
                    if (isset($element['newsName']['eng'])) {

                        return $element['newsName']['eng'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "shops") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {

                        return $element['shops']['shopName']['eng'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "newsType") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['news_types']['newsTypeName']['eng'])) {

                        return $element['news_types']['newsTypeName']['eng'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "gender") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['gender'])) {

                        return $element['gender'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "minAge") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['minAge'])) {

                        return $element['minAge'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "maxAge") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['maxAge'])) {

                        return $element['maxAge'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "isActive") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['newsActive'])) {

                        return $element['newsActive'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "isReachDateTime") {

                $arrayNews = array_map(function ($element) {
                    if (isset($element['isReachDateTime'])) {
                        return $element['isReachDateTime'];
                    }
                }, $dataNews);
            } else if ($fieldNews == "isExpired") {
                $arrayNews = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $dataNews);
            }
        } else {
            $arrayNews = array_map(function ($element) {
                return $element['newsName']['eng'];
            }, $dataNews);
        }


        array_multisort($arrayNews, $sortDataNews, $dataNews);

        $array = [];
        if (count($dataNews) > 0) {
            for ($i = 0; $i < count($dataNews); $i++) {

                array_push(
                    $array,
                    array(
                        'PromotionName' =>
                        isset($dataNews[$i]['newsName']['eng']) ?
                            $dataNews[$i]['newsName']['eng'] : "",
                        'PromotionType' =>
                        isset($dataNews[$i]['news_types']['newsTypeName']['eng']) ?
                            $dataNews[$i]['news_types']['newsTypeName']['eng'] : "",
                        'IsReachDateTime' => $dataNews[$i]['isReachDateTime'],
                        'Continued' => $dataNews[$i]['isExpired'],
                        'newsActive' => $dataNews[$i]['newsActive'],
                    )
                );
            }
            return Excel::download(new BladeExport($array), 'promotion.xlsx');
        } else {
            return redirect("/shops/{$shop->id}/edit");
        }
    }
    public function update(ShopsRequest $request, Shops $shop)
    {

        if ($shop->update($request->all())) {
            //  dd($request); //
            $imageFile1 = $request->filenameLogo1;
            $imagePath1 = '/public/media/images/';
            $imageFile2 = $request->filenameLogo2;
            $imagePath2 = '/public/media/images/';

            if (empty($request->mainTypes)) {
                $shop->update(['mainTypes' => []]);
            }

            if (empty($request->shopCategoryId)) {
                $shop->update(['shopCategoryId' => []]);
            }

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
                $shop->update(['filenameLogo1' => $imageName1]);
                $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
            }

            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
                $shop->update(['filenameLogo2' => $imageName2]);
                $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
            }

            $shop->update(['shopName' => ConvertValueLanguages::convertData($shop->shopName)]);
            $shop->update(['shopDetail' => ConvertValueLanguages::convertData($shop->shopDetail)]);
            $location = ['coordinates' => [(float) $shop->longitude, (float) $shop->latitude], 'type' => 'Point'];
            $shop->update(['location' => $location]);

            return redirect('shops/' . $shop->id . '/edit')->with('success', 'Edit Shop Success');
        } else {

            return Redirect::route('shops/' . $shop->id . '/edit')->withInput()->with('error', trans('shops/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $shop = Shops::find($id);
        if ($shop) {
            $mallFloorId = $shop->mallFloorId;
            $shop->delete();
            return redirect('mallFloors/' . $mallFloorId . '/edit')->with('success', trans('shops/message.success.destroy'));
        } else {
            return redirect('mallFloors/' . $shop->mallFloorId . '/edit')->withInput()->with('error', trans('shops/message.error.delete'));
        }
    }

    public function shopDetail($id)
    {
        $shop = Shops::find($id);
        return response()->json(['shop' => $shop]);
    }

    public function data()
    {
        $models = Shops::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (Shops $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function queryMallFloors()
    {

        $queryMallFloors = MallFloor::where([])->get();

        $mallFloors = [];
        if ($queryMallFloors) {
            foreach ($queryMallFloors as $mallFloor) {
                $mallFloors[$mallFloor->id] = $mallFloor->mallFloorName['eng'];
            }
        }
        return $mallFloors;
    }

    public function show(Request $request)
    {

        $shops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId(Input::get('mallFloorId'))])->orderBy('shopName.eng', 'asc')->get();
        $data = view('shops.option', compact('shops'))->render();
        return response()->json(['options' => $data]);
    }

    public function searchByMallFloor(Request $request)
    {

        $shops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId(Input::get('mallFloorId'))])->orderBy('shopName.eng', 'asc')->get();
        $data = view('shops.option', compact('shops'))->render();
        return response()->json(['options' => $data]);
    }

    public function search(Request $request)
    {
        $shops = Shops::where(function ($query) use ($request) {
            foreach ($request->mallFloorId as $value) {
                //     //     //you can use orWhere the first time, dosn't need to be ->where
                $query->orWhere('mallFloorId', new \MongoDB\BSON\ObjectId($value));
            }
        })->get();
        $data = view('shops.option', compact('shops'))->render();

        return response()->json(['options' => $data]);
    }


    public function searchByPlayer()
    {
        $user = UserPlayer::raw(function ($collection) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => '_id',
                        'foreignField' => 'userId',
                        'as' => 'players'
                    )),
                    array('$unwind' => '$players'),
                )
            );
        });
        $listId = [];
        if (!empty(Input::get('mallId')) || !empty(Input::get('mallFloorId')) || !empty(Input::get('shopId')) || !empty(Input::get('newsId'))) {

            $shops = Shops::raw(function ($collection) {
                $query = array(

                    array('$lookup' => array(
                        'from' => 'mallFloors',
                        'localField' => 'mallFloorId',
                        'foreignField' => '_id',
                        'as' => 'mallFloor'
                    )),
                    array('$unwind' => '$mallFloor'),
                    array('$lookup' => array(
                        'from' => 'malls',
                        'localField' => 'mallFloor.mallId',
                        'foreignField' => '_id',
                        'as' => 'mall'
                    )),
                    array('$unwind' => '$mall'),

                    array('$lookup' => array(
                        'from' => 'channels',
                        'localField' => '_id',
                        'foreignField' => 'shopId',
                        'as' => 'channels'
                    )),
                    array('$unwind' => '$channels'),
                    array('$lookup' => array(
                        'from' => 'joinChannels',
                        'localField' => 'channels._id',
                        'foreignField' => 'channelId',
                        'as' => 'joinChannel'
                    )),
                    array('$unwind' => '$joinChannel'),
                    array('$lookup' => array(
                        'from' => 'monsterPlayers',
                        'localField' => 'joinChannel.mPlayerId',
                        'foreignField' => '_id',
                        'as' => 'monsterPlayer'
                    )),
                    array('$unwind' => '$monsterPlayer'),
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => 'joinChannel.playerId',
                        'foreignField' => '_id',
                        'as' => 'player'
                    )),
                    array('$unwind' => '$player'),
                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'player.userId',
                        'foreignField' => '_id',
                        'as' => 'user'
                    )),
                    array('$unwind' => '$user'),

                );

                if (!empty(Input::get('shopId'))) {
                    array_push(
                        $query,
                        array('$match' => array("_id" => new \MongoDB\BSON\ObjectId(Input::get('shopId'))))
                    );
                }
                if (!empty(Input::get('mallFloorId'))) {
                    array_push(
                        $query,
                        array('$match' => array("mallFloorId" => new \MongoDB\BSON\ObjectId(Input::get('mallFloorId'))))
                    );
                }
                if (!empty(Input::get('mallId'))) {
                    array_push(
                        $query,
                        array('$match' => array("mall._id" => new \MongoDB\BSON\ObjectId(Input::get('mallId'))))
                    );
                }

                if (!empty(Input::get('newsId'))) {
                    array_push(
                        $query,
                        array('$lookup' => array(
                            'from' => 'news',
                            'localField' => '_id',
                            'foreignField' => 'shopId',
                            'as' => 'news'
                        )),
                        array('$unwind' => '$news'),
                        array('$match' => array("news._id" => new \MongoDB\BSON\ObjectId(Input::get('newsId'))))
                    );
                }

                return $collection->aggregate($query);
            });
            foreach ($shops as $data) {
                array_push($listId, (string) $data->user->_id);
            }
        }

        $data = view('privilegeDefaultsPlayer.option', compact('listId', 'user'))->render();
        return response()->json(['options' => $data]);
    }

    public function searchByPlayerFixBug()
    {
        $listId = [];
        if (!empty(Input::get('mallId')) || !empty(Input::get('mallFloorId')) || !empty(Input::get('shopId')) || !empty(Input::get('newsId'))) {

            $shops = Shops::raw(function ($collection) {
                $query = array(

                    array('$lookup' => array(
                        'from' => 'mallFloors',
                        'localField' => 'mallFloorId',
                        'foreignField' => '_id',
                        'as' => 'mallFloor'
                    )),
                    array('$unwind' => '$mallFloor'),
                    array('$lookup' => array(
                        'from' => 'malls',
                        'localField' => 'mallFloor.mallId',
                        'foreignField' => '_id',
                        'as' => 'mall'
                    )),
                    array('$unwind' => '$mall'),

                    array('$lookup' => array(
                        'from' => 'channels',
                        'localField' => '_id',
                        'foreignField' => 'shopId',
                        'as' => 'channels'
                    )),
                    array('$unwind' => '$channels'),
                    array('$lookup' => array(
                        'from' => 'joinChannels',
                        'localField' => 'channels._id',
                        'foreignField' => 'channelId',
                        'as' => 'joinChannel'
                    )),
                    array('$unwind' => '$joinChannel'),
                    array('$lookup' => array(
                        'from' => 'monsterPlayers',
                        'localField' => 'joinChannel.mPlayerId',
                        'foreignField' => '_id',
                        'as' => 'monsterPlayer'
                    )),
                    array('$unwind' => '$monsterPlayer'),
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => 'joinChannel.playerId',
                        'foreignField' => '_id',
                        'as' => 'player'
                    )),
                    array('$unwind' => '$player'),
                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'player.userId',
                        'foreignField' => '_id',
                        'as' => 'user'
                    )),
                    array('$unwind' => '$user'),

                );

                if (!empty(Input::get('newsId'))) {
                    array_push(
                        $query,
                        array('$lookup' => array(
                            'from' => 'news',
                            'localField' => '_id',
                            'foreignField' => 'shopId',
                            'as' => 'news'
                        )),
                        array('$unwind' => '$news'),
                        array('$match' => array("news._id" => new \MongoDB\BSON\ObjectId(Input::get('newsId'))))
                    );
                }
                if (!empty(Input::get('shopId'))) {
                    array_push(
                        $query,
                        array('$match' => array("_id" => new \MongoDB\BSON\ObjectId(Input::get('shopId'))))
                    );
                }
                if (!empty(Input::get('mallFloorId'))) {
                    array_push(
                        $query,
                        array('$match' => array("mallFloorId" => new \MongoDB\BSON\ObjectId(Input::get('mallFloorId'))))
                    );
                }
                if (!empty(Input::get('mallId'))) {
                    array_push(
                        $query,
                        array('$match' => array("mall._id" => new \MongoDB\BSON\ObjectId(Input::get('mallId'))))
                    );
                }

                return $collection->aggregate($query);
            });

            foreach ($shops as $data) {
                array_push($listId, (string) $data->user->_id);
            }
        }
    }

    public function shopCode($shopCode)
    {

        $shops = Shops::where(['shopCode' => $shopCode])->orderBy('shopName.eng', 'asc')->get();
        $data = view('shops.table', compact('shops'))->render();
        return response()->json($data);
    }

    public function searchIndex(Request $request)
    {

        $query = "";
        if ($request->mallId) {
            $query .= "&mallId=" . implode(",", $request->mallId);
        }

        if ($request->mallFloorId) {
            $query .= "&mallFloorId=" . implode(",", $request->mallFloorId);
        }

        if ($request->search) {
            $query .= "&search=" . $request->search;
        }


        return redirect('shops?' . $query);
    }
}
