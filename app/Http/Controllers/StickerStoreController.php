<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\StickerStore;
use App\Http\Requests\StickerStoresRequest;
use App\Helper\ConvertValueLanguages;

class StickerStoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Resource');
    }

    public function index()
    {
        $stickerStores = StickerStore::all();
        return view('stickerStores.index', compact('stickerStores'));
    }

    public function create()
    {
        $stickerStoreImages = null;
        return view('stickerStores.create', compact('stickerStoreImages'));
    }

    public function store(StickerStoresRequest $request)
    {
        $stickerStore = new StickerStore($request->all());

        $imageFile1 = $stickerStore->stickerStoreImage;
        $imagePath1 = '/public/media/images/';

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('stickerStoreImage')->getClientOriginalExtension();
            $stickerStore->stickerStoreImage = $imageName1;
            $request->file('stickerStoreImage')->move(base_path() . $imagePath1, $imageName1);
        }

        $stickerStore->stickerStoreName = ConvertValueLanguages::convertData($stickerStore->stickerStoreName);
        $stickerStore->stickerStoreDetail = ConvertValueLanguages::convertData($stickerStore->stickerStoreDetail);

        if ($stickerStore->save()) {

            return redirect('stickerStores')->with('success', trans('stickerStores/message.success.create'));
        } else {
            return Redirect::route('stickerStores')->withInput()->with('error', trans('stickerStores/message.error.create'));
        }
    }

    public function edit(StickerStore $stickerStore)
    {
        $stickerStoreImages = $stickerStore->stickerStoreImage;
        return view('stickerStores.edit', compact('stickerStore', 'stickerStoreImages'));
    }

    public function update(StickerStoresRequest $request, StickerStore $stickerStore)
    {

        if ($stickerStore->update($request->all())) {

            $imageFile1 = $request->stickerStoreImage;
            $imagePath1 = '/public/media/images/';

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('stickerStoreImage')->getClientOriginalExtension();
                $stickerStore->update(['stickerStoreImage' => $imageName1]);
                $request->file('stickerStoreImage')->move(base_path() . $imagePath1, $imageName1);
            }

            $stickerStore->update(['stickerStoreName' => ConvertValueLanguages::convertData($stickerStore->stickerStoreName)]);
            $stickerStore->update(['stickerStoreDetail' => ConvertValueLanguages::convertData($stickerStore->stickerStoreDetail)]);

            return redirect('stickerStores')->with('success', trans('stickerStores/message.success.update'));
        } else {
            return Redirect::route('stickerStores')->withInput()->with('error', trans('stickerStores/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $stickerStore = StickerStore::find($id);

        if ($stickerStore) {
            $stickerStore->delete();
            return redirect('stickerStores')->with('success', trans('stickerStores/message.success.destroy'));
        } else {
            return redirect('stickerStores')->with('error', trans('stickerStores/message.error.delete'));
        }
    }

    public function data()
    {
        $models = StickerStore::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (StickerStore $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
