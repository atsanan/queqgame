<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\AuthenType;
use App\Http\Requests\AuthenTypeRequest;

class AuthenTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $authenTypes = AuthenType::all();

        return view('authenTypes.index', compact('authenTypes'));
    }

    public function create()
    {
        return view('authenTypes.create');
    }

    public function store(AuthenTypeRequest $request)
    {
        $authenType = new AuthenType($request->all());

        if ($authenType->save()) {
            return redirect('authenTypes')->with('success', trans('authenTypes/message.success.create'));
        } else {
            return Redirect::route('authenTypes')->withInput()->with('error', trans('authenTypes/message.error.create'));
        }
    }

    public function edit(AuthenType $authenType)
    {
        return view('authenTypes.edit', compact('authenType'));
    }

    public function update(AuthenTypeRequest $request, AuthenType $authenType)
    {
        if ($authenType->update($request->all())) {
            return redirect('authenTypes')->with('success', trans('authenTypes/message.success.update'));
        } else {
            return Redirect::route('authenTypes')->withInput()->with('error', trans('authenTypes/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $authenType = AuthenType::find($id);

        if ($authenType->delete()) {
            return redirect('authenTypes')->with('success', trans('authenTypes/message.success.destroy'));
        } else {
            return Redirect::route('authenTypes')->withInput()->with('error', trans('authenTypes/message.error.delete'));
        }
    }

    public function data()
    {
        $models = AuthenType::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (AuthenType $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }

            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

}
