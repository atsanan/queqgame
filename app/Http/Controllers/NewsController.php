<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\News;
use App\Models\NewsTypes;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\WildItem;
use App\Models\Shops;
use App\Http\Requests\NewsRequest;
use Illuminate\Support\Facades\Input;
use App\Helper\ConvertValueLanguages;
use App\Helper\Thumbnail;
use App\Models\Setting;
use Illuminate\Pagination\LengthAwarePaginator;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['searchByShop', 'detail']);
        $this->middleware('role:Promotion_Resource')->except(['searchByShop', 'detail']);
    }

    public $mainTypes = ['Eating' => 'Eating', 'Shopping' => 'Shopping', 'Entertain' => 'Entertain', 'Other' => 'Other'];

    public function index()
    {
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        $search = Input::get('search');
        $malls = Malls::queryMall();

        $mallFloors = [];
        $shops = [];
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);

        $data = News::with(['shops', 'newsTypes']);
        $mallId = Input::get('mallId');
        $_mallId = explode(",", $mallId);

        $whereMallId = [];

        if (isset($mallId)) {

            foreach ($_mallId as $value) {
                //     //you can use orWhere the first time, dosn't need to be ->where
                $whereMallId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryMallFloors = MallFloor::whereIn('mallId', $whereMallId)->get();
        } else {
            $queryMallFloors = MallFloor::get();
        }

        $_findMallFloorId = [];
        if ($queryMallFloors) {

            foreach ($queryMallFloors as $mallFloor) {
                $mallFloors[$mallFloor->id] = $mallFloor->mallFloorName['eng'];
                $_findMallFloorId[] = new \MongoDB\BSON\ObjectId($mallFloor->id);
            }
            $shop = Shops::whereIn('mallFloorId', $_findMallFloorId)->get();
            $findShopId =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            if (isset($mallId)) {
                $data = $data->whereIn('shopId', $findShopId);
            }
        }

        $mallFloorId = Input::get('mallFloorId');
        $_mallFloorId = explode(",", $mallFloorId);
        if (isset($mallFloorId)) {

            foreach ($_mallFloorId as $value) {
                $whereMallFloorId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryShop = shops::whereIn('mallFloorId', $whereMallFloorId)->get();
        } else {
            $queryShop = Shops::get();
        }

        $findShopId = [];
        foreach ($queryShop as $query) {
            $shops[$query->id] = $query->shopName['eng'];
            $findShopId[] = new \MongoDB\BSON\ObjectId($query->id);
        }
        if (isset($mallFloorId)) {
            $data = $data->whereIn('shopId', $findShopId);
        }

        $shopId = Input::get('shopId');
        $_ShopIdIn = [];
        $_shopId = explode(",", $shopId);
        if (isset($shopId)) {
            foreach ($_shopId as $id) {
                $_ShopIdIn[] = new \MongoDB\BSON\ObjectId($id);
            }
            $data = $data->whereIn('shopId', $_ShopIdIn);
        }

        if (isset($search)) {

            $shop = Shops::where('shopName.eng', 'like', "%$search%")->get();
            $shopIdSearch =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('shopId', $shopIdSearch);
            $newsTypes = NewsTypes::where('newsTypeName.eng', 'like', "%$search%")->get();
            $newsTypesId =  $newsTypes->map(function ($item) {

                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('newsType', $newsTypesId)
                ->orWhere('gender', 'like', "%$search%")
                ->orWhere('newsName.eng', 'like', "%$search%");
        }
        $data = $data->get()->toArray();
        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);


        //
        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = true;
                }
            } else {
                $data[$i]['isExpired'] = true;
            }
        }

        $index = array_keys(array_column($data, 'isExpired'), false);

        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }

        if (isset($field)) {
            if ($field == "newsName") {
                $array = array_map(function ($element) {
                    if (isset($element['newsName']['eng'])) {

                        return $element['newsName']['eng'];
                    }
                }, $data);
            } else if ($field == "shops") {

                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {

                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else if ($field == "newsType") {

                $array = array_map(function ($element) {
                    if (isset($element['news_types']['newsTypeName']['eng'])) {

                        return $element['news_types']['newsTypeName']['eng'];
                    }
                }, $data);
            } else if ($field == "gender") {

                $array = array_map(function ($element) {
                    if (isset($element['gender'])) {

                        return $element['gender'];
                    }
                }, $data);
            } else if ($field == "minAge") {

                $array = array_map(function ($element) {
                    if (isset($element['minAge'])) {

                        return $element['minAge'];
                    }
                }, $data);
            } else if ($field == "maxAge") {

                $array = array_map(function ($element) {
                    if (isset($element['maxAge'])) {

                        return $element['maxAge'];
                    }
                }, $data);
            } else if ($field == "isActive") {

                $array = array_map(function ($element) {
                    if (isset($element['newsActive'])) {

                        return $element['newsActive'];
                    }
                }, $data);
            } else if ($field == "isReachDateTime") {

                $array = array_map(function ($element) {
                    if (isset($element['isReachDateTime'])) {
                        return $element['isReachDateTime'];
                    }
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            }

            // if ($field != "isExpired") {
            //     if (count($index) > 0) {
            //         foreach ($index as $i) {
            //             unset($data[$i]);
            //             unset($array[$i]);
            //         }
            //     }
            // }
        } else {
            $array = array_map(function ($element) {
                return $element['newsName']['eng'];
            }, $data);
        }

        $all = Input::get('all');
        if (empty($all)) {
            if (count($index) > 0) {
                foreach ($index as $i) {
                    unset($data[$i]);
                    unset($array[$i]);
                }
            }
        }



        array_multisort($array, $sortData, $data);
        $news  =  new LengthAwarePaginator(array_slice($data,  $offset, 50), count($data), 50);
        $news->setPath('/news');

        return view('news.index', compact(
            'news',
            'search',
            'field',
            'sort',
            'malls',
            'mallFloors',
            'shops',
            'mallId',
            '_mallId',
            'mallFloorId',
            '_mallFloorId',
            'shopId',
            '_shopId',
            'all'
        ));
    }



    public function create()
    {
        $shopId = Input::get('shopId');
        $mallFloor = Shops::where(['_id' => new \MongoDB\BSON\ObjectId($shopId)])->first();
        $mallFloorId = null;
        if (isset($mallFloor)) {
            $mallFloorId = $mallFloor->mallFloorId;
        }
        $mall = MallFloor::where(['_id' => new \MongoDB\BSON\ObjectId($mallFloorId)])->first();
        $mallId = null;
        if (isset($mall)) {
            $mallId = $mall->mallId;
        }
        $mallFloors = MallFloor::queryMallFloors($mallId);
        $malls = Malls::queryMall();
        $newsTypes = $this->queryNewsType();
        $wildItems = $this->queryWilditem();
        $shops = $this->queryShop($mallFloorId);
        $filenameImages1 = null;
        $filenameImages2 = null;
        $filenameImages3 = null;
        $order = 0;
        $mainTypes = $this->mainTypes;

        return view('news.create', compact(
            'order',
            'newsTypes',
            'wildItems',
            'shops',
            'filenameImages1',
            'filenameImages2',
            'filenameImages3',
            'malls',
            'mallId',
            'mallFloors',
            'shopId',
            'mallFloorId',
            'mallId',
            'mainTypes'
        ));
    }

    public function store(NewsRequest $request)
    {

        $news = new News($request->all());
        $thumbnail = Setting::where([])->first();

        $imagePath = '/public/media/images/';
        $imageFile1 = $news->filenameImage1;
        $imageFile2 = $news->filenameImage2;
        $imageFile3 = $news->filenameImage3;
        $imageName1 = "";
        $imageName2 = "";
        $imageName3 = "";
        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('filenameImage1')->getClientOriginalExtension();
            $news->filenameImage1 = $imageName1;
            $uploadFile = $request->file('filenameImage1')->move(base_path() . $imagePath, $imageName1);
            if (isset($uploadFile)) {
                Thumbnail::createThumbnailFile($imageName1, $thumbnail->width, $thumbnail->height);
            }
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('filenameImage2')->getClientOriginalExtension();
            $news->filenameImage2 = $imageName2;
            $uploadFile2 = $request->file('filenameImage2')->move(base_path() . $imagePath, $imageName2);
            if (isset($uploadFile2)) {
                Thumbnail::createThumbnailFile($imageName2, $thumbnail->width, $thumbnail->height);
            }
        }

        if (isset($imageFile3)) {
            $imageName3 = uniqid() . '.' . $request->file('filenameImage3')->getClientOriginalExtension();
            //$news->filenameImage3 = $imageName3;
            $uploadFile3 = $request->file('filenameImage3')->move(base_path() . $imagePath, $imageName3);
            if (isset($uploadFile3)) {
                Thumbnail::createThumbnailFile($imageName3, $thumbnail->width, $thumbnail->height);
            }
        }

        $news->newsName = ConvertValueLanguages::convertData($news->newsName);
        $news->newsDetail = ConvertValueLanguages::convertData($news->newsDetail);
        // if ($news->location['coordinates'][0] == null && $news->location['coordinates'][1] == null) {
        //     $news->location = ['coordinates' => [0.0, 0.0], 'type' => 'Point'];
        // } else {
        //     $coordinates = [(float) $news->location['coordinates'][0], (float) $news->location['coordinates'][1]];
        //     $news->location = ['coordinates' => $coordinates, 'type' => 'Point'];
        // }

        $news->location = ['coordinates' => [(float) $news->longitude, (float) $news->latitude], 'type' => 'Point'];
        if (count($news->shopIdArray) > 0) {
            $recordData = [];
            for ($j = 0; $j < count($news->shopIdArray); $j++) {
                $newShop = Shops::find($news->shopIdArray[$j]);
                if ($newShop) {
                    $newShop->isPromotionAvailable = true;
                    $newShop->update();
                }

                $startDateTime = \DateTime::createFromFormat('Y-m-d h:i A', $request->startDateTime)->format('Y-m-d h:i:sa');
                $expiredDateTime = \DateTime::createFromFormat('Y-m-d h:i A', $request->expiredDateTime)->format('Y-m-d h:i:sa');

                $data = [
                    'newsName' => $news->newsName,
                    'newsDetail' => $news->newsDetail,
                    'shopId' => new \MongoDB\BSON\ObjectId($news->shopIdArray[$j]),
                    'filenameLogo1' => $imageName1,
                    'filenameLogo2' => $imageName2,
                    'filenameLogo3' => $imageName3,
                    'newsActive' => $news->newsActive,
                    'order' => $news->order,
                    'linkListName' => $news->linkListName,
                    //'mainTypes' => $news->mainTypes,
                    //'gender' => $news->gender,
                    'newsType' =>  new \MongoDB\BSON\ObjectId($news->newsType),
                    //'wildItemId' => new \MongoDB\BSON\ObjectId($news->wildItemId),
                    'location' => $news->location,
                    'pushUrl' => $news->pushUrl,
                    'pushTokenUser' => $news->pushTokenUser,
                    'minAge' => $news->minAge,
                    'maxAge' => $news->maxAge,
                    'isNearBy' => $news->isNearBy,
                    'isSponser' => $news->isSponser,
                    //'mainTypes' => $news->mainTypes,
                    'isReachDateTime' => $news->isReachDateTime,
                    'startDateTime'  => new \MongoDB\BSON\UTCDateTime(strtotime($startDateTime) * 1000),
                    'expiredDateTime'  => new \MongoDB\BSON\UTCDateTime(strtotime($expiredDateTime) * 1000),
                    'createAt' => new \MongoDB\BSON\UTCDateTime(now()),
                    'updateAt' => new \MongoDB\BSON\UTCDateTime(now()),
                ];

                if (!empty($news->wildItemId)) {
                    $data['wildItemId'] = new \MongoDB\BSON\ObjectId($news->wildItemId);
                }

                if (!empty($news->mainTypes)) {
                    $data['maintypes'] = $news->mainTypes;
                }

                if (!empty($news->gender)) {
                    $data['gender'] = $news->gender;
                }

                $shop = Shops::find(new \MongoDB\BSON\ObjectId($news->shopIdArray[$j]));
                if ($shop) {
                    $data['location'] = $shop->location;
                }

                $recordData[] = $data;
            }

            News::insert($recordData);
            return redirect('news')->with('success', "Success Create Promotion");
            // if ($news->save()) {
            // if (isset($news->shopId)) {
            //     $newShop = Shops::find($news->shopId);
            //     $newShop->isPromotionAvailable = true;
            //     $newShop->update();
            // }

            // if ($news->shopId != "") {
            //     return redirect("shops/{$news->shopId}/edit")->with('success', trans('news/message.success.create'));
            // } else {
            //return redirect("news/{$news->id}/edit")->with('success', 'Save News Success');
            //}
        } else {
            if ($news->save()) {
                return redirect("news/{$news->id}/edit")->with('success', 'Save News Success');
            } else {
                return redirect("news/create")->withInput()->with('error', 'Save  News Fail');
            }
        }
    }

    public function edit(News $news)
    {
        $newsTypes = $this->queryNewsType();
        $wildItems = $this->queryWilditem();

        $filenameImages1 = $news->filenameImage1;
        $filenameImages2 = $news->filenameImage2;
        $filenameImages3 = $news->filenameImage3;
        $order = $news->order;
        $shopId = $news->shopId;
        $mallFloorId = null;
        $mallId = null;
        $mainTypes = $this->mainTypes;
        if (isset($shopId)) {
            $mallFloor = Shops::where(['_id' => new \MongoDB\BSON\ObjectId($shopId)])->first();
            if (isset($mallFloor)) {
                $mallFloorId = $mallFloor->mallFloorId;
            }
            $mall = MallFloor::where(['_id' => new \MongoDB\BSON\ObjectId($mallFloorId)])->first();
            if (isset($mall)) {
                $mallId = $mall->mallId;
            }
        }

        $shops = $this->queryShop($mallFloorId);
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors($mallId);
        $news->longitude = $news->location['coordinates'][0];
        $news->latitude = $news->location['coordinates'][1];

        return view('news.edit', compact(
            'news',
            'order',
            'newsTypes',
            'wildItems',
            'shops',
            'filenameImages1',
            'filenameImages2',
            'filenameImages3',
            'malls',
            'mallId',
            'mallFloors',
            'shopId',
            'mallFloorId',
            'mainTypes'
        ));
    }

    public function update(NewsRequest $request, News $news)
    {

        if ($news->update($request->all())) {

            $thumbnail = Setting::where([])->first();
            if (isset($request->shopId)) {
                $newShop = Shops::find($request->shopId);
                if (isset($newShop)) {
                    $newShop->isPromotionAvailable = true;
                    $newShop->update();
                }
            }

            if (!empty($request->oldShop)) {
                $findNews = News::where(['shopId' => new \MongoDB\BSON\ObjectId($request->oldShop)])->get();
                $oldShop = Shops::find($request->oldShop);
                if (isset($oldShop)) {
                    $oldShop->isPromotionAvailable = (count($findNews) > 0);
                    $oldShop->update();
                }
            }

            $imagePath = '/public/media/images/';
            $imageFile1 = $request->filenameImage1;
            $imageFile2 = $request->filenameImage2;
            $imageFile3 = $request->filenameImage3;

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('filenameImage1')->getClientOriginalExtension();
                $news->update(['filenameImage1' => $imageName1]);
                $uploadFile = $request->file('filenameImage1')->move(base_path() . $imagePath, $imageName1);
                if (isset($uploadFile)) {
                    Thumbnail::createThumbnailFile($imageName1, $thumbnail->width, $thumbnail->height);
                }
            }


            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('filenameImage2')->getClientOriginalExtension();
                $news->update(['filenameImage2' => $imageName2]);
                $uploadFile2 = $request->file('filenameImage2')->move(base_path() . $imagePath, $imageName2);
                if (isset($uploadFile2)) {
                    Thumbnail::createThumbnailFile($imageName2, $thumbnail->width, $thumbnail->height);
                }
            }

            if (isset($imageFile3)) {
                $imageName3 = uniqid() . '.' . $request->file('filenameImage3')->getClientOriginalExtension();
                $uploadFile3 = $news->update(['filenameImage3' => $imageName3]);
                $request->file('filenameImage3')->move(base_path() . $imagePath, $imageName3);
                if (isset($uploadFile3)) {
                    Thumbnail::createThumbnailFile($imageName3, $thumbnail->width, $thumbnail->height);
                }
            }

            $news->update(['newsName' => ConvertValueLanguages::convertData($news->newsName)]);
            $news->update(['newsDetail' => ConvertValueLanguages::convertData($news->newsDetail)]);
            $location = ['coordinates' => [(float) $news->longitude, (float) $news->latitude], 'type' => 'Point'];
            $news->update(['location' => $location]);

            // if ($request->shopId != "") {
            //     return redirect("shops/{$news->shopId}/edit")->with('success', trans('news/message.success.create'));
            // } else {
            return redirect("news/{$news->id}/edit")->with('success', 'Edit News Success');
            //}
        } else {
            return Redirect::route('news')->withInput()->with('error', trans('news/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $news = News::find($id);

        if ($news) {
            $news->delete();
            return redirect('news')->with('success', trans('news/message.success.destroy'));
        } else {
            return redirect('news')->with('error', trans('news/message.error.delete'));
        }
    }

    public function data()
    {
        $models = News::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (News $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function queryNewsType()
    {

        $queryNewsTypes = NewsTypes::where([])->get();

        $newsTypes = [];
        foreach ($queryNewsTypes as $type) {
            $newsTypes[$type->id] = $type->newsTypeName['eng'];
        }

        return $newsTypes;
    }

    public function queryWilditem($mallFloorId = null)
    {


        $queryWilditems = WildItem::where([])->get();

        $wilditem = [];
        foreach ($queryWilditems as $type) {
            $wilditem[$type->id] = $type->couponGiftName['eng'];
        }

        return $wilditem;
    }

    public function queryShop($mallFloorId = null)
    {

        if ($mallFloorId == null) {
            $queryShops = Shops::where([])->get();
        } else {
            $queryShops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId($mallFloorId)])->get();
        }

        $shops = [];
        foreach ($queryShops as $type) {
            $shops[$type->id] = $type->shopName['eng'];
        }

        return $shops;
    }

    public function push($id)
    {

        return redirect("news/{$id}/edit")
            ->with('success', 'Send News Success');
    }

    public function searchByShop()
    {
        $shopId = Input::get('shopId');
        $news = News::where(['shopId' => new \MongoDB\BSON\ObjectId($shopId)])->get();
        $data = view('news.option', compact('news'))->render();
        return response()->json(['options' => $data]);
    }

    public function detail($id)
    {
        $news = News::find($id);

        return response()->json(['data' => $news]);
    }

    public function search(Request $request)
    {

        $query = "";
        if ($request->mallId) {
            $query .= "&mallId=" . implode(",", $request->mallId);
        }

        if ($request->mallFloorId) {
            $query .= "&mallFloorId=" . implode(",", $request->mallFloorId);
        }


        if ($request->shopId) {
            $query .= "&shopId=" . implode(",", $request->shopId);
        }

        if ($request->search) {
            $query .= "&search=" . $request->search;
        }

        if ($request->all) {
            $query .= "&all=" . $request->all;
        }


        return redirect('news?' . $query);
    }
}
