<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\PrivilegeDefaults;
use App\Models\PrivilegeGroups;
use App\Http\Requests\PrivilegeDefaultsRequest;
use App\Helper\ConvertValueLanguages;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class PrivilegeDefaultsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['searchByPrivilegeGroups']);
        $this->middleware('role:Privilege_Resource')->except(['searchByPrivilegeGroups']);
    }

    public function index()
    {
        $search = Input::get('search');
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);

        $data = PrivilegeDefaults::with(['privilegeGroups']);
        if (isset($search)) {
            $data = $data->where('privilegeDefaultNames.eng', 'like', "%$search%");
            $privilegeGroups = PrivilegeGroups::where('privilegeGroupNames.eng', 'like', "%$search%")->get();
            $privilegeGroupId =  $privilegeGroups->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('privilegeGroupId', $privilegeGroupId);
        }

        $data = $data->get()->toArray();
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        if (isset($field)) {
            if ($field == "privilegeGroups") {
                $array = array_map(function ($element) {
                    if (isset($element['privilege_groups']['privilegeGroupNames']['eng'])) {
                        return $element['privilege_groups']['privilegeGroupNames']['eng'];
                    }
                }, $data);
            } else if ($field == "privilegeDefaultNames") {
                $array = array_map(function ($element) {
                    if (isset($element['privilegeDefaultNames']['eng'])) {
                        return $element['privilegeDefaultNames']['eng'];
                    }
                }, $data);
            } else  if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                if (isset($element['privilege_groups']['privilegeGroupNames']['eng'])) {
                    return $element['privilege_groups']['privilegeGroupNames']['eng'];
                }
            }, $data);
        }

        array_multisort($array, $sortData, $data);
        $privilegeDefaults  = new LengthAwarePaginator(array_slice($data, $offset, 50), count($data), 50);
        $privilegeDefaults->setPath('/privilegeDefaults');

        return view('privilegeDefaults.index', compact('privilegeDefaults', 'search', 'field', 'sort'));
    }

    public function create()
    {
        $privilegeGroups = $this->queryPrivilegeGroup();
        $filenameLogos1 = null;
        $filenameLogos2 = null;
        return view('privilegeDefaults.create', compact('filenameLogos1', 'filenameLogos2', 'privilegeGroups'));
    }

    public function store(PrivilegeDefaultsRequest $request)
    {
        $privilegeDefault = new PrivilegeDefaults($request->all());

        $imageFile1 = $privilegeDefault->filenameLogo1;
        $imagePath1 = '/public/media/images/';
        $imageFile2 = $privilegeDefault->filenameLogo2;
        $imagePath2 = '/public/media/images/';

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
            $privilegeDefault->filenameLogo1 = $imageName1;
            $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
            $privilegeDefault->filenameLogo2 = $imageName2;
            $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
        }

        $privilegeDefault->privilegeDefaultNames = ConvertValueLanguages::convertData($privilegeDefault->privilegeDefaultNames);
        $privilegeDefault->privilegeDefaultDetails = ConvertValueLanguages::convertData($privilegeDefault->privilegeDefaultDetails);

        if ($privilegeDefault->save()) {

            return redirect('privilegeDefaults')->with('success', trans('privilegeDefaults/message.success.create'));
        } else {
            return Redirect::route('privilegeDefaults')->withInput()->with('error', trans('privilegeDefaults/message.error.create'));
        }
    }

    public function edit(PrivilegeDefaults $privilegeDefault)
    {
        $privilegeGroups = $this->queryPrivilegeGroup();

        $filenameLogos1 = $privilegeDefault->filenameLogo1;
        $filenameLogos2 = $privilegeDefault->filenameLogo2;

        return view('privilegeDefaults.edit', compact('privilegeDefault', 'filenameLogos1', 'filenameLogos2', 'privilegeGroups'));
    }

    public function update(PrivilegeDefaultsRequest $request, PrivilegeDefaults $privilegeDefault)
    {

        if ($privilegeDefault->update($request->all())) {
            $imageFile1 = $request->filenameLogo1;
            $imagePath1 = '/public/media/images/';
            $imageFile2 = $request->filenameLogo2;
            $imagePath2 = '/public/media/images/';

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
                $privilegeDefault->update(['filenameLogo1' => $imageName1]);
                $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
            }

            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
                $privilegeDefault->update(['filenameLogo2' => $imageName2]);
                $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
            }

            $privilegeDefault->update(['privilegeDefaultNames' => ConvertValueLanguages::convertData($privilegeDefault->privilegeDefaultNames)]);
            $privilegeDefault->update(['privilegeDefaultDetails' => ConvertValueLanguages::convertData($privilegeDefault->privilegeDefaultDetails)]);

            return redirect('privilegeDefaults')->with('success', trans('privilegeDefaults/message.success.update'));
        } else {
            return Redirect::route('privilegeDefaults')->withInput()->with('error', trans('privilegeDefaults/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $privilegeDefault = PrivilegeDefaults::find($id);

        if ($privilegeDefault) {
            $privilegeDefault->delete();
            return redirect('privilegeDefaults')->with('success', trans('privilegeDefaults/message.success.destroy'));
        } else {
            return redirect('privilegeDefaults')->with('error', trans('privilegeDefaults/message.error.delete'));
        }
    }

    public function data()
    {
        $models = PrivilegeDefaults::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (PrivilegeDefaults $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public static function queryPrivilegeGroup()
    {

        $queryPrivilegeGroups = PrivilegeGroups::where([])->get();
        $privilegeGroups = [];

        if ($queryPrivilegeGroups) {
            foreach ($queryPrivilegeGroups as $type) {
                $privilegeGroups[$type->id] = $type->privilegeGroupNames['eng'];
            }
        }

        return $privilegeGroups;
    }

    public function searchByPrivilegeGroups()
    {
        $privilegeDefaults = PrivilegeDefaults::where(['privilegeGroupId' => new \MongoDB\BSON\ObjectId(Input::get('privilegeGroupId'))])->get();

        $data = view('privilegeDefaults.option', compact('privilegeDefaults'))->render();
        return response()->json(['options' => $data]);
    }
}
