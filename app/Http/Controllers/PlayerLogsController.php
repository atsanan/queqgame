<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\PlayerLogs;
use App\Models\Players;
use App\Models\ItemDefault;
use App\Models\MonstersDefault;
use App\Http\Requests\NewsTypesRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Excel;
use App\Exports\BladeExport;

class PlayerLogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Player_Resource');
    }

    public function index()
    {
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');
        $field = Input::get('field');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }

        $data = PlayerLogs::with(['monsters', 'items', 'player']);
        if (isset($search)) {

            $player = Players::where('playerName', 'like', "%$search%")->get();
            $playerId =  $player->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $item = ItemDefault::where('itemName.eng', 'like', "%$search%")->get();
            $itemId =  $item->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $monster = MonstersDefault::where('mDefaultName.eng', 'like', "%$search%")->get();
            $mDefaultId =  $monster->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $data = $data->where('tltle', 'like', "%$search%")
                ->orWhere('detail', 'like', "%$search%")
                ->orWhere('type', 'like', "%$search%")
                ->orWhereIn('playerId', $playerId)
                ->orWhereIn('itemId', $itemId)
                ->orWhereIn('mDefaultId', $mDefaultId);
        }
        $data = $data->get()->toArray();
        if (isset($field)) {
            if ($field == "title") {
                $array = array_map(function ($element) {
                    return $element['title'];
                }, $data);
            } else if ($field == "type") {
                $array = array_map(function ($element) {
                    return $element['type'];
                }, $data);
            } else if ($field == "player") {
                $array = array_map(function ($element) {
                    if (isset($element['player']['playerName'])) {
                        return $element['player']['playerName'];
                    }
                }, $data);
            } else if ($field == "monster") {
                $array = array_map(function ($element) {
                    if (isset($element['monsters']['mDefaultName']['eng'])) {
                        return $element['monsters']['mDefaultName']['eng'];
                    }
                }, $data);
            } else if ($field == "item") {
                $array = array_map(function ($element) {
                    if (isset($element['items']['itemName']['eng'])) {
                        return $element['items']['itemName']['eng'];
                    }
                }, $data);
            } else if ($field == "money") {
                $array = array_map(function ($element) {
                    return $element['money'];
                }, $data);
            } else if ($field == "coin") {
                $array = array_map(function ($element) {
                    return $element['coin'];
                }, $data);
            } else if ($field == "diamond") {
                $array = array_map(function ($element) {
                    return $element['diamond'];
                }, $data);
            } else if ($field == "createAt") {
                $array = array_map(function ($element) {
                    return $element['createAt'];
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['createAt'];
            }, $data);
        }
        array_multisort($array, $sortData, $data);
        $playerLogs = new LengthAwarePaginator(array_slice($data, $offset, 50), count($data), 50);
        $playerLogs->setPath('playerLogs');
        return view('playerLogs.index', compact('playerLogs', 'search', 'field', 'sort'));
    }

    public function destroy($id)
    {
        PlayerLogs::whereNotNull('_id')->delete();
        return redirect("playerLogs")->with('success', 'Delete Log Success');
    }


    public function excel()
    {
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');
        $field = Input::get('field');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }

        $data = PlayerLogs::with(['monsters', 'items', 'player']);
        if (isset($search)) {

            $player = Players::where('playerName', 'like', "%$search%")->get();
            $playerId =  $player->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $item = ItemDefault::where('itemName.eng', 'like', "%$search%")->get();
            $itemId =  $item->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $monster = MonstersDefault::where('mDefaultName.eng', 'like', "%$search%")->get();
            $mDefaultId =  $monster->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $data = $data->where('tltle', 'like', "%$search%")
                ->orWhere('detail', 'like', "%$search%")
                ->orWhere('type', 'like', "%$search%")
                ->orWhereIn('playerId', $playerId)
                ->orWhereIn('itemId', $itemId)
                ->orWhereIn('mDefaultId', $mDefaultId);
        }
        $data = $data->get()->toArray();
        if (isset($field)) {
            if ($field == "title") {
                $array = array_map(function ($element) {
                    return $element['title'];
                }, $data);
            } else if ($field == "type") {
                $array = array_map(function ($element) {
                    return $element['type'];
                }, $data);
            } else if ($field == "player") {
                $array = array_map(function ($element) {
                    if (isset($element['player']['playerName'])) {
                        return $element['player']['playerName'];
                    }
                }, $data);
            } else if ($field == "monster") {
                $array = array_map(function ($element) {
                    if (isset($element['monsters']['mDefaultName']['eng'])) {
                        return $element['monsters']['mDefaultName']['eng'];
                    }
                }, $data);
            } else if ($field == "item") {
                $array = array_map(function ($element) {
                    if (isset($element['items']['itemName']['eng'])) {
                        return $element['items']['itemName']['eng'];
                    }
                }, $data);
            } else if ($field == "money") {
                $array = array_map(function ($element) {
                    return $element['money'];
                }, $data);
            } else if ($field == "coin") {
                $array = array_map(function ($element) {
                    return $element['coin'];
                }, $data);
            } else if ($field == "diamond") {
                $array = array_map(function ($element) {
                    return $element['diamond'];
                }, $data);
            } else if ($field == "createAt") {
                $array = array_map(function ($element) {
                    return $element['createAt'];
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['createAt'];
            }, $data);
        }
        array_multisort($array, $sortData, $data);

        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $array = [];
        if (count($data) > 0) {
            for ($i = 0; $i < count($data); $i++) {
                $createAt = new \DateTime($data[$i]['createAt'], $gmtTimezone);
                $offsetcreateAt = $userTimezone->getOffset($createAt);
                $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
                $createAt->add($myIntervalcreateAt);
                array_push(
                    $array,
                    array(
                        'Title' => $data[$i]['title'],
                        'Detail' => $data[$i]['detail'],
                        'PlayerName' => isset($data[$i]['player']['playerName']) ? $data[$i]['player']['playerName'] : "",
                        'ItemName' => isset($data[$i]['items']['itemName']['eng']) ? $data[$i]['player']['playerName'] : "",
                        'Money' => $data[$i]['money'],
                        'Coin' => $data[$i]['coin'],
                        'Diamond' => $data[$i]['diamond'],
                        'CraateAt' => $createAt->format('Y-m-d H:i:s')
                    )
                );
            }
            return Excel::download(new BladeExport($array), 'playerLogs.xlsx');
        } else {
            return redirect('playerLogs');
        }
    }
}
