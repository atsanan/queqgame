<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\WildItem;
use App\Models\ItemDefault;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\Shops;
use App\Models\ZoneTypes;
use App\Http\Requests\WildItemsRequest;
use App\Helper\ConvertValueLanguages;
use Illuminate\Support\Facades\Redirect;

class WildItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Wild_Resource');
    }

    public function index()
    {
        $wildItems = WildItem::all();

        return view('wildItems.index', compact('wildItems'));
    }

    public function create()
    {
        $itemDefaults = $this->queryItemDefault();
        $malls = $this->queryMall();
        $mallFloors = $this->queryMallFloor();
        $shops = $this->queryShop();
        $zoneTypes = $this->queryZoneType();
        $zoneType = null;
        return view('wildItems.create', compact('itemDefaults', 'malls', 'mallFloors', 'shops', 'zoneTypes', 'zoneType'));
    }

    public function store(WildItemsRequest $request)
    {
        $wildItem = new WildItem($request->all());

        $wildItem->couponGiftName = ConvertValueLanguages::convertData($wildItem->couponGiftName);

        if ($wildItem->save()) {
            return redirect('wildItems')->with('success', trans('wildItems/message.success.create'));
        } else {
            return Redirect::route('wildItems.index')->withInput()->with('error', trans('wildItems/message.error.create'));
        }
    }

    public function edit(WildItem $wildItem)
    {
        $itemDefaults = $this->queryItemDefault();
        $malls = $this->queryMall();
        $mallFloors = $this->queryMallFloor($wildItem->toMall);
        $shops = $this->queryShop($wildItem->toMallFloor);
        $zoneTypes = $this->queryZoneType();
        $queryZoneTypes = ZoneTypes::where(['_id' => $wildItem->zoneTypeId])->first();
        $zoneType = null;
        if ($queryZoneTypes) {
            $zoneType = $queryZoneTypes->zoneTypeName['eng'];
        }

        return view('wildItems.edit', compact('wildItem', 'itemDefaults', 'malls', 'mallFloors', 'shops', 'zoneTypes', 'zoneType'));
    }

    public function update(WildItemsRequest $request, WildItem $wildItem)
    {
        if ($wildItem->update($request->all())) {
            $wildItem->update(['couponGiftName' => ConvertValueLanguages::convertData($wildItem->couponGiftName)]);
            return redirect('wildItems')->with('success', trans('wildItems/message.success.update'));
        } else {
            return Redirect::route('wildItems.index')->withInput()->with('error', trans('wildItems/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $wildItem = WildItem::find($id);

        if ($wildItem) {
            $wildItem->delete();
            return redirect('wildItems')->with('success', trans('wildItems/message.success.destroy'));
        } else {
            return Redirect::route('wildItems.index')->with('error', trans('wildItems/message.error.delete'));
        }
    }

    public function data()
    {
        $models = WildItem::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (WildItem $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function queryItemDefault()
    {

        $queryItemDefaults = ItemDefault::where([])->get();
        $itemDefaults = [];
        foreach ($queryItemDefaults as $type) {
            $itemDefaults[$type->id] = $type->itemName['eng'];
        }

        return $itemDefaults;
    }

    public function queryMall()
    {

        $queryMalls = Malls::where([])->orderBy('mallName.eng', 'asc')->get();
        $malls = [];
        foreach ($queryMalls as $type) {
            $malls[$type->id] = $type->mallName['eng'];
        }

        return $malls;
    }

    public function queryMallFloor($toMall = null)
    {
        if ($toMall != null) {
            $queryMallFloors = MallFloor::where(['mallId' => new \MongoDB\BSON\ObjectId($toMall)])->orderBy('mallFloorName.eng', 'asc')->get();
        } else {

            $queryMallFloors = MallFloor::where([])->orderBy('mallFloorName.eng', 'asc')->get();
        }

        $mallFloors = [];
        foreach ($queryMallFloors as $type) {
            $mallFloors[$type->id] = $type->mallFloorName['eng'];
        }

        return $mallFloors;
    }

    public function queryShop($toMallFloor = null)
    {

        if ($toMallFloor != null) {
            $queryShops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId($toMallFloor)])->orderBy('shopName.eng', 'asc')->get();
        } else {
            $queryShops = Shops::where([])->orderBy('shopName.eng', 'desc')->get();
        }

        $shops = [];
        foreach ($queryShops as $type) {
            $shops[$type->id] = $type->shopName['eng'];
        }
        return $shops;
    }

    public function queryZoneType()
    {

        $queryZoneTypes = ZoneTypes::where([])->get();
        $zoneTypes = [];
        foreach ($queryZoneTypes as $type) {
            $zoneTypes[$type->id] = $type->zoneTypeName['eng'];
        }
        return $zoneTypes;
    }
}
