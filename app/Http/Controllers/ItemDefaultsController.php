<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ItemDefault;
use App\Models\ItemCategorys;
use App\Models\ItemShop;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\Shops;
use App\Http\Requests\ItemDefaultsRequest;
use App\Helper\ConvertValueLanguages;
use App\Helper\UploadFile;
use App\Helper\Thumbnail;
use App\Http\Requests\CouponItemRequest;
use App\Models\Setting;
use App\Models\PartnerTypes;
use App\Models\CouponItem;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

class ItemDefaultsController extends Controller
{
    protected $imagePath1 = "/public/media/images/";
    public $mainTab = ["None" => "None", "Coin" => "Coin", "Diamond" => "Diamond", "Topup" => "Topup"];
    public $subTab = [
        "None" => "None", "Premium" => "Premium", "Hunger" => "Hunger",
        "Happiness" => "Happiness", "Health" => "Health", " Coin" => "Coin", "Diamond" => "Diamond"
    ];
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Resource');
    }

    public function index()
    {
        $itemDefaults = ItemDefault::all();
        $search = Input::get('search');
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        $type = Input::get('type');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);

        if ($field == "itemOrder") {
            $fieldSort = "itemOrder";
        } else {
            $fieldSort = $field;
        }
        $data = ItemDefault::with([]);
        if (isset($search)) {

            if (isset($sort)) {
                $data = $data->where("itemName.eng", "like", "%$search%")
                    ->orderBy($fieldSort, $sort);
            } else {
                $data = $data->where("itemName.eng", "like", "%$search%");
            }
        } else {
            if (isset($sort)) {
                $data = $data->orderBy($fieldSort, $sort);
            }
        }


        if (isset($type)) {
            $itemCategorys = ItemCategorys::where(['itemCategoryName' => $type])->first();
            if ($itemCategorys) {
                $data = $data->where(['itemCategoryId' => new \MongoDB\BSON\ObjectId($itemCategorys->id)]);
            } else {
                $data = $data->where(['itemCategoryId' => null]);
            }
        }

        $data = $data->get();
        $itemDefaults  = new LengthAwarePaginator($data->slice($offset, 50), $data->count(), 50);

        $itemDefaults->setPath('/itemDefaults');
        return view('itemDefaults.index', compact('itemDefaults', 'search', 'field', 'sort'));
        //return view('itemDefaults.index', compact('itemDefaults'));
    }

    public function create()
    {
        $itemCategorys = ItemCategorys::queryItemCategorys();
        $itemCategory = null;

        $assertModelIOS = null;
        $assertModelAndroid = null;

        $assertImageSlot1 = null;
        $assertImageSlot2 = null;
        $mainTab = $this->mainTab;
        $subTab = $this->subTab;
        $partnerTypes = PartnerTypes::queryPartnerTypes();
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();
        return view('itemDefaults.create', compact(
            'itemCategorys',
            'itemCategory',
            'assertModelIOS',
            'assertModelAndroid',
            'assertImageSlot1',
            'assertImageSlot2',
            'mainTab',
            'subTab',
            'partnerTypes',
            'malls',
            'mallFloors',
            'shops'
        ));
    }

    public function store(ItemDefaultsRequest $request)
    {
        $itemDefault = new ItemDefault($request->all());
        $thumbnail = Setting::where([])->first();

        // dd($request->file('AssertImageSlot1')->getClientOriginalExtension());


        $assetFile = $itemDefault->AssetModel;
        $assetPath = '/public/media/assets/';

        $imageFile = $itemDefault->imageSlots;
        $imagePath = '/public/media/images/';

        $itemAssertModelIOS = $itemDefault->AssertModelIOS;
        $assetIOSPath = '/public/media/ios/itemDefaults/';
        $itemAssertModelAndroid = $itemDefault->AssertModelAndroid;
        $assetAndroidPath = '/public/media/android/itemDefaults/';

        if (isset($itemAssertModelIOS)) {
            $generateAssertModelIOS = uniqid();
            foreach ($itemAssertModelIOS as $assertModelIOS) {
                $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                if (!!$getAssetModelTypeIOS) {
                    $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                    $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                } else {
                    $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                }
            }

            $itemDefault->itemAssertModelIOS = $generateAssertModelIOS;
        }



        if (isset($itemAssertModelAndroid)) {
            $generateAssertModelAndroid = uniqid();
            foreach ($itemAssertModelAndroid as $assertModelAndroid) {
                $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                if (!!$getAssetModelTypeAndroid) {
                    $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                } else {
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                }
            }
            $itemDefault->itemAssertModelAndroid = $generateAssertModelAndroid;
        }


        if (isset($imageFile)) {
            $slots = [];
            foreach ($imageFile as $photo) {
                $imageName = uniqid() . '.' . $photo->getClientOriginalExtension();
                $photo->move(base_path() . $imagePath, $imageName);
                array_push($slots, $imageName);

                Thumbnail::createThumbnailFile($imageName, $thumbnail->width, $thumbnail->height);
            }
            $itemDefault->itemAssertImageSlots = $slots;
        }

        if (isset($assetFile)) {
            $generateName = uniqid();
            foreach ($assetFile as $assets) {
                $getAssetType = $assets->getClientOriginalExtension();

                if (!!$getAssetType) {
                    $assetName = $generateName . '.' . $getAssetType;
                    $assets->move(base_path() . $assetPath, $assetName);
                } else {
                    $assets->move(base_path() . $assetPath, $generateName);
                }
            }
            $itemDefault->itemAssertModel = $generateName;
        }


        $itemDefault->itemName = ConvertValueLanguages::convertData($itemDefault->itemName);
        $itemDefault->itemDetail = ConvertValueLanguages::convertData($itemDefault->itemDetail);
        $itemDefault->itemStoreName = ConvertValueLanguages::convertData($itemDefault->itemStoreName);
        $itemDefault->itemStoreDetail = ConvertValueLanguages::convertData($itemDefault->itemStoreDetail);
        $itemDefault->itemCondition = ConvertValueLanguages::convertData($itemDefault->itemCondition);

        UploadFile::uploadFileNameLogo($request, $itemDefault, $this->imagePath1, "create", 'AssertImageSlot1', 'itemAssertImageSlot1');
        UploadFile::uploadFileNameLogo($request, $itemDefault, $this->imagePath1, "create", 'AssertImageSlot2', 'itemAssertImageSlot2');

        if ($itemDefault->location['coordinates'][0] == null && $itemDefault->location['coordinates'][1] == null) {
            $itemDefault->location = ['coordinates' => [0, 0], 'type' => 'Point'];
        } else {
            $coordinates = [(float) $itemDefault->location['coordinates'][0], (float) $itemDefault->location['coordinates'][1]];
            $itemDefault->location = ['coordinates' => $coordinates, 'type' => 'Point'];
        }

        if ($itemDefault->save()) {

            if (count($request->shopIdArray) > 0) {
                $itemShops = [];
                for ($i = 0; $i < count($request->shopIdArray); $i++) {
                    $array = [];
                    if ($request->shopIdArray[$i] != "") {
                        $array['shopId'] = new \MongoDB\BSON\ObjectId($request->shopIdArray[$i]);
                    }

                    if ($request->mallFloorId[$i] != "") {
                        $array['mallFloorId'] = new \MongoDB\BSON\ObjectId($request->mallFloorId[$i]);
                    }


                    if ($request->mallId[$i] != "") {
                        $array['mallId'] = new \MongoDB\BSON\ObjectId($request->mallId[$i]);
                    }

                    if (count($array)) {

                        $array['created_at'] = new \MongoDB\BSON\UTCDateTime(now());
                        $array['updated_at'] = new \MongoDB\BSON\UTCDateTime(now());
                        $array['itemId'] = new \MongoDB\BSON\ObjectId($itemDefault->id);
                        array_push($itemShops, $array);
                    }
                }

                if (count($itemShops) > 0) {
                    ItemShop::insert($itemShops);
                }
            }

            return redirect("itemDefaults/{$itemDefault->id}/edit")->with('success', trans('itemDefaults/message.success.create'));
        } else {
            return Redirect::route('itemDefaults')->withInput()->with('error', trans('itemDefaults/message.error.create'));
        }
    }

    public function edit(ItemDefault $itemDefault)
    {

        $itemCategorys = ItemCategorys::queryItemCategorys();
        $itemCategory = $itemDefault->itemCategoryId;
        $imageSlot = $itemDefault->itemAssertImageSlots;
        $assertModelIOS = $itemDefault->itemAssertModelIOS;
        $assertModelAndroid = $itemDefault->itemAssertModelAndroid;
        $assertImageSlot1 = $itemDefault->itemAssertImageSlot1;
        $assertImageSlot2 = $itemDefault->itemAssertImageSlot2;
        $mainTab = $this->mainTab;
        $subTab = $this->subTab;
        $partnerTypes = PartnerTypes::queryPartnerTypes();
        // if (!empty($itemDefault->shopId)) {
        //     $shops = Shops::whereIn("_id", $itemDefault->shopId)->get();
        // }
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();

        $page = Input::get('page');

        if ($page == null) {
            $page = 1;
        }

        $numpage = 50;
        $offset = ($page - 1) * $numpage;

        $itemShop = ItemShop::where(['itemId' => new \MongoDB\BSON\ObjectId($itemDefault->id)])->get();
        $data = CouponItem::where(['itemId' => new \MongoDB\BSON\ObjectId($itemDefault->id)])->get();
        $couponItem  = new LengthAwarePaginator($data->slice($offset, $numpage), $data->count(),  $numpage);
        $couponItem->setPath("/itemDefaults/{$itemDefault->id}/edit");
        return view('itemDefaults.edit', compact(
            'itemDefault',
            'itemCategorys',
            'itemCategory',
            'imageSlot',
            'assertModelIOS',
            'assertModelAndroid',
            'assertImageSlot1',
            'assertImageSlot2',
            'mainTab',
            'subTab',
            'partnerTypes',
            'couponItem',
            'malls',
            'mallFloors',
            'shops',
            'itemShop',
            'offset'
        ));
    }

    public function update(ItemDefaultsRequest $request, ItemDefault $itemDefault)
    {
        if ($itemDefault->update($request->all())) {
            $thumbnail = Setting::where([])->first();
            $assetFile = $request->AssetModel;
            $assetPath = '/public/media/assets/';

            $imageFile = $request->imageSlots;
            $imagePath = '/public/media/images/';
            $itemAssertModelIOS = $itemDefault->AssertModelIOS;
            $assetIOSPath = '/public/media/ios/itemDefaults/';
            $itemAssertModelAndroid = $itemDefault->AssertModelAndroid;
            $assetAndroidPath = '/public/media/android/itemDefaults/';

            if (empty($request->mainTab)) {
                $itemDefault->update(['mainTab' => []]);
            }

            if (empty($request->mainTab)) {
                $itemDefault->update(['mainTab' => []]);
            }


            if (empty($request->shopId)) {
                $itemDefault->update(['shopId' => []]);
            }


            if (empty($request->patherTypeId)) {
                $itemDefault->update(['patherTypeId' => []]);
            }

            if (isset($itemAssertModelIOS)) {
                $generateAssertModelIOS = uniqid();
                foreach ($itemAssertModelIOS as $assertModelIOS) {
                    $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                    if (!!$getAssetModelTypeIOS) {
                        $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                        $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                    } else {
                        $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                    }
                }

                $itemDefault->update(['itemAssertModelIOS' => $generateAssertModelIOS]);
            }



            if (isset($itemAssertModelAndroid)) {
                $generateAssertModelAndroid = uniqid();
                foreach ($itemAssertModelAndroid as $assertModelAndroid) {
                    $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                    if (!!$getAssetModelTypeAndroid) {
                        $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                    } else {
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                    }
                }

                $itemDefault->update(['itemAssertModelAndroid' => $generateAssertModelAndroid]);
            }

            if (isset($assetFile)) {
                $generateName = uniqid();
                foreach ($assetFile as $assets) {
                    $getAssetType = $assets->getClientOriginalExtension();

                    if (!!$getAssetType) {
                        $assetName = $generateName . '.' . $getAssetType;
                        $assets->move(base_path() . $assetPath, $assetName);
                    } else {
                        $assets->move(base_path() . $assetPath, $generateName);
                    }
                }
                $itemDefault->update(['itemAssertModel' => $generateName]);
            }

            if (isset($imageFile)) {
                $slots = [];
                foreach ($imageFile as $photo) {
                    $imageName = uniqid() . '.' . $photo->getClientOriginalExtension();
                    $photo->move(base_path() . $imagePath, $imageName);
                    array_push($slots, $imageName);
                    Thumbnail::createThumbnailFile($imageName, $thumbnail->width, $thumbnail->height);
                }
                $itemDefault->update(['itemAssertImageSlots' => $slots]);
            }

            $itemDefault->update(['itemName' => ConvertValueLanguages::convertData($itemDefault->itemName)]);
            $itemDefault->update(['itemDetail' => ConvertValueLanguages::convertData($itemDefault->itemDetail)]);
            $itemDefault->update(['itemStoreName' => ConvertValueLanguages::convertData($itemDefault->itemStoreName)]);
            $itemDefault->update(['itemStoreDetail' => ConvertValueLanguages::convertData($itemDefault->itemStoreDetail)]);
            $itemDefault->update(['itemCondition' => ConvertValueLanguages::convertData($itemDefault->itemCondition)]);


            UploadFile::uploadFileNameLogo($request, $itemDefault, $this->imagePath1, "update", 'AssertImageSlot1', 'itemAssertImageSlot1');
            UploadFile::uploadFileNameLogo($request, $itemDefault, $this->imagePath1, "update", 'AssertImageSlot2', 'itemAssertImageSlot2');

            if ($itemDefault->location['coordinates'][0] == null && $itemDefault->location['coordinates'][1] == null) {
                $itemDefault->location = ['coordinates' => [0, 0], 'type' => 'Point'];
            } else {
                $coordinates = [(float) $itemDefault->location['coordinates'][0], (float) $itemDefault->location['coordinates'][1]];
                $itemDefault->location = ['coordinates' => $coordinates, 'type' => 'Point'];
            }

            ItemShop::where(['itemId' => new \MongoDB\BSON\ObjectId($itemDefault->id)])->delete();
            if (count($request->shopIdArray) > 0) {
                $itemShops = [];
                for ($i = 0; $i < count($request->shopIdArray); $i++) {
                    $array = [];
                    if ($request->shopIdArray[$i] != "") {
                        $array['shopId'] = new \MongoDB\BSON\ObjectId($request->shopIdArray[$i]);
                    }

                    if ($request->mallFloorId[$i] != "") {
                        $array['mallFloorId'] = new \MongoDB\BSON\ObjectId($request->mallFloorId[$i]);
                    }


                    if ($request->mallId[$i] != "") {
                        $array['mallId'] = new \MongoDB\BSON\ObjectId($request->mallId[$i]);
                    }

                    if (count($array)) {

                        $array['created_at'] = new \MongoDB\BSON\UTCDateTime(now());
                        $array['updated_at'] = new \MongoDB\BSON\UTCDateTime(now());
                        $array['itemId'] = new \MongoDB\BSON\ObjectId($itemDefault->id);
                        array_push($itemShops, $array);
                    }
                }

                if (count($itemShops) > 0) {
                    ItemShop::insert($itemShops);
                }
            }

            return redirect("itemDefaults/{$itemDefault->id}/edit")->with('success', trans('itemDefaults/message.success.update'));
        } else {
            return Redirect::route('itemDefaults')->withInput()->with('error', trans('itemDefaults/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $itemDefault = ItemDefault::find($id);

        if ($itemDefault) {
            $itemDefault->delete();
            return redirect('itemDefaults')->with('success', trans('itemDefaults/message.success.destroy'));
        } else {
            return redirect('itemDefaults')->with('error', trans('itemDefaults/message.error.delete'));
        }
    }

    public function data()
    {
        $models = ItemDefault::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (ItemDefault $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
