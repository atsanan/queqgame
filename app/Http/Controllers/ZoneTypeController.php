<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ZoneTypes;
use App\Http\Requests\ZoneTyesRequest;


class ZoneTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Wild_Resource');
    }

    public function index()
    {
        $zoneTypes = ZoneTypes::all();

        return view('zoneTypes.index', compact('zoneTypes'));
    }

    public function create()
    {
        return view('zoneTypes.create');
    }

    public function store(ZoneTyesRequest $request)
    {
        $zoneType = new ZoneTypes($request->all());

        if ($zoneType->save()) {
            return redirect('zoneTypes')->with('success', trans('zoneTypes/message.success.create'));
        } else {
            return Redirect::route('zoneTypes')->withInput()->with('error', trans('zoneTypes/message.error.create'));
        }
    }

    public function edit(ZoneTypes $zoneType)
    {
        return view('zoneTypes.edit', compact('zoneType'));
    }

    public function update(ZoneTyesRequest $request, ZoneTypes $zoneType)
    {
        if ($zoneType->update($request->all())) {
            return redirect('zoneTypes')->with('success', trans('zoneTypes/message.success.update'));
        } else {
            return Redirect::route('zoneTypes')->withInput()->with('error', trans('zoneTypes/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $zoneType = ZoneTypes::find($id);

        if ($zoneType) {
            $zoneType->delete();
            return redirect('zoneTypes')->with('success', trans('zoneTypes/message.success.destroy'));
        } else {
            return redirect('zoneTypes')->with('error', trans('zoneTypes/message.error.delete'));
        }
    }

    public function data()
    {
        $models = ZoneType::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (ZoneType $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
