<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Costumes;

use App\Models\DigitalBoardAsserts;
use App\Http\Requests\CostumesRequest;

class DigitalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $costumes = Costumes::all();

        return view('costumes.index', compact('costumes'));
    }

    public function create()
    {
        $assertModelIOS = null;
        $assertModelAndroid = null;

        return view('costumes.create', compact('assertModelIOS', 'assertModelAndroid'));
    }

    public function store(CostumesRequest $request)
    {
        $costume = new Costumes($request->all());
        $costumeAssertModelIOS = $costume->AssertModelIOS;
        $assetIOSPath = '/public/media/ios/costumes/';
        $costumeAssertModelAndroid = $costume->AssertModelAndroid;
        $assetAndroidPath = '/public/media/android/costumes/';

        if (isset($costumeAssertModelIOS)) {
            $generateAssertModelIOS = uniqid();
            foreach ($costumeAssertModelIOS as $assertModelIOS) {
                $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                if (!!$getAssetModelTypeIOS) {
                    $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                    $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                } else {
                    $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                }
            }

            $costume->custumesAssertModelIOS = $generateAssertModelIOS;
        }



        if (isset($costumeAssertModelAndroid)) {
            $generateAssertModelAndroid = uniqid();
            foreach ($costumeAssertModelAndroid as $assertModelAndroid) {
                $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                if (!!$getAssetModelTypeAndroid) {
                    $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                } else {
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                }
            }
            $costume->custumesAssertModelAndroid = $generateAssertModelAndroid;
        }

        if ($costume->save()) {
            return redirect('costumes')->with('success', trans('costumes/message.success.create'));
        } else {
            return Redirect::route('costumes')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }

    public function edit(DigitalBoardAsserts $costume)
    {
        $assertModelIOS = $costume->custumesAssertModelIOS;
        $assertModelAndroid = $costume->custumesAssertModelAndroid;
        return view('costumes.edit', compact('costume', 'assertModelIOS', 'assertModelAndroid'));
    }

    public function update(CostumesRequest $request, Costumes $costume)
    {
        if ($costume->update($request->all())) {

            $costumeAssertModelIOS = $costume->AssertModelIOS;
            $assetIOSPath = '/public/media/ios/costumes/';
            $costumeAssertModelAndroid = $costume->AssertModelAndroid;
            $assetAndroidPath = '/public/media/android/costumes/';

            if (isset($costumeAssertModelIOS)) {
                $generateAssertModelIOS = uniqid();
                foreach ($costumeAssertModelIOS as $assertModelIOS) {
                    $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                    if (!!$getAssetModelTypeIOS) {
                        $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                        $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                    } else {
                        $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                    }
                }

                $costume->update(['custumesAssertModelIOS' => $generateAssertModelIOS]);
            }



            if (isset($costumeAssertModelAndroid)) {


                $generateAssertModelAndroid = uniqid();
                foreach ($costumeAssertModelAndroid as $assertModelAndroid) {
                    $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                    if (!!$getAssetModelTypeAndroid) {
                        $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                    } else {
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                    }
                }
                $costume->update(['custumesAssertModelAndroid' => $generateAssertModelAndroid]);
            }

            return redirect('costumes')->with('success', trans('costumes/message.success.update'));
        } else {
            return Redirect::route('costumes')->withInput()->with('error', trans('costumes/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $costume = Costumes::find($id);

        if ($costume) {
            $costume->delete();
            return redirect('costumes')->with('success', trans('costumes/message.success.destroy'));
        } else {
            return redirect('costumes')->with('error', trans('costumes/message.error.delete'));
        }
    }

    public function data()
    {
        $models = Costumes::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (Costumes $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
