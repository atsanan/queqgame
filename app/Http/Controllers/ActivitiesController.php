<?php

namespace App\Http\Controllers;

use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\Shops;
use App\Models\ZoneTypes;
use App\Models\Activities;
use App\Helper\UploadFile;
use App\Http\Requests\AcitvitiesRequest;
use App\Helper\ConvertValueLanguages;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class ActivitiesController extends Controller
{
    public $activityTypes = ["Calendar" => "Calendar"];
    public function __construct()
    {
        $this->middleware('auth')->except(['view', 'calender']);
    }

    public function index()
    {
        $search = Input::get('search');
        $data = Activities::with([]);
        $sort = Input::get('sort');
        $page = Input::get('page');
        $field = Input::get('field');
        if ($page == null) {
            $page = 1;
        }

        $offset = ($page - 1) * 50;


        $result = $data->get()->toArray();

        $sortData = SORT_ASC;
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        }

        if (isset($field)) {
            if ($field == "title") {
                $array = array_map(function ($element) {
                    if (isset($element['activityTitle']['eng'])) {
                        return $element['activityTitle']['eng'];
                    }
                }, $result);
            }
        } else {

            $array = array_map(function ($element) {
                if (isset($element['activityTitle']['eng'])) {
                    return $element['activityTitle']['eng'];
                }
            }, $result);
        }
        array_multisort($array, $sortData, $result);
        $activities  = new LengthAwarePaginator(array_slice($result, $offset, 50), count($result), 50);
        $activities->setPath('/activities');
        return view('activities.index', compact(
            'activities',
            'search',
            'field',
            'sort'
        ));
    }

    public function create()
    {

        $zoneTypes = ZoneTypes::queryZoneType();
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();
        $activityTypes = $this->activityTypes;

        return view('activities.create', compact('malls', 'zoneTypes', 'mallFloors', 'shops', 'activityTypes'));
    }

    public function store(AcitvitiesRequest $request)
    {
        $activities = new Activities($request->all());
        $filenameImage = $activities->filenameImage;
        if (isset($filenameImage)) {
            $activities->image =  UploadFile::uploadImage($request, "filenameImage");
        }
        $activities->activityTitle = ConvertValueLanguages::convertData($activities->activityTitle);
        $activities->activityDetail = ConvertValueLanguages::convertData($activities->activityDetail);

        if ($activities->save()) {

            return redirect("activities/{$activities->id}/edit")->with('success', 'Save Activity Success');
        } else {
            return redirect("activities/create")->withInput([])->with('success', 'Save News Success');
        }
    }

    public function edit(Activities $activity)
    {

        $zoneTypes = ZoneTypes::queryZoneType();
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();
        $activityTypes = $this->activityTypes;
        $queryZoneTypes = ZoneTypes::where(['_id' => $activity->zoneTypeId])->first();
        $zoneType = null;
        if ($queryZoneTypes) {
            $zoneType = $queryZoneTypes->zoneTypeName['eng'];
        }

        return view('activities.edit', compact('activity', 'malls', 'zoneTypes', 'zoneType', 'mallFloors', 'shops', 'activityTypes'));
    }


    public function update(AcitvitiesRequest $request, Activities $activity)
    {
        if ($activity->update($request->all())) {

            $filenameImage = $request->filenameImage;
            if (isset($filenameImage)) {
                $activity->update(['image' => UploadFile::uploadImage($request, "filenameImage")]);
            }

            if (empty($request->activityTypes)) {
                $activity->update(['activityTypes' => []]);
            }

            return redirect("activities/{$activity->id}/edit")->with('success', 'Update Activity Success');
        } else {
            return redirect("activities/{$activity->id}/edit")->with('error', 'Error Update Activity');
        }
    }

    public function destroy($id)
    {
        $activities = Activities::find($id);
        if ($activities) {
            $activities->delete();
            return redirect('activities/')->with('success', 'Delete Activity Success');
        } else {
            return redirect('activities/')->with('error', 'Error Delete Activity');
        }
    }

    public function calender()
    {

        $activity = Activities::all();
        $events = [];
        $lang = Input::get('lang');
        if (empty($lang)) {
            $lang = "eng";
        }
        foreach ($activity as $key => $row) {
            $events[] = \Calendar::event(
                $row->activityTitle[$lang], //event title
                false, //full day event?
                $row->startDateTime,
                $row->expiredDateTime,
                0,
                [
                    'color' => $row->color,
                    'url' => url("activities/{$row->id}/view?lang={$lang}"),
                ]
            );
        }
        $calendar = \Calendar::addEvents($events)->setOptions(['height' => 500,]);
        return view('activities.calender', compact('calendar'));
    }

    public function view($id)
    {
        $activity = Activities::find($id);
        $lang = Input::get('lang');
        return view('activities.view', compact('activity', 'lang'));
    }
}
