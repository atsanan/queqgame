<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\MonstersDefaultsTypes;
use App\Http\Requests\MonstersDefaultsTypesRequest;

class MonstersDefaultsTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Resource');
    }

    public function index()
    {
        $monstersDefaultsTypes = MonstersDefaultsTypes::all();

        return view('monstersDefaultsTypes.index', compact('monstersDefaultsTypes'));
    }

    public function create()
    {
        $order = 0;
        return view('monstersDefaultsTypes.create', compact('order'));
    }

    public function store(MonstersDefaultsTypesRequest $request)
    {
        $monstersDefaultsType = new MonstersDefaultsTypes($request->all());

        if ($monstersDefaultsType->save()) {
            return redirect('monstersDefaultsTypes')->with('success', trans('monstersDefaultsTypes/message.success.create'));
        } else {
            return Redirect::route('monstersDefaultsTypes')->withInput()->with('error', trans('monstersDefaultsTypes/message.error.create'));
        }
    }

    public function edit(MonstersDefaultsTypes $monstersDefaultsType)
    {
        $order = $monstersDefaultsType->order;
        return view('monstersDefaultsTypes.edit', compact('monstersDefaultsType', 'order'));
    }

    public function update(MonstersDefaultsTypesRequest $request, MonstersDefaultsTypes $monstersDefaultsType)
    {

        if ($monstersDefaultsType->update($request->all())) {
            return redirect('monstersDefaultsTypes')->with('success', trans('monstersDefaultsTypes/message.success.update'));
        } else {
            return Redirect::route('monstersDefaultsTypes')->withInput()->with('error', trans('monstersDefaultsTypes/message.error.update'));
        }
    }

    public function destroy($id)
    {

        $monstersDefaultsType = MonstersDefaultsTypes::find($id);

        if ($monstersDefaultsType->delete()) {
            return redirect('monstersDefaultsTypes')->with('success', trans('monstersDefaultsTypes/message.success.destroy'));
        } else {
            return Redirect::route('monstersDefaultsTypes')->withInput()->with('error', trans('monstersDefaultsTypes/message.error.delete'));
        }
    }

    public function data()
    {
        $models = monstersDefaultsType::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (MonstersDefaultsTypes $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
