<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Requests\WildItemsRequest;
use App\Helper\ConvertValueLanguages;
use App\Models\UserPlayer;
use App\Models\Players;
use App\Models\MailBox;
use Excel;
use App\Exports\BladeExport;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Player_Resource');
    }

    public function index()
    {
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');

        $field = Input::get('field');
        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;
        $data = UserPlayer::with([]);

        if (isset($search)) {

            if (isset($sort)) {
                $data = $data->where("firstname", "like", "%$search%")
                    ->orWhere("lastname", "like", "%$search%")
                    ->orWhere("email", "like", "%$search%")
                    ->orderBy($field, $sort)->get();
            } else {
                $data = $data->where("firstname", "like", "%$search%")
                    ->orWhere("lastname", "like", "%$search%")
                    ->orWhere("email", "like", "%$search%")
                    ->get();
            }
        } else {
            if (isset($sort)) {
                $data = $data->orderBy($field, $sort)->get();
            } else {
                $data = $data->orderBy("firstname", "asc")->get();
            }
        }
        $user  = new LengthAwarePaginator($data->slice($offset, 50), $data->count(),  50);
        $user->setPath('/user');
        return view('user.index', compact('user', 'field', 'search', 'sort'));
    }

    public function edit(UserPlayer $user)
    {
        $player = Players::where(["userId" => new \MongoDB\BSON\ObjectId($user->id)])->first();
        $mailBox = MailBox::where(["userId" => new \MongoDB\BSON\ObjectId($user->id)])->get();
        return view('user.edit', compact('user', 'player', 'mailBox'));
    }

    public function update(UserPlayer $user, Request $request)
    {
        if ($user->update($request->all())) {
            return redirect('user')->with('success', trans('user/message.success.update'));
        } else {
            return Redirect::route('user')->withInput()->with('error', trans('user/message.error.update'));
        }
    }


    public function excel()
    {
        $user = UserPlayer::all();
        $array = [];

        $tz  = new \DateTimeZone('Asia/Bangkok');
        $gmtTimezone = new \DateTimeZone('GMT');
        foreach ($user as $row) {
            $date = \DateTime::createFromFormat('d,m,Y', $row->birthdate);
            if ($date) {
                $date = $date->diff(new \DateTime('now', $tz))->y;
                $explode = explode(",", $row->birthdate);
                if ($explode[2] == 0) {
                    $date = 0;
                }
            } else {
                $date = 0;
            }
            $createAt = new \DateTime($row->createAt, $gmtTimezone);
            $offsetcreateAt = $tz->getOffset($createAt);
            $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
            $createAt->add($myIntervalcreateAt);

            $lastLogin = new \DateTime($row->lastLogin, $gmtTimezone);
            $offsetlastLogin = $tz->getOffset($lastLogin);
            $myIntervalLastLogin = \DateInterval::createFromDateString((string) $offsetlastLogin . 'seconds');
            $lastLogin->add($myIntervalLastLogin);

            array_push(
                $array,
                array(
                    'firstname' => $row->firstname,
                    'lastName' => $row->lastname,
                    'email' =>  $row->email,
                    'age' => $date,
                    'gender' => $row->gender,
                    'tel' => $row->tel,
                    'authenId' => (string) $row->authenId,
                    'authenType' => ($row->authenTypeId == 1) ? "Facebook" : "Google",
                    'createAt' => $createAt->format('Y-m-d H:i:s'),
                    'lastLogin' => !empty($row->lastLogin) ? $lastLogin->format('Y-m-d H:i:s') : "",
                    'timeLogin' => !empty($row->time) ? $row->time : 0,
                )
            );
        }

        return Excel::download(new BladeExport($array), 'user.xlsx');
    }
}
