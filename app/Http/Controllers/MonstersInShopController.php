<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\MonstersDefaultsTypes;
use App\Http\Requests\MonstersDefaultsTypesRequest;
use App\Models\MonsterPlayer;
use App\Models\MonstersDefault;
use App\Models\Malls;
use Illuminate\Support\Facades\Input;
use App\Models\MallFloor;
use App\Models\Shops;
use App\Http\Requests\MonsterInShopRequest;
use Illuminate\Foundation\Http\FormRequest;
use Excel;
use App\Exports\BladeExport;

class MonstersInShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Report_Resource');
    }

    public function report()
    {
        $search = "";

        $i = 0;
        $mall = Malls::queryMall();
        $mallId = Input::get('mall');
        if (isset($mallId)) {
            $mallId = explode(",", $mallId);
        }

        $mallFloorId = Input::get('mallFloorId');
        if (isset($mallFloorId)) {
            $mallFloorId = explode(",", $mallFloorId);
        }
        $queryMallFloors = MallFloor::where(function ($query) use ($mallId) {
            if (isset($mallId)) {

                foreach ($mallId as $value) {
                    //     //you can use orWhere the first time, dosn't need to be ->where
                    $query->orWhere('mallId', new \MongoDB\BSON\ObjectId($value));
                }
            }
        })->get();
        $mallFloors = [];
        if ($queryMallFloors) {
            foreach ($queryMallFloors as $mallFloor) {
                $mallFloors[$mallFloor->id] = $mallFloor->mallFloorName['eng'];
            }
        }

        $shopId = Input::get('shopId');
        if (isset($shopId)) {
            $shopId = explode(",", $shopId);
        }
        $queryShops = Shops::where(function ($query) use ($mallFloorId) {
            if (isset($mallFloorId)) {
                foreach ($mallFloorId as $value) {
                    //     //     //you can use orWhere the first time, dosn't need to be ->where
                    $query->orWhere('mallFloorId', new \MongoDB\BSON\ObjectId($value));
                }
            }
        })->get();

        $shop = [];
        foreach ($queryShops as $type) {
            $shop[$type->id] = $type->shopName['eng'];
        }


        $monsterInShop = MonsterPlayer::raw(
            function ($collection) use ($search, $mallId, $mallFloorId, $shopId) {
                $query = array(
                    array('$lookup' => array(
                        'from' => 'monsterDefaults',
                        'localField' => 'mDefaultId',
                        'foreignField' => '_id',
                        'as' => 'monsterDefaults'
                    )),
                    array('$unwind' => '$monsterDefaults'),

                    array('$lookup' => array(
                        'from' => 'joinChannels',
                        'localField' => '_id',
                        'foreignField' => 'mPlayerId',
                        'as' => 'joinChannels'
                    )),
                    array('$unwind' => '$joinChannels'),

                    array('$lookup' => array(
                        'from' => 'channels',
                        'localField' => 'joinChannels.channelId',
                        'foreignField' => '_id',
                        'as' => 'channels'
                    )),
                    array('$unwind' => '$channels'),

                    array('$lookup' => array(
                        'from' => 'shops',
                        'localField' => 'channels.shopId',
                        'foreignField' => '_id',
                        'as' => 'shops'
                    )),
                    array('$unwind' => '$shops'),

                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => 'playerId',
                        'foreignField' => '_id',
                        'as' => 'player'
                    )),
                    array('$unwind' => '$player'),

                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'player.userId',
                        'foreignField' => '_id',
                        'as' => 'users'
                    )),
                    array('$unwind' => '$users'),


                    array('$lookup' => array(
                        'from' => 'mallFloors',
                        'localField' => 'shops.mallFloorId',
                        'foreignField' => '_id',
                        'as' => 'mallFloor'
                    )),
                    array('$unwind' => '$mallFloor'),

                    array('$lookup' => array(
                        'from' => 'malls',
                        'localField' => 'mallFloor.mallId',
                        'foreignField' => '_id',
                        'as' => 'mall'
                    )),

                    array('$unwind' => '$mall'),


                );

                if ($mallId != null) {

                    $mall = array();
                    // $mallItem = explode(",", $mallId);
                    foreach ($mallId as $data) {
                        array_push(
                            $mall,
                            array(
                                'mall._id' => new \MongoDB\BSON\ObjectId($data)
                            )
                        );
                    }

                    array_push(
                        $query,
                        array(
                            '$match' => array(
                                '$or' => $mall

                            )

                        )
                    );
                }

                if ($mallFloorId != null) {
                    $mallFloor = array();
                    //  $mallFloorItem = explode(",", $mallFloorId);
                    foreach ($mallFloorId as $data) {
                        array_push(
                            $mallFloor,
                            array(
                                'mallFloor._id' => new \MongoDB\BSON\ObjectId($data)
                            )
                        );
                    }
                    array_push(
                        $query,
                        array('$match' => array(
                            '$or' => $mallFloor

                        ))
                    );
                }


                if ($shopId != null) {
                    $shop = array();
                    //$shopItem = explode(",", $shopId);
                    foreach ($shopId as $data) {
                        array_push(
                            $shop,
                            array(
                                'shops._id' => new \MongoDB\BSON\ObjectId($data)
                            )
                        );
                    }
                    array_push(
                        $query,
                        array('$match' => array(
                            '$or' => $shop
                        ))

                    );
                }
                array_push(
                    $query,
                    array(
                        '$group' => array(
                            '_id' => array('mDefaultId' => '$mDefaultId', 'playerId' => '$playerId'), 'sum' => array('$sum' => 1),
                            'playerId' => array('$first' => '$playerId'),
                            'mPlayerName' => array('$first' => '$mPlayerName'),
                            'monsterDefaults' => array('$first' => '$monsterDefaults'),
                            'shops' => array('$first' => '$shops'),
                            'mallFloor' => array('$first' => '$mallFloor'),
                            'mall' => array('$first' => '$mall'),
                            'users' => array('$first' => '$users'),
                        ),
                    )
                );

                return $collection->aggregate($query);
            }
        );

        $monsterDefaults = MonsterPlayer::raw(function ($collection) use ($search, $mallId, $mallFloorId, $shopId) {


            $query = array(
                array('$lookup' => array(
                    'from' => 'monsterDefaults',
                    'localField' => 'mDefaultId',
                    'foreignField' => '_id',
                    'as' => 'monsterDefaults'
                )),
                array('$unwind' => '$monsterDefaults'),

                array('$lookup' => array(
                    'from' => 'joinChannels',
                    'localField' => '_id',
                    'foreignField' => 'mPlayerId',
                    'as' => 'joinChannels'
                )),
                array('$unwind' => '$joinChannels'),

                array('$lookup' => array(
                    'from' => 'channels',
                    'localField' => 'joinChannels.channelId',
                    'foreignField' => '_id',
                    'as' => 'channels'
                )),
                array('$unwind' => '$channels'),

                array('$lookup' => array(
                    'from' => 'shops',
                    'localField' => 'channels.shopId',
                    'foreignField' => '_id',
                    'as' => 'shops'
                )),
                array('$unwind' => '$shops'),

                array('$lookup' => array(
                    'from' => 'players',
                    'localField' => 'playerId',
                    'foreignField' => '_id',
                    'as' => 'player'
                )),
                array('$unwind' => '$player'),

                array('$lookup' => array(
                    'from' => 'users',
                    'localField' => 'player.userId',
                    'foreignField' => '_id',
                    'as' => 'users'
                )),
                array('$unwind' => '$users'),


                array('$lookup' => array(
                    'from' => 'mallFloors',
                    'localField' => 'shops.mallFloorId',
                    'foreignField' => '_id',
                    'as' => 'mallFloor'
                )),
                array('$unwind' => '$mallFloor'),


                array('$lookup' => array(
                    'from' => 'malls',
                    'localField' => 'mallFloor.mallId',
                    'foreignField' => '_id',
                    'as' => 'mall'
                )),
                array('$unwind' => '$mall'),

            );

            if ($mallId != null) {
                $mall = array();
                //$mallItem = explode(",", $mallId);
                foreach ($mallId as $data) {
                    array_push(
                        $mall,
                        array(
                            'mall._id' => new \MongoDB\BSON\ObjectId($data)
                        )
                    );
                }

                array_push(
                    $query,
                    array(
                        '$match' => array(
                            '$or' => $mall

                        )

                    )
                );
            }
            if ($mallFloorId != null) {
                $mallFloor = array();
                //  $mallFloorItem = explode(",", $mallFloorId);
                foreach ($mallFloorId as $data) {

                    array_push(
                        $mallFloor,
                        array(
                            'mallFloor._id' => new \MongoDB\BSON\ObjectId($data)
                        )
                    );
                }
                array_push(
                    $query,
                    array('$match' => array(
                        '$or' => $mallFloor

                    ))
                );
            }


            if ($shopId != null) {
                $shop = array();
                //   $shopItem = explode(",", $shopId);
                foreach ($shopId as $data) {
                    array_push(
                        $shop,
                        array(
                            'shops._id' => new \MongoDB\BSON\ObjectId($data)
                        )
                    );
                }
                array_push(
                    $query,
                    array('$match' => array(
                        '$or' => $shop
                    ))

                );
            }



            array_push(
                $query,
                array(
                    '$group' => array(
                        '_id' => '$mDefaultId', 'sum' => array('$sum' => 1),
                        'monsterDefaults' => array('$addToSet' => '$monsterDefaults')
                    ),
                )
            );

            return $collection->aggregate($query);
        });


        $stat = [];
        foreach ($monsterDefaults as $data) {

            $stat[] = [
                'sum' => $data->sum,
                'mDefaultName' => $data->monsterDefaults[0]->mDefaultName->eng,
                'mDefaultAssetImageSlot' => $data->monsterDefaults[0]->mDefaultAssetImageSlot
            ];
        }


        return view('monstersInShop.index', compact('monsterInShop', 'stat', 'mall', 'mallId', 'mallFloors', 'mallFloorsId', 'mallFloorId', 'shop', 'shopId'));
    }

    public function store(MonsterInShopRequest $request)
    {
        $mall = "";
        if (isset($request->mallId)) {
            $mall = "&mall=" . implode(",", $request->mallId);
        }
        $mallFloorId = "";
        if (isset($request->mallFloorId)) {
            $mallFloorId = "&mallFloorId=" . implode(",", $request->mallFloorId);
        }
        $shop = "";
        if (isset($request->shopId)) {
            $shop = "&shopId=" . implode(",", $request->shopId);
        }
        return redirect('/report/monsterInShop?' . $mall . $mallFloorId . $shop);
    }

    public function excel()
    {
        $search = "";

        $i = 0;
        $mall = Malls::queryMall();
        $mallId = Input::get('mall');
        if (isset($mallId)) {
            $mallId = explode(",", $mallId);
        }

        $mallFloorId = Input::get('mallFloorId');
        if (isset($mallFloorId)) {
            $mallFloorId = explode(",", $mallFloorId);
        }
        $queryMallFloors = MallFloor::where(function ($query) use ($mallId) {
            if (isset($mallId)) {

                foreach ($mallId as $value) {
                    //     //you can use orWhere the first time, dosn't need to be ->where
                    $query->orWhere('mallId', new \MongoDB\BSON\ObjectId($value));
                }
            }
        })->get();
        $mallFloors = [];
        if ($queryMallFloors) {
            foreach ($queryMallFloors as $mallFloor) {
                $mallFloors[$mallFloor->id] = $mallFloor->mallFloorName['eng'];
            }
        }

        $shopId = Input::get('shopId');
        if (isset($shopId)) {
            $shopId = explode(",", $shopId);
        }
        $queryShops = Shops::where(function ($query) use ($mallFloorId) {
            if (isset($mallFloorId)) {
                foreach ($mallFloorId as $value) {
                    //     //     //you can use orWhere the first time, dosn't need to be ->where
                    $query->orWhere('mallFloorId', new \MongoDB\BSON\ObjectId($value));
                }
            }
        })->get();

        $shop = [];
        foreach ($queryShops as $type) {
            $shop[$type->id] = $type->shopName['eng'];
        }


        $monsterInShop = MonsterPlayer::raw(
            function ($collection) use ($search, $mallId, $mallFloorId, $shopId) {
                $query = array(
                    array('$lookup' => array(
                        'from' => 'monsterDefaults',
                        'localField' => 'mDefaultId',
                        'foreignField' => '_id',
                        'as' => 'monsterDefaults'
                    )),
                    array('$unwind' => '$monsterDefaults'),

                    array('$lookup' => array(
                        'from' => 'joinChannels',
                        'localField' => '_id',
                        'foreignField' => 'mPlayerId',
                        'as' => 'joinChannels'
                    )),
                    array('$unwind' => '$joinChannels'),

                    array('$lookup' => array(
                        'from' => 'channels',
                        'localField' => 'joinChannels.channelId',
                        'foreignField' => '_id',
                        'as' => 'channels'
                    )),
                    array('$unwind' => '$channels'),

                    array('$lookup' => array(
                        'from' => 'shops',
                        'localField' => 'channels.shopId',
                        'foreignField' => '_id',
                        'as' => 'shops'
                    )),
                    array('$unwind' => '$shops'),

                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => 'playerId',
                        'foreignField' => '_id',
                        'as' => 'player'
                    )),
                    array('$unwind' => '$player'),

                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'player.userId',
                        'foreignField' => '_id',
                        'as' => 'users'
                    )),
                    array('$unwind' => '$users'),


                    array('$lookup' => array(
                        'from' => 'mallFloors',
                        'localField' => 'shops.mallFloorId',
                        'foreignField' => '_id',
                        'as' => 'mallFloor'
                    )),
                    array('$unwind' => '$mallFloor'),

                    array('$lookup' => array(
                        'from' => 'malls',
                        'localField' => 'mallFloor.mallId',
                        'foreignField' => '_id',
                        'as' => 'mall'
                    )),

                    array('$unwind' => '$mall'),



                );

                if ($mallId != null) {

                    $mall = array();
                    // $mallItem = explode(",", $mallId);
                    foreach ($mallId as $data) {
                        array_push(
                            $mall,
                            array(
                                'mall._id' => new \MongoDB\BSON\ObjectId($data)
                            )
                        );
                    }

                    array_push(
                        $query,
                        array(
                            '$match' => array(
                                '$or' => $mall

                            )

                        )
                    );
                }

                if ($mallFloorId != null) {
                    $mallFloor = array();
                    //  $mallFloorItem = explode(",", $mallFloorId);
                    foreach ($mallFloorId as $data) {
                        array_push(
                            $mallFloor,
                            array(
                                'mallFloor._id' => new \MongoDB\BSON\ObjectId($data)
                            )
                        );
                    }
                    array_push(
                        $query,
                        array('$match' => array(
                            '$or' => $mallFloor

                        ))
                    );
                }


                if ($shopId != null) {
                    $shop = array();
                    //$shopItem = explode(",", $shopId);
                    foreach ($shopId as $data) {
                        array_push(
                            $shop,
                            array(
                                'shops._id' => new \MongoDB\BSON\ObjectId($data)
                            )
                        );
                    }
                    array_push(
                        $query,
                        array('$match' => array(
                            '$or' => $shop
                        ))

                    );
                }


                // array_push(
                //     $query,

                // );
                return $collection->aggregate($query);
            }
        );



        $array = [];
        if (count($monsterInShop) > 0) {
            foreach ($monsterInShop as $row) {
                array_push(
                    $array,
                    array(
                        'mallName' => $row->mall->mallName->eng,
                        'mallFloorName' => $row->mallFloor->mallFloorName->eng,
                        'shopName' => $row->shops->shopName->eng,
                        'mPlayerName' => $row->mPlayerName,
                        'mDefaultName' => $row->monsterDefaults->mDefaultName->eng,
                        'firstname' => $row->users->firstname,
                        'lastName' => $row->users->lastname,
                        'email' => isset($row->users->email) ? $row->users->email : ""
                    )
                );
            }
        }

        //print_r($array);

        return Excel::download(new BladeExport($array), 'monsterInShop.xlsx');
    }
}
