<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\PrivilegeShops;
use App\Models\Shops;
use App\Models\PrivilegeDefaults;
use Yajra\DataTables\DataTables;
use App\Http\Requests\PrivilegeShopsRequest;
use App\Helper\ConvertValueLanguages;
use Illuminate\Support\Facades\Input;
use App\Helper\Thumbnail;
use App\Models\Setting;
use Illuminate\Pagination\LengthAwarePaginator;
use Excel;
use App\Exports\BladeExport;

class PrivilegeShopsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Privilege_Resource');
    }

    public function index()
    {
        $field = Input::get('field');
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');
        $malls = Malls::queryMall();


        $data = PrivilegeShops::with(['shops', 'privilegeDefaults']);
        $mallId = Input::get('mallId');
        $_mallId = explode(",", $mallId);

        $whereMallId = [];

        if (isset($mallId)) {

            foreach ($_mallId as $value) {
                //     //you can use orWhere the first time, dosn't need to be ->where
                $whereMallId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryMallFloors = MallFloor::whereIn('mallId', $whereMallId)->get();
        } else {
            $queryMallFloors = MallFloor::get();
        }

        $_findMallFloorId = [];
        $mallFloors = [];
        if ($queryMallFloors) {

            foreach ($queryMallFloors as $mallFloor) {
                $mallFloors[$mallFloor->id] = $mallFloor->mallFloorName['eng'];
                $_findMallFloorId[] = new \MongoDB\BSON\ObjectId($mallFloor->id);
            }
            $shop = Shops::whereIn('mallFloorId', $_findMallFloorId)->get();
            $findShopId =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            if (isset($mallId)) {
                $data = $data->whereIn('shopId', $findShopId);
            }
        }

        $mallFloorId = Input::get('mallFloorId');
        $_mallFloorId = explode(",", $mallFloorId);
        if (isset($mallFloorId)) {

            foreach ($_mallFloorId as $value) {
                $whereMallFloorId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryShop = shops::whereIn('mallFloorId', $whereMallFloorId)->get();
        } else {
            $queryShop = Shops::get();
        }

        $findShopId = [];
        foreach ($queryShop as $query) {
            $shops[$query->id] = $query->shopName['eng'];
            $findShopId[] = new \MongoDB\BSON\ObjectId($query->id);
        }
        if (isset($mallFloorId)) {
            $data = $data->whereIn('shopId', $findShopId);
        }

        $shopId = Input::get('shopId');
        $_ShopIdIn = [];
        $_shopId = explode(",", $shopId);
        if (isset($shopId)) {

            foreach ($_shopId as $id) {
                $_ShopIdIn[] = new \MongoDB\BSON\ObjectId($id);
            }
            $data = $data->whereIn('shopId', $_ShopIdIn);
        }

        if (isset($search)) {
            $data = $data->where('privilegeShopTitles.eng', 'like', "%$search%");
            $shop = Shops::where('shopName.eng', 'like', "%$search%")->get();
            $shopId =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('shopId', $shopId);
            $data = $data->orWhere('linkListName', 'like', "%$search%");

            $privilegeDefaults = PrivilegeDefaults::where('privilegeDefaultNames.eng', 'like', "%$search%")->get();
            $privilegeDefaultId =  $privilegeDefaults->map(function ($item) {

                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('privilegeDefaultId', $privilegeDefaultId);
        }
        $data = $data->get()->toArray();
        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);


        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = true;
                }
            } else {
                $data[$i]['isExpired'] = true;
            }
        }

        $index = array_keys(array_column($data, 'isExpired'), false);
        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        $field = Input::get('field');
        if (isset($field)) {
            if ($field == "privilegeDefault") {
                $array = array_map(function ($element) {
                    return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
                }, $data);
            } else  if ($field == "shops") {
                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {
                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else  if ($field == "privilegeShopTitles") {
                $array = array_map(function ($element) {
                    return $element['privilegeShopTitles']['eng'];
                }, $data);
            } else  if ($field == "isReachDateTime") {
                $array = array_map(function ($element) {
                    return $element['isReachDateTime'];
                }, $data);
            } else  if ($field == "isGoldenMinutes") {
                $array = array_map(function ($element) {
                    return $element['isGoldenMinutes'];
                }, $data);
            } else  if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            } else if ($field == "linkListName") {

                $array = array_map(function ($element) {
                    if (isset($element['linkListName'])) {
                        return $element['linkListName'];
                    }
                }, $data);
            }
            if ($field != "isExpired") {
                if (count($index) > 0) {
                    foreach ($index as $i) {
                        unset($data[$i]);
                        unset($array[$i]);
                    }
                }
            }
        } else {
            $array = array_map(function ($element) {
                return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
            }, $data);

            if (count($index) > 0) {
                foreach ($index as $i) {
                    unset($data[$i]);
                    unset($array[$i]);
                }
            }
        }


        array_multisort($array, $sortData, $data);
        $privilegeShops  = new LengthAwarePaginator(array_slice($data, $offset, 50), count($data), 50);

        $privilegeShops->setPath('/privilegeShops');
        return view('privilegeShops.index', compact(
            'privilegeShops',
            'search',
            'field',
            'sort',
            'malls',
            'mallFloors',
            'shops',
            'mallId',
            '_mallId',
            'mallFloorId',
            '_mallFloorId',
            'shopId',
            '_shopId'
        ));
    }

    public function excel()
    {
        $field = Input::get('field');
        $search = Input::get('search');
        $sort = Input::get('sort');
        $page = Input::get('page');
        $data = PrivilegeShops::with(['shops', 'privilegeDefaults']);
        $mallId = Input::get('mallId');
        $_mallId = explode(",", $mallId);

        $whereMallId = [];

        if (isset($mallId)) {

            foreach ($_mallId as $value) {
                //     //you can use orWhere the first time, dosn't need to be ->where
                $whereMallId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryMallFloors = MallFloor::whereIn('mallId', $whereMallId)->get();
        } else {
            $queryMallFloors = MallFloor::get();
        }

        $_findMallFloorId = [];
        if ($queryMallFloors) {

            foreach ($queryMallFloors as $mallFloor) {
                $_findMallFloorId[] = new \MongoDB\BSON\ObjectId($mallFloor->id);
            }
            $shop = Shops::whereIn('mallFloorId', $_findMallFloorId)->get();
            $findShopId =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            if (isset($mallId)) {
                $data = $data->whereIn('shopId', $findShopId);
            }
        }

        $mallFloorId = Input::get('mallFloorId');
        $_mallFloorId = explode(",", $mallFloorId);
        if (isset($mallFloorId)) {

            foreach ($_mallFloorId as $value) {
                $whereMallFloorId[] = new \MongoDB\BSON\ObjectId($value);
            }
            $queryShop = shops::whereIn('mallFloorId', $whereMallFloorId)->get();
        } else {
            $queryShop = Shops::get();
        }

        $findShopId = [];
        foreach ($queryShop as $query) {
            $findShopId[] = new \MongoDB\BSON\ObjectId($query->id);
        }
        if (isset($mallFloorId)) {
            $data = $data->whereIn('shopId', $findShopId);
        }

        $shopId = Input::get('shopId');
        $_ShopIdIn = [];
        $_shopId = explode(",", $shopId);
        if (isset($shopId)) {

            foreach ($_shopId as $id) {
                $_ShopIdIn[] = new \MongoDB\BSON\ObjectId($id);
            }
            $data = $data->whereIn('shopId', $_ShopIdIn);
        }

        if (isset($search)) {
            $data = $data->where('privilegeShopTitles.eng', 'like', "%$search%");
            $shop = Shops::where('shopName.eng', 'like', "%$search%")->get();
            $shopId =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('shopId', $shopId);

            $privilegeDefaults = PrivilegeDefaults::where('privilegeDefaultNames.eng', 'like', "%$search%")->get();
            $privilegeDefaultId =  $privilegeDefaults->map(function ($item) {

                return new \MongoDB\BSON\ObjectId($item->_id);
            });
            $data = $data->orWhereIn('privilegeDefaultId', $privilegeDefaultId);
        }
        $data = $data->get()->toArray();
        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);


        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = true;
                }
            } else {
                $data[$i]['isExpired'] = true;
            }
        }
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        $field = Input::get('field');
        if (isset($field)) {
            if ($field == "privilegeDefault") {
                $array = array_map(function ($element) {
                    return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
                }, $data);
            } else  if ($field == "shops") {
                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {
                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else  if ($field == "privilegeShopTitles") {
                $array = array_map(function ($element) {
                    return $element['privilegeShopTitles']['eng'];
                }, $data);
            } else  if ($field == "isReachDateTime") {
                $array = array_map(function ($element) {
                    return $element['isReachDateTime'];
                }, $data);
            } else  if ($field == "isGoldenMinutes") {
                $array = array_map(function ($element) {
                    return $element['isGoldenMinutes'];
                }, $data);
            } else  if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['privilege_defaults']['privilegeDefaultNames']['eng'];
            }, $data);
        }


        array_multisort($array, $sortData, $data);

        $array = [];
        for ($i = 0; $i < count($data); $i++) {

            array_push(
                $array,
                array(
                    'PrivilegeDefaults' =>
                    isset($data[$i]['privilege_defaults']['privilegeDefaultNames']['eng']) ?
                        $data[$i]['privilege_defaults']['privilegeDefaultNames']['eng'] : "",
                    'Shops' => isset($data[$i]['shops']['shopName']['eng']) ? $data[$i]['shops']['shopName']['eng'] : "",
                    'Title' => $data[$i]['privilegeShopTitles']['eng'],
                    'IsReachDateTime' => $data[$i]['isReachDateTime'],
                    'Continued' => $data[$i]['isExpired'],
                    'IsGoldenMinutes' => $data[$i]['isGoldenMinutes'],
                    'IsActive' => $data[$i]['isActive'],
                )
            );
        }
        return Excel::download(new BladeExport($array), 'privilegeShop.xlsx');
    }

    public function create()
    {

        $shops = $this->queryShop();
        $privilegeDefauls = $this->queryprivilegeDefault();
        $filenameLogo1 = null;
        $filenameLogo2 = null;
        $shopId = Input::get('shopId');
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();

        return view('privilegeShops.create', compact('shops', 'shopId', 'privilegeDefauls', 'malls', 'mallFloors', 'filenameLogo1', 'filenameLogo2'));
    }

    public function store(PrivilegeShopsRequest $request)
    {
        $privilegeShop = new PrivilegeShops($request->all());


        $thumbnail = Setting::where([])->first();
        $imagePath = '/public/media/images/';

        $imageFile1 = $privilegeShop->filenameLogo1;
        $imageFile2 = $privilegeShop->filenameLogo2;
        $imageFile3 = $privilegeShop->filenameLogo3;
        $imageName1 = "";
        $imageName2 = "";
        $imageName3 = "";
        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
            //$privilegeShop->filenameLogo1 = $imageName1;
            $uploadFile = $request->file('filenameLogo1')->move(base_path() . $imagePath, $imageName1);
            if (isset($uploadFile)) {
                Thumbnail::createThumbnailFile($imageName1, $thumbnail->width, $thumbnail->height);
            }
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
            //$privilegeShop->filenameLogo2 = $imageName2;
            $uploadFile2 = $request->file('filenameLogo2')->move(base_path() . $imagePath, $imageName2);
            if (isset($uploadFile2)) {
                Thumbnail::createThumbnailFile($imageName2, $thumbnail->width, $thumbnail->height);
            }
        }

        if (isset($imageFile3)) {
            $imageName3 = uniqid() . '.' . $request->file('filenameLogo3')->getClientOriginalExtension();
            //$privilegeShop->filenameLogo3 = $imageName3;
            $uploadFile3 = $request->file('filenameLogo3')->move(base_path() . $imagePath, $imageName3);
            if (isset($uploadFile3)) {
                Thumbnail::createThumbnailFile($imageName3, $thumbnail->width, $thumbnail->height);
            }
        }

        $recordData = [];

        $privilegeShop->privilegeShopTitles = ConvertValueLanguages::convertData($privilegeShop->privilegeShopTitles);
        $privilegeShop->privilegeShopDetails = ConvertValueLanguages::convertData($privilegeShop->privilegeShopDetails);

        if (count($privilegeShop->privilegeDefaultIdArray) > 0 && count($privilegeShop->shopIdArray) > 0) {
            for ($i = 0; $i < count($privilegeShop->privilegeDefaultIdArray); $i++) {
                for ($j = 0; $j < count($privilegeShop->shopIdArray); $j++) {
                    $recordData[] = [
                        'privilegeShopTitles' => $privilegeShop->privilegeShopTitles,
                        'privilegeShopDetails' => $privilegeShop->privilegeShopDetails,
                        'shopId' => new \MongoDB\BSON\ObjectId($privilegeShop->shopIdArray[$j]),
                        'privilegeDefaultId' => new \MongoDB\BSON\ObjectId($privilegeShop->privilegeDefaultIdArray[$i]),
                        'filenameLogo1' => $imageName1,
                        'filenameLogo2' => $imageName2,
                        'filenameLogo3' => $imageName3,
                        'order' => $privilegeShop->order,
                        'isActive' => $privilegeShop->isActive,
                        'isReachDateTime'  => $privilegeShop->isReachDateTime,
                        'isGoldenMinutes' => $privilegeShop->isGoldenMinutes,
                        'startDateTime'  => new \MongoDB\BSON\UTCDateTime($privilegeShop->startDateTime),
                        'expiredDateTime'  => new \MongoDB\BSON\UTCDateTime($privilegeShop->expiredDateTime),
                        'linkListName' => $privilegeShop->linkListName,
                        'created_at' => new \MongoDB\BSON\UTCDateTime(now()),
                        'updated_at' => new \MongoDB\BSON\UTCDateTime(now()),
                    ];
                }
            }
            PrivilegeShops::insert($recordData);
            return redirect('privilegeShops')->with('success', "Success Create Privilege Shops");
        } else {
            return Redirect::route('privilegeShops')->withInput();
        }

        //$privilegeShop->privilegeShopTitles = ConvertValueLanguages::convertData($privilegeShop->privilegeShopTitles);
        //$privilegeShop->privilegeShopDetails = ConvertValueLanguages::convertData($privilegeShop->privilegeShopDetails);

        // if ($privilegeShop->save()) {

        //     return redirect('shops/' . $privilegeShop->shopId . '/edit')->with('success', trans('privilegeShops/message.success.create'));
        // } else {
        //     return Redirect::route('privilegeShops')->withInput()->with('error', trans('privilegeShops/message.error.create'));
        // }
    }

    public function edit(PrivilegeShops $privilegeShop)
    {

        $shops = $this->queryShop();
        $privilegeDefauls = $this->queryprivilegeDefault();
        $images1 = $privilegeShop->filenameLogo1;
        $images2 = $privilegeShop->filenameLogo2;
        $images3 = $privilegeShop->filenameLogo3;
        $shopId = $privilegeShop->shopId;
        return view('privilegeShops.edit', compact('privilegeShop', 'shopId', 'shops', 'privilegeDefauls', 'images1', 'images2', 'images3'));
    }

    public function update(PrivilegeShopsRequest $request, PrivilegeShops $privilegeShop)
    {
        if ($privilegeShop->update($request->all())) {
            $thumbnail = Setting::where([])->first();

            $imageFile1 = $request->filenameLogo1;
            $imagePath1 = '/public/media/images/';
            $imageFile2 = $request->filenameLogo2;
            $imagePath2 = '/public/media/images/';
            $imageFile3 = $request->filenameLogo3;
            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
                $privilegeShop->update(['filenameLogo1' => $imageName1]);
                $uploadFile =  $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
                if (isset($uploadFile)) {
                    Thumbnail::createThumbnailFile($imageName1, $thumbnail->width, $thumbnail->height);
                }
            }

            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
                $privilegeShop->update(['filenameLogo2' => $imageName2]);
                $uploadFile2 = $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
                if (isset($uploadFile2)) {
                    Thumbnail::createThumbnailFile($imageName2, $thumbnail->width, $thumbnail->height);
                }
            }

            if (isset($imageFile3)) {
                $imageName3 = uniqid() . '.' . $request->file('filenameLogo3')->getClientOriginalExtension();
                $privilegeShop->update(['filenameLogo3' => $imageName3]);
                $uploadFile3 = $request->file('filenameLogo3')->move(base_path() . $imagePath2, $imageName3);
                if (isset($uploadFile3)) {
                    Thumbnail::createThumbnailFile($imageName3, $thumbnail->width, $thumbnail->height);
                }
            }

            $privilegeShop->update(['privilegeShopTitles' => ConvertValueLanguages::convertData($privilegeShop->privilegeShopTitles)]);
            $privilegeShop->update(['privilegeShopDetails' => ConvertValueLanguages::convertData($privilegeShop->privilegeShopDetails)]);

            return redirect('shops/' . $privilegeShop->shopId . '/edit')->with('success', trans('privilegeShops/message.success.update'));
        } else {
            return Redirect::route('privilegeShops')->withInput()->with('error', trans('privilegeShops/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $privilegeShop = PrivilegeShops::find($id);

        if ($privilegeShop) {
            $privilegeShop->delete();
            return redirect('shops/' . $privilegeShop->shopId . '/edit')->with('success', trans('privilegeShops/message.success.destroy'));
        } else {
            return redirect('shops/' . $privilegeShop->shopId . '/edit')->with('error', trans('privilegeShops/message.error.delete'));
        }
    }

    public function data()
    {
        $models = PrivilegeShops::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (PrivilegeShops $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }


    public function queryShop()
    {

        $queryShops = Shops::where([])->get();

        $shops = [];
        foreach ($queryShops as $type) {
            $shops[$type->id] = $type->shopName['eng'];
        }

        return $shops;
    }

    public function queryprivilegeDefault()
    {

        $queryprivilegeDefauls = PrivilegeDefaults::where([])->get();

        $privilegeDefaults = [];
        foreach ($queryprivilegeDefauls as $type) {
            $privilegeDefaults[$type->id] = $type->privilegeDefaultNames['eng'];
        }

        return $privilegeDefaults;
    }

    public function search(Request $request)
    {

        $query = "";
        if ($request->mallId) {
            $query .= "&mallId=" . implode(",", $request->mallId);
        }

        if ($request->mallFloorId) {
            $query .= "&mallFloorId=" . implode(",", $request->mallFloorId);
        }


        if ($request->shopId) {
            $query .= "&shopId=" . implode(",", $request->shopId);
        }

        if ($request->search) {
            $query .= "&search=" . $request->search;
        }


        return redirect('privilegeShops?' . $query);
    }
}
