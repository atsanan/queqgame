<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ShopAsserts;
use App\Http\Requests\ShopAssertsRequest;
use App\Helper\ConvertValueLanguages;

class ShopAssertsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Mall_Resource');
    }

    public function index()
    {
        $shopAsserts = ShopAsserts::all();

        return view('shopAsserts.index', compact('shopAsserts'));
    }

    public function create()
    {
        $fileNameLogos1 = null;
        $fileNameLogos2 = null;
        $assertModelIOS = null;
        $assertModelAndroid = null;

        return view('shopAsserts.create', compact('fileNameLogos1', 'fileNameLogos2', 'assertModelIOS', 'assertModelAndroid'));
    }

    public function store(ShopAssertsRequest $request)
    {
        $shopAssert = new ShopAsserts($request->all());

        $assetFile = $shopAssert->AssetModel;
        $assetPath = '/public/media/assets/';
        $imageFile1 = $shopAssert->fileNameLogo1;
        $imagePath = '/public/media/images/';
        $imageFile2 = $shopAssert->fileNameLogo2;
        $imageFile3 = $shopAssert->fileNameLogo3;

        $shopAssertModelIOS = $shopAssert->AssertModelIOS;
        $assetIOSPath = '/public/media/ios/shopAssert/';
        $shopAssertModelAndroid = $shopAssert->AssertModelAndroid;
        $assetAndroidPath = '/public/media/android/shopAssert/';



        if (isset($shopAssertModelIOS)) {
            $generateAssertModelIOS = uniqid();
            foreach ($shopAssertModelIOS as $assertModelIOS) {
                $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                if (!!$getAssetModelTypeIOS) {
                    $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                    $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                } else {
                    $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                }
            }

            $shopAssert->shopAssertModelIOS = $generateAssertModelIOS;
        } else {
            $shopAssert->shopAssertModelIOS = "";
        }


        if (isset($shopAssertModelAndroid)) {
            $generateAssertModelAndroid = uniqid();
            foreach ($shopAssertModelAndroid as $assertModelAndroid) {
                $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                if (!!$getAssetModelTypeAndroid) {
                    $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                } else {
                    $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                }
            }
            $shopAssert->shopAssertModelAndroid = $generateAssertModelAndroid;
        } else {
            $shopAssert->shopAssertModelAndroid = "";
        }

        if (isset($assetFile)) {
            $generateName = uniqid();
            foreach ($assetFile as $assets) {
                $getAssetType = $assets->getClientOriginalExtension();

                if (!!$getAssetType) {
                    $assetName = $generateName . '.' . $getAssetType;
                    $assets->move(base_path() . $assetPath, $assetName);
                } else {
                    $assets->move(base_path() . $assetPath, $generateName);
                }
            }
            $shopAssert->shopAssertModel = $generateName;
        } else {
            $shopAssert->shopAssertModel = "";
        }

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('fileNameLogo1')->getClientOriginalExtension();
            $shopAssert->fileNameLogo1 = $imageName1;
            $request->file('fileNameLogo1')->move(base_path() . $imagePath, $imageName1);
        } else {
            $shopAssert->fileNameLogo1 = "";
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('fileNameLogo2')->getClientOriginalExtension();
            $shopAssert->fileNameLogo2 = $imageName2;
            $request->file('fileNameLogo2')->move(base_path() . $imagePath, $imageName2);
        } else {
            $shopAssert->fileNameLogo2 = "";
        }

        if (isset($imageFile3)) {
            $imageName3 = uniqid() . '.' . $request->file('fileNameLogo3')->getClientOriginalExtension();
            $shopAssert->fileNameLogo3 = $imageName3;
            $request->file('fileNameLogo3')->move(base_path() . $imagePath, $imageName3);
        } else {
            $shopAssert->fileNameLogo3 = "";
        }


        $shopAssert->shopAssertName = ConvertValueLanguages::convertData($shopAssert->shopAssertName);
        $shopAssert->shopAssertDetail = ConvertValueLanguages::convertData($shopAssert->shopAssertDetail);

        if ($shopAssert->save()) {
            return redirect('shopAsserts')->with('success', trans('shopAsserts/message.success.create'));
        } else {
            return Redirect::route('shopAsserts')->withInput()->with('error', trans('shopAsserts/message.error.create'));
        }
    }

    public function edit(ShopAsserts $shopAssert)
    {
        $fileNameLogos1 = $shopAssert->fileNameLogo1;
        $fileNameLogos2 = $shopAssert->fileNameLogo2;
        $fileNameLogos3 = $shopAssert->fileNameLogo3;
        $assertModelIOS = $shopAssert->shopAssertModelIOS;
        $assertModelAndroid = $shopAssert->shopAssertModelAndroid;
        return view('shopAsserts.edit', compact('shopAssert', 'fileNameLogos1', 'fileNameLogos2', 'fileNameLogos3', 'assertModelIOS', 'assertModelAndroid'));
    }

    public function update(ShopAssertsRequest $request, ShopAsserts $shopAssert)
    {
        $float = floatval($request->shopAssertNameVersion);
        $shopAssert->shopAssertNameVersion = $float;

        if ($shopAssert->update($request->all())) {

            $assetFile = $request->AssetModel;
            $assetPath = '/public/media/assets/';
            $imageFile1 = $request->fileNameLogo1;
            $imagePath = '/public/media/images/';
            $imageFile2 = $request->fileNameLogo2;
            $imageFile3 = $request->fileNameLogo3;
            $shopAssertModelIOS = $shopAssert->AssertModelIOS;
            $assetIOSPath = '/public/media/ios/shopAssert/';
            $shopAssertModelAndroid = $shopAssert->AssertModelAndroid;
            $assetAndroidPath = '/public/media/android/shopAssert/';



            if (isset($shopAssertModelIOS)) {
                $generateAssertModelIOS = uniqid();
                foreach ($shopAssertModelIOS as $assertModelIOS) {
                    $getAssetModelTypeIOS = $assertModelIOS->getClientOriginalExtension();
                    if (!!$getAssetModelTypeIOS) {
                        $assetModelNameIOS = $generateAssertModelIOS . '.' . $getAssetModelTypeIOS;
                        $assertModelIOS->move(base_path() . $assetIOSPath, $assetModelNameIOS);
                    } else {
                        $assertModelIOS->move(base_path() . $assetIOSPath, $generateAssertModelIOS);
                    }
                }

                $shopAssert->update(['shopAssertModelIOS' => $generateAssertModelIOS]);
            }


            if (isset($shopAssertModelAndroid)) {
                $generateAssertModelAndroid = uniqid();
                foreach ($shopAssertModelAndroid as $assertModelAndroid) {
                    $getAssetModelTypeAndroid = $assertModelAndroid->getClientOriginalExtension();
                    if (!!$getAssetModelTypeAndroid) {
                        $assetModelNameAndroid = $generateAssertModelAndroid . '.' . $getAssetModelTypeAndroid;
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $assetModelNameAndroid);
                    } else {
                        $assertModelAndroid->move(base_path() . $assetAndroidPath, $generateAssertModelAndroid);
                    }
                }

                $shopAssert->update(['shopAssertModelAndroid' => $generateAssertModelAndroid]);
            }
            if (isset($assetFile)) {
                $generateName = uniqid();
                foreach ($assetFile as $assets) {
                    $getAssetType = $assets->getClientOriginalExtension();

                    if (!!$getAssetType) {
                        $assetName = $generateName . '.' . $getAssetType;
                        $assets->move(base_path() . $assetPath, $assetName);
                    } else {
                        $assets->move(base_path() . $assetPath, $generateName);
                    }
                }
                $shopAssert->update(['shopAssertModel' => $generateName]);
            }

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('fileNameLogo1')->getClientOriginalExtension();
                $shopAssert->update(['fileNameLogo1' => $imageName1]);
                $request->file('fileNameLogo1')->move(base_path() . $imagePath, $imageName1);
            }

            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('fileNameLogo2')->getClientOriginalExtension();
                $shopAssert->update(['fileNameLogo2' => $imageName2]);
                $request->file('fileNameLogo2')->move(base_path() . $imagePath, $imageName2);
            }

            if (isset($imageFile3)) {
                $imageName3 = uniqid() . '.' . $request->file('fileNameLogo3')->getClientOriginalExtension();
                $shopAssert->update(['fileNameLogo3' => $imageName3]);
                $request->file('fileNameLogo3')->move(base_path() . $imagePath, $imageName3);
            }


            $shopAssert->update(['shopAssertName' => ConvertValueLanguages::convertData($shopAssert->shopAssertName)]);
            $shopAssert->update(['shopAssertDetail' => ConvertValueLanguages::convertData($shopAssert->shopAssertDetail)]);

            return redirect('shopAsserts')->with('success', trans('shopAsserts/message.success.update'));
        } else {
            return Redirect::route('shopAsserts')->withInput()->with('error', trans('shopAsserts/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $shopAssert = ShopAsserts::find($id);

        if ($shopAssert) {
            $shopAssert->delete();
            return redirect('shopAsserts')->with('success', trans('shopAsserts/message.success.destroy'));
        } else {
            return redirect('shopAsserts')->with('error', trans('shopAsserts/message.error.delete'));
        }
    }

    public function data()
    {
        $models = ShopAsserts::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (ShopAsserts $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
