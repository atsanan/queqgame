<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\WildMonster;
use App\Models\MonstersDefault;
use App\Models\MallFloor;
use App\Models\Malls;
use App\Models\ItemDefault;
use App\Models\Shops;
use App\Models\ZoneTypes;
use App\Http\Requests\WildMonstersRequest;
use Excel;
use App\Exports\BladeExport;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Setting;

class WildMonstersController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except(['locationZone']);
        $this->middleware('role:Wild_Resource', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']])
            ->except(['locationZone']);
        $this->middleware('role:Report_Resource', ['only' => ['report', 'excel', 'excelInstantiate', 'instantiate']])
            ->except(['locationZone']);
    }

    public function index()
    {
        $search = Input::get('search');
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);

        if (isset($search)) {
            $shop = Shops::where('shopName.eng', 'like', "%$search%")->get();
            $toShop =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $monstersDefault = MonstersDefault::where('mDefaultName.eng', 'like', "%$search%")->get();
            $mDefaultId =  $monstersDefault->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $zoneTypes = ZoneTypes::where('zoneTypeName.eng', 'like', "%$search%")->get();
            $zoneTypeId =  $zoneTypes->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $mallFloorName = MallFloor::where('mallFloorName.eng', 'like', "%$search%")->get();
            $toMallFloor =  $mallFloorName->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $data = WildMonster::with(['shops', 'mallfloors', 'monstersdefaults', 'zonetypes']);
            $data = $data->whereIn('toShop', $toShop)
                ->orWhereIn('mDefaultId', $mDefaultId)
                ->orWhereIn('zoneTypeId', $zoneTypeId)
                ->orWhereIn('toMallFloor', $toMallFloor)
                ->get()->toArray();
        } else {
            $data = WildMonster::with(['shops', 'mallfloors', 'monstersdefaults', 'zonetypes'])
                ->get()->toArray();
        }

        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);

        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = ($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = true;
                }
            } else {
                $data[$i]['isExpired'] = true;
            }
        }
        $index = array_keys(array_column($data, 'isExpired'), false);
        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }

        if (isset($field)) {
            if ($field == "monstersdefaults") {
                $array = array_map(function ($element) {
                    if (isset($element['monstersdefaults']['mDefaultName']['eng'])) {
                        return $element['monstersdefaults']['mDefaultName']['eng'];
                    }
                }, $data);
            } else if ($field == "count") {
                $array = array_map(function ($element) {
                    return $element['count'];
                }, $data);
            } else if ($field == "mallFloor") {
                $array = array_map(function ($element) {
                    if (isset($element['mallfloors']['mallFloorName']['eng'])) {
                        return $element['mallfloors']['mallFloorName']['eng'];
                    }
                }, $data);
            } else if ($field == "shop") {
                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {
                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else if ($field == "zoneType") {
                $array = array_map(function ($element) {
                    if (isset($element['zonetypes']['zoneTypeName']['eng'])) {
                        return $element['zonetypes']['zoneTypeName']['eng'];
                    }
                }, $data);
            } else if ($field == "zoneType") {
                $array = array_map(function ($element) {
                    if (isset($element['zonetypes']['zoneTypeName']['eng'])) {
                        return $element['zonetypes']['zoneTypeName']['eng'];
                    }
                }, $data);
            } else if ($field == "isGoldenMinutes") {
                $array = array_map(function ($element) {
                    return $element['isGoldenMinutes'];
                }, $data);
            } else if ($field == "isReachDateTime") {
                $array = array_map(function ($element) {
                    return $element['isReachDateTime'];
                }, $data);
            } else if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            }

            if ($field != "isExpired") {
                if (count($index) > 0) {
                    foreach ($index as $i) {
                        unset($data[$i]);
                        unset($array[$i]);
                    }
                }
            }
        } else {
            $array = array_map(function ($element) {
                if (isset($element['monstersdefaults']['mDefaultName']['eng'])) {
                    return $element['monstersdefaults']['mDefaultName']['eng'];
                }
            }, $data);

            if ($field != "isExpired") {
                if (count($index) > 0) {
                    foreach ($index as $i) {
                        unset($data[$i]);
                        unset($array[$i]);
                    }
                }
            }
        }

        array_multisort($array, $sortData, $data);


        $wildMonsters  =  new LengthAwarePaginator(array_slice($data,  $offset, 50), count($data), 50);
        $wildMonsters->setPath('/wildMonsters');

        return view('wildMonsters.index', compact('wildMonsters', 'search', 'field', 'sort'));
    }

    public function create()
    {
        $monstersDefaults = $this->querymonstersDefault();
        $malls = $this->queryMall();
        $mallFloors = $this->queryMallFloor();
        $shops = $this->queryShop(null);
        $zoneTypes = $this->queryZoneType();
        $zoneType = null;
        $mall = null;
        $itemDefault = ItemDefault::queryItem();
        return view('wildMonsters.create', compact('monstersDefaults', 'mall', 'malls', 'mallFloors', 'shops', 'zoneTypes', 'zoneType', 'itemDefault'));
    }

    public function store(WildMonstersRequest $request)
    {
        $wildMonster = new WildMonster($request->all());

        if ($wildMonster->save()) {
            return redirect('wildMonsters')->with('success', trans('Success Create Wild Monster'));
        } else {
            return Redirect::route('wildMonsters')->withInput()->with('error', trans('wildMonsters/message.error.create'));
        }
    }

    public function edit(WildMonster $wildMonster)
    {
        $monstersDefaults = $this->querymonstersDefault();
        $malls = $this->queryMall();

        $mallFloors = $this->queryMallFloor($wildMonster->toMall);
        $shops = $this->queryShop($wildMonster->toMallFloor);
        $zoneTypes = $this->queryZoneType();
        $itemDefault = ItemDefault::queryItem();
        $queryZoneTypes = ZoneTypes::where(['_id' => $wildMonster->zoneTypeId])->first();
        if ($queryZoneTypes) {
            $zoneType = $queryZoneTypes->zoneTypeName['eng'];
        }

        return view('wildMonsters.edit', compact(
            'malls',
            'wildMonster',
            'monstersDefaults',
            'mallFloors',
            'shops',
            'zoneTypes',
            'itemDefault',
            'zoneType'
        ));
    }

    public function update(WildMonstersRequest $request, WildMonster $wildMonster)
    {
        if ($wildMonster->update($request->all())) {
            return redirect('wildMonsters/' . $wildMonster->id . '/edit')->with('success', trans('Success Update Wild Monster'));
        } else {
            return Redirect::route('wildMonsters')->withInput()->with('error', trans('wildMonsters/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $wildMonster = WildMonster::find($id);

        if ($wildMonster) {
            $wildMonster->delete();
            return redirect('wildMonsters')->with('success', trans('wildMonsters/message.success.destroy'));
        } else {
            return redirect('wildMonsters')->with('error', trans('wildMonsters/message.error.delete'));
        }
    }

    public function data()
    {
        $models = WildMonster::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (WildMonster $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function queryMonstersDefault()
    {

        $queryMonstersDefaults = MonstersDefault::where([])->get();
        $monstersDefaults = [];
        foreach ($queryMonstersDefaults as $type) {
            $monstersDefaults[$type->id] = $type->mDefaultName['eng'];
        }

        return $monstersDefaults;
    }

    public function queryMall()
    {

        $queryMalls = Malls::where([])->orderBy('mallName.eng', 'asc')->get();
        $mall = [];
        foreach ($queryMalls as $type) {
            $mall[$type->id] = $type->mallName['eng'];
        }

        return $mall;
    }

    public function queryMallFloor($toMall = null)
    {
        if ($toMall != null) {
            $queryMallFloors = MallFloor::where(['mallId' => new \MongoDB\BSON\ObjectId($toMall)])->orderBy('mallFloorName.eng', 'asc')->get();
        } else {

            $queryMallFloors = MallFloor::where([])->orderBy('mallFloorName.eng', 'asc')->get();
        }


        $mallFloors = [];
        foreach ($queryMallFloors as $type) {
            $mallFloors[$type->id] = $type->mallFloorName['eng'];
        }

        return $mallFloors;
    }

    public function queryShop($toMallFloor)
    {

        if ($toMallFloor != null) {
            $queryShops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId($toMallFloor)])->orderBy('shopName.eng', 'asc')->get();
        } else {
            $queryShops = Shops::where([])->orderBy('shopName.eng', 'desc')->get();
        }
        $shops = [];
        foreach ($queryShops as $type) {
            $shops[$type->id] = $type->shopName['eng'];
        }
        return $shops;
    }

    public function queryZoneType()
    {

        $queryZoneTypes = ZoneTypes::where([])->get();
        $zoneTypes = [];
        foreach ($queryZoneTypes as $type) {
            $zoneTypes[$type->id] = $type->zoneTypeName['eng'];
        }
        return $zoneTypes;
    }


    public function report()
    {
        $search = "";
        $wildMonsters = WildMonster::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'zonetypes',
                        'localField' => 'zoneTypeId',
                        'foreignField' => '_id',
                        'as' => 'zonetypes'
                    )),
                    array('$unwind' => '$zonetypes'),

                    array('$match' => array('zonetypes.zoneTypeName.eng' => 'World')),
                )
            );
        });

        return view('wildMonsters.report', compact('wildMonsters'));
    }

    public function excel()
    {
        $search = "";
        $wildMonsters =  WildMonster::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'zonetypes',
                        'localField' => 'zoneTypeId',
                        'foreignField' => '_id',
                        'as' => 'zonetypes'
                    )),
                    array('$unwind' => '$zonetypes'),

                    array('$match' => array('zonetypes.zoneTypeName.eng' => 'World')),
                )
            );
        });


        $array = [];
        if (count($wildMonsters) > 0) {
            foreach ($wildMonsters as $row) {
                array_push(
                    $array,
                    array(
                        'mDefaultName' => $row->monstersDefaults->mDefaultName['eng'],
                        'count' => $row->count,
                        'countMax' => $row->countMax,
                        'isPickToBag' => ($row->isPickToBag) ? "true" : "false",
                        'itemName' => $row->item->itemName['eng']
                    )
                );
            }
        }

        return Excel::download(new BladeExport($array), 'wildMonster.xlsx');
    }

    public function instantiate()
    {
        $search = "";
        $wildMonsters = WildMonster::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'zonetypes',
                        'localField' => 'zoneTypeId',
                        'foreignField' => '_id',
                        'as' => 'zonetypes'
                    )),
                    array('$unwind' => '$zonetypes'),
                    array('$lookup' => array(
                        'from' => 'itemDefaults',
                        'localField' => 'itemId',
                        'foreignField' => '_id',
                        'as' => 'item'
                    )),
                    array('$unwind' => '$item'),
                    //array('$match' => array('zonetypes.zoneTypeName.eng' => 'World')),
                )
            );
        });
        return view('wildMonsters.instantiate', compact('wildMonsters'));
    }

    public function excelInstantiate()
    {
        $search = "";
        $wildMonsters =  WildMonster::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'zonetypes',
                        'localField' => 'zoneTypeId',
                        'foreignField' => '_id',
                        'as' => 'zonetypes'
                    )),
                    array('$unwind' => '$zonetypes'),
                    array('$lookup' => array(
                        'from' => 'itemDefaults',
                        'localField' => 'itemId',
                        'foreignField' => '_id',
                        'as' => 'item'
                    )),
                    array('$unwind' => '$item'),
                    //array('$match' => array('zonetypes.zoneTypeName.eng' => 'World')),
                )
            );
        });


        $array = [];
        if (count($wildMonsters) > 0) {
            foreach ($wildMonsters as $row) {
                array_push(
                    $array,
                    array(
                        'monsterDefaultName' => $row->monstersDefaults->mDefaultName['eng'],
                        'count' => $row->count,
                        'countMax' => $row->countMax,
                        'isPickToBag' => ($row->isPickToBag) ? "true" : "false",
                        'itemName' => $row->item->itemName['eng'],
                        'zoneTypes' => $row->zoneTypes->zoneTypeName['eng'],
                        'mall' => $row->malls->mallName['eng'],
                        'mallFloor' => $row->mallFloors->mallFloorName['eng'],
                        'shops' => $row->Shops->shopName['eng']
                    )
                );
            }
        }

        return Excel::download(new BladeExport($array), 'monstersInstantiate.xlsx');
    }

    public function excelIndex()
    {
        $search = Input::get('search');
        $field = Input::get('field');
        $page = Input::get('page');
        $sort = Input::get('sort');
        if ($page == null) {
            $page = 1;
        }

        $offset = 50 * ($page - 1);

        if (isset($search)) {
            $shop = Shops::where('shopName.eng', 'like', "%$search%")->get();
            $toShop =  $shop->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $monstersDefault = MonstersDefault::where('mDefaultName.eng', 'like', "%$search%")->get();
            $mDefaultId =  $monstersDefault->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $zoneTypes = ZoneTypes::where('zoneTypeName.eng', 'like', "%$search%")->get();
            $zoneTypeId =  $zoneTypes->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $mallFloorName = MallFloor::where('mallFloorName.eng', 'like', "%$search%")->get();
            $toMallFloor =  $mallFloorName->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $data = WildMonster::with(['shops', 'mallfloors', 'monstersdefaults', 'zonetypes']);
            $data = $data->whereIn('toShop', $toShop)
                ->orWhereIn('mDefaultId', $mDefaultId)
                ->orWhereIn('zoneTypeId', $zoneTypeId)
                ->orWhereIn('toMallFloor', $toMallFloor)
                ->get()->toArray();
        } else {
            $data = WildMonster::with(['shops', 'mallfloors', 'monstersdefaults', 'zonetypes'])
                ->get()->toArray();
        }

        $userTimezone = new \DateTimeZone("Asia/Bangkok");
        $gmtTimezone = new \DateTimeZone('GMT');
        $date = new \DateTime();
        $offsetcreateAt = $userTimezone->getOffset($date);
        $myIntervalcreateAt = \DateInterval::createFromDateString((string) $offsetcreateAt . 'seconds');
        $date->add($myIntervalcreateAt);

        for ($i = 0; $i < count($data); $i++) {
            if (isset($data[$i]['isReachDateTime'])) {
                if ($data[$i]['isReachDateTime']) {
                    $expiredDateTime = new \DateTime($data[$i]['expiredDateTime']);
                    $expiredDateTime->setTime(23, 59, 59);
                    $data[$i]['isExpired'] = !($date->format('Y-m-d H:i:s') <= $expiredDateTime->format('Y-m-d H:i:s'));
                } else {
                    $data[$i]['isExpired'] = false;
                }
            } else {
                $data[$i]['isExpired'] = false;
            }
        }

        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }

        $index = array_keys(array_column($data, 'isExpired'), false);

        if (isset($field)) {
            if ($field == "monstersdefaults") {
                $array = array_map(function ($element) {
                    if (isset($element['monstersdefaults']['mDefaultName']['eng'])) {
                        return $element['monstersdefaults']['mDefaultName']['eng'];
                    }
                }, $data);
            } else if ($field == "count") {
                $array = array_map(function ($element) {
                    return $element['count'];
                }, $data);
            } else if ($field == "mallFloor") {
                $array = array_map(function ($element) {
                    if (isset($element['mallfloors']['mallFloorName']['eng'])) {
                        return $element['mallfloors']['mallFloorName']['eng'];
                    }
                }, $data);
            } else if ($field == "shop") {
                $array = array_map(function ($element) {
                    if (isset($element['shops']['shopName']['eng'])) {
                        return $element['shops']['shopName']['eng'];
                    }
                }, $data);
            } else if ($field == "zoneType") {
                $array = array_map(function ($element) {
                    if (isset($element['zonetypes']['zoneTypeName']['eng'])) {
                        return $element['zonetypes']['zoneTypeName']['eng'];
                    }
                }, $data);
            } else if ($field == "zoneType") {
                $array = array_map(function ($element) {
                    if (isset($element['zonetypes']['zoneTypeName']['eng'])) {
                        return $element['zonetypes']['zoneTypeName']['eng'];
                    }
                }, $data);
            } else if ($field == "isGoldenMinutes") {
                $array = array_map(function ($element) {
                    return $element['isGoldenMinutes'];
                }, $data);
            } else if ($field == "isReachDateTime") {
                $array = array_map(function ($element) {
                    return $element['isReachDateTime'];
                }, $data);
            } else if ($field == "isActive") {
                $array = array_map(function ($element) {
                    return $element['isActive'];
                }, $data);
            } else if ($field == "isExpired") {
                $array = array_map(function ($element) {
                    if (isset($element['isExpired'])) {
                        return $element['isExpired'];
                    }
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                if (isset($element['monstersdefaults']['mDefaultName']['eng'])) {
                    return $element['monstersdefaults']['mDefaultName']['eng'];
                }
            }, $data);
        }

        array_multisort($array, $sortData, $data);
        $array = [];
        for ($i = 0; $i < count($data); $i++) {

            array_push(
                $array,
                array(
                    'monstersDefaults' => $data[$i]['monstersdefaults']['mDefaultName']['eng'],
                    'mallFloors' => isset($data[$i]['mallfloors']['mallFloorName']['eng']) ? $data[$i]['mallfloors']['mallFloorName']['eng'] : "",
                    'Count' => $data[$i]['count'],
                    'Shops' => isset($data[$i]['shops']['shopName']['eng']) ? $data[$i]['shops']['shopName']['eng'] : "",
                    'zoneTypes' => isset($data[$i]['zonetypes']['zoneTypeName']['eng']) ? $data[$i]['zonetypes']['zoneTypeName']['eng'] : "",
                    'IsGoldenMinutes' => $data[$i]['isGoldenMinutes'],
                    'IsReachDateTime' => $data[$i]['isReachDateTime'],
                    'Continued' => $data[$i]['isExpired'],
                    'IsActive' => $data[$i]['isActive'],
                )
            );
        }
        return Excel::download(new BladeExport($array), 'wildMonster.xlsx');
    }

    public function locationZone()
    {
        $setting = Setting::where([])->first();
        return response()->json($setting->locationGameZone);
    }
}
