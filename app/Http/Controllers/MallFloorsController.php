<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\MallFloor;
use App\Models\Malls;
use App\Models\Shops;
use App\Http\Requests\MallFloorsRequest;
use Illuminate\Support\Facades\Input;
use App\Helper\ConvertValueLanguages;
use Illuminate\Pagination\LengthAwarePaginator;

class MallFloorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['search']);
        $this->middleware('role:Mall_Resource')->except(['search']);
    }

    public function index()
    {

        $search = Input::get('search');
        $data = MallFloor::with(['malls']);
        $sort = Input::get('sort');
        $page = Input::get('page');

        $field = Input::get('field');
        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;


        if (isset($search)) {
            $mall = Malls::where('mallName.eng', 'like', "%$search%")->get();
            $mallId =  $mall->map(function ($item) {
                return new \MongoDB\BSON\ObjectId($item->_id);
            });

            $data = $data->where("mallFloorName.eng", "like", "%$search%")
                ->orWhere("mapIndoorFloorKey", "like", "%$search%")
                ->orWhere("mapIndoorFloorData", "like", "%$search%")
                ->orWhere("mallId", $mallId);
        }

        $data = $data->get()->toArray();


        if (isset($sort)) {
            if ($sort == "desc") {
                $sortData = SORT_DESC;
            } else {
                $sortData = SORT_ASC;
            }
        } else {
            $sortData = SORT_ASC;
        }
        $field = Input::get('field');
        if (isset($field)) {
            if ($field == "mallFloorName") {
                $array = array_map(function ($element) {
                    return $element['mallFloorName']['eng'];
                }, $data);
            } else if ($field == "mallName") {
                $array = array_map(function ($element) {
                    if (isset($element['malls']['mallName']['eng'])) {

                        return $element['malls']['mallName']['eng'];
                    }
                }, $data);
            } else if ($field == "mapIndoorFloorKey") {
                $array = array_map(function ($element) {
                    return $element['mapIndoorFloorKey'];
                }, $data);
            } else {
                $array = array_map(function ($element) {
                    return $element['mapIndoorFloorData'];
                }, $data);
            }
        } else {
            $array = array_map(function ($element) {
                return $element['mallFloorName']['eng'];
            }, $data);
        }
        array_multisort($array, $sortData, $data);
        $mallFloors  =  new LengthAwarePaginator(array_slice($data, $offset, 50), count($data), 50);
        $mallFloors->setPath('/mallFloors');


        return view('mallFloors.index', compact('mallFloors', 'search', 'field', 'sort'));
    }

    public function create()
    {
        $mallId = Input::get('mallId');
        $mall = $this->queryMall();

        return view('mallFloors.create', compact('mall', 'mallId'));
    }



    public function store(MallFloorsRequest $request)
    {
        $mallFloor = new MallFloor($request->all());
        $mallFloor->mallFloorName = ConvertValueLanguages::convertData($mallFloor->mallFloorName);
        $mallFloor->mallFloorDetail = ConvertValueLanguages::convertData($mallFloor->mallFloorDetail);
        if ($mallFloor->save()) {
            return redirect('malls/' . $mallFloor->mallId . '/edit')->with('success', trans('mallFloors/message.success.create'));
        } else {
            return Redirect::route('malls/' . $mallFloor->mallId . '/edit')->withInput()->with('error', trans('mallFloors/message.error.create'));
        }
    }

    public function edit(MallFloor $mallFloor)
    {

        $mall = $this->queryMall();
        $mallId = $mallFloor->mallId;
        $shops = Shops::where(['mallFloorId' => new \MongoDB\BSON\ObjectId($mallFloor->id)])->get();

        return view('mallFloors.edit', compact('mallFloor', 'shops', 'mall', 'mallId'));
    }

    public function update(MallFloorsRequest $request, MallFloor $mallFloor)
    {
        if ($mallFloor->update($request->all())) {
            $mallFloor->update(['mallFloorName' => ConvertValueLanguages::convertData($mallFloor->mallFloorName)]);
            $mallFloor->update(['mallFloorDetail' => ConvertValueLanguages::convertData($mallFloor->mallFloorDetail)]);
            return redirect('malls/' . $mallFloor->mallId . '/edit')->with('success', trans('mallFloors/message.success.update'));
        } else {
            return Redirect::route('malls/' . $mallFloor->mallId . '/edit')->withInput()->with('error', trans('mallFloors/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $mallFloor = MallFloor::find($id);

        if ($mallFloor) {
            $mallFloor->delete();
            return redirect('malls/' . $mallFloor->mallId . '/edit')->with('success', trans('mallFloors/message.success.destroy'));
        } else {
            return redirect('malls/' . $mallFloor->mallId . '/edit')->withInput()->with('error', trans('mallFloors/message.error.delete'));
        }
    }

    public function data()
    {
        $models = MallFloor::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (MallFloor $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function queryMall()
    {

        $queryMalls = Malls::where([])->orderBy('mallName.eng', 'desc')->get();

        $malls = [];
        if ($queryMalls) {
            foreach ($queryMalls as $mall) {
                $malls[$mall->id] = $mall->mallName['eng'];
            }
        }

        return $malls;
    }

    public function show(Request $request)
    {
        $mallId = Input::get('mallId');
        $mallFloors = MallFloor::where(['mallId' => new \MongoDB\BSON\ObjectId($mallId)])->orderBy('mallFloorName.eng', 'asc')->get();
        $data = view('mallFloors.option', compact('mallFloors'))->render();
        return response()->json(['options' => $data]);
    }

    public function searchByMall(Request $request)
    {
        $mallId = Input::get('mallId');
        $mallFloors = MallFloor::where(['mallId' => new \MongoDB\BSON\ObjectId($mallId)])->orderBy('mallFloorName.eng', 'asc')->get();
        $data = view('mallFloors.option', compact('mallFloors'))->render();
        return response()->json(['options' => $data]);
    }

    public function search(Request $request)
    {
        $mallFloors = MallFloor::where(function ($query) use ($request) {
            foreach ($request->mallId as $value) {
                //     //you can use orWhere the first time, dosn't need to be ->where
                $query->orWhere('mallId', new \MongoDB\BSON\ObjectId($value));
            }
        })->get();
        $data = view('mallFloors.option', compact('mallFloors'))->render();

        return response()->json(['options' => $data]);
    }
}
