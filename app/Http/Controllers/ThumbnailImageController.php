<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Setting;
use App\Http\Requests\ShopAssertsRequest;
use App\Helper\ConvertValueLanguages;
use Image;

class ThumbnailImageController extends Controller
{
    public function __construct()
    {
        set_time_limit(8000000);
        $this->middleware('auth');
    }

    public function edit()
    {
        $thumbnail = Setting::where([])->first();
        return view('thumbnail.edit', compact('thumbnail'));
    }

    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);
        if ($setting->update($request->all())) {

            return redirect("thumbnail/edit")->withInput()->with('success', 'Edit Data Size Image Success');
        } else {
            return redirect("thumbnail/edit");
        }
    }

    public function setImage(Request $request)
    {
        $params = $request->except('_token');
        $allowed =  array('gif','png' ,'jpg');
        $objScan = scandir(base_path() . '/public/media/images/');
        foreach ($objScan as $file) {
            $oldfile = base_path() . '/public/media/images/' . $file;
            $newfile = base_path() . '/public/media/thumbnail/' . $file;
            if (!is_dir($oldfile)) {
                $ext = pathinfo($oldfile, PATHINFO_EXTENSION);
                if(in_array($ext,$allowed) ) {
                if (copy($oldfile, $newfile)) {

                    if ($params["height"] > 0) {
                        $img = Image::make($newfile)->resize($params["width"], $params["height"]);
                        $img->save($newfile);
                    } else {
                        $img = Image::make($newfile)->resize($params["width"], null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $img->save($newfile);
                    }
                }
            }
            }
        }

        return redirect("thumbnail/edit")->withInput()->with('success', 'Set Image Success');
    }
}
