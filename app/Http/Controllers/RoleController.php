<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Maklad\Permission\Models\Role;
use Maklad\Permission\Models\Permission;
use App\Models\User;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Role_Menage');
    }

    public function index()
    {
        $search = "";
        $role = Role::all();
        return view('role.index', compact('search', 'role'));
    }

    public function edit(Role $role)
    {
        $user = User::all();
        return view('role.edit', compact('search', 'user', 'role'));
    }


    public function update(Request $request, Role $role)
    {

        $user = User::all();
        foreach ($user as  $data) {
            $data->removeRole($role->name);
        }

        $check = $request->all();
        if (isset($check['check'])) {

            for ($i = 0; $i < count($check['check']); $i++) {
                $update = User::find($check['check'][$i]);
                $update->assignRole($role->name);
            }
        }

        return redirect("roles/{$role->id}/edit")->with('success', 'Edit Role Success');
    }
}
