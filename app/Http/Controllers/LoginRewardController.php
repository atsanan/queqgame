<?php

namespace App\Http\Controllers;

use App\Models\ItemDefault;
use App\Models\MonstersDefault;
use App\Http\Requests\LoginRewardRequest;
use App\Models\LoginReward;

class LoginRewardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Promotion_Resource');
    }

    public function index()
    {
        $loginReward = LoginReward::all();
        return view('loginReward.index', compact('loginReward'));
    }

    public function create()
    {
        $itemDefaults = ItemDefault::queryItem();
        $monsterDefaults = MonstersDefault::queryMonstersDefault();
        return view('loginReward.create', compact('itemDefaults', 'monsterDefaults'));
    }

    public function store(LoginRewardRequest $request)
    {
        //echo parent::$imagePath;
        $loginReward = new LoginReward($request->all());
        $filename = $request->filename;
        $thumbnailName = $request->thumbnailName;

        if (isset($filename)) {
            $loginReward->imageName = $this->uploadImage($request, "filename");
        }


        if (isset($thumbnailName)) {
            $loginReward->thumbnail = $this->uploadImage($request, "thumbnailName");
        }

        if ($loginReward->save()) {
            return redirect('loginReward/' . $loginReward->id . '/edit')->with('success', trans('Succcess Create Login Reward'));
        } else {
            return Redirect::route('digitalBoard')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }

    public function edit(LoginReward $loginReward)
    {
        $itemDefaults = ItemDefault::queryItem();
        $monsterDefaults = MonstersDefault::queryMonstersDefault();
        return view('loginReward.edit', compact('loginReward', 'itemDefaults', 'monsterDefaults'));
    }
    public function update(LoginRewardRequest $request, LoginReward $loginReward)
    {
        if ($loginReward->update($request->all())) {
            $filename = $request->filename;
            $thumbnailName = $request->thumbnailName;
            if (isset($filename)) {
                $loginReward->update(['imageName' => $this->uploadImage($request, "filename")]);
            }

            if (isset($thumbnailName)) {
                $loginReward->update(['thumbnail' => $this->uploadImage($request, "thumbnailName")]);
            }
            return redirect('loginReward/' . $loginReward->id . '/edit')->with('success', trans('Succcess Update Login Reward'));
        } else {
            return Redirect::route('loginReward')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }

    public function destroy($id)
    {
        $loginReward = LoginReward::find($id);

        if ($loginReward) {
            $loginReward->delete();
            return redirect('loginReward')->with('success', 'Success Delete Login Reward');
        } else {
            return redirect('loginReward')->with('error', trans('malls/message.error.delete'));
        }
    }

    public function uploadImage($request, $field)
    {
        $imageSlot = uniqid() . '.' . $request->file($field)->getClientOriginalExtension();
        $request->file($field)->move(base_path() . parent::$imagePath, $imageSlot);
        return $imageSlot;
    }
}
