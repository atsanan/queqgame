<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Malls;
use App\Models\MallFloor;
use App\Models\Shops;
use App\Models\DigitalBoard;
use App\Models\DigitalBoardAsserts;
use App\Http\Requests\DigitalBoardRequest;
use App\Helper\ConvertValueLanguages;

class DigitalBoardController extends Controller
{
    private $zoneType = ["Word" => "Word", "Mall" => "Mall", "Shop" => "Shop"];
    private $clickType = [0 => "ไม่มีการทำงาน", 1 => "แสดงข้อมูลภาพ (ADS)", 2 => "โหมดต่อสู้ PVP", 3 => "นำทาง"];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:DigitalBoard_Resource');
    }

    public function index()
    {
        $digitalBoard = DigitalBoard::all();

        return view('digitalBoard.index', compact('digitalBoard'));
    }

    public function create()
    {
        $zoneType = $this->zoneType;
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();
        $clickType = $this->clickType;
        $digitalBoardAsserts = DigitalBoardAsserts::queryData();
        $x = 0;
        $y = 0;
        $z = 0;
        return view('digitalBoard.create', compact('zoneType', 'malls', 'mallFloors', 'shops', 'clickType', 'digitalBoardAsserts', 'x', 'y', 'z'));
    }


    public function store(DigitalBoardRequest $request)
    {
        $digitalBoard = new DigitalBoard($request->all());
        $fileNameImageSlot1 = $request->fileNameImageSlot1;
        $fileNameImageSlot2 = $request->fileNameImageSlot2;
        $filenameImageThumbnail = $request->filenameImageThumbnail;
        $filenameImageFullScreen = $request->filenameImageFullScreen;

        if (isset($fileNameImageSlot1)) {
            $digitalBoard->imageSlot1 = $this->uploadImage($request, "fileNameImageSlot1");
        }

        if (isset($fileNameImageSlot2)) {
            $digitalBoard->imageSlot2 = $this->uploadImage($request, "fileNameImageSlot2");
        }

        if (isset($filenameImageThumbnail)) {
            $digitalBoard->imageThumbnailList = $this->uploadListImage($filenameImageThumbnail);
        }

        if (isset($filenameImageFullScreen)) {
            $digitalBoard->imageFullScreenList = $this->uploadListImage($filenameImageFullScreen);
        }
        $long = $request->location['coordinates'][0];
        $lat = $request->location['coordinates'][1];

        if ($request->location['coordinates'][0] == null) {
            $long = 0;
        }

        if ($request->location['coordinates'][1] == null) {
            $lat = 0;
        }

        $digitalBoard->location = ['coordinates' => [$long, $lat]];

        // if ($digitalBoard->location['coordinates'][1] == null) {
        //     $digitalBoard->location['coordinates'][1] = 0;
        // }

        $digitalBoard->title = ConvertValueLanguages::convertData($digitalBoard->title);
        $digitalBoard->body = ConvertValueLanguages::convertData($digitalBoard->body);

        if ($digitalBoard->save()) {
            return redirect('digitalBoard')->with('success', trans('digitalBoardAsserts/message.success.create'));
        } else {
            return Redirect::route('digitalBoard')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }

    public function edit(DigitalBoard $digitalBoard)
    {

        $zoneType = $this->zoneType;
        $malls = Malls::queryMall();
        $mallFloors = MallFloor::queryMallFloors();
        $shops = Shops::queryShop();
        $clickType = $this->clickType;
        $digitalBoardAsserts = DigitalBoardAsserts::queryData();
        $x = $digitalBoard->x;
        $y = $digitalBoard->y;
        $z = $digitalBoard->z;
        return view('digitalBoard.edit', compact('digitalBoard', 'zoneType', 'malls', 'mallFloors', 'shops', 'clickType', 'digitalBoardAsserts', 'x', 'y', 'z'));
    }

    public function update(DigitalBoardRequest $request, DigitalBoard $digitalBoard)
    {
        if ($digitalBoard->update($request->all())) {
            $fileNameImageSlot1 = $request->fileNameImageSlot1;
            $fileNameImageSlot2 = $request->fileNameImageSlot2;
            $filenameImageThumbnail = $request->filenameImageThumbnail;
            $filenameImageFullScreen = $request->filenameImageFullScreen;

            if (isset($fileNameImageSlot1)) {
                $digitalBoard->update(['imageSlot1' => $this->uploadImage($request, "fileNameImageSlot1")]);
            }

            if (isset($fileNameImageSlot2)) {
                $digitalBoard->update(['imageSlot2' => $this->uploadImage($request, "fileNameImageSlot2")]);
            }

            if (isset($filenameImageThumbnail)) {
                $digitalBoard->update(['imageThumbnailList' => array_merge($digitalBoard->imageThumbnailList, $this->uploadListImage($filenameImageThumbnail))]);
            }

            if (isset($filenameImageFullScreen)) {

                $digitalBoard->update(['imageFullScreenList' => array_merge($digitalBoard->imageFullScreenList, $this->uploadListImage($filenameImageFullScreen))]);
            }
            $digitalBoard->update(['title' => ConvertValueLanguages::convertData($digitalBoard->title)]);
            $digitalBoard->update(['body' => ConvertValueLanguages::convertData($digitalBoard->body)]);
            $long = $request->location['coordinates'][0];
            $lat = $request->location['coordinates'][1];

            if ($request->location['coordinates'][0] == null) {
                $long = 0;
            }

            if ($request->location['coordinates'][1] == null) {
                $lat = 0;
            }

            $digitalBoard->update(['location' => ['coordinates' => [$long, $lat]]]);

            return redirect('digitalBoard');
        } else {
            return Redirect::route('digitalBoard')->withInput()->with('error', trans('costumes/message.error.create'));
        }
    }

    public function destroy($id)
    {
        $digitalBoard = DigitalBoard::find($id);
        if ($digitalBoard) {
            $digitalBoard->delete();
            return redirect('digitalBoard');
        } else {
            return redirect('digitalBoard');
        }
    }
    public function deleteThumbnail($id, $image, $type)
    {
        $digitalBoard = DigitalBoard::find($id);

        if ($type == 1) {
            $imageThumbnailList = $digitalBoard->imageThumbnailList;
            unset($imageThumbnailList[array_search($image, $imageThumbnailList)]);
            $digitalBoard->imageThumbnailList = $imageThumbnailList;
            if ($digitalBoard->update()) {
                \File::Delete(base_path() . parent::$imagePath . $image);
                return redirect('digitalBoard/' . $id . '/edit');
            }
        } else {
            $imageFullScreenList = $digitalBoard->imageFullScreenList;
            unset($imageFullScreenList[array_search($image, $imageFullScreenList)]);
            $digitalBoard->imageFullScreenList = $imageFullScreenList;
            if ($digitalBoard->update()) {
                \File::Delete(base_path() . parent::$imagePath . $image);
                return redirect('digitalBoard/' . $id . '/edit');
            }
        }
    }

    public function uploadImage($request, $field)
    {
        $imageSlot = uniqid() . '.' . $request->file($field)->getClientOriginalExtension();
        $request->file($field)->move(base_path() . parent::$imagePath, $imageSlot);
        return $imageSlot;
    }

    public function uploadListImage($files)
    {

        $image = [];
        foreach ($files as $file) {
            $getAssetModelType = $file->getClientOriginalExtension();
            if (!!$getAssetModelType) {
                $fileName = uniqid() . '.' . $getAssetModelType;
                if ($file->move(base_path() . parent::$imagePath, $fileName)) {
                    array_push($image, $fileName);
                }
            }
        }

        return $image;
    }
}
