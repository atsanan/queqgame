<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\PrivilegeGroups;
use App\Models\PrivilegeDefaults;
use App\Http\Requests\PrivilegeGroupsRequest;
use App\Helper\ConvertValueLanguages;

class PrivilegeGroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Privilege_Resource');
    }

    public function index()
    {
        $privilegeGroups = PrivilegeGroups::all();

        return view('privilegeGroups.index', compact('privilegeGroups'));
    }

    public function create()
    {
        $filenameLogos1 = null;
        $filenameLogos2 = null;
        return view('privilegeGroups.create', compact('filenameLogos1', 'filenameLogos2'));
    }

    public function store(PrivilegeGroupsRequest $request)
    {
        $privilegeGroup = new PrivilegeGroups($request->all());

        $imageFile1 = $privilegeGroup->filenameLogo1;
        $imagePath1 = '/public/media/images/';
        $imageFile2 = $privilegeGroup->filenameLogo2;
        $imagePath2 = '/public/media/images/';

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
            $privilegeGroup->filenameLogo1 = $imageName1;
            $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
        }

        if (isset($imageFile2)) {
            $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
            $privilegeGroup->filenameLogo2 = $imageName2;
            $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
        }

        $privilegeGroup->privilegeGroupNames = ConvertValueLanguages::convertData($privilegeGroup->privilegeGroupNames);
        $privilegeGroup->privilegeGroupDetails = ConvertValueLanguages::convertData($privilegeGroup->privilegeGroupDetails);

        if ($privilegeGroup->save()) {

            return redirect('privilegeGroups')->with('success', trans('privilegeGroups/message.success.create'));
        } else {
            return Redirect::route('privilegeGroups')->withInput()->with('error', trans('privilegeGroups/message.error.create'));
        }
    }

    public function edit(PrivilegeGroups $privilegeGroup)
    {
        $filenameLogos1 = $privilegeGroup->filenameLogo1;
        $filenameLogos2 = $privilegeGroup->filenameLogo2;
        $privilegeDefaults = PrivilegeDefaults::where(['privilegeGroupId' => new \MongoDB\BSON\ObjectId($privilegeGroup->id)])->get();

        return view('privilegeGroups.edit', compact('privilegeGroup', 'privilegeDefaults', 'filenameLogos1', 'filenameLogos2'));
    }

    public function update(PrivilegeGroupsRequest $request, PrivilegeGroups $privilegeGroup)
    {

        if ($privilegeGroup->update($request->all())) {
            $imageFile1 = $request->filenameLogo1;
            $imagePath1 = '/public/media/images/';
            $imageFile2 = $request->filenameLogo2;
            $imagePath2 = '/public/media/images/';

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('filenameLogo1')->getClientOriginalExtension();
                $privilegeGroup->update(['filenameLogo1' => $imageName1]);
                $request->file('filenameLogo1')->move(base_path() . $imagePath1, $imageName1);
            }

            if (isset($imageFile2)) {
                $imageName2 = uniqid() . '.' . $request->file('filenameLogo2')->getClientOriginalExtension();
                $privilegeGroup->update(['filenameLogo2' => $imageName2]);
                $request->file('filenameLogo2')->move(base_path() . $imagePath2, $imageName2);
            }

            $privilegeGroup->update(['privilegeGroupNames' => ConvertValueLanguages::convertData($privilegeGroup->privilegeGroupNames)]);
            $privilegeGroup->update(['privilegeGroupDetails' => ConvertValueLanguages::convertData($privilegeGroup->privilegeGroupDetails)]);

            return redirect('privilegeGroups')->with('success', trans('privilegeGroups/message.success.update'));
        } else {
            return Redirect::route('privilegeGroups')->withInput()->with('error', trans('privilegeGroups/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $privilegeGroup = PrivilegeGroups::find($id);

        if ($privilegeGroup) {
            $privilegeGroup->delete();
            return redirect('privilegeGroups')->with('success', trans('privilegeGroups/message.success.destroy'));
        } else {
            return redirect('privilegeGroups')->withInput()->with('error', trans('privilegeGroups/message.error.delete'));
        }
    }

    public function data()
    {
        $models = PrivilegeGroups::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (PrivilegeGroups $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
