<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ShopCategories;
use App\Http\Requests\ShopCategoriesRequest;


class ShopCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Mall_Resource');
    }

    public function index()
    {
        $shopCategories = ShopCategories::all();

        return view('shopCategories.index', compact('shopCategories'));
    }

    public function create()
    {
        return view('shopCategories.create');
    }

    public function store(ShopCategoriesRequest $request)
    {
        $shopCategory = new ShopCategories($request->all());

        if ($shopCategory->save()) {
            return redirect('shopCategories')->with('success', trans('shopCategories/message.success.create'));
        } else {
            return Redirect::route('shopCategories')->withInput()->with('error', trans('shopCategories/message.error.create'));
        }
    }

    public function edit(ShopCategories $shopCategory)
    {
        return view('shopCategories.edit', compact('shopCategory'));
    }

    public function update(ShopCategoriesRequest $request, ShopCategories $shopCategory)
    {
        if ($shopCategory->update($request->all())) {
            return redirect('shopCategories')->with('success', trans('shopCategories/message.success.update'));
        } else {
            return Redirect::route('shopCategories')->withInput()->with('error', trans('shopCategories/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $shopCategory = ShopCategories::find($id);

        if ($shopCategory->delete()) {
            return redirect('shopCategories')->with('success', trans('shopCategories/message.success.destroy'));
        } else {
            return Redirect::route('shopCategories')->withInput()->with('error', trans('shopCategories/message.error.delete'));
        }
    }

    public function data()
    {
        $models = ShopCategories::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (ShopCategories $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
