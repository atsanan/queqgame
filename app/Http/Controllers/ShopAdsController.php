<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ShopAds;
use App\Http\Requests\ShopAdsRequest;
use App\Helper\ConvertValueLanguages;

class ShopAdsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Mall_Resource');
    }

    public function index()
    {
        $shopAds = ShopAds::all();

        return view('shopAds.index', compact('shopAds'));
    }

    public function create()
    {
        $shopAdsImages = null;
        return view('shopAds.create', compact('shopAdsImages'));
    }

    public function store(ShopAdsRequest $request)
    {
        $shopAd = new ShopAds($request->all());

        $imageFile1 = $shopAd->shopAdsImageLists;
        $imageFile1 = $shopAd->shopAdsImage;
        $imagePath1 = '/public/media/images/';

        if (isset($imageFile1)) {
            $imageName1 = uniqid() . '.' . $request->file('shopAdsImage')->getClientOriginalExtension();
            $shopAd->shopAdsImage = $imageName1;
            $request->file('shopAdsImage')->move(base_path() . $imagePath1, $imageName1);
        } else {
            $shopAd->shopAdsImage = "";
        }

        $imageFile = $shopAd->imageSlots;
        $imagePath = '/public/media/images/';
        if (isset($imageFile)) {
            $slots = [];
            foreach ($imageFile as $photo) {
                $imageName = uniqid() . '.' . $photo->getClientOriginalExtension();
                $photo->move(base_path() . $imagePath, $imageName);
                array_push($slots, $imageName);
            }
            $shopAd->shopAdsImageLists = $slots;
        } else {
            $shopAd->shopAdsImageLists = [];
        }

        $shopAd->shopAdsName = ConvertValueLanguages::convertData($shopAd->shopAdsName);

        if ($shopAd->save()) {
            return redirect('shopAds')->with('success', trans('shopAds/message.success.create'));
        } else {
            return Redirect::route('shopAds')->withInput()->with('error', trans('shopAds/message.error.create'));
        }
    }

    public function edit(ShopAds $shopAd)
    {
        $shopAdsImages = $shopAd->shopAdsImage;
        $shopAdsImagelist = $shopAd->shopAdsImageLists;
        return view('shopAds.edit', compact('shopAd', 'shopAdsImages', 'shopAdsImagelist'));
    }

    public function update(ShopAdsRequest $request, ShopAds $shopAd)
    {
        if ($shopAd->update($request->all())) {

            $imageFile1 = $request->shopAdsImage;
            $imagePath1 = '/public/media/images/';

            if (isset($imageFile1)) {
                $imageName1 = uniqid() . '.' . $request->file('shopAdsImage')->getClientOriginalExtension();
                $shopAd->update(['shopAdsImage' => $imageName1]);
                $request->file('shopAdsImage')->move(base_path() . $imagePath1, $imageName1);
            }
            $imageFile = $request->imageSlots;
            $imagePath = '/public/media/images/';
            if (isset($imageFile)) {
                $slots = [];
                foreach ($imageFile as $photo) {
                    $imageName = uniqid() . '.' . $photo->getClientOriginalExtension();
                    $photo->move(base_path() . $imagePath, $imageName);
                    array_push($slots, $imageName);
                }
                $shopAd->update(['shopAdsImageLists' => $slots]);
            }


            $shopAd->update(['shopAdsName' => ConvertValueLanguages::convertData($shopAd->shopAdsName)]);

            return redirect('shopAds')->with('success', trans('shopAds/message.success.update'));
        } else {
            return Redirect::route('shopAds')->withInput()->with('error', trans('shopAds/message.error.update'));
        }
    }

    public function destroy($id)
    {
        $shopAd = ShopAds::find($id);

        if ($shopAd) {
            $shopAd->delete();
            return redirect('shopAds')->with('success', trans('shopAds/message.success.destroy'));
        } else {
            return Redirect::route('shopAds')->withInput()->with('error', trans('shopAds/message.error.delete'));
        }
    }

    public function data()
    {
        $models = ShopAds::get(['id', 'first_name', 'last_name', 'email', 'created_at']);

        return DataTables::of($models)
            ->editColumn('created_at', function (ShopAds $models) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status', function ($user) {
                if ($activation = Activation::completed($user)) {
                    return 'Activated';
                } else {
                    return 'Pending';
                }
            })
            ->addColumn('actions', function ($user) {
                $actions = '<a href=' . route('users.show', $user->id) . '><i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view user"></i></a>
                            <a href=' . route('users.edit', $user->id) . '><i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update user"></i></a>';
                if ((Sentinel::getUser()->id != $user->id) && ($user->id != 1)) {
                    $actions .= '<a href=' . route('users.confirm-delete', $user->id) . ' data-id="' . $user->id . '" data-toggle="modal" data-target="#delete_confirm"><i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i></a>';
                }
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
