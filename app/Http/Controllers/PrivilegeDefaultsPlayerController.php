<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\PrivilegeDefaults;
use App\Models\PrivilegeGroups;
use App\Models\Players;
use App\Models\UserPlayer;
use App\Models\PrivilegeDefaultsPlayer;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\PrivilegeDefaultsRequest;
use App\Helper\ConvertValueLanguages;

class PrivilegeDefaultsPlayerController extends Controller
{
    public function searchPlayer()
    {
        $privilegeDefaultId = Input::get('privilegeDefaultId');
        $user = UserPlayer::raw(function ($collection)  {
            return $collection->aggregate(
                array(
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => '_id',
                        'foreignField' => 'userId',
                        'as' => 'players'
                    )),
                    array('$unwind' => '$players'),
                )
            );
        });
        $listId = [];

        $search =  new \MongoDB\BSON\ObjectId(Input::get('privilegeDefaultId'));
        $privilegeDefaultsPlayer =  PrivilegeDefaultsPlayer::raw(function ($collection) use ($search) {
            return $collection->aggregate(
                array(
                    array('$match' => array('privilegeDefaultId' => $search)),
                    array('$lookup' => array(
                        'from' => 'players',
                        'localField' => 'playerId',
                        'foreignField' => '_id',
                        'as' => 'player'
                    )),
                    array('$unwind' => '$player'),
                    array('$lookup' => array(
                        'from' => 'users',
                        'localField' => 'player.userId',
                        'foreignField' => '_id',
                        'as' => 'user'
                    )),
                    array('$unwind' => '$user'),
                )
            );
        });


        foreach ($privilegeDefaultsPlayer as $data) {
            array_push($listId, (string)$data->user->_id);
        }


        $data = view('privilegeDefaultsPlayer.option', compact('listId', 'user'))->render();
        return response()->json(['options' => $data]);
    }
}
