<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\ItemDefault;
use App\Models\ItemPlayer;
use App\Models\CouponItem;
use App\Models\Setting;
use App\Http\Requests\ItemDefaultsRequest;
use App\Helper\ConvertValueLanguages;
use App\Helper\UploadFile;
use Illuminate\Support\Facades\Input;
use Excel;
use App\Exports\BladeExport;
use Illuminate\Pagination\LengthAwarePaginator;

class ItemPlayerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Report_Resource');
    }

    public function report()
    {

        $shopCode = "";
        $setting = Setting::where([])->first();
        $dataShopCode = ["" => "please select"];
        $shopCode = Input::get('shopCode');
        if (isset($shopCode)) {
            $shopCode = explode(",", $shopCode);
        }
        foreach ($setting->shopCodes as $data) {
            $dataShopCode[$data] = $data;
        }
        $arryQuery = array(
            array('$lookup' => array(
                'from' => 'itemDefaults',
                'localField' => 'itemId',
                'foreignField' => '_id',
                'as' => 'item'
            )),

            array('$unwind' => '$item'),
            array('$lookup' => array(
                'from' => 'players',
                'localField' => 'playerId',
                'foreignField' => '_id',
                'as' => 'player'
            )),
            array('$unwind' => '$player'),

            array('$lookup' => array(
                'from' => 'users',
                'localField' => 'player.userId',
                'foreignField' => '_id',
                'as' => 'users'
            )),
            array('$unwind' => '$users')
        );
        $itemPlayer = ItemPlayer::raw(
            function ($collection) use ($shopCode, $arryQuery) {
                $queryShopCode = array();
                if (isset($shopCode)) {
                    foreach ($shopCode as $data) {
                        array_push($queryShopCode, array("couponPassword" => $data));
                    }
                } else {
                    array_push($queryShopCode, array("couponPassword" => array('$ne' =>  "")));
                }
                array_push(
                    $arryQuery,
                    array(
                        '$match' => array(
                            '$or' => $queryShopCode
                        )
                    )
                );


                return $collection->aggregate($arryQuery);
            }
        );

        $itemPlayerGroup = ItemPlayer::raw(
            function ($collection) use ($shopCode, $arryQuery) {
                $queryShopCode = array();
                if (isset($shopCode)) {
                    foreach ($shopCode as $data) {
                        array_push($queryShopCode, array("couponPassword" => $data));
                    }
                } else {
                    array_push($queryShopCode, array("couponPassword" => array('$ne' =>  "")));
                }

                array_push(
                    $arryQuery,
                    array(
                        '$match' => array(
                            '$or' => $queryShopCode
                        )
                    )
                );


                array_push(
                    $arryQuery,
                    array(
                        '$group' => array(
                            '_id' => '$itemId', 'sum' => array('$sum' => 1),
                            'item' => array('$addToSet' => '$item')
                        ),
                    )
                );
                return $collection->aggregate($arryQuery);
            }
        );
        $stat = [];
        if (count($itemPlayerGroup) > 0) {
            foreach ($itemPlayerGroup as $item) {
                $countUse = ItemPlayer::raw(
                    function ($collection) use ($shopCode, $item, $arryQuery) {
                        $queryShopCode = array(
                            array(
                                'couponHashKey' => array('$ne' =>  ""),
                            )
                        );
                        if (isset($shopCode)) {
                            foreach ($shopCode as $data) {
                                array_push($queryShopCode, array("couponPassword" => $data));
                            }
                        } else {
                            array_push($queryShopCode, array("couponPassword" => array('$ne' =>  "")));
                        }


                        array_push($arryQuery, array(
                            '$match' => array(
                                '$and' => $queryShopCode,
                                'itemId' => new \MongoDB\BSON\ObjectId($item->item[0]->_id)
                            )
                        ));
                        return $collection->aggregate($arryQuery);
                    }
                );

                $countUnUse = ItemPlayer::raw(
                    function ($collection) use ($shopCode, $item, $arryQuery) {
                        $queryShopCode = array(
                            array(
                                'couponHashKey' => "",
                            )
                        );
                        if (isset($shopCode)) {
                            foreach ($shopCode as $data) {
                                array_push($queryShopCode, array("couponPassword" => $data));
                            }
                        } else {
                            array_push($queryShopCode, array("couponPassword" => array('$ne' =>  "")));
                        }

                        array_push($arryQuery, array(
                            '$match' => array(
                                '$and' => $queryShopCode,
                                'itemId' => new \MongoDB\BSON\ObjectId($item->item[0]->_id)
                            )
                        ));

                        return $collection->aggregate($arryQuery);
                    }
                );
                array_push($stat, ['itemName' => $item->item[0]->nameReport, 'use' => count($countUse), 'notUse' => count($countUnUse)]);

                // print_r($data->item[0]->itemName->eng);
            }
        }

        return view('itemPlayer.report', compact('itemPlayer', 'setting', 'shopCode', 'dataShopCode', 'stat'));
    }

    public function search(Request $request)
    {

        $shopCode = "";
        if ($request->shopCode) {
            $shopCode = "&shopCode=" . implode(",", $request->shopCode);
        }
        return redirect('report/itemPlayer?' . $shopCode);
    }


    public function excel()
    {
        $shopCode = Input::get('shopCode');
        if (isset($shopCode)) {
            $shopCode = explode(",", $shopCode);
        }

        $arryQuery = array(
            array('$lookup' => array(
                'from' => 'itemDefaults',
                'localField' => 'itemId',
                'foreignField' => '_id',
                'as' => 'item'
            )),

            array('$unwind' => '$item'),
            array('$lookup' => array(
                'from' => 'players',
                'localField' => 'playerId',
                'foreignField' => '_id',
                'as' => 'player'
            )),
            array('$unwind' => '$player'),

            array('$lookup' => array(
                'from' => 'users',
                'localField' => 'player.userId',
                'foreignField' => '_id',
                'as' => 'users'
            )),
            array('$unwind' => '$users')
        );
        $itemPlayer = ItemPlayer::raw(
            function ($collection) use ($shopCode, $arryQuery) {
                $queryShopCode = array();
                if (isset($shopCode)) {
                    foreach ($shopCode as $data) {
                        array_push($queryShopCode, array("couponPassword" => $data));
                    }
                } else {
                    array_push($queryShopCode, array("couponPassword" => array('$ne' =>  "")));
                }
                array_push(
                    $arryQuery,
                    array(
                        '$match' => array(
                            '$or' => $queryShopCode
                        )
                    )
                );


                return $collection->aggregate($arryQuery);
            }
        );

        $array = [];
        if (count($itemPlayer) > 0) {
            foreach ($itemPlayer as $row) {
                array_push(
                    $array,
                    array(
                        'couponPassword' => $row->couponPassword,
                        'itemName' => $row->item->nameReport,
                        'couponGiftId' => $row->couponGiftId,
                        'Status' => empty($row->couponHashKey) ? "Not Use" : "Use",
                        'couponHashKey' => $row->couponHashKey,
                        'firstname' => $row->users->firstname,
                        'lastname' => $row->users->lastname,
                        'email' => isset($row->users->email) ? $row->users->email : ""
                    )
                );
            }
            return Excel::download(new BladeExport($array), 'itemPlayerCoupon.xlsx');
        } else {
            return redirect('report/itemPlayer');
        }
    }

    public function destroy($id)
    {
        $itemPlayer = ItemPlayer::find($id);
        if ($itemPlayer) {
            $playerId = $itemPlayer->playerId;
            $itemPlayer->delete();

            return redirect("players/{$playerId}/edit")->with('success', trans('malls/message.success.destroy'));
        } else {
            return Redirect::route('malls')->withInput()->with('error', trans('malls/message.error.delete'));
        }
    }

    public function itemCoupon()
    {
        $page = Input::get('page');
        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;

        $arryQuery = array(
            array('$match' => array("uniqueId" => array('$exists' => true))),
            array('$lookup' => array(
                'from' => 'itemDefaults',
                'localField' => 'itemId',
                'foreignField' => '_id',
                'as' => 'item'
            )),
            array('$unwind' => '$item'),
        );


        $itemPlayerGroup = ItemPlayer::raw(
            function ($collection) use ($arryQuery) {
                array_push(
                    $arryQuery,
                    array(
                        '$group' => array(
                            '_id' => '$itemId',
                            'sum' => array('$sum' => 1),
                            'item' => array('$addToSet' => '$item'),
                        ),
                    )
                );
                return $collection->aggregate($arryQuery);
            }
        );

        $stat = [];
        if (count($itemPlayerGroup) > 0) {
            foreach ($itemPlayerGroup as $item) {
                $notUse = CouponItem::where([
                    'itemId' =>  new \MongoDB\BSON\ObjectId($item->_id),
                    "itemPlayerId" => array('$exists' => false)
                ])->count();
                $use = CouponItem::where([
                    'itemId' =>  new \MongoDB\BSON\ObjectId($item->_id),
                    "itemPlayerId" => array('$exists' => true)
                ])->count();
                array_push($stat, [
                    'itemName' => $item->item[0]->nameReport,
                    'isActive' => $item->item[0]->isActive,
                    'id' => $item->item[0]->_id,
                    'use' => $use,
                    'notUse' => $notUse
                ]);
            }
        }

        $data  = new LengthAwarePaginator(array_slice($stat, $offset, 50), count($stat), 50);

        return view('itemPlayer.code', compact('stat','data'));
    }

    public function itemCouponExcel(){

        
        $arryQuery = array(
            array('$match' => array("uniqueId" => array('$exists' => true))),
            array('$lookup' => array(
                'from' => 'itemDefaults',
                'localField' => 'itemId',
                'foreignField' => '_id',
                'as' => 'item'
            )),
            array('$unwind' => '$item'),
        );


        $itemPlayerGroup = ItemPlayer::raw(
            function ($collection) use ($arryQuery) {
                array_push(
                    $arryQuery,
                    array(
                        '$group' => array(
                            '_id' => '$itemId',
                            'sum' => array('$sum' => 1),
                            'item' => array('$addToSet' => '$item'),
                        ),
                    )
                );
                return $collection->aggregate($arryQuery);
            }
        );

        $array = [];
        if (count($itemPlayerGroup) > 0) {
            foreach ($itemPlayerGroup as $item) {
                $notUse = CouponItem::where([
                    'itemId' =>  new \MongoDB\BSON\ObjectId($item->_id),
                    "itemPlayerId" => array('$exists' => false)
                ])->count();
                $use = CouponItem::where([
                    'itemId' =>  new \MongoDB\BSON\ObjectId($item->_id),
                    "itemPlayerId" => array('$exists' => true)
                ])->count();
                array_push($array, [
                    'itemName' => $item->item[0]->nameReport,
                    'active' => $use,
                    'isActive' => $notUse
                ]);
            }
            return Excel::download(new BladeExport($array), 'itemPlayerCouponCode.xlsx');
        }else{
            return redirect("report/itemCoupon");
        }

       
    }
}
