<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\UserPlayer;
use App\Http\Requests\CostumesRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

class DeviceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Player_Resource');
    }

    public function index()
    {
        $device = Device::all();
        $search = Input::get('search');
        $data = Device::with([]);
        $sort = Input::get('sort');
        $page = Input::get('page');

        $field = Input::get('field');

        if ($page == null) {
            $page = 1;
        }
        $offset = ($page - 1) * 50;

        if (isset($search)) {

            if (isset($sort)) {
                $data = $data->where("deviceId", "like", "%$search%")
                    ->orWhere("deviceName", "like", "%$search%")
                    ->orWhere("deviceOS", "like", "%$search%")
                    ->orderBy($field, $sort)->get();
            } else {
                $data = $data->where("deviceId", "like", "%$search%")
                    ->orWhere("deviceName", "like", "%$search%")
                    ->orWhere("deviceOS", "like", "%$search%")
                    ->get();
            }
        } else {
            if (isset($sort)) {
                $data = $data->orderBy($field, $sort)->get();
            } else {
                $data = $data->orderBy("deviceId", "asc")->get();
            }
        }
        $device  = new LengthAwarePaginator($data->slice($offset, 50), $data->count(),  50);
        $device->setPath('/device');
        return view('device.index', compact('device', 'search', 'field', 'sort'));
    }

    public function view($deviceId)
    {
        $device = Device::where(['deviceId' => $deviceId])->first();
        $user = UserPlayer::where(['deviceId' => $deviceId])->get();
        return view('device.view', compact('device', 'user'));
    }
}
