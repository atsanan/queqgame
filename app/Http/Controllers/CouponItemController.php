<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\CouponItem;
use App\Models\ItemDefault;
use App\Models\ItemPlayer;
use App\Http\Requests\CouponItemRequest;
use App\Http\Requests\CsvRequest;
use App\Http\Requests\CouponMutipleRequest;
use Illuminate\Support\Facades\Input;

class CouponItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('role:Resource');
    }

    public function index()
    { }

    public function create()
    {
        $data = ItemDefault::queryItem();
        $itemId = Input::get('itemId');
        return view('couponItem.create', compact('data', 'itemId'));
    }


    public function store(CouponItemRequest $request)
    {
        $couponItem = new CouponItem($request->all());
        if ($couponItem->save()) {
            return redirect("itemDefaults/{$couponItem->itemId}/edit")->with('success', 'Success Save Coupon Item');
        } else {
            return redirect("couponItem/create")->withInput();
        }
    }

    public function edit(CouponItem $couponItem)
    {
        $data = ItemDefault::queryItem();
        $itemId = $couponItem->itemId;
        return view('couponItem.edit', compact('couponItem', 'data', 'itemId'));
    }

    public function update(CouponItemRequest $request, CouponItem $couponItem)
    {
        if ($couponItem->update($request->all())) {
            return redirect("itemDefaults/{$couponItem->itemId}/edit")->with('success', 'Success Update Coupon Item');
        } else {
            return redirect("couponItem/{$couponItem->id}/edit")->withInput();
        }
    }


    public function importCsv()
    {
        $data = ItemDefault::queryItem();
        $itemId = Input::get('itemId');
        return view('couponItem.import', compact('data', 'itemId'));
    }

    public function storeCsv(CsvRequest $request)
    {

        $path = $request->file('fileName')->getRealPath();
        $data = array_map('str_getcsv', file($path));
        $record = [];
        $now = new \MongoDB\BSON\UTCDateTime(now());
        foreach ($data as $row) {
            $save['code'] = $row[0];
            $save['comment'] = $row[1];
            $save['isActive'] = true;
            $save['itemId'] = new \MongoDB\BSON\ObjectId($request->itemId);
            $save['created_at'] = $now;
            $save['updated_at'] = $now;
            $record[] = $save;
        }

        CouponItem::insert($record);

        return redirect("itemDefaults/{$request->itemId}/edit")->with('success', "Success Create Coupon Item");
    }

    public function destroy($id)
    {
        $couponItem = CouponItem::find($id);

        if ($couponItem) {
            $itemId = $couponItem->itemId;
            $couponItem->delete();
            return redirect("itemDefaults/{$itemId}/edit")->with('success', "Success Delete Coupon Item");
        } else {
            return redirect('itemDefaults')->with('error', trans('costumes/message.error.delete'));
        }
    }

    public function deleteItemPlayer($id)
    {
        $couponItem = CouponItem::where(["itemPlayerId" => new \MongoDB\BSON\ObjectId($id)])->first();

        if ($couponItem) {
            $couponItem->unset("datetimeGetCoupon");
            $couponItem->unset("itemPlayerId");
            $itemPlayer = ItemPlayer::find($id);
            if ($itemPlayer) {
                $itemPlayer->delete();
            }
            return redirect("itemDefaults/{$couponItem->itemId}/edit")->with('success', "Success Delete Coupon Item");
        } else {
            return redirect("itemDefaults/{$couponItem->itemId}/edit")->with('success', "Fail Delete Coupon Item");
        }
    }

    public function createMultipleCode()
    {
        $data = ItemDefault::queryItem();
        $itemId = Input::get('itemId');
        return view('couponItem.createMutiple', compact('data', 'itemId'));
    }


    public function storeMultipleCode(CouponMutipleRequest $request)
    {
        $record = [];
        $now = new \MongoDB\BSON\UTCDateTime(now());


        for ($i = 0; $i < $request->amount; $i++) {
            $save['code'] = date("i", time()) . date("s", time()) . str_random(3) . str_random(3) . str_random(3);
            $save['comment'] = "";
            $save['isActive'] = true;
            $save['itemId'] = new \MongoDB\BSON\ObjectId($request->itemId);
            $save['created_at'] = $now;
            $save['updated_at'] = $now;
            $record[] = $save;
        }

        CouponItem::insert($record);

        return redirect("itemDefaults/{$request->itemId}/edit")->with('success', "Success Create Coupon Item");
    }
}
