@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Edit And Set Size Thumbnail</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('stickerStores') }}"> Edit And Set Size Thumbnail</a></li>
    </ol>
</section>

@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                {!! Form::model($thumbnail, ['url' => URL::to('thumbnail') . '/'.$thumbnail->id , 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />

                    <fieldset>
                    
                        <div class="col-md-6">
                            @component('components.inputField',[
                                'type'=>'number',
                                'icon'=>'text',
                                'label'=>'Width',
                                'field'=>'width',
                                'value'=>null,
                                'attribute'=>['class'=>'form-control','placeholder'=>'width']
                            ])
                            @endcomponent
                            </div>
                            <div class="col-md-6">
                            @component('components.inputField',[
                                'type'=>'number',
                                'icon'=>'text',
                                'label'=>'Height',
                                'field'=>'height',
                                'value'=>null,
                                'attribute'=>['class'=>'form-control','placeholder'=>'height']
                            ])
                            @endcomponent
                            </div>

                            <div class="form-group">
        
                                <div class="col-md-12 text-right">
                                    <button type="button"  class="btn btn-responsive btn-success btn-sm setImage">Set Image</button>
                                    <button type="submit" class="btn btn-responsive btn-primary btn-sm">Save</button>
                                </div>
                            </div>

                    </fieldset>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <!--row end-->
</section>
@stop

@push('more_scripts')
<script>
    $(".setImage").click(function() {
        $("form").attr("action","/thumbnail/setImage");
        $("form").submit();
    })
</script>
@endpush