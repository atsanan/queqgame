@include('constants.gender')
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Player Name',
        'field'=>'playerName',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Player Name']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'icon'=>'text',
        'label'=>'Team Player',
        'field'=>'teamPlayerId',
        'value'=>null,
        'data'=>$teamPlayers,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Coin',
        'field'=>'coin',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Coin']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Diamond',
        'field'=>'diamond',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Diamond']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'icon'=>'text',
        'label'=>'Costumes',
        'field'=>'costumeSelectId',
        'value'=>null,
        'data'=>$costumes,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select2',
        'icon'=>'text',
        'label'=>'Costumes',
        'field'=>'costumeList[]',
        'value'=>null,
        'data'=>$costumes,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select','id' => uniqid(),'multiple' => true]
    ])
    @endcomponent
</div>


<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'icon'=>'text',
        'label'=>'Gender',
        'field'=>'gender',
        'value'=>null,
        'data'=>$GLOBALS['genderData'],
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
    ])
    @endcomponent
</div>

    <div class="form-group">
        <div class="col-md-12 text-right">
            <a href="{{ URL::to('user/'.$player->userId.'/edit') }}" class="btn btn-success btn-sm" id="addButton">Edit User</a>
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
    </div>