@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
<section class="content-header">
    <h1>Players</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('players') }}"> Players</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                {!! Form::model($player, ['url' => URL::to('players') . '/' . $player->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                     <fieldset>
                    @include('players.field')
                    </fieldset>

                {!! Form::close() !!}

            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active ">
                        <a href="#monster" data-toggle="tab">Monster ({{count($monsterDefaults)+count($monsterInbug)}})</a>
                    </li>
                    <li class="">
                        <a href="#item" data-toggle="tab">Item ({{count($itemPlayer)}})</a>
                    </li>
                    <li class="">
                        <a href="#log" data-toggle="tab">Log ({{count($playerLogs)}})</a>
                    </li>
                </ul>

                    
                <div class="tab-content mar-top">
                    <div id="monster" class="tab-pane fade active in">
                        <ul class="nav nav-tabs">
                                <li class="active ">
                                    <a href="#inChannel" data-toggle="tab"> In Channel ({{count($monsterDefaults)}})</a>
                                </li>
                                <li class="">
                                    <a href="#inBug" data-toggle="tab">In Bug ({{count($monsterInbug)}})</a>
                                </li>
                            </ul>
                    
                        <div class="tab-content mar-top">
                            <div id="inChannel" class="tab-pane active in">
                                    <table class="table" id="table">
                                            <thead>
                                                <tr class="filters">
                                                        <th>Mall </th>
                                                        <th>Mall Floor</th>
                                                        <th>Shop Name</th>
                                                        <th>MonsterPlayer Name</th>
                                                        <th>Monster Name</th>
                                                        <th>Image</th>
                                                        <th>Delete</th>
                                                  </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($monsterDefaults as $row)
                                                    
                                                <tr>
                                                    <td>{{ $row->mall->mallName->eng }}</td>
                                                    <td>{{ $row->mallFloor->mallFloorName->eng }}</td>
                                                    <td>{{ $row->shops->shopName->eng }}</td>
                                                    <td>{{ $row->mPlayerName }}</td>
                                                    <td>{{ $row->monsterDefaults->mDefaultName->eng }}</td>
                                                    <td><img src="{{URL::to('media/images/'.$row->monsterDefaults->mDefaultAssetImageSlot)}}" style="width: 100px; height: 70px" /></td>
                                                    <td>
                                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('monstersPlayer.destroy', collect($row)->first()) }}">
                                                            <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                        </a>
                                                    </td> 
                                                </tr>
                                                @endforeach
                                               
                                            </tbody>
                                    </table>            

                            </div>
                            <div id="inBug" class="tab-pane">
                                    <table class="table" id="monsterTable">
                                            <thead>
                                                <tr class="filters">
                                            
                                                        <th>Monster Name</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                  </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($monsterInbug as $row)
                                                <tr>
                                                    <td>{{ $row->monsterDefaults->mDefaultName->eng }}</td>
                                                    <td><img src="{{URL::to('media/images/'.$row->monsterDefaults->mDefaultAssetImageSlot)}}" style="width: 100px; height: 70px" /></td>
                                                    <td>
                                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('monstersPlayer.destroy', collect($row)->first()) }}">
                                                            <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                        </a>
                                                    </td> 
                                                </tr>
                                                @endforeach
                                               
                                            </tbody>
                                    </table>            
                                 </div>
                        </div>
                    </div>
                    <div id="item" class="tab-pane">
                        <table class="table" id="itemTable">
                            <thead>
                                <tr class="filters"> 
                                    <th>Shop Code </th>
                                    <th>Name Coupon </th>
                                    <th>Id Code </th>
                                    <th>Status </th>
                                    <th>Date Used </th>
                                    <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                
                              
                                @foreach($itemPlayer as $row)
                                <tr>
                                    <td>{{$row->couponPassword}}</td>
                                    <td>{{$row->item->itemName->eng}}</td>
                                    <td>{{$row->couponGiftId}} </td>
                                    <td>
                                       @if(!empty($row->couponHashKey))

                                       <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                       @else 
                                       <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                       @endif

                                    </td>
                                    <td>{{$row->couponHashKey}} </td>     
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('itemPlayer.destroy', collect($row)->first()) }}">
                                            <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        </a>
                                    </td> 
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                     
       
                    </div>
                    <div id="log" class="tab-pane">
                        <table class="table" id="logTable">
                            <thead>
                                <tr class="filters"> 
                                    <th>Title </th>
                                    <th>Detali </th>
                                    <th>Money </th>
                                    <th>Coin </th>
                                    <th>Diamond </th>
                                    <th>Monster </th>
                                    <th>Item </th>
                                    <th>Create Date </th>
                                </tr>
                            </thead>
                            <tbody>
                                
                              
                                @foreach($playerLogs as $row)
                                <tr>
                                    <td>{{$row->title}}</td>
                                    <td>{{$row->detail}}</td>
                                    <td>{{$row->money}}</td>
                                    <td>{{$row->coin}}</td>
                                    <td>{{$row->diamond}}</td>
                                    <td>{{$row->monsters->mDefaultName['eng']}}</td>
                                    <td>{{$row->items->itemName['eng']}}</td>
                                    <td>
                                        @php
                                            $createAt = new DateTime($row->createAt, $gmtTimezone);
                                            $offsetcreateAt = $userTimezone->getOffset($createAt);
                                            $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                            $createAt->add($myIntervalcreateAt);
                                            echo $createAt->format('Y-m-d H:i:s');
                                        @endphp
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                     
       
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal for showing delete confirmation -->
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="user_delete_confirm_title">
                        Delete
                    </h4>
                </div>
                <div class="modal-body">
                    Are you sure to delete this? 
                </div>
                <div class="modal-footer">
                {!! Form::open(['method' => 'DELETE']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <button type="submit" class="btn btn-danger">Delete</a>
                {!! Form::close() !!}                                    
                </div>
            </div>
        </div>
    </div>
@stop
@push('more_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#logTable').dataTable({
            "pageLength": 50
        });
        $('#itemTable').dataTable({
            "pageLength": 50
        });
        $('#monsterTable').dataTable({
            "pageLength": 50
        });
    });
</script>
@endpush