@extends('layouts/default') {{-- Page title --}} 
@section('title') Player Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Players</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Players</a></li>
        <li class="active">Player Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Players</h3>
                    <div class="pull-right">
                    <!-- <a href="{{ URL::to('players/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a> -->
                    </div>
                </div>
                <br/>
                <div class="col-md-12">
                {{-- <div class="col-md-6"> --}}
                    <div class="row">
                    <form id="wildMonstersForm" action="/players" method="GET" enctype="multipart/form-data">
                        <!-- CSRF Token -->
                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                        <fieldset>
                                <div class="col-md-6">
                                <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                        {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                        <div class="col-md-9">
                                                <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                        </span>
                                            {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                </div>
                                            {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                </div>
                        </fieldset>           
                    </form>
                </div>
                </div>
                @php
                                      
                if($field=="playerName"){
                    if($sort=="desc"){
                        $sortName="asc";
                    }else{
                        $sortName="desc";
                    }
                }else {
                    $sortName="asc";
                }   
               
                if($field=="coin"){
                    if($sort=="desc"){
                        $sortCoin="asc";
                    }else{
                        $sortCoin="desc";
                    }
                }else {
                    $sortCoin="asc";
                } 

                if($field=="diamond"){
                    if($sort=="desc"){
                        $sortDiamond="asc";
                    }else{
                        $sortDiamond="desc";
                    }
                }else {
                    $sortDiamond="asc";
                } 
                
                if($field=="gender"){
                    if($sort=="desc"){
                        $sortGender="asc";
                    }else{
                        $sortGender="desc";
                    }
                }else {
                    $sortGender="asc";
                } 

                if($field=="costume"){
                    if($sort=="desc"){
                        $sortCostume="asc";
                    }else{
                        $sortCostume="desc";
                    }
                }else {
                    $sortCostume="asc";
                } 
                @endphp

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Player Name
                                    <a href="{{ URL::to("players?field=playerName&sort={$sortName}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a> 
                                </th>
                                <th>Team Player</th>
                                <th>Coin
                                        <a href="{{ URL::to("players?field=coin&sort={$sortCoin}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                </th>
                                <th>Diamond
                                        <a href="{{ URL::to("players?field=diamond&sort={$sortDiamond}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                </th>
                                <th>Costume
                                        <a href="{{ URL::to("players?field=costume&sort={$sortCostume}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                </th>
                                <th>Gender
                                        <a href="{{ URL::to("players?field=gender&sort={$sortGender}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                </th>
                                <th>Actions</th> 
                            </tr>
                        </thead>
                        <tbody>
                        
                            @foreach($players as $row)
                            <tr>
                                <td>{{ $row['playerName'] }}</td>
                                <td></td>
                                <td>{{ $row['coin'] }}</td>
                                <td>{{ $row['diamond'] }}</td>
                                <td>
                                    @isset($row['costumes']['costumeName']['eng'])
                                    {{ $row['costumes']['costumeName']['eng'] }}    
                                    @endisset
                                    
                                </td>
                                <td>{{ $row['gender'] }}</td>
                               

                                 <td>
                                    <a href="{{{ URL::to('players/' . $row['_id'] . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('players.destroy', collect($row)->first()) }}">
                                    <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                </a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$players->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete User
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this user? This operation is irreversible.
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     "pageLength": 50
        // });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });

</script>







@stop