@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>News Types</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('newsTypes') }}"> News Types</a></li>
        <li class="active">Add new</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Add New</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                <form id="newsTypesForm" action="{{ route('newsTypes.store') }}" method="POST" enctype="multipart/form-data"
                    class="form-horizontal">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <fieldset>
                    @include('newsTypes.field')
                    </fieldset>

                </form>

            </div>
        </div>
    </div>
</section>


@stop