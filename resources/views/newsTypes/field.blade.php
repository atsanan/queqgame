    @component('components.multiLangField',[
        'header'=>'News Type Names',
        'field'=>'newsTypeName',
        'icon'=>'livicon'
        ])
    @endcomponent

    @component('components.multiLangField',[
        'header'=>'News Type Details',
        'field'=>'newsTypeDetail',
        'icon'=>'livicon'
        ])
    @endcomponent
    <h4>Multi</h4>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Order',
        'field'=>'order',
        'value'=>$order,
        'attribute'=>['class'=>'form-control','placeholder'=>'Order']
    ])
    @endcomponent
</div>

    <div class="form-group">
        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
    </div>