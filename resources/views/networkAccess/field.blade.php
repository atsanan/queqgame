@php
 $strRandom=str_random(250);   
@endphp
<div class="row">
    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'text',
        'label'=>'Server',
        'field'=>'server',
        'icon'=>'icon',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Server']
        ])
        @endcomponent
    </div>
    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'textArea',
        'label'=>'Api Key',
        'field'=>'apiKey',
        'icon'=>'icon',
        'value'=>isset($networkAccess->apiKey)?null:$strRandom,
        'attribute'=>['class'=>'form-control','placeholder'=>'Api Key','rows'=>5,'disabled'=>true]
        ])
        @endcomponent
        {!! Form::hidden('apiKey',isset($networkAccess->apiKey)?null:$strRandom, []) !!}
    </div>
    <div class="col-md-9">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Descrtption',
            'field'=>'description',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Descrtption']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-9">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>empty($networkAccess)?true:null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-responsive btn-primary btn-sm save" >Save</button>
    </div>
</div>