@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop {{-- page level styles --}} 
@section('content')
<section class="content-header">
    <h1>Network Access</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('networkAccess') }}"> Network Access</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                {!! Form::model($networkAccess, ['url' => URL::to('networkAccess') . '/' . $networkAccess->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />

                    <fieldset>
                        @include('networkAccess.field')                        
                    </fieldset>

                {!! Form::close() !!}

                <div class="col-lg-12">
                        <div class="panel panel-info filterable ">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title pull-left">Ip Access</h3>
                                <div class="pull-right">
                                <a href="{{ URL::to('ipAccess/create?networkAccessId='.$networkAccess->id) }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                                </div>
                            </div>
            
                            <div class="panel-body table-responsive">
                                <table class="table" id="table">
                                        <thead>
                                                <tr class="filters">
                                                    <th>Ip Name</th>
                                                    <th>Ip Address</th>
                                                    <th>Active</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    @foreach($ipAccess as $row)
                                                    <tr>
                                                        <td>{{$row->ipName}}</td>
                                                        <td>{{$row->ipAddress}}</td>
                                                        <td>
                                                            @if ($row->isActive)
                                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                                            @else
                                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                                <a href="{{ URL::to('ipAccess/' . $row->id . '/edit' ) }}">
                                                                    <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('ipAccess.destroy', collect($row)->first()) }}">
                                                                    <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                                </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
    
            
                                            </tbody>                
                                </table>
                  <!-- Modal for showing delete confirmation -->
                  <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
    <!--row end-->
</section>
@stop {{-- page level scripts --}} 
