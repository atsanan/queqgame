@component('components.multiLangField',[
    'header'=>'Sticker Store Names',
    'field'=>'stickerStoreName',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Sticker Store Details',
    'field'=>'stickerStoreDetail',
    'icon'=>'livicon'
    ])
@endcomponent
<h4>Multi</h4>
<div class="row">
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Level',
        'field'=>'stickerStoreLavel',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Level']
    ])
    @endcomponent
</div>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Coin',
        'field'=>'coin',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Coin']
    ])
    @endcomponent
</div>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Diamond',
        'field'=>'diamond',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Diamond']
    ])
    @endcomponent
</div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
        <div class="col-md-6 col-md-offset-2">
                @if(!empty($stickerStoreImages))
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                    <img src="{{URL::to('media/images/'.$stickerStoreImages)}}" class="img-responsive" alt="$field">
                </div>
                @endif
            </div>    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Sticker Image',
            'field'=>'stickerStoreImage',
            'value'=>$stickerStoreImages,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
</div>
<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

