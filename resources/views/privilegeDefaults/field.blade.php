

@component('components.multiLangField',[
    'header'=>'Privilege Default Names',
    'field'=>'privilegeDefaultNames',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Privilege Default Details',
    'field'=>'privilegeDefaultDetails',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>

<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Privilege Group',
            'field'=>'privilegeGroupId',
            'value'=>null,
            'data'=>$privilegeGroups ,
            'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Order',
            'field'=>'order',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Order']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-md-2 col-xs-offset-2">
        @if(!empty($filenameLogos1))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 210px; height: 160px;">
                <img src="{{URL::to('media/images/'.$filenameLogos1)}}" class="img-responsive" alt="$field"style="width: 200px; height: 150px;">
            </div>
        @endif
    </div>
    <div class="col-md-4 col-xs-offset-4">
        @if(!empty($filenameLogos2))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 210px; height: 160px;">
                <img src="{{URL::to('media/images/'.$filenameLogos2)}}" class="img-responsive" alt="$field" style="width: 200px; height: 150px;">
            </div>
        @endif
    </div> 

    <div class="col-md-6"> 
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 1',
            'field'=>'filenameLogo1',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Logo 2',
                'field'=>'filenameLogo2',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

