
                <div class="col-lg-12">
                    <ul class="nav  nav-tabs">
                        <li class="active">
                            <a href={{'#privilege'}} data-toggle="tab">Privilege</a>
                        </li>
                        <li>
                            <a href={{'#mall'}} data-toggle="tab">Mall</a>
                        </li>
                        <li>
                            <a href={{'#promotion'}} data-toggle="tab">Promotion</a>
                        </li>
                    </ul>
                    <div class="tab-content mar-top">
                        <div id={{'privilege'}} class="tab-pane fade active in">
                            <div class="row">
                                            <div class="col-md-12">
                                            @component('components.inputField',[
                                                            'type'=>'select',
                                                            'icon'=>'text',
                                                            'label'=>'Privilege Groups',
                                                            'field'=>'privilegeGroups',
                                                            'data'=>$privilegeGroups,
                                                            'value'=>null,
                                                            'attribute'=>['class'=>'form-control  privilegeGroups','placeholder'=>'Please Select']
                                                    ])
                                                    @endcomponent
                                            </div>
                                            <div class="col-md-12">
                                                    @component('components.inputField',[
                                                    'type'=>'select',
                                                    'icon'=>'text',
                                                    'label'=>'PrivilegeDefaults',
                                                    'field'=>'privilegeDefaults',
                                                    'data'=>$privilegeDefaults,
                                                    'value'=>null,
                                                    'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
                                                    ])
                                                    @endcomponent
                                            </div>
                    
                                            <div class="form-group">
                                                    <div class="col-md-12 col-md-offset-3">
                                                            <button type="button" class="btn btn-responsive btn-info btn-sm searchPlayer" >Search</button>
                                                    </div>
                                            </div>
                                                
                            </div>
                        </div>
                        <div id={{'mall'}} class="tab-pane fade">
                            <div class="row">
                                    <div class="col-md-12">
                                    @component('components.inputField',[
                                                    'type'=>'select',
                                                    'icon'=>'text',
                                                    'label'=>'Mall',
                                                    'field'=>'mall',
                                                    'data'=>$malls,
                                                    'value'=>null,
                                                    'attribute'=>['class'=>'form-control','placeholder'=>'--- Select Malls ---']
                                            ])
                                            @endcomponent
                                    </div>
                                    <div class="col-md-12">
                                            @component('components.inputField',[
                                            'type'=>'select',
                                            'icon'=>'text',
                                            'label'=>'MallFloor',
                                            'field'=>'mallFloor',
                                            'data'=>$mallFloors,
                                            'value'=>null,
                                            'attribute'=>['class'=>'form-control','placeholder'=>'--- Select Mall Floors ---']
                                            ])
                                            @endcomponent
                                    </div>
                                    <div class="col-md-12">
                                            @component('components.inputField',[
                                            'type'=>'select',
                                            'icon'=>'text',
                                            'label'=>'Shop',
                                            'field'=>'shop',
                                            'data'=>$shops,
                                            'value'=>null,
                                            'attribute'=>['class'=>'form-control','placeholder'=>'--- Select Shops ---']
                                            ])
                                            @endcomponent
                                    </div>
            
                                    <div class="form-group">
                                            <div class="col-md-12 col-md-offset-3">
                                                    <button type="button" class="btn btn-responsive btn-info btn-sm searchPlayerByShop" >Search</button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                        <div id={{'promotion'}} class="tab-pane fade">
                                <div class="row">
                                        <div class="col-md-12">
                                        @component('components.inputField',[
                                                        'type'=>'select',
                                                        'icon'=>'text',
                                                        'label'=>'News',
                                                        'field'=>'news',
                                                        'data'=>$news,
                                                        'value'=>null,
                                                        'attribute'=>['class'=>'form-control','placeholder'=>'--- Select News ---']
                                                ])
                                                @endcomponent
                                        </div>
                                </div> 
                                
                                <div class="form-group">
                                        <div class="col-md-12 col-md-offset-3">
                                                <button type="button" class="btn btn-responsive btn-info btn-sm searchPlayerByShop" >Search</button>
                                        </div>
                                </div>   
                        </div>

                        
                    </div>
                </div>
