<div class="row">
             
        <div class="col-md-10">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'User',
        'field'=>'userId[]',
        'data'=>$player,
        'value'=>null,
        'attribute'=>['class'=>'form-control select2','multiple'=>true,'disabled'=>!empty($mailBox)]
        ])
        @endcomponent
        </div>
        @if(empty($mailBox))
        @component('mailBox.search',
        ['privilegeGroups'=>$privilegeGroups,
        'privilegeDefaults'=>$privilegeDefaults,
        'malls'=>$malls,
        'mallFloors'=>$mallFloors,
        'shops'=>$shops,
        'news'=>$news
        ])@endcomponent
        @endif
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'title',
                    'icon'=>'text',
                    'label'=>'Title',
                    'field'=>'title',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'textArea',
                    'icon'=>'text',
                    'label'=>'Detail',
                    'field'=>'detail',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','rows'=>3]
                ])
                @endcomponent
        </div>
     
</div>
<h4>Attachment</h4>
<div class="row">
    
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'text',
                    'icon'=>'text',
                    'label'=>'Url',
                    'field'=>'url',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
      
        
      
        <div id="preview-img" class="col-md-12 col-md-offset-6" style="display:none">
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img id="img" src="" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                </div>
        </div>

        @if(!empty($mailBox->imageName))
        <div class="col-md-12 col-md-offset-6">
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img src="{{URL::to('media/images/'.$mailBox->imageName)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                </div>
        </div>
       
        <div class="col-md-12 col-md-offset-5"> 
                <div class="form-group">       
                        <p>{{URL::to('media/images/'.$mailBox->imageName)}}</p>
                </div>
        </div>
        @endif
       

        <div class="col-md-12">

                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Image',
                    'field'=>'filename',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','disabled'=>!empty($mailBox)]
                ])
                @endcomponent
        </div>
        
        <div id="preview-thumbnail" class="col-md-12 col-md-offset-6" style="display:none">
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img id="img-thumbnail" src="" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                </div>
        </div>

        @if(!empty($mailBox->thumbnail))
        <div class="col-md-12 col-md-offset-6">
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img src="{{URL::to('media/images/'.$mailBox->thumbnail)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
              
                </div>
             
        </div>
       
        <div class="col-md-12 col-md-offset-5"> 
                <div class="form-group">       
                        <p>{{URL::to('media/images/'.$mailBox->thumbnail)}}</p>
                </div>
        </div>
        @endif
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Thumbnail',
                    'field'=>'thumbnailName',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','disabled'=>!empty($mailBox)]
                ])
                @endcomponent
        </div>
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'number',
                    'icon'=>'text',
                    'label'=>'Coin',
                    'field'=>'coin',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12 ">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'icon'=>'text',
                    'label'=>'Coin Active',
                    'field'=>'coinActive',
                    'value'=>null, 
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'number',
                    'icon'=>'text',
                    'label'=>'Diamond',
                    'field'=>'diamond',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12 ">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'icon'=>'text',
                    'label'=>'Diamond Active',
                    'field'=>'diamondActive',
                    'value'=>null, 
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12">
                @component('components.inputField',[
                'type'=>'select',
                'label'=>'Item',
                'field'=>'itemId',
                'value'=>null,
                'data'=>$itemDefaults,
                'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
                ])
                @endcomponent
        </div>
        <div class="col-md-12 ">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'icon'=>'text',
                    'label'=>'Item Active',
                    'field'=>'itemActive',
                    'value'=>null, 
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12">
                @component('components.inputField',[
                'type'=>'select',
                'label'=>'Monster',
                'field'=>'mDefaultId',
                'value'=>null,
                'data'=>$monsterDefaults,
                'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
                ])
                @endcomponent
        </div>
        <div class="col-md-12 ">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'icon'=>'text',
                    'label'=>'Monster Active',
                    'field'=>'monsterActive',
                    'value'=>null, 
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12 ">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'icon'=>'text',
                    'label'=>'Active',
                    'field'=>'isActive',
                    'value'=>empty($mailBox)?true:null, 
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="form-group">
                <div class="col-md-12 text-right">
                        @if(!empty($mailBox))
                        <button type="button" class="btn btn-responsive btn-success btn-sm pushMail" >Push</button>
                        @endif
                        <button type="button" class="btn btn-responsive btn-primary btn-sm save" >Save</button>
                </div>
        </div>
               
</div>

