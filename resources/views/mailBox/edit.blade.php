@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Mail Box</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('malls') }}"> Mail Box</a></li>
        <li class="active">Edit</li>
    </ol>
</section>


@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h4 class="panel-title">editMail</h4>
            </div>
            <div class="panel-body">
                    {!! Form::model($mailBox, ['url' => URL::to('mailBox') . '/' . $mailBox->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="linkListName" value="{{$mailBox->linkListName}}" />                   
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="monsterActive" value="0" />
                    <input type="hidden" name="coinActive" value="0" />
                    <input type="hidden" name="diamondActive" value="0" />
                    <input type="hidden" name="itemActive" value="0" />

                    <fieldset>
                    @include('mailBox.field')
                    </fieldset>
                
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>

@stop 

@push('more_scripts')
<script>

$(".pushMail").on("click", function(e){
    $.ajax({
            url: "https://gaming.inmall.asia/2019/api/v1/mailBox/sendMailBox",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data:  JSON.stringify({
                listUser:["{{$mailBox->userId}}"],
                title:$("input[name='title']").val(),
                detail:$("textarea[name='detail']").val(),
                linkListName:$("input[name='linkListName']").val(),
            }),
            success: function(data) {
                
                window.location ="{{ url('/mailBox/push/') }}"+"/"+"{{$mailBox->id}}"
            }

        });
})
$(".save").on("click", function(e){
$('form').submit();
})

</script>
@endpush
