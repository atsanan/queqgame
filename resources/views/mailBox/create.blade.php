@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Mail Box</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('malls') }}"> Mail Box</a></li>
        <li class="active">Edit</li>
    </ol>
</section>


@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h4 class="panel-title">SendMail</h4>
            </div>
            <div class="panel-body">
                <form id="mailBox" action="{{ route('mailBox.store') }}" method="POST" enctype="multipart/form-data"
                    class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="linkListName" value="{{uniqid()}}" />
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="thumbnail" value="" />
                    <input type="hidden" name="image" value="" />
                    <input type="hidden" name="checkImage" value="" />
                    <fieldset>
                    @include('mailBox.field')
                    </fieldset>
                </form>

            </div>
        </div>
    </div>
</section>

@stop 

@push('more_scripts')
<script>

$(".save").on("click", function(e){
    e.preventDefault();
   
    $.ajax({
            url: "https://gaming.inmall.asia/2019/api/v1/mailBox/sendMailBox",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data:  JSON.stringify({
                listUser:($("select[name='userId[]']").val()!=null)?$("select[name='userId[]']").val():[],
                title:$("input[name='title']").val(),
                detail:$("textarea[name='detail']").val(),
                linkListName:$("input[name='linkListName']").val(),
            }),
            success: function(data) {
                $('#mailBox').submit();
            }

        });
  
});
$(".privilegeGroups").change(function (){
    $.ajax({
            url: "{{ URL::to('api/privilegeDefault/searchByPrivilegeGroups') }}",
            method: 'GET',
            data: "privilegeGroupId="+$(this).val(),
            success: function(data) {
             $("select[name='privilegeDefaults'").html('');
             $("select[name='privilegeDefaults'").html(data.options);
            }
        });
})
$(".searchPlayer").click(function (){
        
        $.ajax({
            url: "{{ URL::to('api/privilegeDefaultPlayers/searchPlayer') }}",
            method: 'GET',
            data: "privilegeDefaultId="+$("#privilegeDefaults").val(),
            success: function(data) {
             $("select[name='userId[]']").html('');
             $("select[name='userId[]']").html(data.options);
            }
        });
        
})

 
$("select[name='mall']").change(function(){
    $.ajax({
        url: "{{ URL::to('mallFloors/searchByMall') }}",
        method: 'GET',
        data: "mallId="+$(this).val(),
        success: function(data) {
        
        $("select[name='mallFloor']").html('');
        $("select[name='mallFloor']").html(data.options);
        
        }
    });
});

$("select[name='mallFloor']").change(function(){
    $.ajax({
        url: "{{ URL::to('shops/searchByMallFloor') }}",
        method: 'GET',
        data: "mallFloorId="+$(this).val(),
        success: function(data) {
        $("select[name='shop']").html('');
        $("select[name='shop']").html(data.options);
        
        }
    });
});

$("select[name='shop']").change(function(){

    $.ajax({
        url: "{{ URL::to('api/news/searchByShop') }}",
        method: 'GET',
        data: "shopId="+$(this).val(),
        success: function(data) {
        $("select[name='news']").html('');
        $("select[name='news']").html(data.options);
        
        }
    });
});

$("select[name='news']").change(function(){

    $.ajax({
        url: "{{ URL::to('api/news') }}/"+$(this).val(),
        method: 'GET',
        success: function(result) {
            let news=result.data 
            $("input[name='title']").val(news.newsName.eng);
            $("textarea[name='detail']").val(news.newsDetail.eng);  
            if(news.filenameImage1!=undefined&&$("input[name='thumbnailName']").val()==""){
            $("#preview-thumbnail").css("display","block");
            $("#img-thumbnail").attr("src","/media/images/"+news.filenameImage1); 
            $("input[name='thumbnailName']").attr("disabled",true);    
            $("input[name='thumbnail']").val(news.filenameImage1);
            }

            if(news.filenameImage2!=undefined&&$("input[name='filename']").val()==""){
            $("#preview-img").css("display","block");
            $("#img").attr("src","/media/images/"+news.filenameImage2);
            $("input[name='filename']").attr("disabled",true);
            $("input[name='image']").val(news.filenameImage2);
            }
        }
    });

});

$(".searchPlayerByShop").click(function (){
    let query="";
    if($("select[name='mall']").val()!=""){
        query+="&mallId="+$("select[name='mall'").val();
    }
    if($("select[name='mallFloor']").val()!=""){
        query+="&mallFloorId="+$("select[name='mallFloor'").val();
    }

    if($("select[name='shop']").val()!=""){
        query+="&shopId="+$("select[name='shop']").val();
    }

    if($("select[name='news']").val()!=""){
        query+="&newsId="+$("select[name='news']").val();
    }
   
    $.ajax({
        url: "{{ URL::to('/api/shops/searchByPlayer') }}",
        method: 'GET',
        data: query,
        success: function(data) {
        $("select[name='userId[]'").html('');
        $("select[name='userId[]'").html(data.options);
        
        }
    });
})
</script>
@endpush