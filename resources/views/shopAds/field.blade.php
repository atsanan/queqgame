@component('components.multiLangField',[
    'header'=>'Shop Ads Names',
    'field'=>'shopAdsName',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>
<div class="row">
    <div class="col-md-2 col-xs-offset-2">
        @if(!empty($shopAdsImages))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 210px; height: 160px;">
            <img src="{{URL::to('media/images/'.$shopAdsImages)}}" class="img-responsive" alt="$field"  style="width: 200px; height: 150px;">
        </div>
        @endif
    </div> 
    @if(!empty($shopAdsImagelist))    
    <div class="col-md-6 col-xs-offset-2">
        <div class="form-group row">
                <label for="inputUsername" class="col-md-3 control-label">shop Ads Image Lists</label>
                @component('components.modal',['id'=>uniqid(),'title'=>'ImageSlots'])
                    @slot('content')
                    @foreach($shopAdsImagelist as $photo)
                        <img src="{{URL::to('media/images/'.$photo)}}" style="width: 100px; height: 70px" />
                    @endforeach
                    @endslot
                @endcomponent
        </div>   
    </div> 
    @endif    
</div>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Shop Ads Image',
            'field'=>'shopAdsImage',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Shop Ads Image Lists (Multiple)',
            'field'=>'imageSlots[]',
            'value'=>null,
            'attribute'=>['class'=>'form-control','multiple'=>true]
        ])
        @endcomponent
    </div>
</div>


<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

