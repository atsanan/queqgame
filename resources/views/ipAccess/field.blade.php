<div class="row">
        <div class="col-md-9">
            @component('components.inputField',[
                'type'=>'select',
                'label'=>'NetworkAccess',
                'field'=>'networkAccessId',
                'value'=>$networkAccessId,
                'data'=>$networkAccess,
                'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            ])
            @endcomponent
        </div>
        <div class="col-md-9">
            @component('components.inputField',[
            'type'=>'text',
            'label'=>'Ip Name',
            'field'=>'ipName',
            'icon'=>'icon',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Ip Name']
            ])
            @endcomponent
        </div>
        <div class="col-md-9">
            @component('components.inputField',[
            'type'=>'text',
            'label'=>'Ip Address',
            'field'=>'ipAddress',
            'icon'=>'icon',
            'value'=>isset($ipAccess->ipAddress)?null:$_SERVER['REMOTE_ADDR'],
            'attribute'=>['class'=>'form-control','placeholder'=>'Ip Address']
            ])
            @endcomponent
        </div>

        
    <div class="col-md-9">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>isset($ipAccess->isActive)?null:true, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>

        <div class="form-group">
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
            </div>
        </div>
</div>