
@component('components.multiLangField',[
    'header'=>'Privacy Title',
    'field'=>'privacyPolicyTitle',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Privacy Body',
    'field'=>'privacyPolicyBody',
    'icon'=>'livicon'
    ])
@endcomponent

<div class="row">
        <div class="col-md-8">
                @component('components.inputField',[
                    'type'=>'number',
                    'icon'=>'text',
                    'label'=>'Privacy Policy Version',
                    'field'=>'privacyPolicyVersion',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','placeholder'=>'Privacy Policy Version']
                ])
                @endcomponent
        </div>

        <div class="col-md-4">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'label'=>'Active',
                    'field'=>'isActive',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
            </div>

<div class="form-group">  
        <div class="col-md-12 text-right">
            @if(isset($privacyPolicy))
            <a class="btn btn-responsive btn-success btn-sm" href="{{{ URL::to('privacyPolicy/'.$privacyPolicy->privacyPolicyVersion.'/setupCurrent') }}}" role="button">Setup current</a>
            @endif
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
</div>
</div>

@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/ckeditor/js/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/ckeditor/js/jquery.js')}}"></script>

<script>
        
    
    $('textarea').ckeditor({
        height: '200px'
     });
    
    </script>
    
@endpush