@extends('layouts/default') {{-- Page title --}} 
@section('title') {!!$privacyPolicy->privacyPolicyTitle['eng']!!} @parent 
@stop
@section('content')

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
   
            <div class="panel-body">
                    <div class="box-body">
                    {!!$privacyPolicy->privacyPolicyBody['eng']!!}
                    </div>
            </div>
        </div>
    </div>
</section>


@stop