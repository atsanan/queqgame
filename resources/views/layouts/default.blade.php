<!DOCTYPE html>
<html>
 
<head>
    <meta charset="UTF-8">
    <title>
        
    @section('title') | Inmall game System @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    {{--CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- global css -->

    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />
    
    <link href="{{ asset('css/flot.css') }}" rel="stylesheet"  type="text/css" />
    <!-- font Awesome -->

    <!-- end of global css -->
    <!--page level css-->
    @yield('header_styles')
    <!--end of page level css-->
</head>
    @if (Auth::check())
    <body class="skin-josh">
        <header class="header">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/logo-inmall.png') }}" alt="logo">
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <div>
                    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                        <div class="responsive_nav"></div>
                    </a>
                </div>
                <div class="navbar-right toggle">
                    <ul class="nav navbar-nav  list-inline">
                        <li class=" nav-item dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('assets/img/authors/user_default.jpg')}}" width="35" class="img-circle img-responsive pull-left" height="35" alt="riot">
                                <div class="riot">
                                    <div>
                                        {{ Auth::user()->name }}
                                        <span>
                                            <i class="caret"></i>
                                        </span>
                                    </div>
                                </div>
                            </a>

                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div>
                                        <a href="{{ URL::to('logout') }}" 
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                            <i class="livicon" data-name="sign-out" data-s="18"></i>
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
            </nav>
        </header>
      
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar ">
                    <div class="page-sidebar  sidebar-nav">
                        <br/><br/>
                        <div class="clearfix"></div>
                        <!-- BEGIN SIDEBAR MENU -->
                       
                        @include('layouts._left_menu')
                       
                        <!-- END SIDEBAR MENU -->
                    </div>
                </section>
            </aside>
            <aside class="right-side">
                <!-- Content -->
                @yield('content')
            
            </aside>
            <!-- right-side -->
        </div>
            
      
        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
            <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
        </a>
        
        <!-- global js -->
        <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
        <!-- end of global js -->
        <!-- begin page level js -->
        @yield('footer_scripts')
        @stack('more_scripts')
        <!-- end page level js -->
    </body>

        @else
        <body>
            @yield('content')
            <script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
            @stack('more_scripts')
            
        </body>
        @endif
        
</html>