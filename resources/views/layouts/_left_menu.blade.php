       <ul id="menu" class="page-sidebar-menu"> 
            <li  {!! (Request::is('home') ? 'class="active"' : '') !!}>
                <a href="{{ route('home') }}">
                    <i class="livicon" data-name="dashboard" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            @role("Resource")
            <li {!! (Request::is('monstersDefault','monstersDefaultsTypes','itemDefaults','itemCategorys','stickerStores','costumes','giveMonster/edit') ? 'class="active"' : '') !!} >
                <a href="#">
                    <i class="livicon" data-name="info" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Resource</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">

                <li {!! (Request::is('monstersDefault') ? 'class="active"' : '') !!}>
                    <a href="{{ route('monstersDefault.index') }}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Monster Defaults</span>
                    </a>
                </li>

                <li {!! (Request::is('monstersDefaultsTypes') ? 'class="active"' : '') !!}>
                    <a href="{{ route('monstersDefaultsTypes.index')}}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Monster Default Types</span>
                    </a>
                </li>

                
                <li {!! (Request::is('giveMonster/edit') ? 'class="active"' : '') !!}>
                    <a href="{{ Url::to('giveMonster/edit')}}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Give Monster </span>
                    </a>
                </li>

                <li {!! (Request::is('itemDefaults') ? 'class="active"' : '') !!}>
                    <a href="{{ route('itemDefaults.index') }}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Item Defaults</span>
                    </a>
                </li>

                <li {!! (Request::is('itemCategorys') ? 'class="active"' : '') !!}>
                    <a href="{{ route('itemCategorys.index') }}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Item Categories</span>
                    </a>
                </li>

                <li {!! (Request::is('stickerStores') ? 'class="active"' : '') !!}>
                    <a href="{{ route('stickerStores.index') }}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Sticker Stores</span>
                    </a>
                </li>

                <li {!! (Request::is('costumes') ? 'class="active"' : '') !!}>
                    <a href="{{ route('costumes.index') }}">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="title">Costumes</span>
                    </a>
                </li>
            </ul>
        </li>
        @endrole
        @role("Wild_Resource")
            <li {!! (Request::is('wildItems','wildMonsters','zoneTypes') ? 'class="active"' : '') !!} >
                <a href="#">
                    <i class="livicon" data-name="map" data-size="18" data-c="#F89A14" data-hc="#F89A14" data-loop="true"></i>
                    <span class="title">Wild Resource</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li {!! (Request::is('wildItems') ? 'class="active"' : '') !!}>
                        <a href="{{ route('wildItems.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Wild Items</span>
                        </a>
                    </li>
                    
                    <li {!! (Request::is('wildMonsters') ? 'class="active"' : '') !!}>
                        <a href="{{ route('wildMonsters.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Wild Monsters</span>
                        </a>
                    </li>

                    <li {!! (Request::is('zoneTypes') ? 'class="active"' : '') !!}>
                        <a href="{{ route('zoneTypes.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Zone Types</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endrole
            @role("Mall_Resource")
            <li {!! (Request::is('malls','mallFloors','shops','shopAds','shopAsserts','shopCategories') ? 'class="active"' : '') !!}>
                <a href="#">
                    <i class="livicon" data-name="home" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C" data-loop="true"></i>
                    <span class="title">Mall Resources </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li {!! (Request::is('malls') ? 'class="active"' : '') !!}>
                        <a href="{{ route('malls.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Malls</span>
                        </a>
                    </li>

                    <li {!! (Request::is('mallFloors') ? 'class="active"' : '') !!}>
                        <a href="{{ route('mallFloors.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Mall Floors</span>
                        </a>
                    </li>
                    <li {!! (Request::is('shops') ? 'class="active"' : '') !!}>
                        <a href="{{ route('shops.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Shops</span>
                        </a>
                    </li>
                    
                    <li {!! (Request::is('shopAds') ? 'class="active"' : '') !!}>
                        <a href="{{ route('shopAds.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Shop Ads</span>
                        </a>
                    </li>
                    <li {!! (Request::is('shopAsserts') ? 'class="active"' : '') !!}>
                        <a href="{{ route('shopAsserts.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Shop Asserts</span>
                        </a>
                    </li>
                    <li {!! (Request::is('shopCategories') ? 'class="active"' : '') !!}>
                        <a href="{{ route('shopCategories.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Shop Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endrole
            @role("Player_Resource")
            <li {!! (Request::is('players','user','device','privacyPolicy','playerLogs','roles') ? 'class="active"' : '') !!}>
                <a href="#">
                    <i class="livicon" data-name="users" data-size="18" data-c="#5bc0de" data-hc="#5bc0de" data-loop="true"></i>
                    <span class="title">Player Resource</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li {!! (Request::is('players') ? 'class="active"' : '') !!}>
                        <a href="{{ route('players.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Players</span>
                        </a>
                    </li>
                    <li {!! (Request::is('playerLogs') ? 'class="active"' : '') !!}>
                            <a href="{{ route('playerLogs.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="title">Player Logs</span>
                            </a>
                    </li>
                    <li {!! (Request::is('user') ? 'class="active"' : '') !!}>
                            <a href="{{ route('user.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="title">Users</span>
                            </a>
                    </li>
                    <li {!! (Request::is('device') ? 'class="active"' : '') !!}>
                            <a href="{{ route('device.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="title">Devices</span>
                            </a>
                    </li>
                    <li {!! (Request::is('privacyPolicy') ? 'class="active"' : '') !!}>
                            <a href="{{ route('privacyPolicy.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="title">Privacy Policy</span>
                            </a>
                    </li>
                    @role('Role_Menage')
                    <li {!! (Request::is('roles') ? 'class="active"' : '') !!}>
                        <a href="{{ route('roles.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Role Menage</span>
                        </a>
                    </li>
                    @endrole

                </ul>
            </li>
            @endrole
            @role("Privilege_Resource")
            <li {!! (Request::is('privilegeDefaults','privilegeGroups','privilegeShops') ? 'class="active"' : '') !!}>
                <a href="#">
                    <i class="livicon" data-name="credit-card" data-size="18" data-c="#5bc0de" data-hc="#5bc0de" data-loop="true"></i>
                    <span class="title">Privilege Resource</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li {!! (Request::is('privilegeDefaults') ? 'class="active"' : '') !!}>
                        <a href="{{ route('privilegeDefaults.index')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Privilege Defaults</span>
                        </a>
                    </li>
                    <li {!! (Request::is('privilegeGroups') ? 'class="active"' : '') !!}>
                        <a href="{{ route('privilegeGroups.index')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Privilege Groups</span>
                        </a>
                    </li>
                    <li {!! (Request::is('privilegeShops') ? 'class="active"' : '') !!}>
                        <a href="{{ route('privilegeShops.index')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Privilege Shops</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endrole
            @role("Promotion_Resource")
            <li {!! (Request::is('news','newsTypes','mailBox/create','loginReward','activities/calender') ? 'class="active"' : '') !!}>
                <a href="#">
                    <i class="livicon" data-name="notebook" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                    <span class="title">Promotion Resource</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li {!! (Request::is('news') ? 'class="active"' : '') !!}>
                        <a href="{{ route('news.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Promotion</span>
                        </a>
                    </li>

                    <li {!! (Request::is('newsTypes') ? 'class="active"' : '') !!}>
                        <a href="{{ route('newsTypes.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">News Types</span>
                        </a>
                    </li>
                    <li {!! (Request::is('mailBox/create') ? 'class="active"' : '') !!}>
                            <a href="{{ route('mailBox.create') }}">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="title">Create MailBox</span>
                            </a>
                    </li>

                    <li {!! (Request::is('loginReward') ? 'class="active"' : '') !!}>
                        <a href="{{ route('loginReward.index') }}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Login Reward</span>
                        </a>
                    </li>
                    
                    <li {!! (Request::is('activities/calender') ? 'class="active"' : '') !!}>
                        <a href="{{ route('activities.calender')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Activities Calender</span>
                        </a>
                    </li>

                </ul>
            </li>
           @endrole
           {{-- @role("authenType")
            <li {!! (Request::is('authenTypes') ? 'class="active"' : '') !!}>
                <a href="{{ route('authenTypes.index') }}">
                    <i class="livicon" data-name="globe" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                    <span class="title">Authen Types</span>
                </a>
            </li>
            @endrole --}}
            @role("Report_Resource")   
            <li {!! (Request::is('report/monsterInShop','report/monstersDefault','report/wildMonsters','report/monstersInstantiate','report/itemPlayer') ? 'class="active"' : '') !!}>
                <a href="#">
                    <i class="livicon" data-name="notebook" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                    <span class="title">Report 
                       
                    </span>
                    <span class="fa arrow"></span>
                </a>


                <ul class="sub-menu">
                  
                    
                    <li {!! (Request::is('report/monsterInShop') ? 'class="active"' : '') !!}>
                            <a href="{{ route('report.monsterInShop')}}">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="title">Monster In Shop</span>
                            </a>
                    </li>
 
                    <li {!! (Request::is('report/monstersDefault') ? 'class="active"' : '') !!}>
                        <a href="{{ route('report.monstersDefault')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Monster Defaults</span>
                        </a>
                    </li>

                    
                    <li {!! (Request::is('report/wildMonsters') ? 'class="active"' : '') !!}>
                        <a href="{{ route('report.wildMonsters')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Wild Monsters </span>
                        </a>
                    </li>

                    <li {!! (Request::is('report/monstersInstantiate') ? 'class="active"' : '') !!}>
                        <a href="{{ route('report.monstersInstantiate')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Monsters Instantiate</span>
                        </a>
                    </li>

                    
                    <li {!! (Request::is('report/itemPlayer') ? 'class="active"' : '') !!}>
                        <a href="{{ route('report.itemPlayer')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Item Player Coupon</span>
                        </a>
                    </li>

                    
                    <li {!! (Request::is('report/itemCoupon') ? 'class="active"' : '') !!}>
                        <a href="{{ route('report.itemCoupon')}}">
                            <i class="fa fa-angle-double-right"></i>
                            <span class="title">Item Player Coupon Code</span>
                        </a>
                    </li>

                </ul>
                </li>
                @endrole
                @role("DigitalBoard_Resource")   
                <li {!! (Request::is('digitalBoard','digitalBoardAsserts') ? 'class="active"' : '') !!}>
                    <a href="#">
                        <i class="livicon" data-name="notebook" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                        <span class="title">DigitalBoard Resources
                           
                        </span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="sub-menu">
                            <li {!! (Request::is('digitalBoard') ? 'class="active"' : '') !!}>
                                <a href="{{ route('digitalBoard.index')}}">
                                    <i class="fa fa-angle-double-right"></i>
                                    <span class="title">Digital Board</span>
                                </a>
                            </li>
                            <li {!! (Request::is('digitalBoardAsserts') ? 'class="active"' : '') !!}>
                                    <a href="{{ route('digitalBoardAsserts.index')}}">
                                        <i class="fa fa-angle-double-right"></i>
                                        <span class="title">Digital Board Asserts</span>
                                    </a>
                            </li>
        
                    </ul>
                </li>   
                @endrole 
                @role("Setting_Resource")
                <li {!! (Request::is('networkAccess','ipAccess','patherTypes') ? 'class="active"' : '') !!}>
                        <a href="#">
                            <i class="livicon" data-name="notebook" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                            <span class="title">Setting
                               
                            </span>
                            <span class="fa arrow"></span>
                        </a>
    
                        <ul class="sub-menu">
                                <li {!! (Request::is('networkAccess') ? 'class="active"' : '') !!}>
                                    <a href="{{ route('networkAccess.index')}}">
                                        <i class="fa fa-angle-double-right"></i>
                                        <span class="title">Network access api</span>
                                    </a>
                                </li>
                                {{-- <li {!! (Request::is('ipAccess') ? 'class="active"' : '') !!}>
                                    <a href="{{ route('ipAccess.index')}}">
                                        <i class="fa fa-angle-double-right"></i>
                                        <span class="title">Ip access api</span>
                                    </a>
                                </li> --}}
                                <li {!! (Request::is('partnerTypes') ? 'class="active"' : '') !!}>
                                    <a href="{{ route('partnerTypes.index')}}">
                                        <i class="fa fa-angle-double-right"></i>
                                        <span class="title">Partner Types</span>
                                    </a>
                                </li>
                        </ul>
                @endrole
                {{-- <li {!! (Request::is('privacyPolicy') ? 'class="active"' : '') !!}>
                    <a href="{{ route('privacyPolicy.index') }}">
                        <i class="livicon" data-name="notebook" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                        <span class="title">Privacy Policy</span>
                    </a>
                </li> --}}
          
            
    </ul>
