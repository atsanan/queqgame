@extends('layouts/default') {{-- Page title --}} 
@section('title') Device Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
<section class="content-header">
    <h1>Device</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Device</a></li>
        <li class="active">Device Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Device</h3>
                </div>
                @php
                
                if($field=="deviceId"){
                    if($sort=="desc"){
                        $sortDeviceId="asc";
                    }else{
                        $sortDeviceId="desc";
                    }
                }else {
                    $sortDeviceId="asc";
                }   

                
                if($field=="deviceName"){
                    if($sort=="desc"){
                        $sortDeviceName="asc";
                    }else{
                        $sortDeviceName="desc";
                    }
                }else {
                    $sortDeviceName="asc";
                }   

                
                if($field=="deviceOS"){
                    if($sort=="desc"){
                        $sortDeviceOs="asc";
                
                    }else{
                    
                        $sortDeviceOs="desc";
                    }
                }else{
                
                    $sortDeviceOs="asc";
                }

                if($field=="createAt"){
                    if($sort=="desc"){
                        $sortCreateAt="asc";
                
                    }else{
                    
                        $sortCreateAt="desc";
                    }
                }else{
                
                    $sortCreateAt="asc";
                }

                if($field=="lastModified"){
                    if($sort=="desc"){
                        $sortLastModified="asc";
                
                    }else{
                    
                        $sortLastModified="desc";
                    }
                }else{
                
                    $sortLastModified="asc";
                }

               @endphp
                <div class="panel-body table-responsive">
                        <div class="col-md-12">
                                <div class="row">
                                <form id="wildMonstersForm" action="/device" method="GET" enctype="multipart/form-data">
                                    <!-- CSRF Token -->
                                    {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                                    <fieldset>
                                            <div class="col-md-6">
                                            <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                    {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                    <div class="col-md-9">
                                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                    </span>
                                                        {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                            </div>
                                                        {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                            </div>
                                    </fieldset>           
                                </form>
                                {{-- </div> --}}
                            </div>
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Device Id  
                                    <a href="{{ URL::to("device?field=deviceId&sort={$sortDeviceId}&search={$search}") }}" >
                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                 </th>
                                <th>Device Name
                                        <a href="{{ URL::to("device?field=deviceName&sort={$sortDeviceName}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Os
                                        <a href="{{ URL::to("device?field=deviceOS&sort={$sortDeviceOs}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>PrivacyPolicyVersion</th>
                                <th>Create At
                                        <a href="{{ URL::to("device?field=createAt&sort={$sortCreateAt}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Update At
                                        <a href="{{ URL::to("device?field=lastModified&sort={$sortLastModified}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($device as $row)
                            <tr>    
                                <td>{{$row->deviceId}}</td>
                                <td>{{$row->deviceName}}</td>
                                <td>{{$row->deviceOS}}</td>
                                <td>{{$row->privacyPolicyVersion}}</td>
                                <td>   
                                    @php
                                        $createAt = new DateTime($row->createAt, $gmtTimezone);
                                        $offsetcreateAt = $userTimezone->getOffset($createAt);
                                        $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                        $createAt->add($myIntervalcreateAt);
                                        echo $createAt->format('Y-m-d H:i:s');
                                    @endphp
                                </td>
                                <td>
                                    @php
                                        $lastModified = new DateTime($row->lastModified, $gmtTimezone);
                                        $offsetLastModified = $userTimezone->getOffset($lastModified);
                                        $myIntervalLastModified=DateInterval::createFromDateString((string)$offsetLastModified . 'seconds');
                                        $lastModified->add($myIntervalLastModified);
                                        echo $lastModified->format('Y-m-d H:i:s');
                                    @endphp
                                </td>
                                <td>
                                        <a href="{{{ URL::to('device/' . $row->deviceId . '/view') }}}">
                                            <i class="livicon" data-name="search" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c" title="view"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('players.destroy', collect($row)->first()) }}">
                                            <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        </a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$device->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     "pageLength": 50
        // });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })
</script>

@yield('modalScripts')
@stop