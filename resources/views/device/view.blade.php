@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop
@section('content')
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
<section class="content-header">
    <h1>Device</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('device') }}"> Device </a></li>
        <li class="active">View</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">view</h3>
            </div>
            <div class="panel-body">
                    <table id="user" class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Device Id</th>
                                    <td>
                                        {{$device->deviceId}}
                                    </td>
                                </tr>  
                                <tr>
                                    <th>Device Name</th>
                                    <td>
                                        {{$device->deviceName}}
                                    </td>
                                </tr>  
                                <tr>
                                    <th>Device Model</th>
                                    <td>
                                        {{$device->deviceModel}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Device Type</th>
                                    <td>
                                        {{$device->deviceType}}
                                    </td>
                                </tr> 
                                <tr>
                                    <th>Device Os</th>
                                    <td>
                                        {{$device->deviceOS}}
                                    </td>
                                </tr>  
                                <tr>
                                    <th>Privacy Policy Version</th>
                                    <td>
                                        {{$device->privacyPolicyVersion}}
                                    </td>
                                </tr>  
                                <tr>
                                    <th>Create At</th>
                                    <td>
                                        @php
                                        $createAt = new DateTime($device->createAt, $gmtTimezone);
                                        $offsetcreateAt = $userTimezone->getOffset($createAt);
                                        $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                        $createAt->add($myIntervalcreateAt);
                                        echo $createAt->format('Y-m-d H:i:s');
                                        @endphp
                                    </td>
                                </tr>  
                                <tr>
                                    <th>Update At</th>
                                    <td>
                                        @php
                                        $lastModified = new DateTime($device->lastModified, $gmtTimezone);
                                        $offsetLastModified = $userTimezone->getOffset($lastModified);
                                        $myIntervalLastModified=DateInterval::createFromDateString((string)$offsetLastModified . 'seconds');
                                        $lastModified->add($myIntervalLastModified);
                                        echo $lastModified->format('Y-m-d H:i:s');
                                        @endphp
                                    </td>
                                </tr>  
                                     
                            </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">User</h3>
                    <div class="pull-right">
                    </div>
                </div>

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Autnen Type</th>
                                <th>CreateAt</th>
                                <th>LastLogin</th>
                                <th>Action</th>
                                <th>Action</th>
                            </tr>
                        </thead
                        <tbody>

                            @foreach($user as $row)
                            <tr>
                                <td>{{$row->firstname}} </td>
                                <td>{{$row->lastname}} </td>
                                <td>{{$row->email}} </td>
                                <td>
                                    @if ($row->authenTypeId==1)
                                        FaceBook                                       
                                    @else
                                        Google
                                    @endif
                                </td>
                                <td>
                                    @php
                                        $createAt = new DateTime($row->createAt, $gmtTimezone);
                                        $offsetcreateAt = $userTimezone->getOffset($createAt);
                                        $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                        $createAt->add($myIntervalcreateAt);
                                        echo $createAt->format('Y-m-d H:i:s');
                                    @endphp
                                </td>
                                <td>
                                    @php
                                    if(isset($row->lastLogin)){
                                        $lastLogin = new DateTime($row->lastLogin, $gmtTimezone);
                                        $offsetLastLogin = $userTimezone->getOffset($lastLogin);
                                        $myIntervalLastLogin=DateInterval::createFromDateString((string)$offsetLastLogin . 'seconds');
                                        $lastLogin->add($myIntervalLastLogin);
                                        echo $lastLogin->format('Y-m-d H:i:s');
                                    }
                                    @endphp
                                </td>
                                <td>  @if ($row->verified)
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                        <a href="{{{ URL::to('user/' . $row->id . '/edit' ) }}}">
                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                        </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>


@stop