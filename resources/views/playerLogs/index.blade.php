@extends('layouts/default') {{-- Page title --}} 
@section('title') Player Logs Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
<section class="content-header">
    <h1>Player Logs</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Player Logs </a></li>
        <li class="active">Player Logs Lists</li>
    </ol>
</section>
@if ($message = Session::get('success'))
<div class="col-lg-12">
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
</div>
@endif
<section class="content paddingleft_right15">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info filterable ">
                    <div class="panel-heading clearfix">
                        <h3 class="panel-title pull-left">Player Logs</h3>
                        <div class="pull-right">
                        <a href="{{ route('playerLogs.excel',['field'=>$field,'sort'=>$sort,'search'=>$search]) }}" class="btn btn-success btn-sm">Export</a>
                        </div>
                    </div>
                    <br/>
                    <div class="col-md-12">
                            {{-- <div class="col-md-6"> --}}
                                <div class="row">
                                <form id="wildMonstersForm" action="{{ URL::to('playerLogs') }}" method="GET" enctype="multipart/form-data">
                                    <!-- CSRF Token -->
                                    {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                                    <fieldset>
                                            <div class="col-md-6">
                                            <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                    {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                    <div class="col-md-9">
                                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                    </span>
                                                        {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                            </div>
                                                        {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-ุ2 ">
                                                    <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                                    <a href="#" class="btn btn-responsive btn-danger btn-sm" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('playerLogs.destroy',1) }}">
                                                        Clear
                                                    </a>
                                            </div>
                                    </fieldset>           
            
                                </form>
                                {{-- </div> --}}
                            </div>
                        </div>
                    @php

                                        
                    if($field=="title"){
                        if($sort=="desc"){
                            $sortTitle="asc";
                        }else{
                            $sortTitle="desc";
                        }
                    }else {
                        $sortTitle="asc";
                    }   
                    
                                    
                    if($field=="detail"){
                        if($sort=="desc"){
                            $sortType="asc";
                        }else{
                            $sortType="desc";
                        }
                    }else {
                        $sortType="asc";
                    }   
                                
                    if($field=="player"){
                        if($sort=="desc"){
                            $sortPlayer="asc";
                        }else{
                            $sortPlayer="desc";
                        }
                    }else {
                        $sortPlayer="asc";
                    }
                    
                    if($field=="monster"){
                        if($sort=="desc"){
                            $sortMonster="asc";
                        }else{
                            $sortMonster="desc";
                        }
                    }else {
                        $sortMonster="asc";
                    }

                    if($field=="item"){
                        if($sort=="desc"){
                            $sortItem="asc";
                        }else{
                            $sortItem="desc";
                        }
                    }else {
                        $sortItem="asc";
                    }

                    if($field=="money"){
                        if($sort=="desc"){
                            $sortMoney="asc";
                        }else{
                            $sortMoney="desc";
                        }
                    }else {
                        $sortMoney="asc";
                    }

                    if($field=="coin"){
                        if($sort=="desc"){
                            $sortCoin="asc";
                        }else{
                            $sortCoin="desc";
                        }
                    }else {
                        $sortCoin="asc";
                    }

                    if($field=="diamond"){
                        if($sort=="desc"){
                            $sortDiamond="asc";
                        }else{
                            $sortDiamond="desc";
                        }
                    }else {
                        $sortDiamond="asc";
                    }

                    
                    if($field=="createAt"){
                        if($sort=="desc"){
                            $sortCreateAt="asc";
                        }else{
                            $sortCreateAt="desc";
                        }
                    }else {
                        $sortCreateAt="asc";
                    }
                    @endphp
    
                    <div class="panel-body table-responsive">
                        <table class="table" id="table">
                            <thead>
                                <tr class="filters">
                                    <th>Title 
                                            <a href="{{ URL::to("playerLogs?field=title&sort={$sortTitle}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>Type
                                            <a href="{{ URL::to("playerLogs?field=type&sort={$sortType}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>Player 
                                            <a href="{{ URL::to("playerLogs?field=player&sort={$sortPlayer}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>
                                       Edit Player 
                                    </th>
                                    {{-- <th>Monster 
                                            <a href="{{ URL::to("playerLogs?field=monster&sort={$sortMonster}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>
                                        Edit Monster 
                                    </th> --}}
                                    <th>Item 
                                            <a href="{{ URL::to("playerLogs?field=item&sort={$sortItem}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>
                                        Edit Item 
                                    </th>
                                    <th>Money 
                                            <a href="{{ URL::to("playerLogs?field=money&sort={$sortMoney}&search={$search}") }}" >
                                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>Coin 
                                            <a href="{{ URL::to("playerLogs?field=coin&sort={$sortCoin}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>Diamond 
                                            <a href="{{ URL::to("playerLogs?field=diamond&sort={$sortDiamond}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                    <th>Create At 
                                            <a href="{{ URL::to("playerLogs?field=createAt&sort={$sortCreateAt}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a> 
                                    </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
    
                                @foreach($playerLogs as $row)
                                <tr>
                                    <td>{{$row['title']}}</td>
                                    <td>{{$row['type']}}</td>
                                    <td>
                                        @isset($row['player']['playerName'])
                                        {{$row['player']['playerName']}}    
                                        @endisset
                                    </td>
                                    <td>
                                            @isset($row['player']['playerName'])
                                            <a href="{{{ URL::to('players/' . $row['playerId'] . '/edit' ) }}}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                            </a>
                                            @endisset
                                    </td>
                                    {{-- <td>
                                        @isset($row['monsters']['mDefaultName']['eng'])
                                        {{$row['monsters']['mDefaultName']['eng']}}
                                        @endisset
                                    </td>
                                    <td>
                                            @isset($row['monsters']['mDefaultName']['eng'])
                                            <a href="{{{ URL::to('monstersDefault/' . $row['mDefaultId'] . '/edit' ) }}}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                            </a>
                                            @endisset
                                    </td> --}}
                                    <td>
                                        @isset($row['items']['itemName']['eng'])
                                        {{$row['items']['itemName']['eng']}}
                                        @endisset
                                    </td>
                                    <td>
                                        @isset($row['items']['itemName']['eng'])
                                        <a href="{{{ URL::to('itemDefaults/' . $row['itemId'] . '/edit' ) }}}">
                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                        </a>

                                        @endisset
                                    </td>
                                    <td>
                                        @isset($row['money'])
                                        {{$row['money']}}
                                        @endisset
                                    </td>
                                    <td>@isset($row['coin'])
                                        {{$row['coin']}}
                                        @endisset
                                    </td>
                                    <td>@isset($row['diamond'])
                                        {{$row['diamond']}}
                                        @endisset
                                    </td>
                                    <td>@isset($row['createAt'])
                                        @php
                                            $createAt = new DateTime($row['createAt'], $gmtTimezone);
                                            $offsetcreateAt = $userTimezone->getOffset($createAt);
                                            $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                            $createAt->add($myIntervalcreateAt);
                                            echo $createAt->format('Y-m-d H:i:s');
                                        @endphp
                                        @endisset
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <?=$playerLogs->appends(request()->query())->render()?>
                        <!-- Modal for showing delete confirmation -->
                        <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="user_delete_confirm_title">
                                            Delete
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure to delete this?
                                    </div>
                                    <div class="modal-footer">
                                    {!! Form::open(['method' => 'DELETE']) !!}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <button type="submit" class="btn btn-danger">Delete</a>
                                    {!! Form::close() !!}   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row-->
    </section>
    
    
    
    @stop {{-- page level scripts --}}
    @section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     "pageLength": 50
        // });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });

</script>

@stop