@component('components.multiLangField',[
    'header'=>'Coupon Gift Names',
    'field'=>'couponGiftName',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Item',
        'field'=>'itemId',
        'value'=>null,
        'data'=>$itemDefaults,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Zone Type',
        'field'=>'zoneTypeId',
        'value'=>null,
        'data'=>$zoneTypes,
        'attribute'=>['id'=>'zoneTypeWildItem','class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Mall',
        'field'=>'toMall',
        'value'=>null,
        'data'=>$malls,
        'attribute'=>['class'=>'form-control toMall','placeholder'=>'Please Select','id'=>'toMall','disabled'=>(isset($zoneType)&&($zoneType!="World"))?false:true]
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Mall Floor',
        'field'=>'toMallFloor',
        'value'=>null,
        'data'=>$mallFloors,
        'attribute'=>['class'=>'form-control toMallFloor','placeholder'=>'Please Select','id'=>'toMallFloorWildItem','disabled'=>(isset($zoneType)&&($zoneType=="InMall Floor"||$zoneType=="InMall Shop"))?false:true]
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Shop',
        'field'=>'toShop',
        'value'=>null,
        'data'=>$shops,
        'attribute'=>['class'=>'form-control toShop','placeholder'=>'Please Select','disabled'=>(isset($zoneType)&&($zoneType=="InMall Shop"))?false:true]
        ])
        @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Coupon Gift',
            'field'=>'couponGiftId',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'couponGiftId']
        ])
        @endcomponent
</div>
   
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'number',
                'icon'=>'text',
                'label'=>'Count',
                'field'=>'count',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Count']
            ])
            @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Count Max',
            'field'=>'countMax',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Count']
        ])
        @endcomponent
</div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Reach Date Time',
            'field'=>'isReachDateTime',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'datetime',
                'label'=>'Start Date',
                'field'=>'startDateTime',
                'attribute'=>['class'=>'form-control customDate','id'=>'startDateTime']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'datetime',
                'label'=>'Expired Date',
                'field'=>'expiredDateTime',
                'attribute'=>['class'=>'form-control customDate','id'=>'endDateTime']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
   
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Golden Minutes',
                'field'=>'isGoldenMinutes',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>

</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'isToItemPlayer',
                'field'=>'isToItemPlayer',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

