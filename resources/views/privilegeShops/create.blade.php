@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop
@section('content')
<section class="content-header">
    <h1>Privilege Shops</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('privilegeShops') }}"> Privilege Shops</a></li>
        <li class="active">Add new</li>
    </ol>
</section>

@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h4 class="panel-title">Add New</h4>
            </div>
            <div class="panel-body">
                <form id="privilegeShopsForm" action="{{ route('privilegeShops.store') }}" method="POST" enctype="multipart/form-data"
                    class="form-horizontal">
                    <input type="hidden" name="linkListName" value="{{uniqid()}}" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <fieldset>
                    @include('privilegeShops.field')
                    </fieldset>
                </form>

            </div>
        </div>
    </div>
</section>

@stop 
