@component('components.multiLangField',[
    'header'=>'Privilege Shop Titles',
    'field'=>'privilegeShopTitles',
    'icon'=>'livicon'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Privilege Shop Details',
    'field'=>'privilegeShopDetails',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>
<div class="row">
    @if(empty($privilegeShop))
        <table class="table table-bordered tbshops">
                <thead>
                    <tr>
                        <th>malls </th>
                        <th>mall floors </th>
                        <th>Shops </th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>
                            @component('components.inputField', [
                                    'type'=>'select',                          
                                    'label'=>'mall',
                                    'field'=>'mallId[]',
                                    'value'=>null,
                                    'data'=>$malls,
                                    'attribute'=>['class'=>'form-control', 'id' => uniqid(),'placeholder'=>'Please Select','required'=>true]
                                ])
                            @endcomponent
                            </td>
                            <td>
                                    @component('components.inputField', [
                                            'type'=>'select',                          
                                            'label'=>'floor',
                                            'field'=>'mallFloorId[]',
                                            'value'=>null,
                                            'data'=>$mallFloors,
                                            'attribute'=>['class'=>'form-control mallFloor', 'id' => uniqid(),'placeholder'=>'Please Select','required'=>true]
                                        ])
                                    @endcomponent
                            </td>
                            <td>
                                    @component('components.inputField',[
                                        'type'=>'select',
                                        'label'=>'shops',
                                        'field'=>'shopIdArray[]',
                                        'value'=>null,
                                        'data'=>$shops,
                                        'attribute'=>['class'=>'form-control shop','placeholder'=>'Please Select','required'=>true]
                                    ])
                                    @endcomponent
                            </td>
                            <td style="vertical-align: middle">
                                <button type="button" class="btn btn-danger btn-sm remove" style="line-height: 0px">
                                    <i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Delete"></i>
                                </button>
                            </td>
                        </tr>
                   
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <button type="button" class="btn btn-info btn-sm add" style="line-height: 0px">
                                <i class="livicon" data-name="plus" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Add"></i>
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

            <table class="table table-bordered tbshops">
                    <thead>
                        <tr>
                            <th>List Privilege Default </th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td>
                                        @component('components.inputField',[
                                            'type'=>'select',
                                            'label'=>'Privilege Default',
                                            'field'=>'privilegeDefaultIdArray[]',
                                            'value'=>null,
                                            'data'=>$privilegeDefauls,
                                            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select','required'=>true]
                                        ])
                                        @endcomponent
                                </td>
                                <td style="vertical-align: middle">
                                    <button type="button" class="btn btn-danger btn-sm remove" style="line-height: 0px">
                                        <i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Delete"></i>
                                    </button>
                                </td>
                            </tr>
                       
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <button type="button" class="btn btn-info btn-sm add" style="line-height: 0px">
                                    <i class="livicon" data-name="plus" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Add"></i>
                                </button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
        @else
        <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Shop',
            'field'=>'shopId',
            'value'=>$shopId,
            'data'=>$shops,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
        </div>
        <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'select',
                'label'=>'Privilege Default',
                'field'=>'privilegeDefaultId',
                'value'=>null,
                'data'=>$privilegeDefauls,
                'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            ])
            @endcomponent
    </div>
    @endif
    
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'datetime',
                'label'=>'Start Date',
                'field'=>'startDateTime',
                //'attribute'=>['class'=>'form-control customDate','disabled'=>empty($privilegeShop)?true:!$privilegeShop->isReachDateTime]
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'datetime',
                'label'=>'Expired Date',
                'field'=>'expiredDateTime',
                //'attribute'=>['class'=>'form-control customDate','disabled'=>empty($privilegeShop)?true:!$privilegeShop->isReachDateTime]
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Reach Date Time',
            'field'=>'isReachDateTime',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>


    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'number',
                'icon'=>'text',
                'label'=>'Order',
                'field'=>'order',
                'value'=>null, 
                'attribute'=>['class'=>'form-control','placeholder'=>'Order']
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Golden Minutes',
            'field'=>'isGoldenMinutes',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>

</div>


<div class="row">
    <div class="col-md-2 col-xs-offset-2">
        @if(!empty($images1))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
                <img src="{{URL::to('media/images/'.$images1)}}" class="img-responsive" alt="$field"  style="width: 200px; height: 150px;">
            </div>
        @endif
    </div>
    <div class="col-md-2 col-xs-offset-4">
        @if(!empty($images2))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
                <img src="{{URL::to('media/images/'.$images2)}}" class="img-responsive" alt="$field" style="width: 200px; height: 150px;">
            </div>
        @endif
    </div> 
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 1',
            'field'=>'filenameLogo1',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Logo 2',
                'field'=>'filenameLogo2',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>

 
        <div class="col-md-10 col-xs-offset-2">
        
            @if(!empty($images3))
                <div class=" fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
                    <img src="{{URL::to('media/images/'.$images3)}}" class="img-responsive" alt="$field" style="width: 200px; height: 150px;">
                </div>
            @endif
        </div>
 
        <div class="col-md-6">
         @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 3',
            'field'=>'filenameLogo3',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
        </div>

</div>
<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>
@push('more_scripts')
<script>
//     var c = $("#checkboxid");
// if($('input[type="checkbox"]').prop(":checked")){
//             //if($(this).is(":checked")){
//                 alert("Checkbox is checked.");
//             //}
//            // else if($(this).is(":not(:checked)")){
//            //     alert("Checkbox is unchecked.");
//           //  }
//         }//);

        $(".add").on("click", function() {
            $(this).closest("table").find("tbody tr:last-child");
            const tbody = $(this).closest("table").find("tbody");
            tbody.find("tr:last-child").clone().appendTo(tbody);
    
            const lastRow = tbody.find("tr:last-child");
             //const classList = lastRow.find(".custom-select2").attr("class").split(" ");
            // const className = classList[classList.length - 2];
            // // remove input value
          
            // remove select2
            lastRow.find("td:first select").removeClass("select2-hidden-accessible");
            lastRow.find("td:first>div>div>span").remove();
    
          //  window.select2[className]();
            // remove select2 value
           // $(`.${className}:last`).val(null).trigger('change');
        });


        $(document).on("click", ".remove", function() {
        const rowLenght = $(this).closest("tbody").find("tr").length;
        let id = $(this).closest("tr").find("td").find("select").val()
        if (rowLenght > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("This table must has at least 1 row!");
        }

   
    });  

    $(document).on("change", "table select[name='mallId[]']", function(i,el) {
      
    let option=$(this).closest("tr").find("td").find(".mallFloor").html('');        
    $.ajax({
            url: "{{ URL::to('mallFloors/show') }}",
            method: 'GET',
            data: "mallId="+$(this).val(),
            success: function(data) {
                option.html(data.options);
            }
        });
    });

    $(document).on("change", "table select[name='mallFloorId[]']", function(i,el) {
        let option=$(this).closest("tr").find("td").find(".shop").html('');        
        $.ajax({
            url: "{{ URL::to('shops/show') }}",
            method: 'GET',
            data: "mallFloorId="+$(this).val(),
            success: function(data) {                
                option.html(data.options);
            }
        });
    })
</script>
@endpush