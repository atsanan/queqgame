@extends('layouts/default') {{-- Page title --}} 
@section('title') Privilege Shop Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
<link href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Privilege Shops</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Privilege Shops</a></li>
        <li class="active">Privilege Shop Lists</li>
    </ol>
</section>

@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="col-lg-12">
        <div class="row">   
            <div class="panel panel-success filterable">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$message}}</h3>
                    </div>
            </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
               
            <div class="panel panel-info filterable ">
                
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Privilege Shops</h3>
                    <div class="pull-right">
                    <a href="{{ route('privilegeShops.excel',[
                    'field'=>$field,
                    'sort'=>$sort,
                    'search'=>$search,
                    'mallId'=>$mallId,
                    'mallFloorId'=>$mallFloorId,
                    'shopId'=>$shopId,
                    ]) }}" class="btn btn-success btn-sm" >Export</a>
                    <a href="{{ URL::to('privilegeShops/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>

                <br/>
                <div class="col-md-12">
                {{-- <div class="col-md-6"> --}}
                    <div class="row">
                    <form id="wildMonstersForm" action="{{ route('privilegeShops.search') }}" method="POST" enctype="multipart/form-data">
                        <!-- CSRF Token -->
                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                        <fieldset>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="col-md-6">
                                        @component('components.select2', [
                                            'type'=>'select2',                          
                                            'label'=>'Mall',
                                            'field'=>'mallId[]',
                                            'value'=>$_mallId,
                                            'data'=>$malls,
                                            'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                        ])
                                        @endcomponent
                                </div>
                                <div class="col-md-6">
                                        @component('components.select2', [
                                            'type'=>'select2',                          
                                            'label'=>'MallFloor',
                                            'field'=>'mallFloorId[]',
                                            'value'=>$_mallFloorId,
                                            'data'=>$mallFloors,
                                            'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                        ])
                                        @endcomponent    
                                </div>

                                <div class="col-md-6">
                                        @component('components.select2', [
                                            'type'=>'select2',                          
                                            'label'=>'shop',
                                            'field'=>'shopId[]',
                                            'value'=>$_shopId,
                                            'data'=>$shops,
                                            'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                        ])
                                        @endcomponent    
                                </div>
                                <div class="col-md-6">
                                <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                        {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                        <div class="col-md-9">
                                                <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                        </span>
                                            {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                </div>
                                            {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>         
                                </div>
                        </fieldset>           
                    </form>
                    {{-- </div> --}}
                </div>
                </div>

                @php
                                      
                if($field=="shops"){
                    if($sort=="desc"){
                        $sortShop="asc";
                    }else{
                        $sortShop="desc";
                    }
                }else {
                $sortShop="asc";
                }   
             
                if($field=="privilegeDefault"){
                    if($sort=="desc"){
                        $sortPrivilegeDefault="asc";
                    }else{
                        $sortPrivilegeDefault="desc";
                    }
                }else {
                    $sortPrivilegeDefault="asc";
                }   
             
                
                if($field=="privilegeShopTitles"){
                    if($sort=="desc"){
                        $sortPrivilegeShopTitles="asc";
                    }else{
                        $sortPrivilegeShopTitles="desc";
                    }
                }else {
                    $sortPrivilegeShopTitles="asc";
                }   
                
                if($field=="isReachDateTime"){
                    if($sort=="desc"){
                        $sortIsReachDateTime="asc";
                    }else{
                        $sortIsReachDateTime="desc";
                    }
                }else {
                    $sortIsReachDateTime="asc";
                }   

                
                if($field=="isGoldenMinutes"){
                    if($sort=="desc"){
                        $sortIsGoldenMinutes="asc";
                    }else{
                        $sortIsGoldenMinutes="desc";
                    }
                }else {
                    $sortIsGoldenMinutes="asc";
                }   

                if($field=="isExpired"){
                    if($sort=="desc"){
                        $sortIsExpired="asc";
                    }else{
                        $sortIsExpired="desc";
                    }
                }else {
                    $sortIsExpired="asc";
                } 

                if($field=="isActive"){
                    if($sort=="desc"){
                        $sortIsActive="asc";
                    }else{
                        $sortIsActive="desc";
                    }
                }else {
                    $sortIsActive="asc";
                }   
                
                if($field=="linkListName"){
                    if($sort=="desc"){
                        $sortLinkListName="asc";
                    }else{
                        $sortLinkListName="desc";
                    }
                }else {
                    $sortLinkListName="asc";
                }   
            @endphp
                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Link List name 
                                    <a href="{{ URL::to("privilegeShops?field=linkListName&sort={$sortLinkListName}&search={$search}") }}" >
                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a> 
                                    
                                </th>
                                <th>Privilege Default 
                                    <a href="{{ URL::to("privilegeShops?field=privilegeDefault&sort={$sortPrivilegeDefault}&search={$search}") }}" >
                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a> 
                                    
                                </th>
                                <th>Shops 
                                    <a href="{{ URL::to("privilegeShops?field=shops&sort={$sortShop}&search={$search}") }}" >
                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a> 
                                </th>
                                <th>Privilege Shop Title 
                                    <a href="{{ URL::to("privilegeShops?field=privilegeShopTitles&sort={$sortPrivilegeShopTitles}&search={$search}") }}" >
                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>Logo</th>
                                <th>Reach Date Time
                                        <a href="{{ URL::to("privilegeShops?field=isReachDateTime&sort={$sortIsReachDateTime}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Continued
                                    <a href="{{ URL::to("privilegeShops?field=isExpired&sort={$sortIsExpired}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>  
                                </th>
                                <th>Golden Minutes
                                        <a href="{{ URL::to("privilegeShops?field=isGoldenMinutes&sort={$sortIsGoldenMinutes}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Active
                                        <a href="{{ URL::to("privilegeShops?field=isActive&sort={$sortIsActive}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($privilegeShops as $row)
                            <tr>
                                <td>
                                    @if(isset($row['linkListName']))
                                    {{$row['linkListName']}}
                                    @endif
                                </td>    
                                <td>{{$row['privilege_defaults']['privilegeDefaultNames']['eng']}}</td>
                                    <td>
                                        @if(isset($row['shops']['shopName']['eng']))
                                        {{ $row['shops']['shopName']['eng'] }}
                                        @endif
                                    </td>
                                    <td>{{ $row['privilegeShopTitles']['eng']}}</td>
                                
                                <td>
                                    @if(isset($row['filenameLogo1'])||isset($row['filenameLogo2']))
                                    @component('components.modal',['id'=>uniqid(),'title'=>'Image Lists'])
                                    @slot('content')
                                    @if(isset($row['filenameLogo1']))
                                    <img src="{{URL::to('media/images/'.$row['filenameLogo1'])}}" style="width: 100px; height: 70px" />
                                    @endif
                                    @if(isset($row['filenameLogo2']))
                                    <img src="{{URL::to('media/images/'.$row['filenameLogo2'])}}" style="width: 100px; height: 70px" />
                                    @endif
                                    @endslot
                                    @endcomponent
                                    @endif
                                </td>
                                <td> @if ($row['isReachDateTime'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                    @if(isset($row['isExpired']))
                                        @if ($row['isExpired'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>@if ($row['isGoldenMinutes'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>@if ($row['isActive'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{{ URL::to('privilegeShops/' . $row['_id'] . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('privilegeShops.destroy', collect($row)->first()) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                    </a>
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$privilegeShops->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     "pageLength": 50
        // });
    });

    $("select[name='mallId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getMallFloor/') }}",
          method: 'POST',
          data:{mallId: $(this).val()},
         success: function(data) {
            $("select[name='mallFloorId[]'").html('');
            $("select[name='mallFloorId[]'").html(data.options);
            
          }
      });     
    });
    
    $("select[name='mallFloorId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getShop/') }}",
          method: 'POST',
          data:{mallFloorId: $(this).val()},
         success: function(data) {
            $("select[name='shopId[]'").html('');
            $("select[name='shopId[]'").html(data.options);
            
          }
      });
            
    });
    
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })
</script>







@stop