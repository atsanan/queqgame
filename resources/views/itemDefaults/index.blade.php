@extends('layouts/default') {{-- Page title --}} 
@section('title') Item Default Lists @parent 
@stop 

@section('content')
<section class="content-header">
    <h1>Item Defaults</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Item Defaults</a></li>
        <li class="active">Item Default Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Item Defaults</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('itemDefaults/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    {{-- <div class="col-md-6"> --}}
                        <div class="row">
                        <form id="wildMonstersForm" action="{{ route('itemDefaults.index') }}" method="GET" enctype="multipart/form-data">
                            <!-- CSRF Token -->
                            {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                                    <fieldset>
                                    <div class="col-md-6">
                                    <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                            {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                            <div class="col-md-9">
                                                    <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                            </span>
                                                {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                    </div>
                                                {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ route('itemDefaults.index',["type"=>"Coupon","search"=>$search]) }}" class="btn btn-success btn-sm" >Coupon</a>         
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>         
                                    </div>
                            </fieldset>           
                        </form>
                    </div>
            </div>
            @php
            $sortItemOrder="asc";                        
            if($field=="itemOrder"){
                if($sort=="asc"){
                    $sortItemOrder="desc";
                }
            }
            @endphp
                <div class="panel-body table-responsive">
                    <table class="table" id="">
                        <thead>
                            <tr class="filters">
                                <th>Item Image</th>
                                <th>Item Name</th>
                                {{-- <th>Coin</th>
                                <th>Diamond</th> --}}
                                <th>Item Order
                                    <a href="{{ URL::to("itemDefaults?field=itemOrder&sort={$sortItemOrder}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                    </th>
                                <th>Item Assert Version</th>
                                <th>Item Category</th>
                                {{-- <th>Image Slot</th> --}}
                                <th>Active</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($itemDefaults as $row)
                            <tr>
                                <td>
                                        @isset($row->itemAssertImageSlot1)
                                        <img src="{{URL::to('media/thumbnail/'.$row->itemAssertImageSlot1)}}" style="width: 100px; height: 70px" />      
                                        @endisset
                                </td>
                                <td>{{ $row->itemName['eng'] }}</td>
                                {{-- <td>{{ $row->coin }}</td>
                                <td>{{ $row->diamond }}</td> --}}
                                <td>{{ $row->itemOrder }}</td>
                                
                                <td>{{ $row->itemAssertVersion }}</td>
                                <td>{{ $row->itemCategory->itemCategoryName }}</td>
                                {{-- <td>
                                    @if(isset($row->itemAssertImageSlots))
                                    @component('components.modal',['id'=>uniqid(),'title'=>'Image Slots'])
                                        @slot('content')  
                                        @foreach($row->itemAssertImageSlots as $photo)
                                            <img src="{{URL::to('media/images/'.$photo)}}" style="width: 100px; height: 70px" />
                                        @endforeach
                                        @endslot
                                    @endcomponent
                                    @endif
                                </td> --}}
                                <td>
                                    @if ($row->isActive)
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{{ URL::to('itemDefaults/' . $row->id . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('itemDefaults.destroy', collect($row)->first()) }}">
                                    <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$itemDefaults->appends(request()->query())->render()?>
                    
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#table').dataTable({
            "pageLength": 50
        });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });

</script>
@stop