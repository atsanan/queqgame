@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
@section('content')
<section class="content-header">
    <h1>Item Defaults</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('itemDefaults') }}"> Item Defaults</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                {!! Form::model($itemDefault, ['url' => URL::to('itemDefaults') . '/' . $itemDefault->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="isStore" value="0" />
                    <input type="hidden" name="isHighlight" value="0" />
                    <input type="hidden" name="isReachDateTime" value="0" />
                    <input type="hidden" name="isRealPosition" value="0" />
                    <fieldset>
                    @include('itemDefaults.field')
                    </fieldset>

                {!! Form::close() !!}
                <div class="col-lg-12">
                    <div class="panel panel-info filterable ">
                        <div class="panel-heading clearfix">
                            <h3 class="panel-title pull-left">Coupon Item</h3>
                            <div class="pull-right">
                                <a href="{{ route('couponItem.create',['itemId'=>$itemDefault->id]) }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                                <a href="{{ route('couponItem.createMultipleCode',['itemId'=>$itemDefault->id]) }}" class="btn btn-danger btn-sm" >Add Mutiple</a>
                                <a href="{{ route('couponItem.importCsv',['itemId'=>$itemDefault->id]) }}" class="btn btn-success btn-sm" id="addButton">Import Csv</a>

                            </div>
                        </div>
        
                        <div class="panel-body table-responsive">
                            <table class="table">
                                    <thead>
                                            <tr class="filters">
                                                <th>Count</th> 
                                                <th>Code</th>  
                                                <th>Player</th> 
                                                <th>uniqueId</th> 
                                                <th>Create Coupon At</th>  
                                                <th>Get Coupon At</th>  
                                                <th>Redeem Coupon At</th>    
                                                <th>Actions</th>
                                                <th>Item Player</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                             $i=1;   
                                            @endphp
                                            @foreach ($couponItem as $row)
                                            <tr>
                                                <td>{{$offset + ($i++)}}</td>
                                                <td>{{$row->code}}</td>
                                                <td>{{$row->itemPlayer->player->playerName}}</td>
                                                <td>{{$row->itemPlayer->uniqueId}}</td>
                                                <td> @isset($row->itemPlayer->createAt)
                                                    @php
                                                    $createAt = new DateTime($row->itemPlayer->createAt, $gmtTimezone);
                                                    $offsetcreateAt = $userTimezone->getOffset($createAt);
                                                    $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                                    $createAt->add($myIntervalcreateAt);
                                                    echo $createAt->format('Y-m-d H:i:s');
                                                    @endphp
                                                    @endisset
                                                </td>
                                                
                                                <td>
                                                        @isset($row->itemPlayer->couponDatetime1)
                                                        @php
                                                        $couponDatetime1 = new DateTime($row->itemPlayer->couponDatetime1, $gmtTimezone);
                                                        $offsetCouponDatetime1 = $userTimezone->getOffset($couponDatetime1);
                                                        $myIntervalCouponDatetime1=DateInterval::createFromDateString((string)$offsetCouponDatetime1 . 'seconds');
                                                        $couponDatetime1->add($myIntervalCouponDatetime1);
                                                        echo $couponDatetime1->format('Y-m-d H:i:s');
                                                        @endphp
                                                        @endisset
       
                                                    </td>
                                                <td>
                                                    @isset($row->itemPlayer->couponDatetime2)
                                                    @php
                                                    $couponDatetime2 = new DateTime($row->itemPlayer->couponDatetime2, $gmtTimezone);
                                                    $offsetCouponDatetime2 = $userTimezone->getOffset($couponDatetime2);
                                                    $myIntervalCouponDatetime2=DateInterval::createFromDateString((string)$offsetCouponDatetime2 . 'seconds');
                                                    $couponDatetime2->add($myIntervalCouponDatetime2);
                                                    echo $couponDatetime2->format('Y-m-d H:i:s');
                                                    @endphp
                                                    @endisset
   
                                                </td>
                                                <td>
                                                        <a href="{{{ URL::to('couponItem/' . $row->id . '/edit' ) }}}">
                                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                        </a>
                                                    
                                                        <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('couponItem.destroy', collect($row)->first()) }}">
                                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    @isset($row->itemPlayer->id)
                                                    <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('couponItem.destroyItemPlayer', $row->itemPlayer->id) }}">
                                                    <i class="livicon" data-name="user-ban" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                    </a>
                                                    @endisset
                                            </td>
                                            </tr>    
                                            @endforeach
                                        </tbody>                
                            </table>
                            <?=$couponItem->appends(request()->query())->render()?>
                              <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="user_delete_confirm_title">
                                            Delete
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure to delete this?
                                    </div>
                                    <div class="modal-footer">
                                    {!! Form::open(['method' => 'DELETE']) !!}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <button type="submit" class="btn btn-danger">Delete</a>
                                    {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
    <!--row end-->
</section>


@stop 