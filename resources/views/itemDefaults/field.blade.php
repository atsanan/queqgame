@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
$expireDate="";
if(isset($itemDefault->expireDate)){
$myDateTime = new DateTime($itemDefault->expireDate, $gmtTimezone);
$offset = $userTimezone->getOffset($myDateTime);
$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
$myDateTime->add($myInterval);
$expireDate = $myDateTime->format('Y-m-d');
}
    $dataLength = 1;
    if (isset($itemDefault)) {
        $dataLength = count($itemShop)>0?count($itemShop):1;
    }
@endphp 
<h4>Name Report</h4>
<div class="col-md-12">
    <div class="form-group row {{ $errors->first('nameReport', 'has-error') }}">

            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                    </span>
                {!! Form::text('nameReport', null,['class'=>'form-control','placeholder'=>'Name Report']) !!}
            </div>
            {!! $errors->first('nameReport', '<span class="help-block">:message</span> ') !!}
        
    </div> 
    
    </div> 
@component('components.multiLangField',[
        'header'=>'Item Names',
        'field'=>'itemName',
        'icon'=>'livicon'
        ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Item Details',
    'field'=>'itemDetail',
    'icon'=>'livicon'
    ])
@endcomponent


<h4>Item Store</h4>

<div class="col-md-12">
    <div class="col-md-6">
    
        @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Item StoreBy',
                'field'=>'itemStoreBy',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Item StoreBy']
            ])
             @endcomponent
                    
        </div>
    </div>

@component('components.multiLangField',[
    'header'=>'Item Store Name',
    'field'=>'itemStoreName',
    'icon'=>'livicon'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Item Store Detail',
    'field'=>'itemStoreDetail',
    'icon'=>'livicon'
    ])
@endcomponent


@component('components.multiLangTextAreaField',[
    'header'=>'Item Condition',
    'field'=>'itemCondition',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Assert Version',
        'field'=>'itemAssertVersion',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Assert Version']
    ])
     @endcomponent
            
</div>
        
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'label'=>'Item Category',
        'field'=>'itemCategoryId',
        'value'=>$itemCategory,
        'data'=>$itemCategorys,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
    ])
    @endcomponent
</div>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Coin',
        'field'=>'coin',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Coin']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Diamond',
        'field'=>'diamond',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Diamond']
    ])
    @endcomponent
</div>
    

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Item Order',
        'field'=>'itemOrder',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Item Order']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'icon'=>'text',
        'label'=>'Reach Date time',
        'field'=>'isReachDateTime',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'datetime',
        'label'=>'Expire date',
        'field'=>'expireDate',
        'value'=>null,
        'attribute'=>['class'=>'form-control dateTime','placeholder'=>'Expire date']
    ])
    @endcomponent
   
</div>

   

<div class="col-md-6">
    @component('components.inputField',[
    'type'=>'select',
    'label'=>'Main Tabs',
    'field'=>'mainTab[]',
    'value'=>null,
    'data'=>$mainTab,
    'attribute'=>['class'=>'form-control select2','multiple'=>true]
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
    'type'=>'select',
    'label'=>'Sub Tabs',
    'field'=>'subTab[]',
    'value'=>null,
    'data'=>$subTab,
    'attribute'=>['class'=>'form-control select2','multiple'=>true]
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'label'=>'Partner Types',
        'field'=>'partnerTypeId[]',
        'value'=>null,
        'data'=>$partnerTypes,
        'attribute'=>['class'=>'form-control select2','multiple'=>true]
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Item Order',
        'field'=>'itemOrder',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Item Order']
    ])
    @endcomponent
</div>


<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Short Text',
        'field'=>'shortText',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Short Text']
    ])
    @endcomponent
</div>
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Diamond Include',
        'field'=>'diamondInclude',
        'value'=>isset($itemDefault->diamondInclude)?$itemDefault->diamondInclude:0,
        'attribute'=>['class'=>'form-control','placeholder'=>'Diamond Include']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'label'=>'Is Highlight',
        'field'=>'isHighlight',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'label'=>'Is Real Position',
        'field'=>'isRealPosition',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'label'=>'Active',
        'field'=>'isActive',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>
{{-- 
<div class="col-md-12">
    @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Item Assert Models (Multiple)',
        'field'=>'AssetModel[]',
        'value'=>null,
        'attribute'=>['class'=>'form-control','multiple'=>true],
    ])
    @endcomponent
</div>   --}}

<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Code_json',
            'field'=>'itemDetailEffect',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>5,'placeholder'=>'Code_json']
        ])
        @endcomponent
</div>
<div class="col-md-12 col-md-offset-2">
    
    <div class="form-group row">
    <button type="button" class="btn btn-responsive btn-default btn-sm coupon">Coupon</button>
    <button type="button" class="btn btn-responsive btn-default btn-sm coin">Bonus Coin</button>
    <button type="button" class="btn btn-responsive btn-default btn-sm diamon">Bonus Diamond</button>
    <button type="button" class="btn btn-responsive btn-default btn-sm inApp">InApp</button>
    </div>
</div>
<div class="col-md-12 col-md-offset-2">
    
    <div class="form-group row">
    
    <button type="button" class="btn btn-responsive btn-default btn-sm qrCodeCoupon">QR generation V.1</button>
    <button type="button" class="btn btn-responsive btn-default btn-sm qrCode">QR generation V.2</button>
    </div>
    @if(isset($itemDefault->itemDetailEffect))
    <div class="fileinput-preview  thumbnail qr-preview" data-trigger="fileinput" 
    style="width: 250px; height: 250px;">
        {!! QrCode::size(250)->generate($itemDefault->itemDetailEffect); !!}
    </div>
    @else
    <div class="fileinput-preview  thumbnail qr-preview" data-trigger="fileinput" 
    style="width: 250px; height: 250px;display:none;">
    </div>
    @endif
    
</div>
<div class="col-md-offset-2 col-md-10">
        <table class="table table-bordered tbshops">
                <thead>
                    <tr>
                        <th>malls </th>
                        <th>mall floors </th>
                        <th>Shops </th>
                        <th>Set </th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i=0;$i<$dataLength;$i++)
                        <tr>
                            <td>
                            @component('components.inputField', [
                                    'type'=>'select',                          
                                    'label'=>'mall',
                                    'field'=>'mallId[]',
                                    'value'=>isset($itemShop[$i]->mallId)?$itemShop[$i]->mallId:null,
                                    'data'=>$malls,
                                    'attribute'=>['class'=>'form-control', 'id' => uniqid(),'placeholder'=>'Please Select']
                                ])
                            @endcomponent
                            </td>
                            <td>
                                    @component('components.inputField', [
                                            'type'=>'select',                          
                                            'label'=>'floor',
                                            'field'=>'mallFloorId[]',
                                            'value'=>isset($itemShop[$i]->mallFloorId)?$itemShop[$i]->mallFloorId:null,
                                            'data'=>$mallFloors,
                                            'attribute'=>['class'=>'form-control mallFloor', 'id' => uniqid(),'placeholder'=>'Please Select']
                                        ])
                                    @endcomponent
                            </td>
                            <td>
                                    @component('components.inputField',[
                                        'type'=>'select',
                                        'label'=>'shops',
                                        'field'=>'shopIdArray[]',
                                        'value'=>isset($itemShop[$i]->shopId)?$itemShop[$i]->shopId:null,
                                        'data'=>$shops,
                                        'attribute'=>['class'=>'form-control shop','placeholder'=>'Please Select']
                                    ])
                                    @endcomponent
                            </td>
                            <td style="vertical-align: middle">
                                @if(isset($itemDefault->shopSelect)&&$itemShop[$i]->shopId==$itemDefault->shopSelect)
                                
                                <button type="button" class="btn btn-success btn-sm setLocation" style="line-height: 10px">
                                    set
                                </button>      
                                @else
                                
                                <button type="button" class="btn btn-primary btn-sm setLocation" style="line-height: 10px">
                                    set
                                </button>      
                                @endif
                            </td>
                            <td style="vertical-align: middle">
                                <button type="button" class="btn btn-danger btn-sm remove" style="line-height: 0px">
                                    <i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Delete"></i>
                                </button>
                            </td>
                        </tr>
                    @endfor
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <button type="button" class="btn btn-info btn-sm add" style="line-height: 0px">
                                <i class="livicon" data-name="plus" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Add"></i>
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>

{{--     

<table class="table" >
        <thead>
            <tr class="filters">
                <th>Shop Name</th>
                <th>Action</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($shops))
                @foreach($shops as $row)
                <tr>
                    <td>{{$row->shopName['eng']}}    
                    </td>
                    <td>
                            <a href="{{{ URL::to('shops/' . $row->id . '/edit' ) }}}">
                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                            </a>

                    <td><input name='shopId[]' type='hidden' value="{{$row->id}}"></td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table> --}}
</div>

<div class="col-md-12">
    
    
    @if(!empty($imageSlot))

    <div class="form-group row">
    <label for="inputUsername" class="col-md-3 control-label">Item Assert Image Slots </label>
        @component('components.modal',['id'=>uniqid(),'title'=>'Image Slots'])
            @slot('content')
            @foreach($imageSlot as $photo)
                <img src="{{URL::to('media/images/'.$photo)}}" style="width: 100px; height: 70px" />
            @endforeach
            @endslot
        @endcomponent
    </div>
    @endif
            
    @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Item Assert Image Slots (Multiple)',
        'field'=>'imageSlots[]',	
        'value'=>null,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    @endcomponent

</div>
<div class="col-md-12">
    @component('components.inputField',[
    'type'=>'file',
    'icon'=>'user',
    'label'=>'Assert Model IOS (Multiple)',
    'field'=>'AssertModelIOS[]',
    'value'=>$assertModelIOS,
    'attribute'=>['class'=>'form-control','multiple'=>true]
])

@endcomponent
    
</div>

<div class="col-md-12">
    @component('components.inputField',[
    'type'=>'file',
    'icon'=>'user',
    'label'=>'Assert Model Android (Multiple)',
    'field'=>'AssertModelAndroid[]',
    'value'=>$assertModelAndroid,
    'attribute'=>['class'=>'form-control','multiple'=>true]
])

@endcomponent
    
</div>


<div class="col-md-2 col-xs-offset-4">
    @if(!empty($assertImageSlot1))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
            <img src="{{URL::to("media/images/{$assertImageSlot1}")}}" class="img-responsive" alt="$field" style="width: 120px; height: 120px;">
        </div>
    @endif
</div> 

<div class="col-md-12">
    @component('components.inputField',[
    'type'=>'file',
    'icon'=>'user',
    'label'=>'Item Assert Image Slot 1',
    'field'=>'AssertImageSlot1',
    'value'=>null,
    'attribute'=>['class'=>'form-control','multiple'=>false]
])
@endcomponent
</div>
<div class="col-md-2 col-xs-offset-4">
    @if(!empty($assertImageSlot2))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
            <img src="{{URL::to("media/images/{$assertImageSlot2}")}}" class="img-responsive" alt="$field" style="width: 120px; height: 120px;">
        </div>
    @endif
</div> 

<div class="col-md-12">
    @component('components.inputField',[
    'type'=>'file',
    'icon'=>'user',
    'label'=>'Item Assert Image Slot 2',
    'field'=>'AssertImageSlot2',
    'value'=>null,
    'attribute'=>['class'=>'form-control','multiple'=>false]
])
@endcomponent
</div>

{{-- 
<div class="col-md-12">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Item Detail Store',
        'field'=>'itemDetailStore',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Item Detail Store']
    ])
    @endcomponent
</div> --}}


<div class="col-md-12">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Item Url',
        'field'=>'itemUrl',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Item Detail Effect']
    ])
    @endcomponent
</div>

<div class="col-md-12">
    @component('components.inputField',[
        'type'=>'checkbox',
        'label'=>'IsStore',
        'field'=>'isStore',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>
{!! Form::hidden("location[coordinates][0]", isset($itemDefault->location['coordinates'][0])?null:0, ["class"=>"long"]) !!}
{!! Form::hidden("location[coordinates][1]", isset($itemDefault->location['coordinates'][1])?null:0, ["class"=>"lat"]) !!}
{!! Form::hidden("shopSelect", null,["class"=>"shopSelect"] ) !!}
    <div class="form-group">
        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
    </div>

@push('more_scripts')
<script>
    $(".coupon").click(function() {
        $("#itemDetailEffect").val('{"TYPE":"Coupon","SHOP_CODE":"Demo"}');
    })
    $(".coin").click(function() {
        $("#itemDetailEffect").val('{"ADD_COIN": 5}');
    })
    $(".diamon").click(function() {
        $("#itemDetailEffect").val('{"ADD_DIAMOND": 5}');
    })
    $(".inApp").click(function() {
        $("#itemDetailEffect").val('{"ADD_DIAMOND": 1800,"INAPP_ID_IOS":"inmallgame.diamond1800.ios","INAPP_ID_ANDROID": "inmallgame.diamond1800","INAPP_COSTLABEL":""}');
    })
    $(".qrCodeCoupon").click(function() {
       let obj = JSON.parse($("#itemDetailEffect").val());
       if(obj.SHOP_CODE!=undefined){
        $.ajax({
          url: "{{ URL::to('genCouponPassword') }}"+"/"+obj.SHOP_CODE,
          method: 'GET',
          success: function(data) {
            
            $(".qr-preview").show()
            $(".qr-preview").html(data)
          }
      });
       }
    })

    $(".qrCode").click(function() {
       let obj = JSON.parse($("#itemDetailEffect").val());
       if(obj.SHOP_CODE!=undefined){
        $.ajax({
          url: "{{ URL::to('genQrCode') }}"+"/"+obj.SHOP_CODE,
          method: 'GET',
          success: function(data) {
            
            $(".qr-preview").show()
            $(".qr-preview").html(data)
          }
      });
       }
    })

    $(".addShop").click(function() {
        $(".table > tbody").empty();
        let obj = JSON.parse($("#itemDetailEffect").val());
       if(obj.SHOP_CODE!=undefined){
        $.ajax({
          url: "{{ URL::to('/api/getShopCode/') }}"+"/"+obj.SHOP_CODE,
          method: 'GET',
          success: function(data) {
            if(data.length>0){
                $(".table > tbody").html(data)
            }
          }
        })
    
       }
    })

    $(".add").on("click", function() {
            $(this).closest("table").find("tbody tr:last-child");
            const tbody = $(this).closest("table").find("tbody");
            tbody.find("tr:last-child").clone().appendTo(tbody);
            tbody.find("tr:last-child").find("td").find(".setLocation").attr("class","btn btn-primary btn-sm setLocation");
        
            const lastRow = tbody.find("tr:last-child");
            lastRow.find("td:first select").removeClass("select2-hidden-accessible");
            lastRow.find("td:first>div>div>span").remove();
      });


        $(document).on("click", ".remove", function() {
        const rowLenght = $(this).closest("tbody").find("tr").length;
        let id = $(this).closest("tr").find("td").find("select").val()
        if (rowLenght > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("This table must has at least 1 row!");
        }

   
    });  

    $(document).on("change", "table select[name='mallId[]']", function(i,el) {
      
    let option=$(this).closest("tr").find("td").find(".mallFloor").html('');        
    $.ajax({
            url: "{{ URL::to('mallFloors/show') }}",
            method: 'GET',
            data: "mallId="+$(this).val(),
            success: function(data) {
                option.html(data.options);
            }
        });
    });

    $(document).on("change", "table select[name='mallFloorId[]']", function(i,el) {
        let option=$(this).closest("tr").find("td").find(".shop").html('');        
        $.ajax({
            url: "{{ URL::to('shops/show') }}",
            method: 'GET',
            data: "mallFloorId="+$(this).val(),
            success: function(data) {                
                option.html(data.options);
            }
        });
    })
    
    $(document).on("click", ".setLocation", function(i,el) {
        $(".setLocation").attr("class","btn btn-primary btn-sm setLocation")
        let shopId=$(this).closest("tr").find("td").find(".shop");        
        let setLocation=$(this).closest("tr").find("td").find(".setLocation");        
        $.ajax({
            url: "{{ URL::to('api/shops') }}"+"/"+shopId.val(),
            method: 'GET',
            //data:$(this).val(),
            success: function(data) {                
                $(".long").val(data.shop.location.coordinates[0])
                $(".lat").val(data.shop.location.coordinates[1])
                $(".shopSelect").val(shopId.val())

                setLocation.attr("class","btn btn-success btn-sm setLocation")
            }
        });
    })
</script>
@endpush