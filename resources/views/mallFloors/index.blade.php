@extends('layouts/default') {{-- Page title --}} 
@section('title') Mall Floor Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Mall Floors</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Mall Floors</a></li>
        <li class="active">Mall Floor Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Mall Floors</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('mallFloors/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>

                <br/>
                <div class="col-md-12">
                        {{-- <div class="col-md-6"> --}}
                            <div class="row">
                            <form id="wildMonstersForm" action="/mallFloors" method="GET" enctype="multipart/form-data">
                                <!-- CSRF Token -->
                                {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                                <fieldset>
                                        <div class="col-md-6">
                                        <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                        <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                </span>
                                                    {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                        </div>
                                                    {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                        </div>
                                </fieldset>           
                            </form>
                        </div>
                </div>
                @php
                if($field=="mallFloorName"){
                    if($sort=="desc"){
                        $sortMallFloorName="asc";
                    }else{
                        $sortMallFloorName="desc";
                    }
                }else {
                    $sortMallFloorName="asc";
                }

                if($field=="mapIndoorFloorKey"){
                    if($sort=="desc"){
                        $sortMapIndoorFloorKey="asc";
                    }else{
                        $sortMapIndoorFloorKey="desc";
                    }
                }else {
                    $sortMapIndoorFloorKey="asc";
                } 
                
                if($field=="mapIndoorFloorData"){
                    if($sort=="desc"){
                        $sortMapIndoorFloorData="asc";
                    }else{
                        $sortMapIndoorFloorData="desc";
                    }
                }else {
                    $sortMapIndoorFloorData="asc";
                }  
                
                if($field=="mallName"){
                    if($sort=="desc"){
                        $sortMallName="asc";
                    }else{
                        $sortMallName="desc";
                    }
                }else {
                    $sortMallName="asc";
                }  
                @endphp
                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Mall Floor Name
                                    <a href="{{ URL::to("mallFloors?field=mallFloorName&sort={$sortMallFloorName}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                   
                                </th>
                                <th>Map Indoor Key
                                        <a href="{{ URL::to("mallFloors?field=mapIndoorFloorKey&sort={$sortMapIndoorFloorKey}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>    
                                </th>
                                <th>Map Indoor Data
                                        <a href="{{ URL::to("mallFloors?field=mapIndoorFloorData&sort={$sortMapIndoorFloorData}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                </th>
                                <th>Mall
                                        <a href="{{ URL::to("mallFloors?field=mallName&sort={$sortMallName}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Actions</th>

                                
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($mallFloors as $row)
                            <tr>
                                <td>{{ $row['mallFloorName']['eng'] }}</td>
                                <td>
                                    @isset($row['mapIndoorFloorKey'])
                                    {{ $row['mapIndoorFloorKey'] }}
                                    @endisset
                                </td>
                                <td>{{ $row['mapIndoorFloorData'] }}</td>
                                <td>
                                    @if(isset($row['malls']['mallName']['eng']))
                                    {{ $row['malls']['mallName']['eng'] }}
                                    @endif
                                </td>
                                <td>
                                    <a href="{{{ URL::to('mallFloors/' . $row['_id'] . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('mallFloors.destroy', collect($row)->first()) }}">
                                    <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$mallFloors->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     "pageLength": 50
        // });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });

</script>







@stop