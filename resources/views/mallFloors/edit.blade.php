@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Mall Floors</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('mallFloors') }}"> Mall Floors</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                {!! Form::model($mallFloor, ['url' => URL::to('mallFloors') . '/' . $mallFloor->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <fieldset>
                   @include('mallFloors.field')
                    </fieldset>

                {!! Form::close() !!}
                
                <div class="col-lg-12">
                        <div class="panel panel-info filterable ">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title pull-left">Shops</h3>
                                <div class="pull-right">
                                <a href="{{ URL::to('shops/create?mallFloorId='.$mallFloor->id) }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                                </div>
                            </div>
            
                            <div class="panel-body table-responsive">
                                <table class="table" id="table">
                                    <thead>
                                        <tr class="filters">
                                                <th>image1 </th> 
                                                <th>Shop Name</th>
                                                <th>Order</th>
                                                <th>Sponser</th>
                                                <th>Active</th>
                                                <th>Actions</th>                
                                        </tr>
                                    </thead>
                                    <tbody>
            
                                        @foreach($shops as $row)
                                        <tr>
                                                <td>
                                                        @if(isset($row->filenameLogo1))
                                                        <img src="{{URL::to('media/images/'.$row->filenameLogo1)}}" style="width: 120px; height: 120px" />
                                                        @endif
                                                    </td>
                                                    <td>{{ $row->shopName['eng'] }}</td>
                                                    <td>{{ $row->order}}</td>
                                                    <td>
                                                        @if ($row->isSponser)
                                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                                        @else
                                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($row->isActive)
                                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                                        @else
                                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                        @endif
                                                    </td>
                    
                                                    <td>
                                                        <a href="{{{ URL::to('shops/' . $row->id . '/edit' ) }}}">
                                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                        </a>
                                                    </a>
                                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('shops.destroy', $row->id) }}">
                                                            <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                            
                                                        </a>
                                                    </td>                    
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!-- Modal for showing delete confirmation -->
                                <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="user_delete_confirm_title">
                                                    Delete
                                                </h4>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure to delete this?
                                            </div>
                                            <div class="modal-footer">
                                            {!! Form::open(['method' => 'DELETE']) !!}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <button type="submit" class="btn btn-danger">Delete</a>
                                            {!! Form::close() !!}  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

@stop