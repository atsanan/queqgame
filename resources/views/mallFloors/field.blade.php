    @component('components.multiLangField',[
        'header'=>'Mall Floor Names',
        'field'=>'mallFloorName',
        'icon'=>'livicon'
        ])
    @endcomponent

    @component('components.multiLangTextAreaField',[
        'header'=>'Mall Floor Details',
        'field'=>'mallFloorDetail',
        'icon'=>'livicon'
        ])
    @endcomponent
<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Mall',
            'field'=>'mallId',
            'value'=>$mallId,
            'data'=>$mall,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Map Indoor Key',
            'field'=>'mapIndoorFloorKey',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Map Indoor Key']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Map Indoor Data',
            'field'=>'mapIndoorFloorData',
            'value'=>null,
            'attribute'=>['class'=>'form-control',''=>'Map Indoor Data']
        ])
        @endcomponent
    </div>
    
</div>




    <div class="form-group">
        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
    </div>