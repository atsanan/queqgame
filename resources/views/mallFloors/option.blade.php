<option value="">--- Select mallFloors ---</option>
@if(!empty($mallFloors))
  @foreach($mallFloors as $row)
    <option value="{{ $row->id }}">{{ $row->mallFloorName['eng'] }}</option>
  @endforeach
@endif