@extends('layouts/default') {{-- Page title --}} 
@section('title') Wild Monster Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Monsters Instantiate Report</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Report</a></li>
        <li class="active">Monster Instantiate Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
            <div class="col-lg-12">
                    <!-- toggling series charts strats here-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="linechart" data-size="16" data-loop="true"
                                    data-c="#fff" data-hc="#fff"></i> Chart
                            </h3>
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
           </div>
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Monsters Instantiate Report</h3>
                    <div class="pull-right">
                        <a href="{{ route('monstersInstantiate.excel') }}" class="btn btn-success btn-sm" id="addButton">Export</a>
                    </div>
                </div>

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Monster Name</th>
                                <th>Image</th>
                                <th>Count/CountMax</th>
                                <th>Is Active</th>
                                <th>Item Name</th>
                                <th>Zone Type</th>
                                <th>Malls</th> 
                                <th>Mall Floors</th>
                                <th>Shop</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($wildMonsters as $row)
                            <tr>
                                <td>{{ $row->monstersDefaults->mDefaultName['eng'] }}</td>
                                <td><img src="{{URL::to('media/images/'.$row->monstersDefaults->mDefaultAssetImageSlot)}}" style="width: 100px; height: 70px" >
                                <td>{{ $row->count."/".$row->countMax }}</td>
                                <td>
                                        @if ($row->isActive)
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                </td>
                                <td>{{ $row->item->itemName['eng'] }}</td>
                                <td>{{ $row->zoneTypes->zoneTypeName['eng'] }}</td>
                                <td>{{ $row->malls->mallName['eng'] }}</td>
                                <td>{{ $row->mallFloors->mallFloorName['eng'] }}</td>
                                <td>{{ $row->Shops->shopName['eng'] }}</td>
                                <td>
                                    <a href="{{{ URL::to('wildMonsters/' . $row->id . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('wildMonsters.destroy', collect($row)->first()) }}">
                                        <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        <!-- {{route('wildMonsters.destroy', collect($row)->first() )}} -->
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 


@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.categories.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.stack.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
    <!-- <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.stack.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.crosshair.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.time.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.selection.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.symbol.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.resize.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.categories.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/splinecharts/jquery.flot.spline.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script> -->
    <script language="javascript" type="text/javascript" >
    
    $(document).ready(function() {
        $('#table').dataTable({
            "pageLength": 50
        });
    });
    
    let count=[ ];
    let countMax=[];
    let images=[]
    @foreach($wildMonsters as $row)
    count.push({label:"{{$row->monstersDefaults->mDefaultName['eng']}}",y:{{$row->count}}})
    countMax.push({label:"{{$row->monstersDefaults->mDefaultName['eng']}}",y:{{$row->countMax}}})
    images.push({url: "{{URL::to('media/images/'.$row->monstersDefaults->mDefaultAssetImageSlot)}}"})
    @endforeach

    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer", {
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "Wild Monsters Report"
	}, 
	axisX: {
		title: "States"
	},
	axisY: {
		title: "Count Max",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisY2: {
		title: "Count",
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E"
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Count Max",
		showInLegend: true,      
		yValueFormatString: "#,##0.# Units",
		dataPoints: countMax
	},
	{
		type: "column",
		name: "Count",
		axisYType: "secondary",
		showInLegend: true,
		yValueFormatString: "#,##0.# Units",
		dataPoints:count
	}]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

var fruits= [];
  
  addImages(chart);

  function addImages(chart){
    for(var i = 0; i < chart.data[0].dataPoints.length; i++){
      var label = chart.data[0].dataPoints[i].label;
      
      if(label){
        fruits.push( $("<img>").attr("src", images[i].url)
                    .attr("class", label)
                    .css("display", "none")
                    .appendTo($("#chartContainer>.canvasjs-chart-container"))
                   );        
      }
      
      positionImage(fruits[i], i);
    }    
  }
  
  function positionImage(fruit, index){ 
    var imageBottom = chart.axisX[0].bounds.y1;     
    var imageCenter = chart.axisX[0].convertValueToPixel(chart.data[0].dataPoints[index].x);
    
    fruit.width(20);
   fruit.height(20);
    fruit.css({"position": "absolute", 
               "display": "block",
               "top": imageBottom  - fruit.height(),
               "left": imageCenter - fruit.width()/2
              });
    chart.render();
  }
  
  $( window ).resize(function() {
    for(var i = 0; i < chart.data[0].dataPoints.length; i++){
    	positionImage(fruits[i], i);
    }
  }); 
}
    </script>

<script src="{{ asset('assets/js/canvasjs.min.js') }}"></script>
    <!-- end of page level js -->
@endpush