
<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Monster',
        'field'=>'mDefaultId',
        'value'=>null,
        'data'=>$monstersDefaults,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Zone Type',
        'field'=>'zoneTypeId',
        'value'=>null,
        'data'=>$zoneTypes,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Mall',
        'field'=>'toMall',
        'value'=>null,
        'data'=>$malls,
        'attribute'=>['class'=>'form-control select2 toMall','placeholder'=>'Please Select', 
        'disabled'=>(isset($zoneType)&&($zoneType!="World"))?false:true ]
        ])
        @endcomponent
    </div>

    @php
        $activeMallFloor=true;
        if(isset($zoneType)){
            if($zoneType=="InMall Floor"){
                $activeMallFloor=false;
            }else if($zoneType=="InMall Shop") {
                $activeMallFloor=false;                
            }
        }   
    @endphp
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Mall Floor',
        'field'=>'toMallFloor',
        'value'=>null,
        'data'=>$mallFloors,
        'attribute'=>['class'=>'form-control toMallFloor','placeholder'=>'Please Select',
        'disabled'=>$activeMallFloor
      
    ],
        ])
        @endcomponent
    </div>
    @php
    $activeMallFloorShop=true;
    if(isset($zoneType)){
        if($zoneType=="InMall Shop"){
            $activeMallFloorShop=false;
        }
    }
    @endphp
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Shop',
        'field'=>'toShop',
        'value'=>null,
        'data'=>$shops,
        'attribute'=>['class'=>'form-control toShop','placeholder'=>'Please Select',
        'disabled'=>  $activeMallFloorShop
        ]
        ])
        @endcomponent
    </div>
    <div class="col-md-offset-2 col-md-6">
        <div class="form-group">
            <button type="button" class="btn btn-success btn-sm setLocation">
                    Set Location
            </button>      
        </div>   
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Longitude',
            'field'=>'location[coordinates][0]',
            'value'=>isset($wildMonster->location['coordinates'][0])?null:0,
            'attribute'=>['class'=>'form-control long','placeholder'=>'Longitude']
        ])
        @endcomponent
        </div>
        <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Latitude',
            'field'=>'location[coordinates][1]',
            'value'=>isset($wildMonster->location['coordinates'][1])?null:0,
            'attribute'=>['class'=>'form-control lat','placeholder'=>'Latitude']
        ])
        @endcomponent
    </div>
        
   
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Item',
        'field'=>'itemId',
        'value'=>null,
        'data'=>$itemDefault,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'number',
                'icon'=>'text',
                'label'=>'Item Order',
                'field'=>'order',
                'value'=>isset($wildMonster->order)?null:0,
                'attribute'=>['class'=>'form-control','placeholder'=>'Item Order']
            ])
            @endcomponent
    </div>
    
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Count',
                'field'=>'count',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Count']
            ])
            @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Count Max',
            'field'=>'countMax',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Count']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'datetime',
            'label'=>'Start Date',
            'field'=>'startDateTime',
            'attribute'=>['class'=>'form-control dateTime']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'datetime',
            'label'=>'Expired Date',
            'field'=>'expiredDateTime',
            'attribute'=>['class'=>'form-control dateTime']
        ])
        @endcomponent
    </div> 
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Reach Date time',
            'field'=>'isReachDateTime',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Is Real Position',
            'field'=>'isRealPosition',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Item Stock',
            'field'=>'itemStock',
            'value'=>isset($wildMonster->itemStock)?null:0,
            'attribute'=>['class'=>'form-control','placeholder'=>'Item Stock','disabled'=>empty($wildMonster->itemId)]
        ])
        @endcomponent
    </div>

    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Pick Percent',
            'field'=>'pickPercent',
            'value'=>isset($wildMonster->pickPercent)?null:100,
            'attribute'=>['class'=>'form-control','placeholder'=>'Item Stock']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Diamond',
            'field'=>'diamond',
            'value'=>isset($wildMonster->diamond)?null:0,
            'attribute'=>['class'=>'form-control','placeholder'=>'Diamond']
        ])
        @endcomponent
    </div>
    
   
   

</div>
<div class="row">
    
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Golden Minutes',
                'field'=>'isGoldenMinutes',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Is PickTo Bag',
                'field'=>'isPickToBag',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'IsFDGetCoupon',
                'field'=>'isFDGetCoupon',
                'value'=>null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>


</div>

<div class="form-group">
    <div class="col-md-12 text-right">
            @isset($wildMonster->itemId)
            <a href="{{ URL::to('itemDefaults/'.$wildMonster->itemId.'/edit') }}" class="btn btn-success btn-sm" id="addButton">Item Default</a>
            @endisset 
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>


@push('more_scripts')
<script type="text/javascript">
$("select[name='itemId']").change(function(){
    //console.log($(this).val())
    if($(this).val()!=""){
        $("input[name='itemStock']").attr('disabled',false);
    }else{
        
        $("input[name='itemStock']").attr('disabled',true);
    }
})
 
$("select[name='zoneTypeId']").change(function(){
    if($(this).val()=="5bb7582acf7ae9e79e22de48"){
    $.ajax({
            url: "{{ URL::to('api/getLocationZone') }}",
            method: 'GET',
            //data:$(this).val(),
            success: function(data) {               
                $(".long").val(data.coordinates[0])
                $(".lat").val(data.coordinates[1])
             }
        });
    }
})
$(document).on("click", ".setLocation", function(i,el) {
        let shopId =$("select[name='toShop']");
        $.ajax({
            url: "{{ URL::to('api/shops') }}"+"/"+shopId.val(),
            method: 'GET',
            //data:$(this).val(),
            success: function(data) {                
                $(".long").val(data.shop.location.coordinates[0])
                $(".lat").val(data.shop.location.coordinates[1])

                setLocation.attr("class","btn btn-success btn-sm setLocation")
            }
        });
    })
</script>
@endpush