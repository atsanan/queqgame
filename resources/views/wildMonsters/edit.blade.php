@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Wild Monsters</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('wildMonsters') }}"> Wild Monsters</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                {!! Form::model($wildMonster, ['url' => URL::to('wildMonsters') . '/' . $wildMonster->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="isReachDateTime" value="0" />
                    <input type="hidden" name="isGoldenMinutes" value="0" />
                    <input type="hidden" name="isPickToBag" value="0" />
                    <input type="hidden" name="isRealPosition" value="0" />
                    <input type="hidden" name="isFDGetCoupon" value="0" />
                    <fieldset>
                    @include('wildMonsters.field')
                    </fieldset>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <!--row end-->
</section>

@stop