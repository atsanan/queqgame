@extends('layouts/default') {{-- Page title --}} 
@section('title') Wild Monster Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Wild Monsters</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Wild Monsters</a></li>
        <li class="active">Wild Monster Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Wild Monsters</h3>
                    <div class="pull-right">
                            <a href="{{ route('wildMonsters.excelIndex',['field'=>$field,'sort'=>$sort,'search'=>$search]) }}" class="btn btn-success btn-sm" >Export</a>
                    <a href="{{ URL::to('wildMonsters/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>
                
                <br/>
                <div class="col-md-12">
                {{-- <div class="col-md-6"> --}}
                    <div class="row">
                    <form id="wildMonstersForm" action="{{ URL::to("wildMonsters")}}" method="GET" enctype="multipart/form-data">
                        <!-- CSRF Token -->
                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                        <fieldset>
                                <div class="col-md-6">
                                <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                        {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                        <div class="col-md-9">
                                                <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                        </span>
                                            {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                </div>
                                            {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                </div>
                        </fieldset>           
                    </form>
                    {{-- </div> --}}
                </div>
                </div>
                @php
                                      
                    if($field=="monstersdefaults"){
                        if($sort=="desc"){
                            $sortMonstersdefaults="asc";
                        }else{
                            $sortMonstersdefaults="desc";
                        }
                    }else {
                        $sortMonstersdefaults="asc";
                    } 

                    if($field=="count"){
                        if($sort=="desc"){
                            $sortCount="asc";
                        }else{
                            $sortCount="desc";
                        }
                    }else {
                        $sortCount="asc";
                    } 

                    if($field=="mallFloor"){
                        if($sort=="desc"){
                            $sortMallFloor="asc";
                        }else{
                            $sortMallFloor="desc";
                        }
                    }else {
                        $sortMallFloor="asc";
                    } 

                    
                    if($field=="shop"){
                        if($sort=="desc"){
                            $sortShop="asc";
                        }else{
                            $sortShop="desc";
                        }
                    }else {
                        $sortShop="asc";
                    } 

                    if($field=="zoneType"){
                        if($sort=="desc"){
                            $sortZoneType="asc";
                        }else{
                            $sortZoneType="desc";
                        }
                    }else {
                        $sortZoneType="asc";
                    } 

                    
                    if($field=="isGoldenMinutes"){
                        if($sort=="desc"){
                            $sortIsGoldenMinutes="asc";
                        }else{
                            $sortIsGoldenMinutes="desc";
                        }
                    }else {
                        $sortIsGoldenMinutes="asc";
                    } 

                    if($field=="isReachDateTime"){
                        if($sort=="desc"){
                            $sortIsReachDateTime="asc";
                        }else{
                            $sortIsReachDateTime="desc";
                        }
                    }else {
                        $sortIsReachDateTime="asc";
                    } 

                    if($field=="isExpired"){
                        if($sort=="desc"){
                            $sortIsExpired="asc";
                        }else{
                            $sortIsExpired="desc";
                        }
                    }else {
                        $sortIsExpired="asc";
                    } 

                    if($field=="isActive"){
                        if($sort=="desc"){
                            $sortIsActive="asc";
                        }else{
                            $sortIsActive="desc";
                        }
                    }else {
                        $sortIsActive="asc";
                    } 
                @endphp

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Monster Name 
                                    <a href="{{ URL::to("wildMonsters?field=monstersdefaults&sort={$sortMonstersdefaults}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a> 
                                </th>
                                <th>Count
                                        <a href="{{ URL::to("wildMonsters?field=count&sort={$sortCount}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Mall Floor
                                        <a href="{{ URL::to("wildMonsters?field=mallFloor&sort={$sortMallFloor}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Shop
                                        <a href="{{ URL::to("wildMonsters?field=shop&sort={$sortShop}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Zone Type
                                        <a href="{{ URL::to("wildMonsters?field=zoneType&sort={$sortZoneType}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Golden Minutes
                                        <a href="{{ URL::to("wildMonsters?field=isGoldenMinutes&sort={$sortIsGoldenMinutes}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Reach Date Time
                                        <a href="{{ URL::to("wildMonsters?field=isReachDateTime&sort={$sortIsReachDateTime}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Continued
                                    <a href="{{ URL::to("wildMonsters?field=isExpired&sort={$sortIsExpired}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>  
                                </th>
                                <th>Active
                                        <a href="{{ URL::to("wildMonsters?field=isActive&sort={$sortIsActive}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($wildMonsters as $row)
                            <tr>
                                <td>
                                    @isset($row['monstersdefaults']['mDefaultName']['eng'])
                                    {{ $row['monstersdefaults']['mDefaultName']['eng'] }}
                                    @endisset
                                </td>
                                <td>{{ $row['count'] }}</td>
                                <td>
                                    @isset($row['mallfloors']['mallFloorName']['eng'])
                                    {{ $row['mallfloors']['mallFloorName']['eng'] }}
                                    @endisset
                                </td>
                                <td>
                                    @isset($row['shops']['shopName']['eng'])
                                    {{ $row['shops']['shopName']['eng'] }}
                                    @endisset
                                </td>
                                <td>

                                    @isset($row['zonetypes']['zoneTypeName']['eng'])
                                    {{ $row['zonetypes']['zoneTypeName']['eng'] }}       
                                    @endisset
                                 
                                </td>
                                <td>
                                        @if ($row['isGoldenMinutes'])
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                </td>
                                <td>
                                        @if ($row['isReachDateTime'])
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                </td>
                                <td>
                                    @if(isset($row['isExpired']))
                                        @if ($row['isExpired'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                        @if ($row['isActive'])
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                </td>
                                <td>
                                    <a href="{{{ URL::to('wildMonsters/' . $row['_id'] . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('wildMonsters.destroy', collect($row)->first()) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        <!-- {{route('wildMonsters.destroy', collect($row)->first() )}} -->
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$wildMonsters->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     "pageLength": 50
        // });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })

</script>







@stop