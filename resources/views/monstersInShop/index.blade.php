@extends('layouts/default') {{-- Page title --}} 
@section('title') Report Monster In Shop @parent 
@stop {{-- page level styles --}} 


@section('content')

<section class="content-header">
        <h1>Report Monster In Shop</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="#">Report</a></li>
            <li class="active">Report Monster In Shop Lists</li>
        </ol>
    </section>
<!-- Left side column. contains the logo and sidebar -->
      
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  
                    <div class="col-lg-12">
                        <!-- toggling series charts strats here-->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="livicon" data-name="linechart" data-size="16" data-loop="true"
                                        data-c="#fff" data-hc="#fff"></i> Chart
                                </h3>
                                <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                            </div>
                            <div class="panel-body">
                               
                              <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                           </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                            <div class="panel panel-info filterable ">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title pull-left">Monster In shop </h3>
                                    <div class="pull-right">
                                        @php
                                         $query=[];
                                         if(isset($mallId)){
                                          $query['mall']=implode(",",$mallId);
                                         }
                                         if(isset($mallFloorId)){
                                            $query['mallFloorId']=implode(",",$mallFloorId);
                                         }

                                         if(isset($shopId)){
                                            $query['shopId']=implode(",",$shopId);
                                         }
                                        @endphp
                                            <a href="{{ route('monsterInShop.excel',$query) }}" class="btn btn-success btn-sm" id="addButton">Export</a>
                                    </div>
                                </div>
                                <form id="wildMonstersForm" action="{{ route('monsterInShop.store') }}" method="POST" enctype="multipart/form-data">
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <fieldset>
                    
                                @include('monstersInShop.field')                  
                                </fieldset>           
            
                            </form>
            
                                <div class="panel-body table-responsive">
                                    <table class="table" id="table">
                                        <thead>
                                            <tr class="filters">
                                                <th>Mall </th>
                                                <th>Mall Floor</th>
                                                <th>Shop Name</th>
                                                <th>Monster Player Name</th>
                                                <th>Monster Name</th>
                                                <th>Monster Player Count</th>
                                                <th>Image</th>
                                                <th>Name User</th>
                                                <th>Email</th>
                                                <th>User Action</th>
                                                <th>Player Action</th>
                                                <th>Monsters Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                          
                                            @foreach($monsterInShop as $row)
                                            <tr>
                                                    <td>{{ $row->mall->mallName->eng }}</td>
                                                    <td>{{ $row->mallFloor->mallFloorName->eng }}</td>
                                                    <td>{{ $row->shops->shopName->eng }}</td>
                                                    <td>{{ $row->mPlayerName }}</td>
                                                    <td>{{ $row->monsterDefaults->mDefaultName->eng }}</td>
                                                    <td>{{ $row->sum }}</td>
                                                    <td><img src="{{URL::to('media/images/'.$row->monsterDefaults->mDefaultAssetImageSlot)}}" style="width: 100px; height: 70px" /></td>
                                                    <td>{{ $row->users->firstname.' '.$row->users->lastname }}</td>
                                                    <td>
                                                        @if(@isset($row->users->email))
                                                        {{ $row->users->email }}
                                                        @endif
                                                    
                                                    </td>
                                                    <td>  <a href="{{ URL::to('user/' . $row->users->_id . '/edit' ) }}">
                                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                    </a>
                                                    <td>  <a href="{{ URL::to('players/' . $row->playerId . '/edit' ) }}">
                                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                    </a>
                                                    <td>  <a href="{{ URL::to('monstersDefault/' . $row->monsterDefaults->_id . '/edit' ) }}">
                                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                    </a>
                                                  
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- Modal for showing delete confirmation -->
                                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                                        Delete
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure to delete this? 
                                                </div>
                                                <div class="modal-footer">
                                                {!! Form::open(['method' => 'DELETE']) !!}
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <button type="submit" class="btn btn-danger">Delete</a>
                                                {!! Form::close() !!}                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
@stop       
@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.categories.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.stack.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
    <!-- <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.stack.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.crosshair.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.time.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.selection.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.symbol.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.resize.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.categories.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/splinecharts/jquery.flot.spline.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script> -->
    <script language="javascript" type="text/javascript" >
        $("select[name='mallId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getMallFloor/') }}",
          method: 'POST',
          data:{mallId: $(this).val()},
         success: function(data) {
            $("select[name='mallFloorId[]'").html('');
            $("select[name='mallFloorId[]'").html(data.options);
            
          }
      });
            
    });

    $("select[name='mallFloorId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getShop/') }}",
          method: 'POST',
          data:{mallFloorId: $(this).val()},
         success: function(data) {
            $("select[name='shopId[]'").html('');
            $("select[name='shopId[]'").html(data.options);
            
          }
      });
            
    });

     let data = [];
     let images = [];
     @foreach($stat as $value)
     data.push({ label: "{{$value['mDefaultName']}}",  y:{{$value['sum']}}  })
     images.push({url: "{{URL::to('media/images/'.$value['mDefaultAssetImageSlot'])}}"})
     @endforeach

    window.onload = function () {
       
   var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2",//light1
    title:{
      text: "Monster In Shop"              
    },
    data: [              
      {			
        type: "column",
        name: "Amount",
		showInLegend: true,      
		yValueFormatString: "#,##0.# Units",
        dataPoints: data
      }
    ]
  });
  chart.render();
  
  
  var fruits= [];
  
  addImages(chart);

  function addImages(chart){
    for(var i = 0; i < chart.data[0].dataPoints.length; i++){
      var label = chart.data[0].dataPoints[i].label;
      
      if(label){
        fruits.push( $("<img>").attr("src", images[i].url)
                    .attr("class", label)
                    .css("display", "none")
                    .appendTo($("#chartContainer>.canvasjs-chart-container"))
                   );        
      }
      
      positionImage(fruits[i], i);
    }    
  }
  
  function positionImage(fruit, index){ 
    var imageBottom = chart.axisX[0].bounds.y1;     
    var imageCenter = chart.axisX[0].convertValueToPixel(chart.data[0].dataPoints[index].x);
    
    fruit.width(40);
   fruit.height(40);
    fruit.css({"position": "absolute", 
               "display": "block",
               "top": imageBottom  - fruit.height(),
               "left": imageCenter - fruit.width()/2
              });
    chart.render();
  }
  
  $( window ).resize(function() {
    for(var i = 0; i < chart.data[0].dataPoints.length; i++){
    	positionImage(fruits[i], i);
    }
  }); 
}


    </script>

<script src="{{ asset('assets/js/canvasjs.min.js') }}"></script>
   
    <!-- end of page level js -->
@endpush