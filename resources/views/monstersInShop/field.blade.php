

<h4></h4>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField', [
                'type'=>'select2',                          
                'label'=>'Mall',
                'field'=>'mallId[]',
                'value'=>$mallId,
                'data'=>$mall,
                'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
            ])
            @endcomponent
     
    </div>
    <div class="col-md-6">
  
    @component('components.inputField', [
        'type'=>'select2',                          
        'label'=>'MallFloor',
        'field'=>'mallFloorId[]',
        'value'=>$mallFloorId,
        'data'=>$mallFloors,
        'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
    ])
    @endcomponent

</div>

    
<div class="col-md-6">
  
        @component('components.inputField', [
            'type'=>'select2',                          
            'label'=>'shop',
            'field'=>'shopId[]',
            'value'=>$shopId,
            'data'=>$shop,
            'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
        ])
        @endcomponent
    
    </div>
    
        

    
</div>


<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
    </div>
</div>

