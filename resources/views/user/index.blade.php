@extends('layouts/default') {{-- Page title --}} 
@section('title') User Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
<section class="content-header">
    <h1>User</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="stickerStores"> User </a></li>
        <li class="active">User Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">User</h3>
                    <div class="pull-right">
                            <a href="{{ route('user.excel') }}" class="btn btn-success btn-sm" id="addButton">Export</a>
                    </div>
                </div>
                <br/>
                <div class="col-md-12">
                    <div class="row">
                    <form id="wildMonstersForm" action="/user" method="GET" enctype="multipart/form-data">
                        <!-- CSRF Token -->
                        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                        <fieldset>
                                <div class="col-md-6">
                                <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                        {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                        <div class="col-md-9">
                                                <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                        </span>
                                            {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                </div>
                                            {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                </div>
                        </fieldset>           
                    </form>
                </div>
            </div>
                
                @php
                
                if($field=="firstname"){
                    if($sort=="desc"){
                        $sortFirstName="asc";
                    }else{
                        $sortFirstName="desc";
                    }
                }else {
                    $sortFirstName="asc";
                }   

                
                if($field=="lastname"){
                    if($sort=="desc"){
                        $sortLastName="asc";
                    }else{
                        $sortLastName="desc";
                    }
                }else {
                    $sortLastName="asc";
                }   

                
                if($field=="email"){
                    if($sort=="desc"){
                        $sortEmail="asc";
                    }else{
                        $sortEmail="desc";
                    }
                }else {
                    $sortEmail="asc";
                }   

                
                if($field=="authenTypeId"){
                    if($sort=="desc"){
                        $sortAuthenTypeId="asc";
                    }else{
                        $sortAuthenTypeId="desc";
                    }
                }else {
                    $sortAuthenTypeId="asc";
                }   

                
                if($field=="createAt"){
                    if($sort=="desc"){
                        $sortCreateAt="asc";
                    }else{
                        $sortCreateAt="desc";
                    }
                }else {
                    $sortCreateAt="asc";
                }   

                
                if($field=="lastLogin"){
                    if($sort=="desc"){
                        $sortLastLogin="asc";
                    }else{
                        $sortLastLogin="desc";
                    }
                }else {
                    $sortLastLogin="asc";
                }   
                @endphp
                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>First Name
                                    <a href="{{ URL::to("user?field=firstname&sort={$sortFirstName}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>Last Name
                                    <a href="{{ URL::to("user?field=lastname&sort={$sortLastName}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                     
                                </th>
                                <th>Email
                                    <a href="{{ URL::to("user?field=email&sort={$sortEmail}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                              
                                </th>
                                <th>Autnen Type
                                    <a href="{{ URL::to("user?field=authenTypeId&sort={$sortAuthenTypeId}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>CreateAt
                                    <a href="{{ URL::to("user?field=createAt&sort={$sortCreateAt}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>LastLogin
                                    <a href="{{ URL::to("user?field=lastLogin&sort={$sortLastLogin}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead
                        <tbody>

                            @foreach($user as $row)
                            <tr>
                                <td>{{$row->firstname}} </td>
                                <td>{{$row->lastname}} </td>
                                <td>{{$row->email}} </td>
                                <td>
                                    @if ($row->authenTypeId==1)
                                        FaceBook                                       
                                    @else
                                        Google
                                    @endif
                                </td>
                                <td>
                                    @php
                                        $createAt = new DateTime($row->createAt, $gmtTimezone);
                                        $offsetcreateAt = $userTimezone->getOffset($createAt);
                                        $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                        $createAt->add($myIntervalcreateAt);
                                        echo $createAt->format('Y-m-d H:i:s');
                                    @endphp
                                </td>
                                <td>
                                    @php
                                    if(isset($row->lastLogin)){
                                        $lastLogin = new DateTime($row->lastLogin, $gmtTimezone);
                                        $offsetLastLogin = $userTimezone->getOffset($lastLogin);
                                        $myIntervalLastLogin=DateInterval::createFromDateString((string)$offsetLastLogin . 'seconds');
                                        $lastLogin->add($myIntervalLastLogin);
                                        echo $lastLogin->format('Y-m-d H:i:s');
                                    }
                                    @endphp
                                </td>
                                <td>  @if ($row->verified)
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                        <a href="{{{ URL::to('user/' . $row->id . '/edit' ) }}}">
                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                        </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$user->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    // $(document).ready(function() {
    //     $('#table').dataTable({
    //         "pageLength": 50
    //     });
    // });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })

</script>







@stop