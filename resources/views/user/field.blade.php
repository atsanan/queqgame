@include('constants.gender')


@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
$offset="";
if(isset($user->lastLogin)){
$myDateTime = new DateTime($user->lastLogin, $gmtTimezone);
$offset = $userTimezone->getOffset($myDateTime);
$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
$myDateTime->add($myInterval);
$offset = $myDateTime->format('Y-m-d H:i:s');
}
$createAt = new DateTime($user->createAt, $gmtTimezone);
$offsetcreateAt = $userTimezone->getOffset($createAt);
$myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
$createAt->add($myIntervalcreateAt);
$offsetcreateAt = $createAt->format('Y-m-d H:i:s');

$offsetUpdateAt="";
if(isset($user->updated_at)){
$updateAt = new DateTime($user->updated_at, $gmtTimezone);
$offsetUpdateAt = $userTimezone->getOffset($updateAt);
$myInterval=DateInterval::createFromDateString((string)$offsetUpdateAt . 'seconds');
$updateAt->add($myInterval);
$offsetUpdateAt = $updateAt->format('Y-m-d H:i:s');
}
@endphp
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'First Name',
            'field'=>'firstname',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'First Name']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Last Name',
                'field'=>'lastname',
                'value'=>null,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Last Name']
            ])
        @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Name',
                'field'=>'name',
                'value'=>null,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Name']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Email',
                'field'=>'email',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Email']
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Birth Date',
                'field'=>'birthdate',
                'value'=>null,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Birth Date']
            ])
            @endcomponent
    </div>

    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'icon'=>'text',
            'label'=>'Gender',
            'field'=>'gender',
            'value'=>null,
            'data'=>$GLOBALS['genderData'],
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>    

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Authen Type Id',
            'field'=>'authenTypeId',
            'value'=>!empty($user->authenTypeId)?$user->authenTypeId==1?"Facebook":"Google":null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Authen Id','disabled'=>true]
        ])
        @endcomponent
</div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Authen Id',
                'field'=>'authenId',
                'value'=>null,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Authen Id','disabled'=>true]
            ])
            @endcomponent
    </div>

    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Push Messages Access Token',
            'field'=>'pushMessagesAccessToken',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'pushMessagesAccessToken','disabled'=>true]
        ])
        @endcomponent
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Count',
            'field'=>'time',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Count','disabled'=>true]
        ])
        @endcomponent
    </div>

    

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'createAt',
                'field'=>'createAt',
                'value'=>$offsetcreateAt,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Time','disabled'=>true]
            ])
            @endcomponent
    </div>
    
    
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'last Update',
                'field'=>'updated_at',
                'value'=>$offsetUpdateAt,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'last Update','disabled'=>true]
            ])
            @endcomponent
    </div>


    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'last Login',
                'field'=>'lastLogin',
                'value'=>$offset,
                'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Last Login','disabled'=>true]
            ])
            @endcomponent
    </div>
</div>


<div class="form-group">
        <div class="col-md-12 text-right">
            @if($user->deviceId!="")
            <a href="{{ URL::to('device/'.$user->deviceId.'/view') }}" class="btn btn-info btn-sm" id="addButton">Device</a>
            @endif    
            <a href="{{ URL::to('players/'.$player->id.'/edit') }}" class="btn btn-success btn-sm" id="addButton">Edit Player</a>
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
</div>
    