@extends('layouts/default') {{-- Page title --}} 
@section('title') Edit User @parent 
@stop 
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
@section('content')
<section class="content-header">
    <h1>Edit User</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('shopAds') }}"> User </a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                {!! Form::model($user, ['url' => URL::to('user') . '/' . $user->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <fieldset>
                    @include('user.field')
                    </fieldset>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
<section class="content paddingleft_right15">
        <div class="row">
    
                <div class="panel panel-info filterable ">
                    <div class="panel-heading clearfix">
                        <h3 class="panel-title pull-left">Mail Box</h3>
                        <div class="pull-right">
                        <a href="{{ URL::to('mailBox/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                        </div>
                    </div>
    
                    <div class="panel-body table-responsive">
                        <table class="table" id="table">
                            <thead>
                                <tr class="filters">
                                 <th>Linklistname</th>
                                 <th>Title</th>
                                 <th>CreateAt</th>
                                 <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
    
                                @foreach($mailBox as $row)
                                <tr>
                                    <td>{{$row->linkListName}}</td>
                                    <td>{{$row->title}}</td>
                                    <td>
                                            @php
                                                if(isset($row->createAt)){
                                                    $createAt = new DateTime($row->createAt, $gmtTimezone);
                                                    $offsetcreateAt = $userTimezone->getOffset($createAt);
                                                    $myIntervalcreateAt=DateInterval::createFromDateString((string)$offsetcreateAt . 'seconds');
                                                    $createAt->add($myIntervalcreateAt);
                                                    echo $createAt->format('Y-m-d H:i:s');
                                                }
                                            @endphp
                                    </td>
                                    <td>
                                            <a href="{{{ URL::to('mailBox/' . $row->id . '/edit' ) }}}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                            </a>
                                        
                                            <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('mailBox.destroy', collect($row)->first()) }}">
                                            <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                            </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- Modal for showing delete confirmation -->
                        <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="user_delete_confirm_title">
                                            Delete
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure to delete this?
                                    </div>
                                    <div class="modal-footer">
                                    {!! Form::open(['method' => 'DELETE']) !!}
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <button type="submit" class="btn btn-danger">Delete</a>
                                    {!! Form::close() !!}  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
        </div>
        <!-- row-->
    </section>
@stop