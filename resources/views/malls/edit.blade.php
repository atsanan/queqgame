@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Malls</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('malls') }}"> Malls</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                {!! Form::model($mall, ['url' => URL::to('malls') . '/' . $mall->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="isSponser" value="0" />
                    <input type="hidden" name="isFixedBugFloor" value="0" />

                    <fieldset>
                        @include('malls.field')
                    </fieldset>
                {!! Form::close() !!}
                
                <div class="col-lg-12">
                    <div class="panel panel-info filterable ">
                        <div class="panel-heading clearfix">
                            <h3 class="panel-title pull-left">Mall Floors</h3>
                            <div class="pull-right">
                            <a href="{{ URL::to('mallFloors/create?mallId='.$mall->id) }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                            </div>
                        </div>
        
                        <div class="panel-body table-responsive">
                            <table class="table" id="table">
                                    <thead>
                                            <tr class="filters">
                                                <th>Mall Floor Name</th>                                                <!-- <th>MallFloorDetail</th> -->
                                                <th>Map Indoor Key</th>
                                                <th>Map Indoor Data</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                                @foreach($mallFloors as $row)
                                                <tr>

                                                    <td>{{ $row->mallFloorName['eng'] }}</td>
                                                    <td>{{ $row->mapIndoorFloorKey }}</td>
                                                    <td>{{ $row->mapIndoorFloorData }}</td>
                                                    {{-- <td>{{ $row->mallId}}</td> --}}
                                                    <td>
                                                        <a href="{{{ URL::to('mallFloors/' . $row->id . '/edit' ) }}}">
                                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                        </a>
                                                    </a>
                                                        <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('mallFloors.destroy', collect($row)->first()) }}">
                                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                    </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>                
                            </table>
              <!-- Modal for showing delete confirmation -->
              <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title" id="user_delete_confirm_title">
                                    Delete
                                </h4>
                            </div>
                            <div class="modal-body">
                                Are you sure to delete this?
                            </div>
                            <div class="modal-footer">
                            {!! Form::open(['method' => 'DELETE']) !!}
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <button type="submit" class="btn btn-danger">Delete</a>
                            {!! Form::close() !!}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop