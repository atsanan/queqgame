

@component('components.multiLangField',[
    'header'=>'Mall Names',
    'field'=>'mallName',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Mall Details',
    'field'=>'mallDetail',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Mall Address',
    'field'=>'mallAddress',
    'icon'=>'livicon'
    ])
@endcomponent
<h4>Multi</h4>
<div class="row">
        {{-- @component('components.inputField',[
            'type'=>'map'
        ])
        @endcomponent --}}
        
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Longitude',
                'field'=>'longitude',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Longtitude','required'=>true]
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Latitude',
                'field'=>'latitude',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Latitude','required'=>true]
            ])
            @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Sponser',
                'field'=>'isSponser',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Is Fixed Bug Floor',
            'field'=>'isFixedBugFloor',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>
</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Map Indoor',
                'field'=>'mapIndoorId',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Map Indoor']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
                'type'=>'number',
                'icon'=>'text',
                'label'=>'Order',
                'field'=>'order',
                'value'=>$order,
                'attribute'=>['class'=>'form-control','placeholder'=>'Order']
            ])
        @endcomponent
    </div>
    
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'number',
                'icon'=>'text',
                'label'=>'Indoor Start Floor',
                'field'=>'indoorStartFloor',
                'value'=>isset($mall->indoorStartFloor)?null:0,
                'attribute'=>['class'=>'form-control','placeholder'=>'Map Outdoor']
            ])
            @endcomponent
    </div>

    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Indoor Distance From Interest',
            'field'=>'indoorDistanceFromInterest',
            'value'=>isset($mall->indoorDistanceFromInterest)?null:200,
            'attribute'=>['class'=>'form-control','placeholder'=>'Map Outdoor']
        ])
        @endcomponent
    </div>

</div>


<div class="row">
    <div class="col-md-2 col-xs-offset-5">
        @if(!empty($filenameLogos1))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
                <img src="{{URL::to('media/images/'.$filenameLogos1)}}" class="img-responsive" alt="$field" style="width: 200px; height: 150px;">
            </div>
        @endif
    </div>
    <div class="col-md-12"> 
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 1 (120*120)',
            'field'=>'filenameLogo1',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-2 col-xs-offset-4">
        @if(!empty($filenameLogos2))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 522px; height: 111px;">
                <img src="{{URL::to('media/images/'.$filenameLogos2)}}" class="img-responsive" alt="$field" style="width: 522px; height: 101px;">
            </div>
        @endif
    </div> 
    <div class="col-md-12">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Logo 2 (1044 * 202)',
                'field'=>'filenameLogo2',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

