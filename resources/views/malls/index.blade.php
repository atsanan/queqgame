@extends('layouts/default') {{-- Page title --}} 
@section('title') Mall Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Malls</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Malls</a></li>
        <li class="active">Mall Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Malls</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('malls/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>

                <div class="panel-body table-responsive">
                        <div class="col-md-12">
                                <div class="row">
                                <form id="wildMonstersForm" action="/malls" method="GET" enctype="multipart/form-data">
                                    <!-- CSRF Token -->
                                    {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                                    <fieldset>
                                            <div class="col-md-6">
                                            <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                    {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                    <div class="col-md-9">
                                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                    </span>
                                                        {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                            </div>
                                                        {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                            </div>
                                    </fieldset>           
                                </form>
                                {{-- </div> --}}
                            </div>
                    <table class="table" id="table">
                        
                @php
                                      
                if($field=="mallName"){
                    if($sort=="desc"){
                        $sortName="asc";
                    }else{
                        $sortName="desc";
                    }
                }else {
                    $sortName="asc";
                }   

                                
                if($field=="isActive"){
                    if($sort=="desc"){
                        $sortIsActive="asc";
                    }else{
                        $sortIsActive="desc";
                    }
                }else {
                    $sortIsActive="asc";
                }   

                            
                if($field=="isSponser"){
                    if($sort=="desc"){
                        $sortIsSponser="asc";
                    }else{
                        $sortIsSponser="desc";
                    }
                }else {
                    $sortIsSponser="asc";
                }   
                @endphp
                        <thead>
                            <tr class="filters">
                                <th>Mall Name

                                    <a href="{{ URL::to("malls?field=mallName&sort={$sortName}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                {{-- <th>Longitude</th> --}}
                                {{-- <th>Latitude</th> --}}
                                {{-- <th>Map Indoor</th> --}}
                                {{-- <th>Map Outdoor</th> --}}
                                <th>Active
                                    <a href="{{ URL::to("malls?field=isActive&sort={$sortIsActive}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>Sponser
                                        <a href="{{ URL::to("malls?field=isSponser&sort={$sortIsSponser}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                </th>
                                <th>Logo</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($malls as $row)
                            <tr>
                                <td>{{ $row->mallName['eng']}}</td>
                                {{-- <td>{{ $row->location['coordinates']['0'] }}</td> --}}
                                {{-- <td>{{ $row->location['coordinates']['1'] }}</td> --}}
                                {{-- <td>{{ $row->mapIndoorId}}</td> --}}
                                {{-- <td>{{ $row->mapOutdoorId}}</td> --}}
                                <td>
                                     @if ($row->isActive)
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                    @if ($row->isSponser)
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                        @if(isset($row->filenameLogo1)||isset($row->filenameLogo2))
                                        @component('components.modal',['id'=>uniqid(),'title'=>'Image Lists'])
                                        @slot('content')
                                        @if(isset($row->filenameLogo1))
                                        <img src="{{URL::to('media/images/'.$row->filenameLogo1)}}" style="width: 100px; height: 70px" />
                                        @endif
                                        @if(isset($row->filenameLogo2))
                                        <img src="{{URL::to('media/images/'.$row->filenameLogo2)}}" style="width: 100px; height: 70px" />
                                        @endif
                                        @endslot
                                        @endcomponent
                                        @endif

                                </td>
                               
                                <td>
                                    <a href="{{{ URL::to('malls/' . $row->id . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('malls.destroy', collect($row)->first()) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        <!-- {{route('malls.destroy', collect($row)->first() )}} -->
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$malls->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        // $('#table').dataTable({
        //     //"pageLength": 50
        // });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })
</script>

@yield('modalScripts')
@stop