@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop
@section('content')
<section class="content-header">
    <h1>Malls</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('malls') }}"> Malls</a></li>
        <li class="active">Add new</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h4 class="panel-title">Add New</h4>
            </div>
            <div class="panel-body">
                <form id="mallsForm" action="{{ route('malls.store') }}" method="POST" enctype="multipart/form-data"
                    class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <fieldset>
                    @include('malls.field')
                    </fieldset>
                </form>

            </div>
        </div>
    </div>
</section>

@stop 
