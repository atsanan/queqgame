<div class="row">
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'title',
                    'icon'=>'text',
                    'label'=>'Title',
                    'field'=>'title',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'textArea',
                    'icon'=>'text',
                    'label'=>'Detail',
                    'field'=>'detail',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','rows'=>3]
                ])
                @endcomponent
        </div>
</div>

<div id="preview-img" class="col-md-12 col-md-offset-6" style="display:none">
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                <img id="img" src="" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
        </div>
</div>

@if(!empty($loginReward->imageName))
<div class="col-md-12 col-md-offset-6">
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                <img src="{{URL::to('media/images/'.$loginReward->imageName)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
        </div>
</div>

<div class="col-md-12 col-md-offset-5"> 
        <div class="form-group">       
                <p>{{URL::to('media/images/'.$loginReward->imageName)}}</p>
        </div>
</div>
@endif


<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Image',
            'field'=>'filename',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>

<div id="preview-thumbnail" class="col-md-12 col-md-offset-6" style="display:none">
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                <img id="img-thumbnail" src="" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
        </div>
</div>

@if(!empty($loginReward->thumbnail))
<div class="col-md-12 col-md-offset-6">
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                <img src="{{URL::to('media/images/'.$loginReward->thumbnail)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
        </div>
</div>

<div class="col-md-12 col-md-offset-5"> 
        <div class="form-group">       
                <p>{{URL::to('media/images/'.$loginReward->thumbnail)}}</p>
        </div>
</div>
@endif

<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Thumbnail',
            'field'=>'thumbnailName',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>

<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Diamond',
            'field'=>'diamond',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>
<div class="col-md-12 ">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Diamond Active',
            'field'=>'diamondActive',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>

<div class="col-md-12">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Item',
        'field'=>'itemId',
        'value'=>null,
        'data'=>$itemDefaults,
        'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
        ])
        @endcomponent
</div>
<div class="col-md-12 ">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Item Active',
            'field'=>'itemActive',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>

<div class="col-md-12">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Monster',
        'field'=>'mDefaultId',
        'value'=>null,
        'data'=>$monsterDefaults,
        'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
        ])
        @endcomponent
</div>
<div class="col-md-12 ">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Monster Active',
            'field'=>'monsterActive',
            'value'=>null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>

<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Index Reward',
            'field'=>'indexReward',
            'value'=>!empty($loginReward->indexReward)?null:0,
            'attribute'=>['class'=>'form-control','placeholder'=>'Order']
        ])
        @endcomponent
</div>

<div class="col-md-12">
        @component('components.inputField',[
                'type'=>'checkbox',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>
<div class="form-group">
        <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-responsive btn-primary btn-sm save" >Save</button>
        </div>
</div>