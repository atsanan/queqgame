@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Login Reward</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('malls') }}"> Login Reward</a></li>
        <li class="active">Edit</li>
    </ol>
</section>


@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h4 class="panel-title">Edit</h4>
            </div>
            <div class="panel-body">
                    {!! Form::model($loginReward, ['url' => URL::to('loginReward') . '/' . $loginReward->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="monsterActive" value="0" />
                    <input type="hidden" name="diamondActive" value="0" />
                    <input type="hidden" name="itemActive" value="0" />

                    <fieldset>
                    @include('loginReward.field')
                    </fieldset>
                
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>

@stop 