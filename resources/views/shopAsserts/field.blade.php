@component('components.multiLangField',[
    'header'=>'Shop Assert Names',
    'field'=>'shopAssertName',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Shop Assert Details',
    'field'=>'shopAssertDetail',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>
<div class="row">
    {{-- <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Asset Model (Multiple)',
        'field'=>'AssetModel[]',
        'value'=>null,
        'attribute'=>['class'=>'form-control', 'multiple'=>true],
        ])
        @endcomponent
        </div> --}}
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Version',
                'field'=>'shopAssertVersion',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Version']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-md-2 col-xs-offset-5">
        @if(!empty($fileNameLogos1))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                <img src="{{URL::to('media/images/'.$fileNameLogos1)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
            </div>
        @endif
    </div>
 
<div class="col-md-12">
    @component('components.inputField',[
        'type'=>'file',
        'icon'=>'text',
        'label'=>'Logo 1 (512*512)',
        'field'=>'fileNameLogo1',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>

<div class="col-md-2 col-xs-offset-4">
    @if(!empty($fileNameLogos2))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 450px; height: 140px;">
            <img src="{{URL::to('media/images/'.$fileNameLogos2)}}" class="img-responsive" alt="$field" style="width: 430px; height: 120px;">
        </div>
    @endif
</div>  
<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 2 (430*120)',
            'field'=>'fileNameLogo2',
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>
<div class="col-md-2 col-xs-offset-4">
    @if(!empty($fileNameLogos3))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 450px; height: 140px;">
            <img src="{{URL::to('media/images/'.$fileNameLogos3)}}" class="img-responsive" alt="$field" style="width: 430px; height: 120px;">
        </div>
    @endif
</div>  
<div class="col-md-12">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 3 (1000*800)',
            'field'=>'fileNameLogo3',
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>
    
    <div class="col-md-12">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Assert Model IOS (Multiple)',
        'field'=>'AssertModelIOS[]',
        'value'=>$assertModelIOS,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    
    @endcomponent
        
    </div>

    <div class="col-md-12">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Assert Model Android (Multiple)',
        'field'=>'AssertModelAndroid[]',
        'value'=>$assertModelAndroid,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    @endcomponent
        
    </div>
</div>
<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

