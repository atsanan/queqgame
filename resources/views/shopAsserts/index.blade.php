@extends('layouts/default') {{-- Page title --}} 
@section('title') Shop Assert Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Shop Asserts</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Shop Asserts</a></li>
        <li class="active">Shop Assert Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Shop Asserts</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('shopAsserts/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>

                <div class="panel-body table-responsive">
                    <table class="table " id="table" >
                        <thead>
                            <tr class="filters">
                                <th>Logo</th>
                           
                                <th>Shop Assert Name</th>
                                <th>Shop Assert Version</th>
                                {{-- <th>Shop Assert Model</th> --}}
                                <th>Actions</th>

                            </tr>
                        </thead>
                        <tbody>

                                @foreach($shopAsserts as $row)
                            <tr>
                                <td>
                                    @if(isset($row->fileNameLogo1)&&$row->fileNameLogo1!="")
                                    <img src="{{URL::to('media/images/'.$row->fileNameLogo1)}}" style="width: 180px; height:180px" />
                                    @endif
                                   
                                </td>
                                <td>{{ $row->shopAssertName['eng'] }}</td>
                                {{-- <td>{{ $row->shopAssertModel}}</td> --}}
                                <td>{{ $row->shopAssertVersion}}</td>
                           
                                <td>
                                    <a href="{{{ URL::to('shopAsserts/' . $row->id . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                </a>
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('shopAsserts.destroy', collect($row)->first()) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        <!-- {{route('shopAsserts.destroy', collect($row)->first() )}} -->
                                    </a>
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#table').dataTable({
            "pageLength": 50
        });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })

</script>
@yield('modalScripts')
@stop