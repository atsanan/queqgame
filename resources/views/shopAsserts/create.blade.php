@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Shop Asserts</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('shopAsserts') }}"> Shop Asserts</a></li>
        <li class="active">Add new</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Add New</h3>
            </div>
            <div class="panel-body">
                <!--main content-->
                <form id="shopAssertsForm" action="{{ route('shopAsserts.store') }}" method="POST" enctype="multipart/form-data"
                    class="form-horizontal">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <fieldset>
                    @include('shopAsserts.field')
                    </fieldset>

                </form>

            </div>
        </div>
    </div>
    <!--row end-->
</section>

@stop