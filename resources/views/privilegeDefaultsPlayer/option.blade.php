
@if(!empty($user))
  @foreach($user as $row)
    <option value="{{ $row->id }}" 
        @if(in_array( $row->id,$listId))
        selected
        @endif        
        >{{ $row->email }}</option>
  @endforeach
@endif