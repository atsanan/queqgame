@component('components.multiLangField',[
    'header'=>'Shop Category Names',
    'field'=>'shopCategorieName',
    'icon'=>'livicon'
    ])
@endcomponent

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

