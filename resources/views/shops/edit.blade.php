@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Shops</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('shops') }}"> Shops</a></li>
        <li class="active">Edit</li>
    </ol>
</section>


@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                {!! Form::model($shop, ['url' => URL::to('shops') . '/' . $shop->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />
                    <input type="hidden" name="isSponser" value="0" />

                    <fieldset>
                    @include('shops.field')
                    </fieldset>

                {!! Form::close() !!}
                
                @php
                                      
                if($field=="shops"){
                    if($sort=="desc"){
                        $sortShop="asc";
                    }else{
                        $sortShop="desc";
                    }
                }else {
                $sortShop="asc";
                }   
             
                if($field=="privilegeDefault"){
                    if($sort=="desc"){
                        $sortPrivilegeDefault="asc";
                    }else{
                        $sortPrivilegeDefault="desc";
                    }
                }else {
                    $sortPrivilegeDefault="asc";
                }   
             
                
                if($field=="privilegeShopTitles"){
                    if($sort=="desc"){
                        $sortPrivilegeShopTitles="asc";
                    }else{
                        $sortPrivilegeShopTitles="desc";
                    }
                }else {
                    $sortPrivilegeShopTitles="asc";
                }   
                
                if($field=="isReachDateTime"){
                    if($sort=="desc"){
                        $sortIsReachDateTime="asc";
                    }else{
                        $sortIsReachDateTime="desc";
                    }
                }else {
                    $sortIsReachDateTime="asc";
                }   

                
                if($field=="isGoldenMinutes"){
                    if($sort=="desc"){
                        $sortIsGoldenMinutes="asc";
                    }else{
                        $sortIsGoldenMinutes="desc";
                    }
                }else {
                    $sortIsGoldenMinutes="asc";
                }   

                if($field=="isExpired"){
                    if($sort=="desc"){
                        $sortIsExpired="asc";
                    }else{
                        $sortIsExpired="desc";
                    }
                }else {
                    $sortIsExpired="asc";
                } 

                if($field=="isActive"){
                    if($sort=="desc"){
                        $sortIsActive="asc";
                    }else{
                        $sortIsActive="desc";
                    }
                }else {
                    $sortIsActive="asc";
                }   
            @endphp
            @role("Privilege_Resource")
                <div class="col-lg-12">
                    <div class="panel panel-info filterable ">
                        <div class="panel-heading clearfix">
                            <h3 class="panel-title pull-left">Privilege Shop</h3>
                            <div class="pull-right">
                            <a href="{{ route('shops.excelPrivilegeShops',['id'=>$shop->id,'field'=>$field,'sort'=>$sort,'search'=>$search]) }}" class="btn btn-success btn-sm" >Export</a>
                            <a href="{{ URL::to("privilegeShops/create?shopId={$shop->id}") }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table" id="">
                            <thead>
                                <tr class="filters">
                                    <th>Privilege Default
                                        <a href="{{ URL::to("shops/{$shop->id}/edit?field=privilegeDefault&sort={$sortPrivilegeDefault}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a>         
                                    </th>
                                    <th>Privilege Shop Title
                                        <a href="{{ URL::to("shops/{$shop->id}/edit?field=privilegeShopTitles&sort={$sortPrivilegeShopTitles}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a>
                                    </th>
                                    <th>Logo</th>
                                    <th>Reach Date Time
                                        <a href="{{ URL::to("shops/{$shop->id}/edit?field=isReachDateTime&sort={$sortIsReachDateTime}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                    </th>
                                    <th>Continued
                                        <a href="{{ URL::to("shops/{$shop->id}/edit?field=isExpired&sort={$sortIsExpired}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>  
                                    </th>
                                    <th>Golden Minutes
                                        <a href="{{ URL::to("shops/{$shop->id}/edit?field=isGoldenMinutes&sort={$sortIsGoldenMinutes}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                                    </th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach($privilegeShops as $row)
                                    <tr>
                                        <td>{{ $row['privilege_defaults']['privilegeDefaultNames']['eng'] }}</td>
                                        <td>{{ $row['privilegeShopTitles']['eng']}}</td>
                                        
                                        <td>
                                            @if(isset($row['filenameLogo1'])||isset($row['filenameLogo2']))
                                            @component('components.modal',['id'=>uniqid(),'title'=>'Image Lists'])
                                            @slot('content')
                                            @if(isset($row['filenameLogo1']))
                                            <img src="{{URL::to('media/images/'.$row['filenameLogo1'])}}" style="width: 100px; height: 70px" />
                                            @endif
                                            @if(isset($row->filenameLogo2))
                                            <img src="{{URL::to('media/images/'.$row['filenameLogo2'])}}" style="width: 100px; height: 70px" />
                                            @endif
                                            @endslot
                                            @endcomponent
                                            @endif
                                        </td>
                                        <td> @if ($row['isReachDateTime'])
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                            @else
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                            @endif
                                        </td>
                                        <td>
                                            @if(isset($row['isExpired']))
                                                @if ($row['isExpired'])
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                                @else
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                @endif
                                            @else
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                            @endif
                                        </td>
                                        <td>@if ($row['isGoldenMinutes'])
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                            @else
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                            @endif
                                        </td>
                                        <td>@if ($row['isActive'])
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                            @else
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{{ URL::to('privilegeShops/' . $row['_id'] . '/edit' ) }}}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                            </a>
                                        </a>
                                            <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('privilegeShops.destroy', collect($row)->first()) }}">
                                                <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <?=$privilegeShops->appends(request()->query())->render()?>
                
                    </div>    
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endrole
                @php                                  
                if($fieldNews=="newsName"){
                    if($sortNews=="desc"){
                        $sortNewsName="asc";
                    }else{
                        $sortNewsName="desc";
                    }
                }else {
                    $sortNewsName="asc";
                }   
                       
                if($fieldNews=="newsType"){
                    if($sortNews=="desc"){
                        $sortNewsType="asc";
                    }else{
                        $sortNewsType="desc";
                    }
                }else {
                    $sortNewsType="asc";
                }   
           
                if($fieldNews=="isReachDateTime"){
                    if($sortNews=="desc"){
                        $sortNewsIsReachDateTime="asc";
                    }else{
                        $sortNewsIsReachDateTime="desc";
                    }
                }else {
                    $sortNewsIsReachDateTime="asc";
                }   

                if($fieldNews=="isExpired"){
                    if($sortNews=="desc"){
                        $sortNewsIsExpired="asc";
                    }else{
                        $sortNewsIsExpired="desc";
                    }
                }else {
                    $sortNewsIsExpired="asc";
                }   

                if($fieldNews=="isActive"){
                    if($sortNews=="desc"){
                        $sortNewsIsActive="asc";
                    }else{
                        $sortNewsIsActive="desc";
                    }
                }else {
                    $sortNewsIsActive="asc";
                }   
                @endphp

            @role("Promotion_Resource")
                <div class="col-lg-12">
                    <div class="panel panel-info filterable ">
                        <div class="panel-heading clearfix">
                            <h3 class="panel-title pull-left">Promotion</h3>
                            <div class="pull-right">
                                    <a href="{{ route('shops.excelPromotion',['id'=>$shop->id,'fieldNews'=>$fieldNews,'sortNews'=>$sortNews]) }}" class="btn btn-success btn-sm" >Export</a>
                            <a href="{{ URL::to("news/create?shopId={$shop->id}") }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                            </div>
                        </div>
        
                        <div class="panel-body table-responsive">
                            <table class="table" id="">
                                <thead>
                                    <tr class="filters">
                                        <th>Promotion Name
                                                <a href="{{ URL::to("shops/{$shop->id}/edit?fieldNews=newsName&sortNews={$sortNewsName}&search={$search}") }}" >
                                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                </a>      
                                        </th>
                                        {{-- <th>Longitude</th> --}}
                                        {{-- <th>Latitude</th> --}}
                                        <th>Promotion Type
                                                <a href="{{ URL::to("shops/{$shop->id}/edit?fieldNews=newsType&sortNews={$sortNewsType}&search={$search}") }}" >
                                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                </a>      
                                        </th>
                                        {{-- <th>Wild Item</th> --}}
                                        <th>Reach Date Time
                                                <a href="{{ URL::to("shops/{$shop->id}/edit?fieldNews=isReachDateTime&sortNews={$sortNewsIsReachDateTime}&search={$search}") }}" >
                                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                </a>      
                                        </th>
                                        <th>Continued
                                                <a href="{{ URL::to("shops/{$shop->id}/edit?fieldNews=isExpired&sortNews={$sortNewsIsExpired}&search={$search}") }}" >
                                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                </a>      
                                        </th>
                                        <th>Active
                                                <a href="{{ URL::to("shops/{$shop->id}/edit?fieldNews=isExpired&sortNews={$sortNewsIsActive}&search={$search}") }}" >
                                                    <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                                </a>   
                                        </th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
        
                                    @foreach($news as $row)
                                    <tr>
                                        <td>
                                            @isset($row['newsName']['eng'])
                                            {{ $row['newsName']['eng'] }}
                                            @endisset
                                        </td>
                                        {{-- <td>{{ $row->location['coordinates']['0'] }}</td> --}}
                                        {{-- <td>{{ $row->location['coordinates']['1'] }}</td> --}}
                                        <td>
                                            @isset($row['news_types']['newsTypeName']['eng'])
                                            {{ $row['news_types']['newsTypeName']['eng'] }}
                                            @endisset
                                        </td>
                                        <td> @if ($row['isReachDateTime'])
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                            @else
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                            @endif
                                        </td>
                                        <td>
                                                @if(isset($row['isExpired']))
                                                    @if ($row['isExpired'])
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                                    @else
                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                    @endif
                                                @else
                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                @endif
                                            </td>
                                        <td>
                                                @if ($row['newsActive'])
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                                @else
                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                                @endif
                                            </td>
                                        <td>
                                            <a href="{{{ URL::to('news/' . $row['_id'] . '/edit' ) }}}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                            </a>
                                        </a>
                                            <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('news.destroy', collect($row)->first()) }}">
                                            <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                            <!-- {{route('news.destroy', collect($row)->first() )}} -->
                                        </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- Modal for showing delete confirmation -->
                            <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="user_delete_confirm_title">
                                                Delete
                                            </h4>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure to delete this?
                                        </div>
                                        <div class="modal-footer">
                                        {!! Form::open(['method' => 'DELETE']) !!}
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <button type="submit" class="btn btn-danger">Delete</a>
                                        {!! Form::close() !!}   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endrole

            </div>
        </div>
    </div>
    <!--row end-->
</section>
@stop