@extends('layouts/default') {{-- Page title --}} 
@section('title') Shop Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
<link href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Shops</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Shops</a></li>
        <li class="active">Shop Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Shops</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('shops/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>

                @php
                
                if($field=="shopName.eng"){
                    if($sort=="desc"){
                        $sortShopName="asc";
                    }else{
                        $sortShopName="desc";
                    }
                }else {
                    $sortShopName="asc";
                }   
                
                if($field=="order"){
                    if($sort=="desc"){
                        $sortOrder="asc";
                    }else{
                        $sortOrder="desc";
                    }
                }else {
                    $sortOrder="asc";
                }   

                if($field=="isSponser"){
                    if($sort=="desc"){
                        $sortIsSponser="asc";
                    }else{
                        $sortIsSponser="desc";
                    }
                }else {
                    $sortIsSponser="asc";
                }   

                if($field=="isActive"){
                    if($sort=="desc"){
                        $sortIsActive="asc";
                    }else{
                        $sortIsActive="desc";
                    }
                }else {
                    $sortIsActive="asc";
                }   

                
                if($field=="malls"){
                    if($sort=="desc"){
                        $sortMalls="asc";
                    }else{
                        $sortMalls="desc";
                    }
                }else {
                    $sortMalls="asc";
                }   
                @endphp
                <div class="panel-body table-responsive">
                        <div class="col-md-12">
                                <div class="row">
                                <form id="wildMonstersForm" action="{{ route('shops.searchIndex') }}" method="POST" enctype="multipart/form-data">
                                    <!-- CSRF Token -->
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <fieldset>
                                            <div class="col-md-6">
                                                    @component('components.select2', [
                                                        'type'=>'select2',                          
                                                        'label'=>'Mall',
                                                        'field'=>'mallId[]',
                                                        'value'=>$_mallId,
                                                        'data'=>$malls,
                                                        'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                                    ])
                                                    @endcomponent
                                            </div>
                                            <div class="col-md-6">
                                                    @component('components.select2', [
                                                        'type'=>'select2',                          
                                                        'label'=>'MallFloor',
                                                        'field'=>'mallFloorId[]',
                                                        'value'=>$_mallFloorId,
                                                        'data'=>$mallFloors,
                                                        'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                                    ])
                                                    @endcomponent    
                                            </div>
            
                                            <div class="col-md-6">
                                            <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                    {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                    <div class="col-md-9">
                                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                    </span>
                                                        {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                            </div>
                                                        {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                            </div>
                                    </fieldset>           
                                </form>
                                {{-- </div> --}}
                            </div>
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>image1 </th> 
                                <th>Mall Name
                                    <a href="{{ URL::to("shops?field=malls&sort={$sortMalls}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
               
                                </th>
                                <th>Shop Name
                                    <a href="{{ URL::to("shops?field=shopName.eng&sort={$sortShopName}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
               
                                </th>
                                {{-- <th>Order
                                    <a href="{{ URL::to("shops?field=order&sort={$sortOrder}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
               
                                </th> --}}
                                <th>Sponser
                                    <a href="{{ URL::to("shops?field=isSponser&sort={$sortIsSponser}&search={$search}") }}" >
                                        <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>
                                </th>
                                <th>Active
                                    
                                        <a href="{{ URL::to("shops?field=isActive&sort={$sortIsActive}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                   
                                </th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($shops as $row)
                            <tr>
                                <td>
                                    @if(isset($row['filenameLogo1']))
                                    <img src="{{URL::to('media/images/'.$row['filenameLogo1'])}}" style="width: 120px; height: 120px" />
                                    @endif
                                </td>
                                <td>
                                    @if(isset($row['mall_floor']['malls']['mallName']['eng']))
                                    {{ $row['mall_floor']['malls']['mallName']['eng'] }}
                                    @endif    
                                </td>
                                <td>{{ $row['shopName']['eng'] }}</td>
                                 {{-- <td>{{ $row->order }}</td> --}}
                                <td>
                                    @if ($row['isSponser'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                  <td>
                                    @if ($row['isActive'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{{ URL::to('shops/' . $row['_id'] . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                 
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('shops.destroy', $row['_id']) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                    </a> 
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <?=$shops->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    
    $("select[name='mallId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getMallFloor/') }}",
          method: 'POST',
          data:{mallId: $(this).val()},
         success: function(data) {
            $("select[name='mallFloorId[]'").html('');
            $("select[name='mallFloorId[]'").html(data.options);
            
          }
      });     
    });
    
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })
</script>
@yield('modalScripts')

@stop