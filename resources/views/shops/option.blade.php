<option value="">--- Select Shops ---</option>
@if(!empty($shops))
  @foreach($shops as $row)
    <option value="{{ $row->id }}">{{ $row->shopName['eng'] }}</option>
  @endforeach
@endif