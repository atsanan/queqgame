@component('components.multiLangField',[
    'header'=>'Shop Names',
    'field'=>'shopName',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Shop Details',
    'field'=>'shopDetail',
    'icon'=>'livicon'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Address',
    'field'=>'address',
    'icon'=>'livicon'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Working Time',
    'field'=>'workingTime',
    'icon'=>'livicon'    
    ])
@endcomponent

<h4>Multi</h4>
<div class="row">
    
    
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Tel.',
            'field'=>'shopTel',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Tel']
        ])
        @endcomponent
    </div>
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Url',
            'field'=>'shopUrl',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3,'placeholder'=>'Url']
        ])
        @endcomponent
    </div>

    
    <div class="col-md-6">
            @component('components.inputField',[
            'type'=>'select',
            'label'=>'Main Category',
            'field'=>'mainTypes[]',
            'value'=>null,
            'data'=>$mainTypes,
            'attribute'=>['class'=>'form-control select2','multiple'=>true]
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Sub Category',
        'field'=>'shopCategoryId[]',
        'value'=>$shopCategory,
        'data'=>$shopCategories,
        'attribute'=>['class'=>'form-control select2','multiple'=>true]
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Mall Floor',
        'field'=>'mallFloorId',
        'value'=>$mallFloorId,
        'data'=>$mallFloors,
    'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Shop Assert',
        'field'=>'shopAssertId',
        'value'=> $shopAssert,
        'data'=>$shopAsserts,
        'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Shop Ads',
        'field'=>'shopAdsId',
        'value'=>$shopAds,
        'data'=>$shopAdses,
        'attribute'=>['class'=>'form-control select2','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    

    {{-- @component('components.inputField',[
        'type'=>'map'
    ])
    @endcomponent
    --}}
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Longitude',
                'field'=>'longitude',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Longitude','required'=>true]
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Latitude',
                'field'=>'latitude',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Latitude','required'=>true]
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Shop Code',
                'field'=>'shopCode',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Shop Code']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Map Reach',
                'field'=>'mapReach',
                'value'=>empty($shop->mapReach)?5:null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Map Reach']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Order',
                'field'=>'order',
                'value'=>$order,
                'attribute'=>['class'=>'form-control','placeholder'=>'Order']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Sponser',
                'field'=>'isSponser',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
        <div class="col-md-2 col-xs-offset-5">
            @if(!empty($fileNameLogos1))
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                    <img src="{{URL::to('media/images/'.$fileNameLogos1)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                </div>
            @endif
        </div>
     
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Logo 1 (120*120)',
            'field'=>'filenameLogo1',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-2 col-xs-offset-4">
        @if(!empty($fileNameLogos2))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 450px; height: 140px;">
                <img src="{{URL::to('media/images/'.$fileNameLogos2)}}" class="img-responsive" alt="$field" style="width: 430px; height: 120px;">
            </div>
        @endif
    </div>  
    <div class="col-md-12">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Logo 2 (430*120)',
                'field'=>'filenameLogo2',
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>
<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

