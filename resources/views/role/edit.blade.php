@extends('layouts/default') {{-- Page title --}} 
@section('title') Manage Role User  @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Manage Role : {{$role->name}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('roles') }}"> Role</a></li>
        <li class="active">Add new</li>
    </ol>
</section>
@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif


<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
            <h3 class="panel-title">Manage Role User </h3>
            </div>
            <div class="panel-body">
                {!! Form::model($role, ['url' => URL::to('roles') . '/' . $role->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                
                
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>check</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $data)
                        
                        <tr>
                        <td>{{$data->name}}</td>
                        <td>{{$data->email}}</td>
                        <td>   
                            <input type="checkbox" name="check[]" value="{{$data->id}}"
                            @if (array_search($data->id,$role->user_ids)>-1) 
                            checked="checked"
                            @endif        
                            >
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="form-group">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
                        </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>


@stop