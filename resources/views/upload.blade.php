<form id="costumeForm" action="{{ route('testUpload.store') }}" method="POST" enctype="multipart/form-data"
  class="form-horizontal">
  <!-- CSRF Token -->
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  {{Form::file('image')}}
  {{Form::submit('Upload File')}}
</form>
