@push('more_scripts')
@php
    $max = isset($max) ? $max : 0;
    $redirect_url = isset($redirect_url) ? 
        "No Results Found <a href='$redirect_url' class='btn btn-success'>Create</a>"
        :
        "No Results Found";
@endphp
<script>
    window.select2 = {};
    window.select2["{!! $attribute['id'] !!}"] = function (e) {
    
        
        $(".{!! $attribute['id'] !!}").select2({
            theme: "bootstrap",
            maximumSelectionLength: {!! $max !!},
            placeholder: "Please Select",
            "language": {
                "noResults": function(){
                    return {!! json_encode($redirect_url) !!};
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
        })

        $(".{!! $attribute['id'] !!}").on("select2:select", function (evt) {
        let element = evt.params.data.element;
        let $element = $(element);
    
        
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");

        
      
        });

//         $(".{!! $attribute['id'] !!}").on("select2:unselect", function (e) { 
// 	//log("select2:unselect", e);
//     console.log(e) 
//  e.params.data.element.remove();
// });

     
    }
    window.select2["{!! $attribute['id'] !!}"]();
</script>
@endpush

@php
    $attribute["class"] = $attribute["class"]." custom-select2 ".$attribute["id"];
    unset($attribute["id"]);
    if (isset($attribute["multiple"]) && $attribute["multiple"]) {
        unset($data[""]);
    }
@endphp

<div class="form-group row {{ $errors->first($field, 'has-error') }}">
	{!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
	<div class="col-md-9">
		{!! Form::select($field, $data, $value, $attribute) !!}
		{!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
	</div>
</div>

