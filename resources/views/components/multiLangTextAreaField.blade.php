<h4>{{$header}}</h4>
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav  nav-tabs ">
                <li class="active">
                    <a href={{'#'.$field.'1'}} data-toggle="tab">English</a>
                </li>
                <li>
                    <a href={{'#'.$field.'2'}} data-toggle="tab">Thai</a>
                </li>
                <li>
                    <a href={{'#'.$field.'3'}} data-toggle="tab">Chinese 1</a>
                </li>
                <li>
                    <a href={{'#'.$field.'4'}} data-toggle="tab">Chinese 2</a>
                </li>
            </ul>
            <div class="tab-content mar-top">
                <div id={{$field.'1'}} class="tab-pane fade active in">
                    <div class="row">
                        <div class="form-group {{ $errors->first($field.'.eng', 'has-error') }}">
                            <div class="col-md-12">
                                {!! Form::textArea($field.'[eng]', null, array('class' => 'form-control','rows'=>5, 'placeholder'=>'English')) !!}
                                {!! $errors->first($field.'.eng', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div id={{$field.'2'}} class="tab-pane fade">
                    <div class="row">
                        <div class="form-group {{ $errors->first($field.'.thai', 'has-error') }}">
                            <div class="col-md-12">
                                
                                    {!! Form::textArea($field.'[thai]', null, array('class' => 'form-control','rows'=>5, 'placeholder'=>'Thai')) !!}
                                    {!! $errors->first($field.'.thai', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div id={{$field.'3'}} class="tab-pane fade">
                    <div class="row">
                        <div class="form-group {{ $errors->first($field.'.chi1', 'has-error') }}">
                            <div class="col-md-12">  
                                {!! Form::textArea($field.'[chi1]', null, array('class' => 'form-control', 'rows'=>5,'placeholder'=>'Chinese 1')) !!}
                                {!! $errors->first($field.'.chi1', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>  
                    </div>
                </div>
                <div id={{$field.'4'}} class="tab-pane fade">
                    <div class="row">
                        <div class="form-group {{ $errors->first($field.'.chi2', 'has-error') }}">
                            <div class="col-md-12">
                                    {!! Form::textArea($field.'[chi2]', null, array('class' => 'form-control', 'rows'=>5,'placeholder'=>'Chinese 2')) !!}
                                    {!! $errors->first($field.'.chi2', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>