@section('header_styles')
<link href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

{{-- Switchery --}}
<link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css')}}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css')}}">
{{-- Datepicker --}}
<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />

{{-- Google Map --}}
<link rel="stylesheet" href="{{ asset('assets/css/google_map.css')}}">

<link href="{{ asset('assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

@endsection

@if ($type === 'map')
    <div class="col-md-8 col-md-offset-2">
        <div class="form-group row">
            {{Form::text(null,null,['class'=>'controls','id'=>'searchInput','placeholder'=>'Enter a location'])}}
            <div id="map"></div>
        </div>
    </div>
@endif

@if ($type === 'text' || $type ==='number')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="livicon" data-name={{$icon}} data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                        </span>
            {!! Form::$type($field, $value, $attribute) !!}
                </div>
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
        </div>
    </div> 
@endif


@if ($type === 'selectSearch')
{!! Form::select($field, $data ,$value, $attribute) !!}
    
        
@endif

@if ($type === 'title')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-2 control-label']) !!}
        <div class="col-md-10">
                     
            {!! Form::text($field, $value, $attribute) !!}
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
        </div>
    </div> 
@endif

@if ($type === 'textArea')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-2 control-label']) !!}
        <div class="col-md-10">
                     
            {!! Form::$type($field, $value, $attribute) !!}
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
        </div>
    </div> 
@endif

@if ($type === 'file')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="livicon" data-name={{$icon}} data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                        </span>
            {!! Form::$type($field, $attribute) !!}
      
        </div>
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
            @if ($field === 'AssertModelIOS[]'||$field === 'AssertModelAndroid[]')
                  {{$value}}
            @endif
        </div>
     
    </div> 
@endif
@if ($type === 'fileAsserts')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="livicon" data-name={{$icon}} data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                        </span>
            {!! Form::file($field, $attribute) !!}
      
        </div>
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
                  {{$value}}
        </div>
     
    </div> 
@endif

@if ($type === 'select')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::$type($field, $data ,$value, $attribute) !!}
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
        </div>
    </div> 
@endif


@if ($type === 'datetime')
    <div class="form-group row {{ $errors->first($field, 'has-error') }}">
        {!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="livicon" data-name='calendar' data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                        </span>
            {!! Form::text($field,isset($value)?$value:null,isset($attribute)?$attribute:['class'=>'form-control customDate']) !!}
                </div>
            {!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
        </div>
    </div> 
@endif

@if ($type === 'checkbox')
    <div class="form-group row">
        {!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::$type($field, true , $value , ['class'=>'js-switch1']) !!} 
        </div>
    </div>
@endif


@if ($type === 'select2')
@push('more_scripts')
@php
    $max = isset($max) ? $max : 0;
    $redirect_url = isset($redirect_url) ? 
        "No Results Found <a href='$redirect_url' class='btn btn-success'>Create</a>"
        :
        "No Results Found";
@endphp
<script>
    window.select2 = {};
    window.select2["{!! $attribute['id'] !!}"] = function (e) {
    
        
        $(".{!! $attribute['id'] !!}").select2({
            theme: "bootstrap",
            maximumSelectionLength: {!! $max !!},
            placeholder: "Please Select",
            "language": {
                "noResults": function(){
                    return {!! json_encode($redirect_url) !!};
                }
            },
            escapeMarkup: function (markup) {
                return markup;
            },
        })

        $(".{!! $attribute['id'] !!}").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
        });
    }
    window.select2["{!! $attribute['id'] !!}"]();
</script>
@endpush


@php
    $attribute["class"] = $attribute["class"]." custom-select2 ".$attribute["id"];
    unset($attribute["id"]);
    if (isset($attribute["multiple"]) && $attribute["multiple"]) {
        unset($data[""]);
    }
@endphp

<div class="form-group row {{ $errors->first($field, 'has-error') }}">
	{!! Form::label($field, $label, ['class'=>'col-md-3 control-label']) !!}
	<div class="col-md-9">
		{!! Form::select($field, $data, $value, $attribute) !!}
		{!! $errors->first($field, '<span class="help-block">:message</span> ') !!}
	</div>
</div>

@endIf


@section('footer_scripts')

    {{-- Multiple Select --}}
    <script src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/custom_elements.js') }}"></script>
    {{-- Switchery --}}
    <script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    {{-- Google Map --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCINZxbfnYKAhXzO2xpr_qfSOCd3zyYZkM&libraries=places&callback=initMap" async defer></script>
    <script type="text/javascript" src="{{ asset('assets/js/google_map.js')}}"></script>
    {{-- Datepicker --}}
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>

    {{-- Others --}}
    <script>
            let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch1'));
            elems.forEach(html=>{
            let switchery = new Switchery(html,{ size: 'small',color: 'rgb(65, 139, 202)' });
            });

            let dateElems = Array.prototype.slice.call(document.querySelectorAll('.customDate'));
            dateElems.forEach(html=>{
            let customDate = new daterangepicker(html,{ 

                singleDatePicker: true,
                showDropdowns: true, 
                locale: {
                    format: 'YYYY-MM-DD'
                    }
                });
            })

            let expireDate = Array.prototype.slice.call(document.querySelectorAll('.expireDate'));
            expireDate.forEach(html=>{
            let customDateExpire = new daterangepicker(html,{ 
                autoUpdateInput: false,
                singleDatePicker: true,
                showDropdowns: true, 
                locale: {
                    format: 'YYYY-MM-DD'
                    }
                });
            })

            
            let dateTime = Array.prototype.slice.call(document.querySelectorAll('.dateTime'));
            dateTime.forEach(html=>{
            let customDateTime = new daterangepicker(html,{ 
                singleDatePicker: true,
                showDropdowns: true, 
                timePicker :true,
                locale: {
                    format: 'YYYY-MM-DD  hh:mm A'
                    }
                });
            })
            $('.expireDate').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD'));
            });

    </script>

    <script>
        $(document).ready(function() {
            $('#table').dataTable({
            "pageLength": 50
        });
        });

        $('#delete_confirm').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget)
            let $recipient = button.data('id');
            console.log("recipient: ",$recipient);

            let modal = $(this);
            modal.find('.modal-footer form').prop("action",$recipient);
        });

        $(".select2").select2({
            theme: "bootstrap",
           // maximumSelectionLength: 4,
            placeholder: "Please Select",
        });        
    </script>

    
<script>

        
    $("select[name='toMall']").change(function(){

      $.ajax({
          url: "{{ URL::to('mallFloors/show') }}",
          method: 'GET',
          data: "mallId="+$(this).val(),
          success: function(data) {
            $("select[name='toMallFloor'").html('');
            $("select[name='toMallFloor'").html(data.options);
            
          }
      });
    });

    $("select[name='mallId']").change(function(){

        $.ajax({
            url: "{{ URL::to('mallFloors/show') }}",
            method: 'GET',
            data: "mallId="+$(this).val(),
            success: function(data) {
            $("select[name='mallFloorId'").html('');
            $("select[name='mallFloorId'").html(data.options);
            
            }
        });
        });

     $("select[name='mallFloorId']").change(function(){
        $.ajax({
            url: "{{ URL::to('shops/show') }}",
            method: 'GET',
            data: "mallFloorId="+$(this).val(),
            success: function(data) {
            $("select[name='toShop'").html('');
            $("select[name='toShop'").html(data.options);
            $("select[name='shopId'").html('');
            $("select[name='shopId'").html(data.options);
            }
        });
        });

        
     $(".toMallFloor").change(function(){
        $.ajax({
            url: "{{ URL::to('shops/show') }}",
            method: 'GET',
            data: "mallFloorId="+$(this).val(),
            success: function(data) {
            $("select[name='toShop'").html('');
            $("select[name='toShop'").html(data.options);
            $("select[name='shopId'").html('');
            $("select[name='shopId'").html(data.options);
            }
        });
        });
        
        $("select[name='zoneTypeId']").change(function(){
            
          var optionSelected = $(this).find("option:selected").text();
          
          
            if(optionSelected=="World"){
                //$('#toMallWildItem').attr('disabled',true);
                $('.toMall').attr('disabled',true);
                $('.toMallFloor').attr('disabled',true);
                $('.toShop').attr('disabled',true);
            }else if(optionSelected=="InMall"){
                $('.toMall').attr('disabled',false);
                //$('#toMallWildItem').attr('disabled',false);
                $('.toMallFloor').attr('disabled',true);
                $('.toShop').attr('disabled',true);
            }else if(optionSelected=="InMall Floor") {
                $('.toMall').attr('disabled',false);
               // $('#toMallWildItem').attr('disabled',false);
                $('.toMallFloor').attr('disabled',false);
                $('.toShop').attr('disabled',true);
            }else if(optionSelected =="InMall Shop") {
                $('.toMall').attr('disabled',false);
                //$('#toMallWildItem').attr('disabled',false);
                $('.toMallFloor').attr('disabled',false);
                $('.toShop').attr('disabled',false);
            }
                
        });
        
    
    $(".send").click(function(){
        let url= "https://gaming.inmall.asia/2019/api/v1/news/sendNews/"+$("#news").val()+"?";
        if ($("#pushTokenUser").val()!=""){
            url+="&pushTokenUser="+$("#pushTokenUser").val()
        }
        if ($("#pushUrl").val()!=""){
            url+="&pushUrl="+$("#pushUrl").val()
        }
    
        $.ajax({
            url:url,
            method: 'get',
            data: "",
            success: function(data) {
                
                window.location ="{{ url("/news/push/") }}"+"/"+$("#news").val()
            },
            error: function (error) {
                console.log("error",error);
                
                alert("cannot send news")
                
            }
        });
        
        
    })

   </script>
    
@endsection
