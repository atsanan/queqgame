{{-- Button trigger modal --}}
<button type="button" class="btn btn-primary" data-toggle="modal" data-target={{'#'.$id}}>
    <i class="livicon" data-name="more" data-size="18" data-c="#FFF" data-hc="#FFF" data-loop="true"></i>
</button>

{{-- Modal --}}
<div class="modal fade" id={{$id}} tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{$title}}</h4>
            </div>
            <div class="modal-body">
                {{$content}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('modalScripts')

<script>
    $('#'+$id).on('shown.bs.modal', function () {})
</script>

@stop
