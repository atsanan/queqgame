@extends('layouts/default') {{-- Page title --}} 
@section('title') Activity @parent 
@stop
@section('content')
@if(Auth::user())
<section class="content-header">
        <h1>Activities</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('home') }}">
                        <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                        Dashboard
                    </a>
            </li>
            <li><a href="{{ URL::to('activities') }}"> Activities</a></li>
            <li class="active">Activities</li>
        </ol>
</section>
@endif    
<section class="content paddingleft_right15">
        <!--main content-->
        <div class="panel panel-info filterable">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8 col-md-9 col-full-width-right">
                      <!-- /.blog-detail-image -->
                    <div class="the-box no-border blog-detail-content">
                         <p class="text-justify">
                               <h4> {!!$activity->activityTitle[$lang]!!}</h4>
                        </p>
                        <div class="blog-detail-image">
                                @isset($activity->image)
                                <img src="{{URL::to('media/images/'.$activity->image)}}"  class="img-responsive" alt="Image">   
                                @endisset
                                 
                        </div>
                             
                        <p class="text-justify">
                                {!!$activity->activityDetail[$lang]!!}
                        </p>                       
                        <hr>
                </div>
                    <!-- /the.box .no-border -->
                </div>
                

                    <!-- /.the-box .no-border -->
                </div>
                </div>
            
        </div>
        
        <!--main content ends-->
    </section>
@stop