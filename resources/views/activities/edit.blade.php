@extends('layouts/default') {{-- Page title --}} 
@section('title') Add User @parent 
@stop 

@section('content')
<section class="content-header">
    <h1>Activities</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('activities') }}"> Activities</a></li>
        <li class="active">Add new</li>
    </ol>
</section>

@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif



@if ($message = Session::get('danger'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-danger filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif



<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
                {!! Form::model($activity, ['url' => URL::to('activities') . '/' . $activity->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="isActive" value="0" />
                   
                    <fieldset>
                    @include('activities.field')
                    </fieldset>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>


@stop 