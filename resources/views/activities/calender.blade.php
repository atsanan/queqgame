@extends('layouts/default') {{-- Page title --}} 
@section('title') Activities @parent 
@stop {{-- page level styles --}} 

@section('header_styles') 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection

@section('content')
@if(Auth::user())
<section class="content-header">
        <h1>Activities</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('home') }}">
                        <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                        Dashboard
                    </a>
            </li>
            <li><a href="{{ URL::to('activities') }}"> Activities</a></li>
            <li class="active">Calendar</li>
        </ol>
</section>
@endif    
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-info filterable ">
               
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Calendar</h3>
                    @if(Auth::user())
                    <div class="pull-right">
                        <a href="{{ route('activities.create') }}" class="btn btn-primary btn-sm" >Add new</a>
                        <a href="{{ route('activities.index') }}" class="btn btn-success btn-sm" >Index</a>
                    </div>
                    @endif
                </div>
                <div class="panel-body">
                        {!! $calendar->calendar() !!}    
                </div>
            </div>
        </div>
    </section>

    <style>
    .fc-center > h2{
        font-size: 16px;
        padding-top: 16px;
    }
    </style>
                   
@stop       
@push('more_scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
{!! $calendar->script() !!}
@endpush