@extends('layouts/default') {{-- Page title --}} 
@section('title') Activities Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
<link href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
        <h1>Activities</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('home') }}">
                        <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                        Dashboard
                    </a>
            </li>
            <li><a href="{{ URL::to('activities') }}"> Activities</a></li>
            <li class="active">Activities Lists</li>
        </ol>
</section>

@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="col-lg-12">
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
    </div>
</section>
@endif



@if ($message = Session::get('danger'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="col-lg-12">
        <div class="panel panel-danger filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
        </div>
    </div>
</section>
@endif


    
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Activities</h3>
                    <div class="pull-right">
                    <a href="{{ route('activities.create') }}" class="btn btn-primary btn-sm" >Add new</a>
                    <a href="{{ route('activities.calender') }}" class="btn btn-success btn-sm" >Calender</a>
                    </div>
                </div>

                @php

                if($field=="title"){
                    if($sort=="desc"){
                        $sortTitle="asc";
                    }else{
                        $sortTitle="desc";
                    }
                }else {
                    $sortTitle="asc";
                }   

                if($field=="isActive"){
                    if($sort=="desc"){
                        $sortIsActive="asc";
                    }else{
                        $sortIsActive="desc";
                    }
                }else {
                    $sortIsActive="asc";
                }   

                
                @endphp
                <div class="panel-body table-responsive">
                        <div class="col-md-12">
                                <div class="row">
                                <form id="wildMonstersForm" action="{{ route('shops.searchIndex') }}" method="POST" enctype="multipart/form-data">
                                    <!-- CSRF Token -->
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <fieldset>
                                            <div class="col-md-6">
                                            <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                    {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                    <div class="col-md-9">
                                                            <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                    </span>
                                                        {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                            </div>
                                                        {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
                                            </div>
                                    </fieldset>           
                                </form>
                                {{-- </div> --}}
                            </div>
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>image </th>
                                <th>title 
                                        <a href="{{ URL::to("activities?field=title&sort={$sortTitle}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                            </a>    
                                </th>  
                                <th>type </th>
                                <th>Active
                                    
                                        <a href="{{ URL::to("activities?field=isActive&sort={$sortIsActive}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>
                   
                                </th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($activities as $row)
                            <tr>
                                <td>
                                        @isset($row['image'])
                                        <img src="{{URL::to('media/images/'.$row['image'])}}" style="width: 100px; height: 70px" />      
                                        @endisset
                                </td>
                                <td>
                                        @isset($row['activityTitle']['eng'])
                                       {{$row['activityTitle']['eng']}}      
                                        @endisset
                                </td>
                                <td>
                                    @isset($row['activityTypes'])
                                   {{(implode(",",$row['activityTypes']))}}      
                                    @endisset
                                </td>

                                <td>
                                    @if ($row['isActive'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>

                                <td>
                                        <a href="{{{ URL::to("activities/{$row['_id']}/view?lang=eng") }}}">
                                            <i class="livicon" data-name="search" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c" title="view"></i>
                                        </a>
                                    <a href="{{{ URL::to('activities/' . $row['_id'] . '/edit' ) }}}">
                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                    </a>
                                 
                                    <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('activities.destroy', $row['_id']) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                    </a> 
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <?=$activities->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    
    $("select[name='mallId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getMallFloor/') }}",
          method: 'POST',
          data:{mallId: $(this).val()},
         success: function(data) {
            $("select[name='mallFloorId[]'").html('');
            $("select[name='mallFloorId[]'").html(data.options);
            
          }
      });     
    });
    
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    })
</script>
@yield('modalScripts')

@stop