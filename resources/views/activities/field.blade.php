
@component('components.multiLangField',[
    'header'=>'Activity Title',
    'field'=>'activityTitle',
    'icon'=>'livicon'
    ])
@endcomponent
@component('components.multiLangTextAreaField',[
    'header'=>'Activity Detail',
    'field'=>'activityDetail',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'datetime',
            'label'=>'Start Date Time',
            'field'=>'startDateTime',
            'attribute'=>['class'=>'form-control dateTime']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'datetime',
                'label'=>'Expired Date Time',
                'field'=>'expiredDateTime',
                'attribute'=>['class'=>'form-control dateTime']
            ])
            @endcomponent
        </div>
        
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Color',
            'field'=>'color',
            'value'=>isset($activity->color)?null:"#f2ba29",
            'attribute'=>['class'=>'form-control colorpicker','placeholder'=>'Color']
        ])
        @endcomponent
</div> 
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Activity Types',
        'field'=>'activityTypes[]',
        'value'=>null,
        'data'=>$activityTypes,
        'attribute'=>['class'=>'form-control select2','multiple'=>true]
        ])
        @endcomponent
    </div>

</div>


<div class="row">
    <div class="col-md-2 col-xs-offset-4">
            @if(!empty($activity->image))
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 250px; height: 250px;">
                    <img src="{{URL::to('media/images/'.$activity->image)}}" class="img-responsive" alt="$field" style="width: 250px; height: 250px;">
                </div>
            @endif
    </div>  
    <div class="col-md-12">
                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Image (1000*1000)',
                    'field'=>'filenameImage',
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
    </div>
</div>
    

<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'icon'=>'text',
        'label'=>'Active',
        'field'=>'isActive',
        'value'=>isset($activity->isActive)?$activity->isActive:true, 
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>

<div class="form-group">
        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
</div>

@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/ckeditor/js/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/ckeditor/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
    <script>    
        $('textarea').ckeditor({
            height: '200px'
        });
        $('.colorpicker').colorpicker();
    </script>
    
@endpush