
<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Item Category Name',
                'field'=>'itemCategoryName',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Item Category Name']
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Order',
                'field'=>'itemCategoryOrder',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Order']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

