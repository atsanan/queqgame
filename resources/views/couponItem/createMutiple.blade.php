@extends('layouts/default') {{-- Page title --}} 
@section('title') Add Coupon Item @parent 
@stop 

@section('content')
<section class="content-header">
    <h1>Coupon Item</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('couponItem') }}"> Coupon Item</a></li>
        <li class="active">Add Mutiple Code</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">Add Mutiple Code</h3>
            </div>
            <div class="panel-body">
                <form id="costumeForm" action="{{ route('couponItem.storeMultipleCode') }}" method="POST" enctype="multipart/form-data"
                    class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    <fieldset>
                        <div class="row">
                                <div class="col-md-6">
                                    @component('components.inputField',[
                                        'type'=>'select',
                                        'label'=>'Item',
                                        'field'=>'itemId',
                                        'value'=>$itemId,
                                        'data'=>$data,
                                        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
                                        
                                    ])
                                    @endcomponent 
                                </div>
                                <div class="col-md-6">
                                        @component('components.inputField',[
                                            'type'=>'number',
                                            'icon'=>'text',
                                            'label'=>'Amount',
                                            'field'=>'amount',	
                                            'value'=>0,
                                            'attribute'=>['class'=>'form-control','placeholder'=>'Amount']
                                        ])
                                        @endcomponent
                                </div>
                        </div>    

                        <div class="form-group">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
                                </div>
                        </div>
                    </fieldset>

                </form>

            </div>
        </div>
    </div>
</section>


@stop 