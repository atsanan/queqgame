
<div class="row">

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Item',
            'field'=>'itemId',
            'value'=>$itemId,
            'data'=>$data,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            
        ])
        @endcomponent 
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Code',
            'field'=>'code',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Code']           
        ])
        @endcomponent 
    </div>

    
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Comment',
            'field'=>'comment',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>5,'placeholder'=>'Comment']           
        ])
        @endcomponent 
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>
