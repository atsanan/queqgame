@extends('layouts/default') {{-- Page title --}} 
@section('title') Item Player Coupon Code @parent 
@stop {{-- page level styles --}} 


@section('content')
@php
$userTimezone = new DateTimeZone( "Asia/Bangkok");
$gmtTimezone = new DateTimeZone('GMT');
@endphp
<section class="content-header">
    <h1>Item Player Coupon Code</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Item Player Report</a></li>
        <li class="active">Item Player Coupon Lists</li>
    </ol>
</section>

<!-- Left side column. contains the logo and sidebar -->
      
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  
                    <div class="col-lg-12">
                        <!-- toggling series charts strats here-->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="livicon" data-name="linechart" data-size="16" data-loop="true"
                                        data-c="#fff" data-hc="#fff"></i> Bar Chart
                                </h3>
                                <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                            </div>
                            <div class="panel-body">
                                <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                            <div class="panel panel-info filterable ">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title pull-left">Item Player Coupon </h3>
                                    <div class="pull-right">
                                            <a href="{{ route('itemCoupon.excel') }}" class="btn btn-success btn-sm" id="addButton">Export</a>
                                    </div>
                                </div>
                                <form id="wildMonstersForm" action="/itemPlayer/search" method="POST" enctype="multipart/form-data">
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <fieldset>
                           
                                </fieldset>           
                                </form>
            
                                <div class="panel-body table-responsive">
                                    <table class="table" id="table">
                                        <thead>
                                            <tr class="filters"> 
                                                <th>Item Name </th>
                                                <th>Action</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                        <tbody>
                                                @foreach($data as $row)
                                                <tr>
                                                    <td> {{$row['itemName']}}</td>
                                                    <td>
                                                            <a href="{{{ URL::to('itemDefaults/' . $row['id'] . '/edit' ) }}}">
                                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                            </a>
                                                        </a>
                                                            <a href="#" data-toggle="modal" data-target="#delete_confirm" data-id="{{ route('itemDefaults.destroy', $row['id']) }}">
                                                            <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                                        </a>
                                                        </td>
                                                </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                    <?=$data->appends(request()->query())->render()?>
                                    
                                    <!-- Modal for showing delete confirmation -->
                                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                                        Delete
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure to delete this? 
                                                </div>
                                                <div class="modal-footer">
                                                {!! Form::open(['method' => 'DELETE']) !!}
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <button type="submit" class="btn btn-danger">Delete</a>
                                                {!! Form::close() !!}                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
@stop       
@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.categories.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.stack.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
    <script language="javascript" type="text/javascript" >

    
    let use=[];
    let notUse=[];

    @foreach($stat as $row)
    use.push({label:"{{$row['itemName']}}",y:{{$row['use']}}})

    notUse.push({label:"{{$row['itemName']}}",y:{{$row['notUse']}}})
    @endforeach
    window.onload = function () {

    var chart = new CanvasJS.Chart("chartContainer", {
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "Item Player Coupon Code"
	},
	// subtitles: [{
	// 	text: "Click Legend to Hide or Unhide Data Series"
	// }], 
	axisX: {
		title: "Use"
	},
	axisY: {
		title: "Not",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisY2: {
		title: "Not Use",
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E"
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Use",
		showInLegend: true,      
		yValueFormatString: "#,##0.# Units",
		dataPoints:use
	},
	{
		type: "column",
		name: "Not Use",
		axisYType: "secondary",
		showInLegend: true,
		yValueFormatString: "#,##0.# Units",
		dataPoints: notUse
	}]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

}
    </script>

<script src="{{ asset('assets/js/canvasjs.min.js') }}"></script>

    <!-- end of page level js -->
@endpush