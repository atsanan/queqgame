<div class="row">
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'title',
            'icon'=>'text',
            'label'=>'Title',
            'field'=>'title',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'textArea',
            'icon'=>'text',
            'label'=>'Detail',
            'field'=>'detail',
            'value'=>null,
            'attribute'=>['class'=>'form-control','rows'=>3]
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Monster',
        'field'=>'mDefaultId',
        'value'=>null,
        'data'=>$monstersDefaults,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'text',
        'label'=>'version',
        'icon'=>'text',
        'field'=>'giveVersion',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Version']
        ])
        @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>
    </div>
</div>

