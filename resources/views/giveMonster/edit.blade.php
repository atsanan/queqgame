@extends('layouts/default') {{-- Page title --}} 
@section('title') Give Monster @parent 
@stop 
@section('content')
<section class="content-header">
    <h1>Give Monster</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
        </li>
        <li><a href="{{ URL::to('ipAccess') }}"> Give Monster</a></li>
        <li class="active">Edit</li>
    </ol>
</section>


@if ($message = Session::get('success'))
<section class="content paddingleft_right15">
    <div class="row">   
        <div class="panel panel-success filterable">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$message}}</h3>
                </div>
        </div>
    </div>
</section>
@endif

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-info filterable">
            <div class="panel-heading">
                <h4 class="panel-title">Edit</h4>
            </div>
            <div class="panel-body">
                    {!! Form::model($giveMonster, ['url' => URL::to('giveMonster') . '/' . $giveMonster->id, 'method' => 'put', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <input type="hidden" name="isActive" value="0" />

                    <fieldset>
                    @include('giveMonster.field')
                    </fieldset>
                
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>

@stop 