@component('components.multiLangField',[
    'header'=>'Title',
    'field'=>'title',
    'icon'=>'livicon'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Body',
    'field'=>'body',
    'icon'=>'livicon'
    ])
@endcomponent
<h4>Multi</h4>


<div class="row">

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Digital Board Asset',
            'field'=>'digitalBoardAssertId',
            'value'=>null,
            'data'=>$digitalBoardAsserts,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            
        ])
        @endcomponent 
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'ZoneType',
            'field'=>'zoneType',
            'value'=>null,
            'data'=>$zoneType,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            
        ])
        @endcomponent 
    </div>
    @php
    $disabledMall=true;
    $disabledShop=true;
    if(isset($digitalBoard)){
        if($digitalBoard->zoneType=="Mall"){
            $disabledMall=false;
        }else if($digitalBoard->zoneType=="Shop"){
            $disabledMall=false;
            $disabledShop=false;    
        }
    }
    @endphp
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Mall',
            'field'=>'mallId',
            'value'=>null,
            'data'=>$malls,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select',
            'disabled'=>$disabledMall]
            
        ])
        @endcomponent 
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'MallFloor',
            'field'=>'mallFloorId',
            'value'=>null,
            'data'=>$mallFloors,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select','disabled'=>$disabledMall]
            
        ])
        @endcomponent 
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Shops',
            'field'=>'shopId',
            'value'=>null,
            'data'=>$shops,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select','disabled'=>$disabledShop]
            
        ])
        @endcomponent 
    </div>
    {{-- 
    @component('components.inputField',[
            'type'=>'map'
    ])
    @endcomponent --}}
        
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Longtitude',
                'field'=>'location[coordinates][0]',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Longtitude']
            ])
            @endcomponent
    </div>

    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Latitude',
                'field'=>'location[coordinates][1]',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Latitude']
            ])
            @endcomponent
    </div>
</div>

<div class="row">

    <div class="col-md-4">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'X',
        'field'=>'x',
        'value'=>$x,
        'attribute'=>['class'=>'form-control','placeholder'=>'X']
    ])
    @endcomponent
</div>

<div class="col-md-4">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Y',
        'field'=>'y',
        'value'=>$y,
        'attribute'=>['class'=>'form-control','placeholder'=>'Y']
    ])
    @endcomponent
</div>

<div class="col-md-4">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Z',
        'field'=>'z',
        'value'=>$z,
        'attribute'=>['class'=>'form-control','placeholder'=>'Z']
    ])
    @endcomponent
</div>

</div>

<div class="row">
        <div class="col-md-2 col-xs-offset-5">
                @if(!empty($digitalBoard->imageSlot1))
                    <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img src="{{URL::to('media/images/'.$digitalBoard->imageSlot1)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                    </div>
                @endif
        </div>
       
        <div class="col-md-12"> 
                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Image Slot 1 (256*256)',
                    'field'=>'fileNameImageSlot1',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-2 col-xs-offset-5">
                @if(!empty($digitalBoard->imageSlot2))
                    <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img src="{{URL::to('media/images/'.$digitalBoard->imageSlot2)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                    </div>
                @endif
        </div>
       
        <div class="col-md-12"> 
                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Image Slot 2 (1000*1000)',
                    'field'=>'fileNameImageSlot2',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
        <div class="col-md-12"> 
            @component('components.inputField',[
                'type'=>'select',
                'icon'=>'text',
                'label'=>'Click Type',
                'field'=>'clickType',
                'value'=>null,
                'data'=>$clickType,
                'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            ])
            @endcomponent
    </div>
    @if(!empty($digitalBoard))    
    <div class="col-md-6 col-xs-offset-2">
        <div class="form-group row">
                <label for="inputUsername" class="col-md-3 control-label">Image Thumbnail Lists</label>
                @component('components.modal',['id'=>uniqid(),'title'=>'Image Thumbnail'])
                    @slot('content')
                        @foreach($digitalBoard->imageThumbnailList as $photo)
                        <div class="img-wrap">
                        <span class="close" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ URL::to('digitalBoard/deleteThumbnail/'.$digitalBoard->id.'/'.$photo.'/1') }}">&times;</span>
                        <img src="{{URL::to('media/images/'.$photo)}}" style="width: 100px; height: 100px" />
                        </div>
                        @endforeach
                    @endslot
                @endcomponent
        </div>   
    </div> 
    @endif        
    <div class="col-md-12"> 
                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Image Thumbnail List',
                    'field'=>'filenameImageThumbnail[]',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
    </div>
    @if(!empty($digitalBoard))    
    <div class="col-md-6 col-xs-offset-2">
        <div class="form-group row">
                <label for="inputUsername" class="col-md-3 control-label">Image Full Screen Lists</label>
                @component('components.modal',['id'=>uniqid(),'title'=>'ImageThumbnail'])
                    @slot('content')
                        @foreach($digitalBoard->imageFullScreenList as $photo)
                        <div class="img-wrap">
                        <span class="close" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ URL::to('digitalBoard/deleteThumbnail/'.$digitalBoard->id.'/'.$photo.'/2') }}">&times;</span>
                        <img src="{{URL::to('media/images/'.$photo)}}" style="width: 100px; height: 100px" />
                        </div>
                        @endforeach
                    @endslot
                @endcomponent
        </div>   
    </div> 
    @endif  
    <div class="col-md-12"> 
                @component('components.inputField',[
                    'type'=>'file',
                    'icon'=>'text',
                    'label'=>'Image Full Screen List',
                    'field'=>'filenameImageFullScreen[]',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'label'=>'Is Show Content',
            'field'=>'isShowContent',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

<style>
.img-wrap {
    position: relative;
    display: inline-block;
    border: 1px  solid;
    font-size: 0;
}
.img-wrap .close {
    position: absolute;
    top: 2px;
    right: 2px;
    z-index: 100;
    background-color: #FFF;
    padding: 5px 2px 2px;
    color: #000;
    font-weight: bold;
    cursor: pointer;
    opacity: .2;
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
}
.img-wrap:hover .close {
    opacity: 1;
}
</style>
@push('more_scripts')
<script>
  
  $("select[name='zoneType']").change(function(){
      if($(this).val()=="Mall"){
        $("select[name='mallId']").attr('disabled',false);
        $("select[name='mallFloorId']").attr('disabled',false);
        $("select[name='shopId']").attr('disabled',true);
      }else if($(this).val()=="Shop"){
        $("select[name='mallId']").attr('disabled',false);
        $("select[name='mallFloorId']").attr('disabled',false);
        $("select[name='shopId']").attr('disabled',false);
      }else{
        $("select[name='mallId']").attr('disabled',true);
        $("select[name='mallFloorId']").attr('disabled',true);
        $("select[name='shopId']").attr('disabled',true);
      }

  })
  $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });
</script>
@endpush