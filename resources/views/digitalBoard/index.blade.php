@extends('layouts/default') {{-- Page title --}} 
@section('title') DigitalBoard Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>DigitalBoard</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> DigitalBoard</a></li>
        <li class="active">DigitalBoard Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">DigitalBoard</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('digitalBoard/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                            <th>Image Slot </th> 
                            <th>Title</th>
                            <th>Asset</th>
                            <th>ZoneType</th>
                            <th>Mall</th>
                            <th>MallFloors</th>
                            <th>Shop</th>
                            <th>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach ($digitalBoard as $row)
                                <tr>
                                    <td>
                                            @if($row->digitalBoardAssert->imageSlot1!="")
                                            <img src="{{URL::to('media/images/'.$row->digitalBoardAssert->imageSlot1)}}" style="width: 180px; height:180px" />
                                            @endif
                                    </td>
                                    <td>{{$row->title['eng']}}</td>
                                    <td>{{$row->digitalBoardAssert->title['eng']}}</td>
                                    <td>{{$row->zoneType}}</td>
                                    <td>
                                            @if($row->zoneType!="Word")
                                            @isset($row->malls->mallName['eng'])
                                            {{$row->malls->mallName['eng']}}
                                            @endisset
                                            @endif
                                    </td>
                                    <td>
                                        @if($row->zoneType!="Word")
                                        @isset($row->mallFloors->mallFloorName['eng'])
                                        {{$row->mallFloors->mallFloorName['eng']}}
                                        @endisset
                                        @endif
                                </td>
                                    <td>
                                        @if($row->zoneType=="Shop")
                                            @isset($row->shops->shopName['eng'])
                                            {{$row->shops->shopName['eng']}}
                                            @endisset
                                        @endif
                                    </td>
                                    <td>
                                            <a href="{{{ URL::to('digitalBoard/' . $row->id . '/edit' ) }}}">
                                                <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('digitalBoard.destroy', collect($row)->first()) }}">
                                            <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                            </a>
                                    </td>
                                </tr>
                                @endforeach
                            
                        </tbody>
                    </table>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!} 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#table').dataTable({
            "pageLength": 50
        });
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });

</script>
@stop