@component('components.multiLangField',[
    'header'=>'Title',
    'field'=>'title',
    'icon'=>'livicon'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Body',
    'field'=>'body',
    'icon'=>'livicon'
    ])
@endcomponent

<h4>Multi</h4>

<div class="row">
        <div class="col-md-2 col-xs-offset-5">
                @if(!empty($digitalBoardAsserts->imageSlot1))
                    <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img src="{{URL::to('media/images/'.$digitalBoardAsserts->imageSlot1)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                    </div>
                @endif
            </div>
         
        <div class="col-md-12">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Image Slot 1 (512*512)',
                'field'=>'fileNameImageSlot1',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
        </div>

        <div class="col-md-2 col-xs-offset-5">
                @if(!empty($digitalBoardAsserts->imageSlot2))
                    <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                        <img src="{{URL::to('media/images/'.$digitalBoardAsserts->imageSlot2)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                    </div>
                @endif
            </div>
         
        <div class="col-md-12">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Image Slot 2',
                'field'=>'fileNameImageSlot2',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
        </div>

       
</div>

<div class="row">
        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'fileAsserts',
                    'icon'=>'text',
                    'label'=>'Model In Map IOS',
                    'field'=>'fileModelInMapIOS[]',
                    'value'=>!empty($digitalBoardAsserts->modelInMapIos)?$digitalBoardAsserts->modelInMapIos:null,
                    'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
        </div>
        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'fileAsserts',
                    'icon'=>'text',
                    'label'=>'Model In Map Android',
                    'field'=>'fileModelInMapAndroid[]',
                    'value'=>!empty($digitalBoardAsserts->modelInMapAndroid)?$digitalBoardAsserts->modelInMapAndroid:null,
                    'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
        </div>

        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'number',
                    'icon'=>'text',
                    'label'=>'Model In Map Version',
                    'field'=>'modelInMapVersion',
                    'value'=>$modelInMapVersion,
                   'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
        </div>

        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'select',
                    'label'=>'Display Type',
                    'field'=>'displayType',
                    'value'=>null,
                    'data'=>$digitalType,
                    'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
                    
                ])
                @endcomponent 
        </div>
</div>


<div class="row">
        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'fileAsserts',
                    'icon'=>'text',
                    'label'=>'Model Stage IOS',
                    'field'=>'fileModelStageIOS[]',
                    'value'=>!empty($digitalBoardAsserts->modelStageIos)?$digitalBoardAsserts->modelStageIos:null,
                    'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
        </div>
        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'fileAsserts',
                    'icon'=>'text',
                    'label'=>'Model Stage Android',
                    'field'=>'fileModelStageAndroid[]',
                    'value'=>!empty($digitalBoardAsserts->modelStageAndroid)?$digitalBoardAsserts->modelStageAndroid:null,
                    'attribute'=>['class'=>'form-control','multiple'=>true]
                ])
                @endcomponent
        </div>
        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'number',
                    'icon'=>'text',
                    'label'=>'Model Stage Version',
                    'field'=>'modelStageVersion',
                    'value'=>$modelStageVersion,
                    'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
                    
                ])
                @endcomponent 
        </div>
            
        <div class="col-md-6">
                @component('components.inputField',[
                    'type'=>'checkbox',
                    'label'=>'Active',
                    'field'=>'isActive',
                    'value'=>null,
                    'attribute'=>['class'=>'form-control']
                ])
                @endcomponent
        </div>
</div>


<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>