@include('constants.gender')
    @component('components.multiLangField',[
        'header'=>'Costume Names',
        'field'=>'costumeName',
        'icon'=>'livicon'
        ])
    @endcomponent

    @component('components.multiLangTextAreaField',[
        'header'=>'Costume Details',
        'field'=>'costumeDetail',
        'icon'=>'livicon'
        ])
    @endcomponent
<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Coin',
            'field'=>'coin',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Coin']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Diamond',
            'field'=>'diamond',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Diamond']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
   
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Gender',
            'field'=>'gender',
            'value'=>null,
            'data'=>$GLOBALS['genderData'],
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
            
        ])
        @endcomponent 
    </div>

    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'number',
            'icon'=>'text',
            'label'=>'Asset Version',
            'field'=>'costumesAssertVersion',
            'value'=>empty($costume->costumesAssertVersion)?1:$costume->costumesAssertVersion,
            'attribute'=>['class'=>'form-control','placeholder'=>'Asset Version']
        ])
        @endcomponent
    </div>
   

    <div class="col-md-12">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Assert Model IOS (Multiple)',
        'field'=>'AssertModelIOS[]',
        'value'=>$assertModelIOS,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    
    @endcomponent
        
    </div>

    <div class="col-md-12">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Assert Model Android (Multiple)',
        'field'=>'AssertModelAndroid[]',
        'value'=>$assertModelAndroid,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    
    @endcomponent
        
    </div>
    
</div>


    <div class="form-group">
        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
        </div>
    </div>