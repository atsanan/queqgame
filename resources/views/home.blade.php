
@extends('layouts.default') {{-- Page title --}} 
@section('title') Dashboard @parent 
@stop {{-- page level styles--}}

@section('header_styles') 
{{-- <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}"> --}}
{{-- <link rel="stylesheet" href="{{ asset('assets/css/bootstrap1.min.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
{{-- <meta name="_token" content="{{ csrf_token() }}"> --}}
<link rel="stylesheet" href="{{ asset('assets/vendors/morrisjs/morris.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/dashboard.css') }}"/>
<style>
.list_of_items{
    overflow: auto;
    height:20px;
}
</style>

@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
    </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 animated fadeInLeftBig">
        <!-- Trans label pie charts strats here-->
        <div class="widget-1">
            <div class="card-body squarebox square_box">
                <div class="col-md-12 col-xs-12  nopadmar" >
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <span>Monster Defaults</span>

                            <div class="number" id="myTargetElement3">{{$countMonsterDefault}}</div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                        <span class="widget_circle3 pull-right">
                            <i class="livicon livicon-evo-holder " data-name="bug" data-l="true" data-c="#01BC8C"
                                data-hc="#01BC8C" data-s="40"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 animated fadeInLeftBig" >
        <!-- Trans label pie charts strats here-->
        <div class="widget-1">
            <div class="card-body squarebox square_box">
                <div class="col-md-12 col-xs-12 nopadmar" >
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <span>Players</span>

                            <div class="number" id="myTargetElement3">{{$countPlayer}}</div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                        <span class="widget_circle2 pull-right">
                            <i class="livicon livicon-evo-holder " data-name="users" data-l="true" data-c="#418BCA"
                                data-hc="#418BCA" data-s="40"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 animated fadeInRightBig">
        <!-- Trans label pie charts strats here-->
        <div class="widget-1">
            <div class="card-body squarebox square_box">
                <div class="col-md-12 col-xs-12 nopadmar" >
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <span>Item Defaults</span>

                            <div class="number" id="myTargetElement3">{{$countItemDefault}}</div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                        <span class="widget_circle4 pull-right">
                            <i class="livicon livicon-evo-holder " data-name="tasks" data-l="true" data-c="#F89A14"
                                data-hc="#F89A14" data-s="40"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 animated fadeInRightBig">
        <!-- Trans label pie charts strats here-->
        <div class="widget-1">
            <div class="card-body squarebox square_box">
                <div class="col-md-12 col-xs-12 nopadmar" >
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <span>Sticker Stores</span>

                            <div class="number" id="myTargetElement3">{{$countStickerStore}}</div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <span class="widget_circle2 pull-right">
                                <i class="livicon livicon-evo-holder" data-name="thumbs-up" data-l="true" data-c="#418BCA"
                                    data-hc="#418BCA" data-s="40"></i>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 animated fadeInRightBig">
        <!-- Trans label pie charts strats here-->
        <div class="widget-1">
            <div class="card-body squarebox square_box">
                <div class="col-md-12 col-xs-12 nopadmar" >
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <span>Costumes</span>

                            <div class="number" id="myTargetElement3">{{$countCostumes}}</div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <span class="widget_circle4 pull-right">
                                <i class="livicon livicon-evo-holder" data-name="umbrella" data-l="true" data-c="#F89A14"
                                    data-hc="#418BCA" data-s="40"></i>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
<!-- Back to Top-->
<script type="text/javascript" src="{{ asset('assets/vendors/countUp.js/js/countUp.js') }}"></script>
{{-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyADWjiTRjsycXf3Lo0ahdc7dDxcQb475qw&libraries=places"></script> --}}
<script src="{{ asset('assets/vendors/morrisjs/morris.min.js') }}"></script>
@stop