@include('constants.gender')
@component('components.multiLangField',[
        'header'=>'Promotion Names',
        'field'=>'newsName',
        'icon'=>'livicon'
        ])
    @endcomponent

    @component('components.multiLangTextAreaField',[
        'header'=>'Promotion Details',
        'field'=>'newsDetail',
        'icon'=>'livicon'
        ])
    @endcomponent
<h4>Multi</h4>
<div class="row">
        @if(empty($news))
        <table class="table table-bordered tbshops">
                <thead>
                    <tr>
                        <th>malls </th>
                        <th>mall floors </th>
                        <th>Shops </th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>
                            @component('components.inputField', [
                                    'type'=>'select',                          
                                    'label'=>'mall',
                                    'field'=>'mallId[]',
                                    'value'=>null,
                                    'data'=>$malls,
                                    'attribute'=>['class'=>'form-control', 'id' => uniqid(),'placeholder'=>'Please Select','required'=>false]
                                ])
                            @endcomponent
                            </td>
                            <td>
                                    @component('components.inputField', [
                                            'type'=>'select',                          
                                            'label'=>'floor',
                                            'field'=>'mallFloorId[]',
                                            'value'=>null,
                                            'data'=>$mallFloors,
                                            'attribute'=>['class'=>'form-control mallFloor', 'id' => uniqid(),'placeholder'=>'Please Select','required'=>false]
                                        ])
                                    @endcomponent
                            </td>
                            <td>
                                    @component('components.inputField',[
                                        'type'=>'select',
                                        'label'=>'shops',
                                        'field'=>'shopIdArray[]',
                                        'value'=>null,
                                        'data'=>$shops,
                                        'attribute'=>['class'=>'form-control shop','placeholder'=>'Please Select','required'=>false]
                                    ])
                                    @endcomponent
                            </td>
                            <td style="vertical-align: middle">
                                <button type="button" class="btn btn-danger btn-sm remove" style="line-height: 0px">
                                    <i class="livicon" data-name="remove" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Delete"></i>
                                </button>
                            </td>
                        </tr>
                   
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <button type="button" class="btn btn-info btn-sm add" style="line-height: 0px">
                                <i class="livicon" data-name="plus" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff" title="Add"></i>
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>
    @else
        <div class="col-md-6">
            @component('components.inputField',[
            'type'=>'select',
            'label'=>'Mall',
            'field'=>'mallId',
            'value'=>$mallId,
            'data'=>$malls,
            'attribute'=>['class'=>'form-control toMall','placeholder'=>'Please Select','id'=>'toMall']
            ])
            @endcomponent
        </div>
        <div class="col-md-6">
            @component('components.inputField',[
            'type'=>'select',
            'label'=>'Mall Floor',
            'field'=>'mallFloorId',
            'value'=>$mallFloorId,
            'data'=>$mallFloors,
            'attribute'=>['class'=>'form-control toMallFloor','placeholder'=>'Please Select','id'=>'toMallFloorWildItem']
            ])
            @endcomponent
        </div>
        <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'select',
            'label'=>'Shop',
            'field'=>'shopId',
            'value'=>$shopId,
            'data'=>$shops,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
        </div>

        
        <div class=" col-md-6">
            <div class="form-group row">
            <button type="button"  class="btn btn-responsive btn-success setLocation">Set Location</button>
            </div>
        </div>
    @endif
    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'label'=>'News Type',
        'field'=>'newsType',
        'value'=>null,
        'data'=>$newsTypes,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
    ])
    @endcomponent
    </div>
    
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Main Category',
        'field'=>'mainTypes[]',
        'value'=>null,
        'data'=>$mainTypes,
        'attribute'=>['class'=>'form-control select2','multiple'=>true]
        ])
        @endcomponent
    </div>

    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'label'=>'Wild Item',
        'field'=>'wildItemId',
        'value'=>null,
        'data'=>$wildItems,
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
    ])
    @endcomponent
    </div>
    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'select',
        'label'=>'Gender',
        'field'=>'gender',
        'value'=>null,
        'data'=>$GLOBALS['genderData'],
        'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        
    ])
    @endcomponent
    </div>
    
    {{-- @component('components.inputField',[
        'type'=>'map'
    ])
    @endcomponent --}}

    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Longitude',
        'field'=>'longitude',
        'value'=>null,
        'attribute'=>['class'=>'form-control long','placeholder'=>'Longitude','disabled'=>empty($news)]
    ])
    @endcomponent
    </div>
    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'text',
        'icon'=>'text',
        'label'=>'Latitude',
        'field'=>'latitude',
        'value'=>null,
        'attribute'=>['class'=>'form-control lat','placeholder'=>'Latitude','disabled'=>empty($news)]
    ])
    @endcomponent
    </div>
    
    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Min Age',
        'field'=>'minAge',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Min Age']
    ])
    @endcomponent
    </div>
    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Max Age',
        'field'=>'maxAge',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Max Age']
    ])
    @endcomponent
    </div>

    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'number',
        'icon'=>'text',
        'label'=>'Order',
        'field'=>'order',
        'value'=>$order,
        'attribute'=>['class'=>'form-control','placeholder'=>'Order']
    ])
    @endcomponent
    </div>

    
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Reach Date time',
            'field'=>'isReachDateTime',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'datetime',
            'label'=>'Start Date',
            'field'=>'startDateTime',
            'attribute'=>['class'=>'form-control dateTime']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'datetime',
            'label'=>'Expired Date',
            'field'=>'expiredDateTime',
            'attribute'=>['class'=>'form-control dateTime']
        ])
        @endcomponent
    </div> 
    

    <div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'label'=>'Active',
        'field'=>'newsActive',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
    </div>

</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Sponser',
                'field'=>'isSponser',
                'value'=>null,
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Is NearBy',
            'field'=>'isNearBy',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    
<div class="col-md-6">
    @component('components.inputField',[
        'type'=>'checkbox',
        'label'=>'Is Highlight',
        'field'=>'isHighlight',
        'value'=>null,
        'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
</div>
</div>

<div class="row">
        <div class="col-md-2 col-xs-offset-5">
            @if(!empty($filenameImages1))
                <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                    <img src="{{URL::to('media/images/'.$filenameImages1)}}" class="img-responsive"  alt="$field" style="width: 120px; height: 120px;">
                </div>
            @endif
        </div>
     
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'file',
            'icon'=>'text',
            'label'=>'Image 1 (120*120)',
            'field'=>'filenameImage1',
            'value'=>null,
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>
    <div class="col-md-2 col-xs-offset-5">
        @if(!empty($filenameImages2))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 140px; height: 140px;">
                <img src="{{URL::to('media/images/'.$filenameImages2)}}" class="img-responsive" alt="$field" style="width: 120px; height: 120px;">
            </div>
        @endif
    </div>  
    <div class="col-md-12">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Image 2 (1000*1000)',
                'field'=>'filenameImage2',
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>

    <div class="col-md-2 col-xs-offset-4">
        @if(!empty($filenameImages3))
            <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 500px; height: 530px;">
                <img src="{{URL::to('media/images/'.$filenameImages3)}}" class="img-responsive" alt="$field" style="width: 500px; height: 500px;">
            </div>
        @endif
    </div>  
    <div class="col-md-12">
            @component('components.inputField',[
                'type'=>'file',
                'icon'=>'text',
                'label'=>'Image 3 (1200*680)',
                'field'=>'filenameImage3',
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Push Url',
            'field'=>'pushUrl',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Push Url']
        ])
        @endcomponent
    </div>

    <div class="col-md-12">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'Push Token User',
            'field'=>'pushTokenUser',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Push Token User']
        ])
        @endcomponent
    </div>
    @if(isset($news))
    <input type="hidden" id="news"  value="{{$news->id}}" >
    <input type="hidden" name="oldShop" value="{{$news->shopId}}">
    @endif
</div>
    

<div class="form-group">
        
        <div class="col-md-12">
            @if(isset($news))
            <div class="text-left">
            <button type="button"  class="btn btn-responsive btn-success btn-sm send">Push</button>
            </div>
            @endif
            <div class="text-right">
                <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
            </div>
        </div>
</div>
@push('more_scripts')
<script>
//     var c = $("#checkboxid");
// if($('input[type="checkbox"]').prop(":checked")){
//             //if($(this).is(":checked")){
//                 alert("Checkbox is checked.");
//             //}
//            // else if($(this).is(":not(:checked)")){
//            //     alert("Checkbox is unchecked.");
//           //  }
//         }//);


        $(".setLocation").on("click", function() {
        let shopId=$("select[name='shopId']");        
        $.ajax({
            url: "{{ URL::to('api/shops') }}"+"/"+shopId.val(),
            method: 'GET',
            //data:$(this).val(),
            success: function(data) {                
                $(".long").val(data.shop.location.coordinates[0])
                $(".lat").val(data.shop.location.coordinates[1])
   
                //setLocation.attr("class","btn btn-success btn-sm setLocation")
            }
        });
    })
        $(".add").on("click", function() {
            $(this).closest("table").find("tbody tr:last-child");
            const tbody = $(this).closest("table").find("tbody");
            tbody.find("tr:last-child").clone().appendTo(tbody);
    
            const lastRow = tbody.find("tr:last-child");
             //const classList = lastRow.find(".custom-select2").attr("class").split(" ");
            // const className = classList[classList.length - 2];
            // // remove input value
          
            // remove select2
            lastRow.find("td:first select").removeClass("select2-hidden-accessible");
            lastRow.find("td:first>div>div>span").remove();
    
          //  window.select2[className]();
            // remove select2 value
           // $(`.${className}:last`).val(null).trigger('change');
        });


        $(document).on("click", ".remove", function() {
        const rowLenght = $(this).closest("tbody").find("tr").length;
        let id = $(this).closest("tr").find("td").find("select").val()
        if (rowLenght > 1) {
            $(this).closest("tr").remove();
        } else {
            alert("This table must has at least 1 row!");
        }

   
    });  

    $(document).on("change", "table select[name='mallId[]']", function(i,el) {
      
    let option=$(this).closest("tr").find("td").find(".mallFloor").html('');        
    $.ajax({
            url: "{{ URL::to('mallFloors/show') }}",
            method: 'GET',
            data: "mallId="+$(this).val(),
            success: function(data) {
                option.html(data.options);
            }
        });
    });

    $(document).on("change", "table select[name='mallFloorId[]']", function(i,el) {
        let option=$(this).closest("tr").find("td").find(".shop").html('');        
        $.ajax({
            url: "{{ URL::to('shops/show') }}",
            method: 'GET',
            data: "mallFloorId="+$(this).val(),
            success: function(data) {                
                option.html(data.options);
            }
        });
    })
</script>
@endpush