<option value="">--- Select News ---</option>
@if(!empty($news))
  @foreach($news as $row)
    <option value="{{ $row->id }}">{{ $row->newsName['eng'] }}</option>
  @endforeach
@endif