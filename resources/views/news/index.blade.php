@extends('layouts/default') {{-- Page title --}} 
@section('title') Promotion Lists @parent 
@stop {{-- page level styles--}}

@section('header_styles')
<link href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">

{{-- Switchery --}}
<link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css')}}">
<link rel="stylesheet" href="{{ asset('assets/css/pages/radio_checkbox.css')}}">

@stop
@section('content')
<section class="content-header">
    <h1>Promotion</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Promotion</a></li>
        <li class="active">Promotion Lists</li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Promotion</h3>
                    <div class="pull-right">
                    <a href="{{ URL::to('news/create') }}" class="btn btn-primary btn-sm" id="addButton">Add new</a>
                    </div>
                </div>
                <br/>
                <div class="col-md-12">
                        {{-- <div class="col-md-6"> --}}
                            <div class="row">
                            <form id="wildMonstersForm" action="{{ route('news.search') }}" method="POST" enctype="multipart/form-data">
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <fieldset>
                                        <div class="col-md-6">
                                                @component('components.select2', [
                                                    'type'=>'select2',                          
                                                    'label'=>'Mall',
                                                    'field'=>'mallId[]',
                                                    'value'=>$_mallId,
                                                    'data'=>$malls,
                                                    'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                                ])
                                                @endcomponent
                                        </div>
                                        <div class="col-md-6">
                                                @component('components.select2', [
                                                    'type'=>'select2',                          
                                                    'label'=>'MallFloor',
                                                    'field'=>'mallFloorId[]',
                                                    'value'=>$_mallFloorId,
                                                    'data'=>$mallFloors,
                                                    'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                                ])
                                                @endcomponent    
                                        </div>
                                        <div class="col-md-6">
                                                @component('components.select2', [
                                                    'type'=>'select2',                          
                                                    'label'=>'Shop',
                                                    'field'=>'shopId[]',
                                                    'value'=>$_shopId,
                                                    'data'=>$shops,
                                                    'attribute'=>['class'=>'form-control', 'id' => uniqid(),'multiple' => true]
                                                ])
                                                @endcomponent    
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group row {{ $errors->first("search", 'has-error') }}">
                                                {!! Form::label("search", "search", ['class'=>'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                        <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="livicon" data-name="text" data-size="18" data-c="#000" data-hc="#000" data-loop="true"></i>
                                                                </span>
                                                    {!! Form::text("search", $search, ['class'=>'form-control']) !!}
                                                        </div>
                                                    {!! $errors->first("search", '<span class="help-block">:message</span> ') !!}
                                                </div>
                                            </div> 
                                        </div>
                                        
                                        
                                        <div class="col-md-6">
                                                <div class="form-group row">
                                                        {!! Form::label("all", "all", ['class'=>'col-md-3 control-label']) !!}
                                                        <div class="col-md-9">
                                                            {!! Form::checkbox("all", true , $all , ['class'=>'js-switch1']) !!} 
                                                        </div>
                                                    </div>
                                        </div>
                                
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn btn-responsive btn-primary btn-sm">Search</button>         
                                        </div>
                                </fieldset>           
                            </form>
                        </div>
                </div>
                @php
                                      
                if($field=="shops"){
                    if($sort=="desc"){
                        $sortShop="asc";
                    }else{
                        $sortShop="desc";
                    }
                }else {
                    $sortShop="asc";
                }   

                                  
                if($field=="newsName"){
                    if($sort=="desc"){
                        $sortNewsName="asc";
                    }else{
                        $sortNewsName="desc";
                    }
                }else {
                    $sortNewsName="asc";
                }   

                                  
                if($field=="newsType"){
                    if($sort=="desc"){
                        $sortNewsType="asc";
                    }else{
                        $sortNewsType="desc";
                    }
                }else {
                    $sortNewsType="asc";
                }   
                                  
                if($field=="gender"){
                    if($sort=="desc"){
                        $sortGender="asc";
                    }else{
                        $sortGender="desc";
                    }
                }else {
                    $sortGender="asc";
                }   
                                  
                if($field=="minAge"){
                    if($sort=="desc"){
                        $sortMinAge="asc";
                    }else{
                        $sortMinAge="desc";
                    }
                }else {
                    $sortMinAge="asc";
                }   
                                  
                if($field=="maxAge"){
                    if($sort=="desc"){
                        $sortMaxAge="asc";
                    }else{
                        $sortMaxAge="desc";
                    }
                }else {
                    $sortMaxAge="asc";
                }   
                                  
                if($field=="shops"){
                    if($sort=="desc"){
                        $sortShop="asc";
                    }else{
                        $sortShop="desc";
                    }
                }else {
                    $sortShop="asc";
                }   

                
                if($field=="isReachDateTime"){
                    if($sort=="desc"){
                        $sortIsReachDateTime="asc";
                    }else{
                        $sortIsReachDateTime="desc";
                    }
                }else {
                    $sortIsReachDateTime="asc";
                }   

                if($field=="isExpired"){
                    if($sort=="desc"){
                        $sortIsExpired="asc";
                    }else{
                        $sortIsExpired="desc";
                    }
                }else {
                    $sortIsExpired="asc";
                }   

                if($field=="isActive"){
                    if($sort=="desc"){
                        $sortIsActive="asc";
                    }else{
                        $sortIsActive="desc";
                    }
                }else {
                    $sortIsActive="asc";
                }   
                @endphp

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                                <th>Promotion Image</th>
                                <th>Promotion Name
                                        <a href="{{ URL::to("news?field=newsName&sort={$sortNewsName}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>      
                                </th>
                                <th>Shop

                                        <a href="{{ URL::to("news?field=shops&sort={$sortShop}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a> 
                                </th>
                                {{-- <th>Promotion Type
                                        <a href="{{ URL::to("news?field=newsType&sort={$sortNewsType}&search={$search}") }}" >
                                                <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                        </a>      
                                </th> --}}
                                <th>Reach Date time
                                    <a href="{{ URL::to("news?field=isReachDateTime&sort={$sortIsReachDateTime}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>  
                                </th>
                                <th>Continued
                                    <a href="{{ URL::to("news?field=isExpired&sort={$sortIsExpired}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>  
                                </th>
                                <th>Active
                                    <a href="{{ URL::to("news?field=isActive&sort={$sortIsActive}&search={$search}") }}" >
                                            <i class="glyphicon glyphicon-sort-by-attributes"></i>
                                    </a>  
                                </th>
                              
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($news as $row)
                            <tr>
                                <td>
                                    @isset($row['filenameImage2'])
                                    <img src="{{URL::to('media/thumbnail/'.$row['filenameImage2'])}}" style="width: 100px; height: 70px" />      
                                    @endisset
                                </td>
                                <td>{{ $row['newsName']['eng'] }}</td>
                                {{-- <td>{{ $row->location['coordinates']['0'] }}</td> --}}
                                {{-- <td>{{ $row->location['coordinates']['1'] }}</td> --}}
                           
                                {{-- <td>{{ $row->wildItems->couponGiftName['eng'] }}</td> --}}
                                <td>
                                        @if(isset($row['shops']['shopName']['eng']))
                                        {{ $row['shops']['shopName']['eng'] }}
                                        @endif
                                </td>
                                {{-- <td>
                                        @if(isset($row['news_types']['newsTypeName']['eng']))
                                        {{ $row['news_types']['newsTypeName']['eng'] }}
                                        @endif
                                </td> --}}
                                <td>
                                    @if(isset($row['isReachDateTime']))
                                        @if ($row['isReachDateTime'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                    @if(isset($row['isExpired']))
                                        @if ($row['isExpired'])
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                        @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                        @endif
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                    @endif
                                </td>
                                <td>
                                        @if(isset($row['newsActive']))
                                            @if ($row['newsActive'])
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true" style="color:#5cb85c;font-size:16px;"></span>
                                            @else
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"style="color:#d9534f;font-size:16px;"></span>
                                            @endif
                                        @endif
                                </td>
                                <td>
                                        <a href="{{{ URL::to('news/' . $row['_id'] . '/edit' ) }}}">
                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                        </a>
                                    </a>
                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('news.destroy', collect($row)->first()) }}">
                                        <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        <!-- {{route('news.destroy', collect($row)->first() )}} -->
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?=$news->appends(request()->query())->render()?>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this?
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 
@section('footer_scripts')
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

{{-- Switchery --}}
<script type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js')}}"></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script>
            let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch1'));
            elems.forEach(html=>{
            let switchery = new Switchery(html,{ size: 'small',color: 'rgb(65, 139, 202)' });
            });

     $("select[name='mallId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getMallFloor/') }}",
          method: 'POST',
          data:{mallId: $(this).val()},
         success: function(data) {
            $("select[name='mallFloorId[]'").html('');
            $("select[name='mallFloorId[]'").html(data.options);
            
          }
      });     
    });
    
    $("select[name='mallFloorId[]']").change(function(){
        $.ajax({
          url: "{{ URL::to('api/getShop/') }}",
          method: 'POST',
          data:{mallFloorId: $(this).val()},
         success: function(data) {
            $("select[name='shopId[]'").html('');
            $("select[name='shopId[]'").html(data.options);
            
          }
      });
            
    });
    $('#delete_confirm').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget)
        let $recipient = button.data('id');
        console.log("recipient: ",$recipient);

        let modal = $(this);
        modal.find('.modal-footer form').prop("action",$recipient);
    });
</script>
@yield('modalScripts')
@stop