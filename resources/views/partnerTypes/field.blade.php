<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
            'type'=>'text',
            'icon'=>'text',
            'label'=>'PartnerTypes Type Name',
            'field'=>'partnerTypeName',
            'value'=>null,
            'attribute'=>['class'=>'form-control','placeholder'=>'Partner Type Name']
        ])
        @endcomponent
    </div>
    
    <div class="col-md-9">
        @component('components.inputField',[
            'type'=>'checkbox',
            'icon'=>'text',
            'label'=>'Active',
            'field'=>'isActive',
            'value'=>empty($patherType)?true:null, 
            'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
</div>
</div>

<div class="form-group"> 
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>