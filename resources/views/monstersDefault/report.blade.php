@extends('layouts/default') {{-- Page title --}} 
@section('title') Monster Default Report @parent 
@stop {{-- page level styles--}}

@section('header_styles') 

@stop {{-- Page content --}} 
@section('content')
<section class="content-header">
    <h1>Monster Defaults Report</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Report
            </a>
        </li>
        <li>Monster Defaults Report</li>

    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        
            <div class="col-lg-12">
                    <!-- toggling series charts strats here-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="linechart" data-size="16" data-loop="true"
                                    data-c="#fff" data-hc="#fff"></i> Chart
                            </h3>
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div id="chartContainer" style="height: 300px; width: 100%;"></div>                       </div>
                    </div>
                </div>
           </div>
        <div class="col-lg-12">
            <div class="panel panel-info filterable ">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Monster Defaults Report</h3>
                    <div class="pull-right">
                            <a href="{{ route('monstersDefault.excel') }}" class="btn btn-success btn-sm" id="addButton">Export</a>
                    </div>
                </div>

                <div class="panel-body table-responsive">
                    <table class="table" id="table">
                        <thead>
                            <tr class="filters">
                            <th> Monster Name</th>
                            <th>Image</th>
                            <th> Amount</th>
                            <th> Percent </th>
                            <th> Action </th>
                       
                        </tr>
                        </thead>
                        <tbody>
                                @foreach($monsterDefaults as $row)
                                <tr>
                                <td>{{ $row->monsterDefaults[0]->mDefaultName->eng }}</td>
                                <td><img src="{{URL::to('media/images/'.$row->monsterDefaults[0]->mDefaultAssetImageSlot)}}" style="width: 100px; height: 70px" /></td>
                                <td>{{ $row->sum }}</td>
                                <td>{{ round(($row->sum/$total)*100,2) }}</td>
                                <td>
                                        <a href="{{ URL::to('monstersDefault/' . $row->id . '/edit' ) }}">
                                            <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#delete_confirm"  data-id="{{ route('monstersDefault.destroy', collect($row)->first()) }}">
                                            <i class="livicon" data-name="user-remove" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete user"></i>
                                        </a>
                                        
                                        </td>
                           
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    <!-- Modal for showing delete confirmation -->
                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                        Delete
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure to delete this? 
                                </div>
                                <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE']) !!}
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <button type="submit" class="btn btn-danger">Delete</a>
                                {!! Form::close() !!}                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>



@stop {{-- page level scripts --}} 

@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.categories.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.stack.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
    <!-- <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.stack.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.crosshair.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.time.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.selection.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.symbol.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.resize.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.categories.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/splinecharts/jquery.flot.spline.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script> -->
    <script language="javascript" type="text/javascript" >
    
    $(document).ready(function() {
        $('#table').dataTable({
            "pageLength": 50
        });
    });


    let count = [];
    let images = [];
    @foreach($monsterDefaults as $row)
    count.push({label:"{{$row->monsterDefaults[0]->mDefaultName->eng}}",y:{{$row->sum}}})
    images.push({url: "{{URL::to('media/images/'.$row->monsterDefaults[0]->mDefaultAssetImageSlot)}}"})
    @endforeach


window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "Monster Defaults Report"
	},
	// subtitles: [{
	// 	text: "Click Legend to Hide or Unhide Data Series"
	// }], 
	axisX: {
		title: "Count"
	},
	axisY: {
		title: "Count",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	toolTip: {
		shared: true
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Count",
		showInLegend: true,      
		yValueFormatString: "#,##0.# Units",
		dataPoints:count
	},]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

var fruits= [];
  
  addImages(chart);

  function addImages(chart){
    for(var i = 0; i < chart.data[0].dataPoints.length; i++){
      var label = chart.data[0].dataPoints[i].label;
      
      if(label){
        fruits.push( $("<img>").attr("src", images[i].url)
                    .attr("class", label)
                    .css("display", "none")
                    .appendTo($("#chartContainer>.canvasjs-chart-container"))
                   );        
      }
      
      positionImage(fruits[i], i);
    }    
  }
  
  function positionImage(fruit, index){ 
    var imageBottom = chart.axisX[0].bounds.y1;     
    var imageCenter = chart.axisX[0].convertValueToPixel(chart.data[0].dataPoints[index].x);
    
    fruit.width(40);
   fruit.height(40);
    fruit.css({"position": "absolute", 
               "display": "block",
               "top": imageBottom  - fruit.height(),
               "left": imageCenter - fruit.width()/2
              });
    chart.render();
  }
  
  $( window ).resize(function() {
    for(var i = 0; i < chart.data[0].dataPoints.length; i++){
    	positionImage(fruits[i], i);
    }
  }); 
}   
</script>

<script src="{{ asset('assets/js/canvasjs.min.js') }}"></script>
    <!-- end of page level js -->
@endpush