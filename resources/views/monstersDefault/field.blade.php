
@component('components.multiLangField',[
    'header'=>'Monster Default Names',
    'field'=>'mDefaultName',
    'icon'=>'user'
    ])
@endcomponent

@component('components.multiLangTextAreaField',[
    'header'=>'Monster Details',
    'field'=>'mDefaultTextDetail',
    'icon'=>'bug'
    ])
@endcomponent
<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'number',
        'label'=>'Job Level',
        'field'=>'mDefaultJobLevel',
        'icon'=>'icon',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Job Level']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'select',
        'label'=>'Monster Categories',
        'field'=>'mDefaultJobCategory[]',
        'value'=>null,
        'data'=> $monstersDefaults,
        'attribute'=>['class'=>'form-control select2','multiple'=>true]
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
        @component('components.inputField',[
        'type'=>'number',
        'label'=>'Group',
        'field'=>'mDefaultGroupId',
        'icon'=>'icon',
        'value'=>null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Group']
        ])
        @endcomponent
            
    </div>
    
    <div class="col-md-6">
    @component('components.inputField',[
            'type'=>'select',
            'label'=>'Monster Type',
            'field'=>'mDefaultTypeId',
            'value'=>null,
            'data'=>$monstersDefaultsTypes,
            'attribute'=>['class'=>'form-control','placeholder'=>'Please Select']
        ])
        @endcomponent
    </div>
    <div class="col-md-6">
    @component('components.inputField',[
    'type'=>'number',
    'label'=>'Group Level',
    'field'=>'mDefaultGroupLevel',
    'icon'=>'icon',
    'value'=>null,
    'attribute'=>['class'=>'form-control','placeholder'=>'Group Level']
    ])
    @endcomponent
    </div>
   
    <div class="col-md-6">
    @component('components.inputField',[
    'type'=>'number',
    'label'=>'Order',
    'field'=>'mDefaultOrder',
    'icon'=>'icon',
    'value'=>$order,
    'attribute'=>['class'=>'form-control','placeholder'=>'Order']
])
@endcomponent
    </div>
    
</div>
<div class="row">
  

<div class="row">
    <div class="col-md-6 col-xs-offset-3">
        @if(!empty($imageSlot))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
            <img src="{{URL::to('media/images/'.$imageSlot)}}" class="img-responsive" alt="$field"  style="width: 200px; height: 150px;">
        </div>
        @endif
    </div>    
</div>

    <div class="col-md-9">
    @component('components.inputField',[
    'type'=>'file',
    'icon'=>'user',
    'label'=>'Asset Image Slot (200*200)',
    'field'=>'mDefaultAssetImageSlot',
    'value'=>$imageSlot,
    'attribute'=>['class'=>'form-control']
    ])
    @endcomponent
    </div>
    <div class="col-md-6 col-xs-offset-3">
        @if(!empty($imageSlot2))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
            <img src="{{URL::to('media/images/'.$imageSlot2)}}" class="img-responsive" alt="$field"  style="width: 200px; height: 150px;">
        </div>
        @endif
    </div>   
    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Asset Image Slot 2 (200*200)',
        'field'=>'mDefaultAssetImageSlot2',
        'value'=>$imageSlot2,
        'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>

    
    <div class="col-md-6 col-xs-offset-3">
        @if(!empty($imageSlot3))
        <div class="fileinput-preview  thumbnail" data-trigger="fileinput" style="width: 220px; height: 170px;">
            <img src="{{URL::to('media/images/'.$imageSlot3)}}" class="img-responsive" alt="$field"  style="width: 200px; height: 150px;">
        </div>
        @endif
    </div>   
    
    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Asset Image Slot 3 (200*200)',
        'field'=>'mDefaultAssetImageSlot3',
        'value'=>$imageSlot3,
        'attribute'=>['class'=>'form-control']
        ])
        @endcomponent
    </div>

    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Assert Model IOS (Multiple)',
        'field'=>'AssertModelIOS[]',
        'value'=>$assertModelIOS,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    
    @endcomponent    
    </div>
    
    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'file',
        'icon'=>'user',
        'label'=>'Assert Model Android (Multiple)',
        'field'=>'AssertModelAndroid[]',
        'value'=>$assertModelAndroid,
        'attribute'=>['class'=>'form-control','multiple'=>true]
    ])
    @endcomponent
    </div>

    <div class="col-md-9">
        @component('components.inputField',[
        'type'=>'number',
        'label'=>'Asset Version',
        'field'=>'mDefaultAssetVersion',
        'icon'=>'icon',
        'value'=>empty($monstersDefault->mDefaultAssetVersion)?1:null,
        'attribute'=>['class'=>'form-control','placeholder'=>'Asset Version']
        ])
        @endcomponent
        </div>

            
</div>

<!-- Form actions -->
<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>