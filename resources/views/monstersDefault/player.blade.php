@extends('layouts/default') {{-- Page title --}} 
@section('title') Report Monster Player @parent 
@stop {{-- page level styles --}} 
@php
$count=30;
if(count($monsterPlayer)<30){
    $count=count($monsterPlayer);
}
@endphp
@section('content')

<section class="content-header">
        <h1>Report Monster Player</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('home') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="#">Report</a></li>
            <li class="active">Report Monster Player Lists</li>
        </ol>
    </section>
<!-- Left side column. contains the logo and sidebar -->
      
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  
                    <div class="col-lg-12">
                        <!-- toggling series charts strats here-->
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="livicon" data-name="linechart" data-size="16" data-loop="true"
                                        data-c="#fff" data-hc="#fff"></i> Chart
                                </h3>
                                <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                            </div>
                            <div class="panel-body">
                               
                              <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                           </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                            <div class="panel panel-info filterable ">
                                <div class="panel-heading clearfix">
                                    <h3 class="panel-title pull-left">Monster Player </h3>
                                    <div class="pull-right">
                                                </div>
                                </div>
                                <form id="wildMonstersForm" action="{{ route('monsterInShop.store') }}" method="POST" enctype="multipart/form-data">
                                <!-- CSRF Token -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <fieldset>
                       
                                </fieldset>           
            
                            </form>
            
                                <div class="panel-body table-responsive">
                                    <table class="table" id="table">
                                        <thead>
                                            <tr class="filters">
                                                <th>No</th>
                                                <th>Name </th>
                                                <th>Email </th>
                                                <th>Amount </th>
                                                <th>Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                        @for($i=0;$i<$count;$i++)
                                        <tr>
                                            <td>{{$i+1}}</td> 
                                            <td>{{$monsterPlayer[$i]->user->name}}</td> 
                                            <td>{{$monsterPlayer[$i]->user->email}}</td> 
                                            <td>{{$monsterPlayer[$i]->sum}}</td>
                                            <td>
                                                    <a href="{{ URL::to('user/' . $monsterPlayer[$i]->user->_id . '/edit' ) }}">
                                                        <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Edit"></i>
                                                    </a>
                                            </td>
                       
                                        </tr>
                                        @endfor                   
                                         </tbody>
                                    </table>
                                    <!-- Modal for showing delete confirmation -->
                                    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title" id="user_delete_confirm_title">
                                                        Delete
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure to delete this? 
                                                </div>
                                                <div class="modal-footer">
                                                {!! Form::open(['method' => 'DELETE']) !!}
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <button type="submit" class="btn btn-danger">Delete</a>
                                                {!! Form::close() !!}                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
@stop       
@push('more_scripts')
    <!-- ./wrapper -->
   
    <!-- global js -->
    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.categories.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flotchart/js/jquery.flot.stack.js')}}"></script>
    <script language="javascript" type="text/javascript" src="{{asset('flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
    <!-- <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.stack.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.crosshair.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.time.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.selection.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.symbol.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.resize.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flotchart/js/jquery.flot.categories.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/splinecharts/jquery.flot.spline.js"></script>
    <script language="javascript" type="text/javascript" src="vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script> -->
    <script language="javascript" type="text/javascript" >
    window.onload = function () {
    let dataPoints=[];
    @for($i=0;$i<$count;$i++)
    dataPoints.push({label:"{{$i+1}}",y:{{round((($monsterPlayer[$i]->sum/$sum)*100),2)}}})
    @endfor
var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2", // "light1", "light2", "dark1", "dark2"
	title: {
		text: "monster"
	},
	axisY: {
		title: "Monster Rate",
		suffix: "%",
		includeZero: false
	},
	axisX: {
		title: "Countries"
	},
	data: [{
		type: "column",
		//ValueFormatString: "#,##0.0#\"%\"",
		dataPoints:dataPoints
	}]
});
chart.render();

}


    </script>

<script src="{{ asset('assets/js/canvasjs.min.js') }}"></script>
   
    <!-- end of page level js -->
@endpush