
<h4>Multi</h4>
<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'text',
                'icon'=>'text',
                'label'=>'Type Name',
                'field'=>'authenTypeName',
                'value'=>null,
                'attribute'=>['class'=>'form-control','placeholder'=>'Type Name']
            ])
            @endcomponent
    </div>
</div>

<div class="row">
    <div class="col-md-6">
            @component('components.inputField',[
                'type'=>'checkbox',
                'icon'=>'text',
                'label'=>'Active',
                'field'=>'isActive',
                'value'=>null, 
                'attribute'=>['class'=>'form-control']
            ])
            @endcomponent
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 text-right">
        <button type="submit" class="btn btn-responsive btn-primary btn-sm">Submit</button>
    </div>
</div>

